// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QtTest>

#include "src/core/private/plugin-api/backendhandlererror.h"
#include "src/core/private/plugin-api/backendhandlerstate.h"
#include "src/core/private/archivetypedetector.h"

#include "src/plugin-gnutar-core/backendhandler.h"

#include "tests/tests-shared/testarchivedelete.h"


using namespace Packuru::Plugins::GnuTar::Core;
using namespace Packuru::Tests::Shared;
using namespace Packuru::Core;


namespace Packuru::Tests::Plugins::GnuTar
{

class BackendHandlerDelete : public QObject
{
    Q_OBJECT

public:
    BackendHandlerDelete();
    ~BackendHandlerDelete() override;

private slots:
   void deleteSelected_data();
   void deleteSelected();

private:
    ArchiveTypeDetector typeDetector;
};


BackendHandlerDelete::BackendHandlerDelete()
{  

}


BackendHandlerDelete::~BackendHandlerDelete()
{

}


void BackendHandlerDelete::deleteSelected_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<std::vector<QString>>("filePaths");

    QString path = QFINDTESTDATA("../../data/tar/files.tar");

    QTest::newRow(path.toLatin1())
            << path
            << std::vector<QString>{"files/file1.txt"};
}


void BackendHandlerDelete::deleteSelected()
{
    QFETCH(QString, archivePath);
    QFETCH(std::vector<QString>, filePaths);

    if (!BackendHandler::isTarExecAvailable())
        QSKIP("Tar executable not found");

    BackendHandler handler;

    TestArchiveDeleteParams params;
    params.handler = &handler;
    params.archivePath = archivePath;
    params.absoluteFilePaths = filePaths;

    testArchiveDelete(params);

    const auto args = handler.getBackendArgs();
    QCOMPARE(args.size(), static_cast<std::size_t>(1));

    for (const auto& item : params.absoluteFilePaths)
    {
        QVERIFY2(args[0].contains(item), "Item=" + item.toLatin1());
    }
}

}

QTEST_GUILESS_MAIN(Packuru::Tests::Plugins::GnuTar::BackendHandlerDelete)

#include "tst_backendhandlerdelete.moc"
