// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QObject>
#include <QtTest>

#include "src/plugin-unarchiver-core/backendhandler.h"

#include "tests/tests-shared/backendhandler7zipunarread.h"


using namespace Packuru::Plugins::Unarchiver::Core;
using namespace Packuru::Tests::Shared;

namespace Packuru::Tests::Plugins::Unarchiver
{

class BackendHandlerReadShared : public BackendHandler7ZipUnarRead
{
    Q_OBJECT
public:
    BackendHandlerReadShared()
        : BackendHandler7ZipUnarRead({ [] () { return std::make_unique<BackendHandler>(); },
    [] () { return BackendHandler::executableAvailable(); } }) {}
};

}

QTEST_GUILESS_MAIN(Packuru::Tests::Plugins::Unarchiver::BackendHandlerReadShared)

#include "tst_backendhandlerreadshared.moc"
