# SPDX-FileCopyrightText: The Packuru Developers
#
# SPDX-License-Identifier: 0BSD

include(../common-testcases.pri)

DESTDIR += $${TESTS_TARGET_DIR}/plugin-unarchiver

LIBS += -l$${PROJECT_BUILD_NAME}-plugin-unarchiver-core
