// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QObject>
#include <QtTest>

#include "src/plugin-unarchiver-core/backendhandler.h"

#include "tests/tests-shared/backendhandler7zipunartest.h"


using namespace Packuru::Plugins::Unarchiver::Core;
using namespace Packuru::Tests::Shared;


namespace Packuru::Tests::Plugins::Unarchiver
{

class BackendHandlerTestShared : public BackendHandler7ZipUnarTest
{
    Q_OBJECT
public:
    BackendHandlerTestShared()
        : BackendHandler7ZipUnarTest({ [] () { return std::make_unique<BackendHandler>(); },
    [] () { return BackendHandler::executableAvailable(); } }) {}
};

}

QTEST_GUILESS_MAIN(Packuru::Tests::Plugins::Unarchiver::BackendHandlerTestShared)

#include "tst_backendhandlertestshared.moc"
