// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QtTest>

#include "src/core/private/plugin-api/backendhandlererror.h"
#include "src/core/private/plugin-api/backendhandlerstate.h"

#include "src/plugin-unarchiver-core/backendhandler.h"

#include "tests/tests-shared/testarchiveextract.h"
#include "tests/tests-shared/passwords.h"
#include "tests/tests-shared/passwordtestmode.h"



using namespace Packuru::Core;
using namespace Packuru::Plugins::Unarchiver::Core;
using namespace Packuru::Tests::Shared;


namespace
{

const auto commonChecker7zZip = [] (const QFileInfoMap& map)
{
    QCOMPARE(map.size(), static_cast<std::size_t>(5));

    const QFileInfo* item = getItem(map, "files");
    QVERIFY(item);
    QVERIFY(item->isDir());

    QVERIFY(item = getItem(map, "files/blob100b"));
    QCOMPARE(item->size(), 100);

    QVERIFY(item = getItem(map, "files/file1.txt"));
    QCOMPARE(item->size(), 4);

    QVERIFY(item = getItem(map, "files/file2.txt"));
    QCOMPARE(item->size(), 4);

    QVERIFY(item = getItem(map, "files/link_to_dev_null"));
};

}

namespace Packuru::Tests::Plugins::Unarchiver
{

class BackendHandlerExtract : public QObject
{
    Q_OBJECT

public:
    BackendHandlerExtract();
    ~BackendHandlerExtract() override;

private slots:
    void initTestCase();

    void extractEntireArchive_data();
    void extractEntireArchive();

    void extractSelectedFiles_data();
    void extractSelectedFiles();

    void extractSelectedFolder_data();
    void extractSelectedFolder();

    void extractionError_data();
    void extractionError();

    void extractEncrypted_data();
    void extractEncrypted();

    void extractEncryptedError_data();
    void extractEncryptedError();
};


BackendHandlerExtract::BackendHandlerExtract()
{

}


BackendHandlerExtract::~BackendHandlerExtract()
{

}


void BackendHandlerExtract::initTestCase()
{
    if (!BackendHandler::executableAvailable())
        QSKIP("Executable not found");
}


void BackendHandlerExtract::extractEntireArchive_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<QFileInfoChecker>("itemChecker");

    const auto checker1 = [] (const QFileInfoMap& map)
    {
        QCOMPARE(map.size(), static_cast<std::size_t>(5));

        const QFileInfo* item = getItem(map, "files");
        QVERIFY(item);
        QVERIFY(item->isDir());

        QVERIFY(item = getItem(map, "files/blob100b"));
        QCOMPARE(item->size(), 100);

        QVERIFY(item = getItem(map, "files/blob1kb"));
        QCOMPARE(item->size(), 1000);

        QVERIFY(item = getItem(map, "files/file1.txt"));
        QCOMPARE(item->size(), 3);

        QVERIFY(item = getItem(map, "files/file2.txt"));
        QCOMPARE(item->size(), 3);
    };

    const char* path = "../../data/rar/multipart.comment.lock.recovery.solid.rar_new.part01.rar";
    QTest::newRow(path)
            << path
            << QFileInfoChecker(checker1);

    path = "../../data/rar/multipart.comment.lock.recovery.solid.rar_old.rar";
    QTest::newRow(path)
            << path
            << QFileInfoChecker(checker1);

    path = "../../data/7z/solid.unix_link.7z";
    QTest::newRow(path)
            << path
            << QFileInfoChecker(commonChecker7zZip);

    path = "../../data/zip/unix_link.zip";
    QTest::newRow(path)
            << path
            << QFileInfoChecker(commonChecker7zZip);
}


void BackendHandlerExtract::extractEntireArchive()
{
    BackendHandler handler;

    QFETCH(QString, archivePath);
    QFETCH(QFileInfoChecker, itemChecker);

    TestArchiveExtractParams params;
    params.handler = &handler;
    params.archivePath = archivePath;
    params.itemChecker = itemChecker;

    testArchiveExtract(params);
}


void BackendHandlerExtract::extractSelectedFiles_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<std::vector<QString>>("filesToExtract");
    QTest::addColumn<QFileInfoChecker>("itemChecker");

    const std::vector<QString> files1
    {
        "files/blob100b",
        "files/file1.txt"
    };

    const auto checker1 = [totalItems = files1.size() + 1] (const QFileInfoMap& map)
    {
        QCOMPARE(map.size(), totalItems);

        const QFileInfo* item = getItem(map, "files");
        QVERIFY(item);
        QVERIFY(item->isDir());

        QVERIFY(item = getItem(map, "files/blob100b"));
        QCOMPARE(item->size(), 100);

        QVERIFY(item = getItem(map, "files/file1.txt"));
        QCOMPARE(item->size(), 3);
    };

    const char* path = "../../data/rar/multipart.comment.lock.recovery.solid.rar_new.part01.rar";
    QTest::newRow(path)
            << path
            << files1
            << QFileInfoChecker(checker1);

    path = "../../data/rar/multipart.comment.lock.recovery.solid.rar_old.rar";
    QTest::newRow(path)
            << path
            << files1
            << QFileInfoChecker(checker1);
}


void BackendHandlerExtract::extractSelectedFiles()
{
    BackendHandler handler;

    QFETCH(QString, archivePath);
    QFETCH(std::vector<QString>, filesToExtract);
    QFETCH(QFileInfoChecker, itemChecker);

    TestArchiveExtractParams params;
    params.handler = &handler;
    params.archivePath = archivePath;
    params.filesToExtract = filesToExtract;
    params.itemChecker = itemChecker;

    testArchiveExtract(params);
}


void BackendHandlerExtract::extractSelectedFolder_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<QString>("folderToExtract");
    QTest::addColumn<QFileInfoChecker>("itemChecker");

    const QString folder = "files/";

    const char* path = "../../data/7z/solid.unix_link.7z";
    QTest::newRow(path)
            << path
            << folder
            << QFileInfoChecker(commonChecker7zZip);

    path = "../../data/zip/unix_link.zip";
    QTest::newRow(path)
            << path
            << folder
            << QFileInfoChecker(commonChecker7zZip);
}


void BackendHandlerExtract::extractSelectedFolder()
{
    BackendHandler handler;

    QFETCH(QString, archivePath);
    QFETCH(QString, folderToExtract);
    QFETCH(QFileInfoChecker, itemChecker);

    TestArchiveExtractParams params;
    params.handler = &handler;
    params.archivePath = archivePath;
    params.filesToExtract = {folderToExtract};
    params.itemChecker = itemChecker;

    testArchiveExtract(params);
}

void BackendHandlerExtract::extractionError_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<BackendHandlerError>("expectedError");

    const std::initializer_list<QLatin1String> paths
    {
        QLatin1String("../../data/rar/missing_part.rar_new.part01.rar"),
                QLatin1String("../../data/rar/missing_part.rar_old.rar"),
                QLatin1String("../../data/zip/missing_part.scheme1.zip")
    };

    for (const QLatin1String& path : paths)
        QTest::newRow(path.data())
                << path.data()
                << BackendHandlerError::DataError;
}


void BackendHandlerExtract::extractionError()
{
    BackendHandler handler;

    QFETCH(QString, archivePath);
    QFETCH(BackendHandlerError, expectedError);

    TestArchiveExtractParams params;
    params.handler = &handler;
    params.archivePath = archivePath;
    params.expectedEndState = BackendHandlerState::Errors;
    params.expectedError = expectedError;

    testArchiveExtract(params);
}


void BackendHandlerExtract::extractEncrypted_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<QFileInfoChecker>("itemChecker");

    const auto checker1 = [] (const QFileInfoMap& map)
    {
        QCOMPARE(map.size(), static_cast<std::size_t>(4));

        const QFileInfo* item = getItem(map, "files");
        QVERIFY(item);
        QVERIFY(item->isDir());

        QVERIFY(item = getItem(map, "files/blob100b"));
        QCOMPARE(item->size(), 100);

        QVERIFY(item = getItem(map, "files/file1.txt"));
        QCOMPARE(item->size(), 3);

        QVERIFY(item = getItem(map, "files/file2.txt"));
        QCOMPARE(item->size(), 3);
    };

    const std::initializer_list<QLatin1String> windowsPaths
    {
        QLatin1String("../../data/rar/encrypted.solid.rar_new.rar"),
                QLatin1String("../../data/rar/encrypted.non_solid.rar_old.rar"),
                QLatin1String("../../data/rar/encrypted_content.rar_new.rar"),
                QLatin1String("../../data/rar/encrypted_content.rar_old.rar"),
    };

    for (const QLatin1String& path : windowsPaths)
        QTest::newRow(path.data())
                << path.data()
                << QFileInfoChecker(checker1);
}


void BackendHandlerExtract::extractEncrypted()
{
    QFETCH(QString, archivePath);
    QFETCH(QFileInfoChecker, itemChecker);

    BackendHandler handler1;

    TestArchiveExtractParams params;
    params.handler = &handler1;
    params.archivePath = archivePath;
    params.password = validPassword;
    params.itemChecker = itemChecker;

    // Test with password provided in the command-line
    testArchiveExtract(params);

    // BackendHandlers must be used only once
    BackendHandler handler2;

    params.handler = &handler2;
    params.passwordTestMode = PasswordTestMode::PromptPasswordEntry;

    // Test with password provided in the prompt
    testArchiveExtract(params);
}


void BackendHandlerExtract::extractEncryptedError_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<QString>("password");
    QTest::addColumn<BackendHandlerError>("expectedError");

    const std::initializer_list<QLatin1String> validArchivePaths
    {
        QLatin1String("../../data/rar/encrypted.solid.rar_new.rar"),
                QLatin1String("../../data/rar/encrypted.non_solid.rar_old.rar"),
                QLatin1String("../../data/rar/encrypted_content.rar_new.rar"),
                QLatin1String("../../data/rar/encrypted_content.rar_old.rar"),
    };

    for (const QLatin1String& path : validArchivePaths)
        QTest::newRow(path.data())
                << path.data()
                << invalidPassword
                << BackendHandlerError::InvalidPasswordOrDataError;

    const std::initializer_list<QLatin1String> corruptedArchivePaths
    {
        QLatin1String("../../data/rar/corrupted.encrypted.non_solid.rar_old.rar"),
                QLatin1String("../../data/rar/corrupted.encrypted_content.non_solid.rar_new.rar"),
                QLatin1String("../../data/rar/corrupted.encrypted.rar_new.rar"),
                QLatin1String("../../data/rar/corrupted.encrypted.solid.rar_new.rar"),
    };

    for (const QLatin1String& path : corruptedArchivePaths)
        QTest::newRow(path.data())
                << path.data()
                << validPassword
                << BackendHandlerError::InvalidPasswordOrDataError;
}


void BackendHandlerExtract::extractEncryptedError()
{
    QFETCH(QString, archivePath);
    QFETCH(QString, password);
    QFETCH(BackendHandlerError, expectedError);

    BackendHandler handler1;

    TestArchiveExtractParams params;
    params.handler = &handler1;
    params.archivePath = archivePath;
    params.password = password;
    params.expectedEndState = BackendHandlerState::Errors;
    params.expectedError = expectedError;

    // Test with password provided in the command-line
    testArchiveExtract(params);

    // BackendHandlers must be used only once
    BackendHandler handler2;

    params.handler = &handler2;
    params.passwordTestMode = PasswordTestMode::PromptPasswordEntry;

    // Test with password provided in the prompt
    testArchiveExtract(params);
}

}

QTEST_GUILESS_MAIN(Packuru::Tests::Plugins::Unarchiver::BackendHandlerExtract)

#include "tst_backendhandlerextract.moc"
