// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QtTest>

#include "src/core/private/plugin-api/backendhandlererror.h"
#include "src/core/private/plugin-api/backendhandlerstate.h"

#include "src/plugin-unarchiver-core/backendhandler.h"

#include "tests/tests-shared/testarchivetest.h"
#include "tests/tests-shared/passwords.h"
#include "tests/tests-shared/passwordtestmode.h"


using namespace Packuru::Core;
using namespace Packuru::Plugins::Unarchiver::Core;
using namespace Packuru::Tests::Shared;


namespace Packuru::Tests::Plugins::Unarchiver
{

class BackendHandlerTest : public QObject
{
    Q_OBJECT

public:
    BackendHandlerTest();
    ~BackendHandlerTest() override;

private slots:
    void initTestCase();

    void testArchive_data();
    void testArchive();

    void testError_data();
    void testError();

    void testEncrypted_data();
    void testEncrypted();

    void testEncryptedError_data();
    void testEncryptedError();
};


BackendHandlerTest::BackendHandlerTest()
{

}


BackendHandlerTest::~BackendHandlerTest()
{

}


void BackendHandlerTest::initTestCase()
{
    if (!BackendHandler::executableAvailable())
        QSKIP("Executable not found");
}


void BackendHandlerTest::testArchive_data()
{
    QTest::addColumn<QString>("archivePath");

    const std::initializer_list<QLatin1String> paths
    {
        QLatin1String("../../data/rar/multipart.comment.lock.recovery.solid.rar_new.part01.rar"),
                QLatin1String("../../data/rar/multipart.comment.lock.recovery.solid.rar_old.rar"),
    };

    for (const QLatin1String& path : paths)
        QTest::newRow(path.data())
                << path.data();
}


void BackendHandlerTest::testArchive()
{
    BackendHandler handler;

    QFETCH(QString, archivePath);

    TestArchiveTestParams params;
    params.handler = &handler;
    params.archivePath = archivePath;

    testArchiveTest(params);
}


void BackendHandlerTest::testError_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<BackendHandlerError>("expectedError");

    const std::initializer_list<QLatin1String> paths
    {
        QLatin1String("../../data/rar/missing_part.rar_new.part01.rar"),
                QLatin1String("../../data/rar/missing_part.rar_old.rar"),
                QLatin1String("../../data/zip/missing_part.scheme1.zip")
    };

    for (const QLatin1String& path : paths)
        QTest::newRow(path.data())
                << path.data()
                << BackendHandlerError::DataError;
}


void BackendHandlerTest::testError()
{
    BackendHandler handler;

    QFETCH(QString, archivePath);
    QFETCH(BackendHandlerError, expectedError);

    TestArchiveTestParams params;
    params.handler = &handler;
    params.archivePath = archivePath;
    params.expectedEndState = BackendHandlerState::Errors;
    params.expectedError = expectedError;

    testArchiveTest(params);
}


void BackendHandlerTest::testEncrypted_data()
{
    QTest::addColumn<QString>("archivePath");

    const std::initializer_list<QLatin1String> paths
    {
        QLatin1String("../../data/rar/encrypted.solid.rar_new.rar"),
                QLatin1String("../../data/rar/encrypted.non_solid.rar_old.rar"),
                QLatin1String("../../data/rar/encrypted_content.rar_new.rar"),
                QLatin1String("../../data/rar/encrypted_content.rar_old.rar"),
    };

    for (const QLatin1String& path : paths)
        QTest::newRow(path.data())
                << path.data();
}


void BackendHandlerTest::testEncrypted()
{
    BackendHandler handler;

    QFETCH(QString, archivePath);

    TestArchiveTestParams params;
    params.handler = &handler;
    params.archivePath = archivePath;
    params.password = validPassword;

    // Test with password provided in the command-line
    testArchiveTest(params);

    // BackendHandlers must be used only once
    BackendHandler handler2;

    params.handler = &handler2;
    params.passwordTestMode = PasswordTestMode::PromptPasswordEntry;

    // Test with password provided in the prompt
    testArchiveTest(params);
}


void BackendHandlerTest::testEncryptedError_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<QString>("password");
    QTest::addColumn<BackendHandlerError>("expectedError");

    const std::initializer_list<QLatin1String> validArchivePaths
    {
        QLatin1String("../../data/rar/encrypted.solid.rar_new.rar"),
                QLatin1String("../../data/rar/encrypted.non_solid.rar_old.rar"),
                QLatin1String("../../data/rar/encrypted_content.rar_new.rar"),
                QLatin1String("../../data/rar/encrypted_content.rar_old.rar"),
    };

    for (const QLatin1String& path : validArchivePaths)
        QTest::newRow(path.data())
                << path.data()
                << invalidPassword
                << BackendHandlerError::InvalidPasswordOrDataError;

    const std::initializer_list<QLatin1String> corruptedArchivePaths
    {
        QLatin1String("../../data/rar/corrupted.encrypted.non_solid.rar_old.rar"),
                QLatin1String("../../data/rar/corrupted.encrypted_content.non_solid.rar_new.rar"),
                QLatin1String("../../data/rar/corrupted.encrypted.rar_new.rar"),
                QLatin1String("../../data/rar/corrupted.encrypted.solid.rar_new.rar"),
    };

    for (const QLatin1String& path : corruptedArchivePaths)
        QTest::newRow(path.data())
                << path.data()
                << validPassword
                << BackendHandlerError::InvalidPasswordOrDataError;
}


void BackendHandlerTest::testEncryptedError()
{
    BackendHandler handler;

    QFETCH(QString, archivePath);
    QFETCH(QString, password);
    QFETCH(BackendHandlerError, expectedError);

    TestArchiveTestParams params;
    params.handler = &handler;
    params.archivePath = archivePath;
    params.password = password;
    params.expectedEndState = BackendHandlerState::Errors;
    params.expectedError = expectedError;

    // Test with password provided in the command-line
    testArchiveTest(params);

    // BackendHandlers must be used only once
    BackendHandler handler2;

    params.handler = &handler2;
    params.passwordTestMode = PasswordTestMode::PromptPasswordEntry;

    // Test with password provided in the prompt
    testArchiveTest(params);
}

}

QTEST_GUILESS_MAIN(Packuru::Tests::Plugins::Unarchiver::BackendHandlerTest)

#include "tst_backendhandlertest.moc"
