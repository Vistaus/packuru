// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QObject>
#include <QtTest>

#include "src/plugin-unarchiver-core/backendhandler.h"

#include "tests/tests-shared/backendhandler7zipunarextract.h"


using namespace Packuru::Plugins::Unarchiver::Core;
using namespace Packuru::Tests::Shared;


namespace Packuru::Tests::Plugins::Unarchiver
{

class BackendHandlerExtractShared : public BackendHandler7ZipUnarExtract
{
    Q_OBJECT
public:
    BackendHandlerExtractShared()
        : BackendHandler7ZipUnarExtract({ [] () { return std::make_unique<BackendHandler>(); },
    [] () { return BackendHandler::executableAvailable(); } }) {}
};

}

QTEST_GUILESS_MAIN(Packuru::Tests::Plugins::Unarchiver::BackendHandlerExtractShared)

#include "tst_backendhandlerextractshared.moc"
