// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QObject>
#include <QtTest>

#include "src/plugin-7zip-core/backendhandler.h"

#include "tests/tests-shared/backendhandler7zipunartest.h"


using namespace Packuru::Plugins::_7Zip::Core;
using namespace Packuru::Tests::Shared;

namespace Packuru::Tests::Plugins::_7Zip
{

class BackendHandlerTestShared : public BackendHandler7ZipUnarTest
{
    Q_OBJECT
public:
    BackendHandlerTestShared()
        : BackendHandler7ZipUnarTest({ [] () { return std::make_unique<BackendHandler>(); },
    [] () { return BackendHandler::executableAvailable(); } }) {}
};

}

QTEST_GUILESS_MAIN(Packuru::Tests::Plugins::_7Zip::BackendHandlerTestShared)

#include "tst_backendhandlertestshared.moc"
