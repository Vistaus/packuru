// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <unordered_map>

#include <QtTest>

#include "src/core/private/plugin-api/backendhandlererror.h"
#include "src/core/private/plugin-api/backendhandlerstate.h"
#include "src/core/private/plugin-api/backendarchiveproperties.h"
#include "src/core/private/plugin-api/backendarchiveproperty.h"
#include "src/core/private/plugin-api/encryptionstate.h"

#include "src/plugin-7zip-core/backendhandler.h"

#include "tests/tests-shared/testarchiveread.h"
#include "tests/tests-shared/createmultipartproperties.h"


using namespace Packuru::Core;
using namespace Packuru::Tests::Shared;
using Packuru::Plugins::_7Zip::Core::BackendHandler;


namespace Packuru::Tests::Plugins::_7Zip
{

class BackendHandlerRead : public QObject
{
    Q_OBJECT

public:
    BackendHandlerRead();
    ~BackendHandlerRead() override;

private slots:
    void initTestCase();

    void readSuccess_data();
    void readSuccess();

    void readError_data();
    void readError();
};


BackendHandlerRead::BackendHandlerRead()
{

}


BackendHandlerRead::~BackendHandlerRead()
{

}


void BackendHandlerRead::initTestCase()
{
    if (!BackendHandler::executableAvailable())
        QSKIP("Executable not found");
}


void BackendHandlerRead::readSuccess_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<BackendArchiveProperties>("requiredProperties");
    QTest::addColumn<ArchiveItemChecker>("itemChecker");
}


void BackendHandlerRead::readSuccess()
{
//    BackendHandler handler;

//    QFETCH(QString, archivePath);
//    QFETCH(BackendArchiveProperties, requiredProperties);
//    QFETCH(ArchiveItemChecker, itemChecker);

//    TestArchiveReadParams params;
//    params.handler = &handler;
//    params.archivePath = archivePath;
//    params.requiredProperties = requiredProperties;
//    params.itemChecker = itemChecker;

//    testArchiveRead(params);
}


void BackendHandlerRead::readError_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<BackendHandlerError>("expectedError");

    const char* path = "../../data/zip/corrupted1.zip";
    QTest::newRow(path)
            << path
            << BackendHandlerError::DataError;

    path = "../../data/zip/corrupted2.zip";
    QTest::newRow(path)
            << path
            << BackendHandlerError::DataError;

    path = "../../data/zip/missing_part.scheme1.zip";
    QTest::newRow(path)
            << path
            << BackendHandlerError::ArchivePartNotFound;
}


void BackendHandlerRead::readError()
{
    BackendHandler handler;

    QFETCH(QString, archivePath);
    QFETCH(BackendHandlerError, expectedError);

    TestArchiveReadParams params;
    params.handler = &handler;
    params.archivePath = archivePath;
    params.expectedEndState = BackendHandlerState::Errors;
    params.expectedError = expectedError;

    testArchiveRead(params);
}

}

QTEST_GUILESS_MAIN(Packuru::Tests::Plugins::_7Zip::BackendHandlerRead)

#include "tst_backendhandlerread.moc"
