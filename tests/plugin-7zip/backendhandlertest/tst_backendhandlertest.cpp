// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QtTest>

#include "src/core/private/plugin-api/backendhandlererror.h"
#include "src/core/private/plugin-api/backendhandlerstate.h"

#include "src/plugin-7zip-core/backendhandler.h"

#include "tests/tests-shared/testarchivetest.h"


using namespace Packuru::Core;
using namespace Packuru::Tests::Shared;
using Packuru::Plugins::_7Zip::Core::BackendHandler;


namespace Packuru::Tests::Plugins::_7Zip
{

class BackendHandlerTest : public QObject
{
    Q_OBJECT

public:
    BackendHandlerTest();
    ~BackendHandlerTest() override;

private slots:
    void initTestCase();

    void testError_data();
    void testError();
};


BackendHandlerTest::BackendHandlerTest()
{

}


BackendHandlerTest::~BackendHandlerTest()
{

}


void BackendHandlerTest::initTestCase()
{
    if (!BackendHandler::executableAvailable())
        QSKIP("Executable not found");
}


void BackendHandlerTest::testError_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<BackendHandlerError>("expectedError");

    const std::initializer_list<QLatin1String> paths
    {
        QLatin1String("../../data/zip/corrupted1.zip"),
                QLatin1String("../../data/zip/corrupted2.zip"),
    };

    for (const QLatin1String& path : paths)
        QTest::newRow(path.data())
                << path.data()
                << BackendHandlerError::DataError;

    const std::initializer_list<QLatin1String> paths2
    {
        QLatin1String("../../data/zip/missing_part.scheme1.zip")
    };

    for (const QLatin1String& path : paths2)
        QTest::newRow(path.data())
                << path.data()
                << BackendHandlerError::ArchivePartNotFound;
}


void BackendHandlerTest::testError()
{
    BackendHandler handler;

    QFETCH(QString, archivePath);
    QFETCH(BackendHandlerError, expectedError);

    TestArchiveTestParams params;
    params.handler = &handler;
    params.archivePath = archivePath;
    params.expectedEndState = BackendHandlerState::Errors;
    params.expectedError = expectedError;

    testArchiveTest(params);
}

}

QTEST_GUILESS_MAIN(Packuru::Tests::Plugins::_7Zip::BackendHandlerTest)

#include "tst_backendhandlertest.moc"
