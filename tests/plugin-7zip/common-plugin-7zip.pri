# SPDX-FileCopyrightText: The Packuru Developers
#
# SPDX-License-Identifier: 0BSD

include(../common-testcases.pri)

DESTDIR += $${TESTS_TARGET_DIR}/plugin-7zip

LIBS += -l$${PROJECT_BUILD_NAME}-plugin-7zip-core
