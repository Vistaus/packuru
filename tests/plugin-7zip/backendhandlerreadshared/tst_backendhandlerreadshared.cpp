// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QObject>
#include <QtTest>

#include "src/plugin-7zip-core/backendhandler.h"

#include "tests/tests-shared/backendhandler7zipunarread.h"


using namespace Packuru::Plugins::_7Zip::Core;
using namespace Packuru::Tests::Shared;

namespace Packuru::Tests::Plugins::_7Zip
{

class BackendHandlerReadShared : public BackendHandler7ZipUnarRead
{
    Q_OBJECT
public:
    BackendHandlerReadShared()
        : BackendHandler7ZipUnarRead({ [] () { return std::make_unique<BackendHandler>(); },
    [] () { return BackendHandler::executableAvailable(); } }) {}
};

}

QTEST_GUILESS_MAIN(Packuru::Tests::Plugins::_7Zip::BackendHandlerReadShared)

#include "tst_backendhandlerreadshared.moc"
