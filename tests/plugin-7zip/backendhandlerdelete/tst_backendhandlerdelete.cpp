// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QtTest>

#include "src/core/private/plugin-api/backendhandlererror.h"
#include "src/core/private/plugin-api/backendhandlerstate.h"

#include "src/plugin-7zip-core/backendhandler.h"

#include "tests/tests-shared/testarchivedelete.h"
#include "tests/tests-shared/passwords.h"


using namespace Packuru::Plugins::_7Zip::Core;
using namespace Packuru::Tests::Shared;


namespace Packuru::Tests::Plugins::_7Zip
{

class BackendHandlerDelete : public QObject
{
    Q_OBJECT

public:
    BackendHandlerDelete();
    ~BackendHandlerDelete();

private slots:
    void initTestCase();

    void deleteSelected_data();
    void deleteSelected();
};


BackendHandlerDelete::BackendHandlerDelete()
{  

}


BackendHandlerDelete::~BackendHandlerDelete()
{

}


void BackendHandlerDelete::initTestCase()
{
    if (!BackendHandler::executableAvailable())
        QSKIP("Executable not found");
}


void BackendHandlerDelete::deleteSelected_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<std::vector<QString>>("filePaths");
    QTest::addColumn<QString>("password");

    const char* path = "../../data/7z/solid.unix_link.7z";

    QTest::newRow(path)
            << path
            << std::vector<QString>{"files/file1.txt"}
            << "";

    path = "../../data/7z/encrypted_content.solid.7z";
    QTest::newRow(path)
            << path
            << std::vector<QString>{"files/"}
            << "";

    path = "../../data/7z/encrypted.solid.7z";
    QTest::newRow(path)
            << path
            << std::vector<QString>{"files/"}
            << validPassword;

    path = "../../data/zip/unix_link.zip";
    QTest::newRow(path)
            << path
            << std::vector<QString>{"files/"}
            << "";

    path = "../../data/zip/encrypted_content.zip";
    QTest::newRow(path)
            << path
            << std::vector<QString>{"files/file1.txt"}
            << "";
}


void BackendHandlerDelete::deleteSelected()
{
    BackendHandler handler;

    QFETCH(QString, archivePath);
    QFETCH(std::vector<QString>, filePaths);
    QFETCH(QString, password);

    TestArchiveDeleteParams params;
    params.handler = &handler;
    params.archivePath = archivePath;
    params.absoluteFilePaths = filePaths;
    params.password = password;

    testArchiveDelete(params);

    const auto args = handler.getBackendArgs();
    QCOMPARE(args.size(), static_cast<std::size_t>(1));

    for (const auto& item : params.absoluteFilePaths)
    {
        QVERIFY2(args[0].contains(item ), "Item=" + item.toLatin1());
    }
}

}

QTEST_GUILESS_MAIN(Packuru::Tests::Plugins::_7Zip::BackendHandlerDelete)

#include "tst_backendhandlerdelete.moc"
