// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QtTest>
#include <QDirIterator>

#include "src/core/private/plugin-api/backendhandlererror.h"
#include "src/core/private/plugin-api/backendhandlerstate.h"

#include "src/plugin-zpaq-core/backendhandler.h"

#include "tests/tests-shared/testarchivetest.h"
#include "tests/tests-shared/addtestfiles.h"
#include "tests/tests-shared/passwords.h"
#include "tests/tests-shared/passwordtestmode.h"


using namespace Packuru::Core;
using namespace Packuru::Tests::Shared;
using Packuru::Plugins::ZPAQ::Core::BackendHandler;


namespace Packuru::Tests::Plugins::ZPAQ
{

class BackendHandlerTest : public QObject
{
    Q_OBJECT

public:
    BackendHandlerTest();
    ~BackendHandlerTest();

private slots:
    void initTestCase();

    void testArchive_data();
    void testArchive();

    void testError_data();
    void testError();

    void testEncrypted_data();
    void testEncrypted();

    void testEncryptedError_data();
    void testEncryptedError();

    void testEncryptedAbort_data();
    void testEncryptedAbort();

private:
    const QString dataDir = QFINDTESTDATA("../../data/zpaq");
};


BackendHandlerTest::BackendHandlerTest()
{

}


BackendHandlerTest::~BackendHandlerTest()
{

}


void BackendHandlerTest::initTestCase()
{
    if (!BackendHandler::executableAvailable())
        QSKIP("Executable not found");
}


void BackendHandlerTest::testArchive_data()
{
    QTest::addColumn<QString>("archivePath");

    const char* path = "../../data/zpaq/files.zpaq";
    QTest::newRow(path)
            << path;
}


void BackendHandlerTest::testArchive()
{
    BackendHandler handler;

    QFETCH(QString, archivePath);

    TestArchiveTestParams params;
    params.handler = &handler;
    params.archivePath = archivePath;

    testArchiveTest(params);
}


void BackendHandlerTest::testError_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<BackendHandlerError>("expectedError");

    const auto insp = [] (QTestData& data)
    { data << BackendHandlerError::DataError; };

    addTestFiles(dataDir, QRegularExpression("corrupted.\\d+.zpaq"), insp);

    const char* path = "../../data/zpaq/truncated.zpaq";
    QTest::newRow(path)
            << path
            << BackendHandlerError::DataError;
}


void BackendHandlerTest::testError()
{
    BackendHandler handler;
    QFETCH(QString, archivePath);
    QFETCH(BackendHandlerError, expectedError);

    TestArchiveTestParams params;
    params.handler = &handler;
    params.archivePath = archivePath;
    params.expectedEndState = BackendHandlerState::Errors;
    params.expectedError = expectedError;

    testArchiveTest(params);
}


void BackendHandlerTest::testEncrypted_data()
{
    QTest::addColumn<QString>("archivePath");

    const char* path = "../../data/zpaq/encrypted.zpaq";
    QTest::newRow(path)
            << path;
}


void BackendHandlerTest::testEncrypted()
{
    BackendHandler handler;

    QFETCH(QString, archivePath);

    TestArchiveTestParams params;
    params.handler = &handler;
    params.archivePath = archivePath;
    params.password = validPassword;

    // Test with password provided in the command-line
    testArchiveTest(params);

    // BackendHandlers must be used only once
    BackendHandler handler2;

    params.handler = &handler2;
    params.passwordTestMode = PasswordTestMode::PromptPasswordEntry;

    // Test with password provided in the prompt
    testArchiveTest(params);
}


void BackendHandlerTest::testEncryptedError_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<QString>("password");
    QTest::addColumn<BackendHandlerError>("expectedError");

    const char* path = "../../data/zpaq/encrypted.zpaq";
    QTest::newRow(path)
            << path
            << invalidPassword
            << BackendHandlerError::InvalidPasswordOrDataError;

    path = "../../data/zpaq/corrupted.encrypted.1.zpaq";
    QTest::newRow(path)
            << path
            << validPassword
            << BackendHandlerError::InvalidPasswordOrDataError;

    path = "../../data/zpaq/corrupted.encrypted.2.zpaq";
    QTest::newRow(path)
            << path
            << validPassword
            << BackendHandlerError::DataError;
}


void BackendHandlerTest::testEncryptedError()
{
    BackendHandler handler;

    QFETCH(QString, archivePath);
    QFETCH(QString, password);
    QFETCH(BackendHandlerError, expectedError);

    TestArchiveTestParams params;
    params.handler = &handler;
    params.archivePath = archivePath;
    params.password = password;
    params.expectedEndState = BackendHandlerState::Errors;
    params.expectedError = expectedError;

    // Test with password provided in the command-line
    testArchiveTest(params);

    // BackendHandlers must be used only once
    BackendHandler handler2;

    params.handler = &handler2;
    params.passwordTestMode = PasswordTestMode::PromptPasswordEntry;

    // Test with password provided in the prompt
    testArchiveTest(params);
}


void BackendHandlerTest::testEncryptedAbort_data()
{
    QTest::addColumn<QString>("archivePath");

    const char* path = "../../data/zpaq/encrypted.zpaq";
    QTest::newRow(path)
            << path;
}


void BackendHandlerTest::testEncryptedAbort()
{
    BackendHandler handler;

    QFETCH(QString, archivePath);

    TestArchiveTestParams params;
    params.handler = &handler;
    params.archivePath = archivePath;
    params.passwordTestMode = PasswordTestMode::PromptAbort;
    params.expectedEndState = BackendHandlerState::Aborted;

    testArchiveTest(params);
}


}

QTEST_GUILESS_MAIN(Packuru::Tests::Plugins::ZPAQ::BackendHandlerTest)

#include "tst_backendhandlertest.moc"
