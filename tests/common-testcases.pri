# SPDX-FileCopyrightText: The Packuru Developers
#
# SPDX-License-Identifier: 0BSD

include(common-tests.pri)

LIBS += -L$${TESTS_SHARED_LIB_TARGET_DIR}

LIBS += -l$${TESTS_SHARED_LIB_BUILD_NAME}

# project core libs
QMAKE_RPATHDIR += $ORIGIN/../../libs

# project test libs
QMAKE_RPATHDIR += $ORIGIN/../libs
