// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QtTest>

#include "src/utils/qvariant_utils.h"

#include "src/core/private/archivetypedetector.h"
#include "src/core/private/plugin-api/archivetype.h"
#include "src/core/private/plugin-api/backendhandlererror.h"
#include "src/core/private/plugin-api/backendhandlerstate.h"
#include "src/core/private/plugin-api/backendreaddata.h"
#include "src/core/private/plugin-api/abstractbackendhandler.h"
#include "src/core/private/plugin-api/encryptionstate.h"
#include "src/core/private/plugin-api/backendarchiveproperty.h"

#include "passwordtestmode.h"
#include "testarchiveread.h"
#include "checkbackendexpectederror.h"


using namespace Packuru::Core;
using namespace Packuru::Utils;

namespace
{
const ArchiveTypeDetector typeDetector;
}

namespace Packuru::Tests::Shared
{

TestArchiveReadParams::TestArchiveReadParams()
    : handler(nullptr),
      passwordTestMode(PasswordTestMode::StartUpPasswordEntry),
      expectedEndState(BackendHandlerState::Success),
      expectedError(BackendHandlerError::___INVALID)
{


}


void testArchiveRead(const TestArchiveReadParams& params)
{
    BackendReadData data;
    data.archiveInfo.setFile(QFINDTESTDATA(params.archivePath));

    if (data.archiveInfo.fileName().isEmpty())
        QSKIP("");

    data.archiveType = typeDetector.detectType(data.archiveInfo.absoluteFilePath());
    data.pluginPrivateData = params.pluginPrivateData;

    ArchiveItemMap itemMap;

    if (params.itemChecker)
    {
        QObject::connect(params.handler, &AbstractBackendHandler::newItems,
                         [&itemMap = itemMap]
                         (std::vector<ArchiveItem>& newItems) mutable
        {
            for (auto& item : newItems)
                itemMap[item.name] = std::move(item);
        });
    }

    if (params.passwordTestMode == PasswordTestMode::StartUpPasswordEntry)
    {
        data.password = params.password;
        params.handler->readArchive(data);
    }
    else
    {
        params.handler->readArchive(data);

        QTRY_VERIFY2(params.handler->getState() == BackendHandlerState::WaitingForPassword,
                     (QLatin1String("Actual state=") + toString(params.handler->getState())).toLatin1());

        if (params.passwordTestMode == PasswordTestMode::PromptAbort)
        {
            params.handler->stop();
            QTRY_VERIFY2(params.handler->getState() == BackendHandlerState::Aborted,
                         (QLatin1String("Actual state=") + toString(params.handler->getState())).toLatin1());
            return;
        }
        else
            params.handler->enterPassword(params.password);
    }

    QTRY_VERIFY2(params.handler->isFinished() == true,
                 (QLatin1String("Actual state=") + toString(params.handler->getState())).toLatin1());

    const BackendHandlerState actualState = params.handler->getState();
    QCOMPARE(actualState, params.expectedEndState);

    const auto errors = params.handler->getErrors();
    const auto warnings = params.handler->getWarnings();

    QVERIFY(actualState != BackendHandlerState::Success || (errors.empty() && warnings.empty()));
    QVERIFY(actualState != BackendHandlerState::Warnings || errors.empty());

    if (params.expectedError != BackendHandlerError::___INVALID)
    {
        QVERIFY2(const auto check = checkBackendExpectedError(errors, params.expectedError),
                 check.errorString.toLatin1());
    }

    qDebug() << "Checking archive's required properties";

    if (!params.requiredProperties.empty())
    {
        const auto actualProperties = params.handler->getArchiveProperties();

        for (const auto& mIt : params.requiredProperties)
        {
            const auto aIt = actualProperties.find(mIt.first);
            QVERIFY2(aIt != actualProperties.end(),
                     QString("Actual properties do not contain a required property %1")
                     .arg(toLatin1(mIt.first))
                     .toLatin1());
            const QVariant actualPropertyValue = aIt->second;
            const QVariant requiredPropertyValue = mIt.second;


            const QString actualPropertyType (QMetaType::typeName(actualPropertyValue.userType()));
            const QString requiredPropertyType (QMetaType::typeName(requiredPropertyValue.userType()));

            qDebug() << "Property key=" << toString(mIt.first);

            QCOMPARE(actualPropertyType, requiredPropertyType);

            // Enums are not printable without Q_ENUM
            if (containsType<EncryptionState>(requiredPropertyValue))
            {
                const auto actual = getValueAs<EncryptionState>(actualPropertyValue);
                const auto required = getValueAs<EncryptionState>(requiredPropertyValue);
                QCOMPARE(toString(actual), toString(required));
            }
            else
                QCOMPARE(actualPropertyValue, requiredPropertyValue);
        }
    }

    qDebug() << "Checking items' required properties";

    if (params.itemChecker)
        params.itemChecker(itemMap);
}

}
