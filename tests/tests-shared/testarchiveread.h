// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QString>

#include "src/core/private/plugin-api/backendarchiveproperties.h"
#include "src/core/private/plugin-api/archiveitem.h"

#include "itemchecker.h"
#include "symbol_export.h"


namespace Packuru::Core
{
class AbstractBackendHandler;
enum class ArchiveType;
enum class BackendHandlerState;
enum class BackendHandlerError;
}


namespace Packuru::Tests::Shared
{

enum class PasswordTestMode;
using ArchiveItemMap = ItemMap<Packuru::Core::ArchiveItem>;
using ArchiveItemChecker = ItemChecker<Packuru::Core::ArchiveItem>;

struct PACKURU_TESTS_SHARED_EXPORT TestArchiveReadParams
{
    TestArchiveReadParams();

    Packuru::Core::AbstractBackendHandler* handler;
    QString archivePath;
    PasswordTestMode passwordTestMode;
    QString password;
    QVariant pluginPrivateData;
    Packuru::Core::BackendHandlerState expectedEndState;
    Packuru::Core::BackendHandlerError expectedError;
    Packuru::Core::BackendArchiveProperties requiredProperties;
    ArchiveItemChecker itemChecker;
};


PACKURU_TESTS_SHARED_EXPORT void testArchiveRead(const TestArchiveReadParams& params);

}

Q_DECLARE_METATYPE(Packuru::Tests::Shared::ArchiveItemChecker);
