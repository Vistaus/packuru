// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <functional>

#include <QString>
#include <QFileInfo>

#include "src/core/private/plugin-api/backendarchiveproperties.h"

#include "itemchecker.h"
#include "symbol_export.h"


namespace Packuru::Core
{
class AbstractBackendHandler;
enum class BackendHandlerState;
enum class BackendHandlerError;
enum class ArchivingFilePaths;
enum class EncryptionState;
enum class ArchiveType;
}


namespace Packuru::Tests::Shared
{

struct PACKURU_TESTS_SHARED_EXPORT TestArchiveAddParams
{
    TestArchiveAddParams();

    Packuru::Core::AbstractBackendHandler* handler;
    QString existingArchivePath; // Required when adding files to existing archive
    Packuru::Core::ArchiveType archiveType;
    Packuru::Core::EncryptionState currentArchiveEncryption;
    Packuru::Core::EncryptionState newArchiveEncryption;
    QString password;
    std::vector<QFileInfo> inputItems;
    QString internalPath;
    qulonglong partSize;
    Packuru::Core::ArchivingFilePaths filePaths;
    bool createNewArchive;
    QVariant pluginPrivateData;
    Packuru::Core::BackendHandlerState expectedEndState;
    Packuru::Core::BackendHandlerError expectedError;
};


PACKURU_TESTS_SHARED_EXPORT void testArchiveAdd(const TestArchiveAddParams& params);

}
