// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QtCore/qglobal.h>

#if defined(PACKURU_TESTS_SHARED_LIBRARY)
#  define PACKURU_TESTS_SHARED_EXPORT Q_DECL_EXPORT
#else
#  define PACKURU_TESTS_SHARED_EXPORT Q_DECL_IMPORT
#endif
