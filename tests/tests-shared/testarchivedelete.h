// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <functional>

#include <QString>
#include <QFileInfo>

#include "src/core/private/plugin-api/backendarchiveproperties.h"

#include "itemchecker.h"
#include "symbol_export.h"


namespace Packuru::Core
{
class AbstractBackendHandler;
enum class BackendHandlerState;
enum class BackendHandlerError;
enum class ArchivingFilePaths;
enum class EncryptionState;
enum class ArchiveType;
}


namespace Packuru::Tests::Shared
{

struct PACKURU_TESTS_SHARED_EXPORT TestArchiveDeleteParams
{
    TestArchiveDeleteParams();

    Packuru::Core::AbstractBackendHandler* handler;
    QString archivePath; // Required when adding files to existing archive
    QString password;
    std::vector<QString> absoluteFilePaths;
    QVariant pluginPrivateData;
    Packuru::Core::BackendHandlerState expectedEndState;
    Packuru::Core::BackendHandlerError expectedError;
};


PACKURU_TESTS_SHARED_EXPORT void testArchiveDelete(const TestArchiveDeleteParams& params);

}
