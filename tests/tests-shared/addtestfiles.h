// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <functional>

#include <QtTest>

#include "symbol_export.h"


namespace Packuru::Tests::Shared
{

using QTestDataInspector = std::function<void(QTestData&)>;

// folderPath must be resolved with QFINDTESTDATA
PACKURU_TESTS_SHARED_EXPORT void addTestFiles(const QString& folderPath,
                  const QString& nameFilter,
                  const QTestDataInspector& insp = QTestDataInspector());

PACKURU_TESTS_SHARED_EXPORT void addTestFiles(const QString& folderPath,
                  const QRegularExpression& nameFilter,
                  const QTestDataInspector& insp = QTestDataInspector());

}
