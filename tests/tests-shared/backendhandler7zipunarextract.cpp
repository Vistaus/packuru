// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QtTest>

#include "src/core/private/plugin-api/abstractbackendhandler.h"
#include "src/core/private/plugin-api/backendhandlererror.h"
#include "src/core/private/plugin-api/backendhandlerstate.h"

#include "backendhandler7zipunarextract.h"
#include "passwordtestmode.h"
#include "passwords.h"
#include "testarchiveextract.h"


using namespace Packuru::Core;


namespace Packuru::Tests::Shared
{

namespace
{

const auto commonChecker7zZip = [] (const QFileInfoMap& map)
{
    QCOMPARE(map.size(), static_cast<std::size_t>(4));

    const QFileInfo* item = getItem(map, "files");
    QVERIFY(item);
    QVERIFY(item->isDir());

    QVERIFY(item = getItem(map, "files/blob100b"));
    QCOMPARE(item->size(), 100);

    QVERIFY(item = getItem(map, "files/file1.txt"));
    QCOMPARE(item->size(), 4);

    QVERIFY(item = getItem(map, "files/file2.txt"));
    QCOMPARE(item->size(), 4);
};

}

BackendHandler7ZipUnarExtract::BackendHandler7ZipUnarExtract(const CLIBackendHandlerFactory& factory,
                                                             QObject *parent)
    : QObject(parent),
      bhFactory(factory)
{

}


BackendHandler7ZipUnarExtract::~BackendHandler7ZipUnarExtract()
{

}

void BackendHandler7ZipUnarExtract::initTestCase()
{
    if (!bhFactory.executablAvailable())
        QSKIP("Executable not available");
}


void BackendHandler7ZipUnarExtract::extractEntireArchive_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<QFileInfoChecker>("itemChecker");

    const auto checker1 = [] (const QFileInfoMap& map)
    {
        const QFileInfo* item = getItem(map, "blob1kb");
        QVERIFY(item);
        QCOMPARE(item->size(), 1000);
    };

    const char* path = "../../data/7z/multipart.7z.001";
    QTest::newRow(path)
            << path
            << QFileInfoChecker(checker1);

    path = "../../data/zip/multipart.scheme1.zip";
    QTest::newRow(path)
            << path
            << QFileInfoChecker(checker1);

    path = "../../data/zip/multipart.scheme2.zip.001";
    QTest::newRow(path)
            << path
            << QFileInfoChecker(checker1);

    const auto checkerAbsolutePaths = [] (const QFileInfoMap& map)
    {
        QCOMPARE(map.size(), static_cast<std::size_t>(5));

        const QFileInfo* item = getItem(map, "tmp");
        QVERIFY(item);
        QVERIFY(item->isDir());

        QVERIFY(item = getItem(map, "tmp/files"));
        QVERIFY(item);
        QVERIFY(item->isDir());

        QVERIFY(item = getItem(map, "tmp/files/blob100b"));
        QCOMPARE(item->size(), 100);

        QVERIFY(item = getItem(map, "tmp/files/file1.txt"));
        QCOMPARE(item->size(), 4);

        QVERIFY(item = getItem(map, "tmp/files/file2.txt"));
        QCOMPARE(item->size(), 4);
    };

    path = "../../data/7z/unix_absolute_paths.7z";
    QTest::newRow(path)
            << path
            << QFileInfoChecker(checkerAbsolutePaths);
}


void BackendHandler7ZipUnarExtract::extractEntireArchive()
{
    const auto handler = bhFactory.create();

    QFETCH(QString, archivePath);
    QFETCH(QFileInfoChecker, itemChecker);

    TestArchiveExtractParams params;
    params.handler = handler.get();
    params.archivePath = archivePath;
    params.itemChecker = itemChecker;

    testArchiveExtract(params);
}


void BackendHandler7ZipUnarExtract::extractSelectedFiles_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<std::vector<QString>>("filesToExtract");
    QTest::addColumn<QFileInfoChecker>("itemChecker");

    const std::vector<QString> files1
    {
        "files/blob100b",
        "files/file1.txt"
    };

    const auto checker1 = [totalItems = files1.size() + 1] (const QFileInfoMap& map)
    {
        QCOMPARE(map.size(), totalItems);

        const QFileInfo* item = getItem(map, "files");
        QVERIFY(item);
        QVERIFY(item->isDir());

        QVERIFY(item = getItem(map, "files/blob100b"));
        QCOMPARE(item->size(), 100);

        QVERIFY(item = getItem(map, "files/file1.txt"));
        QCOMPARE(item->size(), 4);
    };

    const char* path = "../../data/7z/solid.unix_link.7z";
    QTest::newRow(path)
            << path
            << files1
            << QFileInfoChecker(checker1);

    path = "../../data/zip/unix_link.zip";
    QTest::newRow(path)
            << path
            << files1
            << QFileInfoChecker(checker1);

    const std::vector<QString> files2
    {
        "/tmp/files/blob100b",
        "/tmp/files/file1.txt"
    };

    const auto checkerAbsolutePaths = [] (const QFileInfoMap& map)
    {
        QCOMPARE(map.size(), static_cast<std::size_t>(4));

        const QFileInfo* item = getItem(map, "tmp");
        QVERIFY(item);
        QVERIFY(item->isDir());

        QVERIFY(item = getItem(map, "tmp/files"));
        QVERIFY(item);
        QVERIFY(item->isDir());

        QVERIFY(item = getItem(map, "tmp/files/blob100b"));
        QCOMPARE(item->size(), 100);

        QVERIFY(item = getItem(map, "tmp/files/file1.txt"));
        QCOMPARE(item->size(), 4);
    };

    path = "../../data/7z/unix_absolute_paths.7z";
    QTest::newRow(path)
            << path
            << files2
            << QFileInfoChecker(checkerAbsolutePaths);
}


void BackendHandler7ZipUnarExtract::extractSelectedFiles()
{
    const auto handler = bhFactory.create();

    QFETCH(QString, archivePath);
    QFETCH(std::vector<QString>, filesToExtract);
    QFETCH(QFileInfoChecker, itemChecker);

    TestArchiveExtractParams params;
    params.handler = handler.get();
    params.archivePath = archivePath;
    params.filesToExtract = filesToExtract;
    params.itemChecker = itemChecker;

    testArchiveExtract(params);
}


void BackendHandler7ZipUnarExtract::extractSelectedFolder_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<QString>("folderToExtract");
    QTest::addColumn<QFileInfoChecker>("itemChecker");

    const QString folder = "files/";

    const char* path = "../../data/7z/files.7z";
    QTest::newRow(path)
            << path
            << folder
            << QFileInfoChecker(commonChecker7zZip);
}


void BackendHandler7ZipUnarExtract::extractSelectedFolder()
{
    const auto handler = bhFactory.create();

    QFETCH(QString, archivePath);
    QFETCH(QString, folderToExtract);
    QFETCH(QFileInfoChecker, itemChecker);

    TestArchiveExtractParams params;
    params.handler = handler.get();
    params.archivePath = archivePath;
    params.filesToExtract = {folderToExtract};
    params.itemChecker = itemChecker;

    testArchiveExtract(params);
}


void BackendHandler7ZipUnarExtract::extractError_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<BackendHandlerError>("expectedError");

    const std::initializer_list<QLatin1String> paths
    {
        QLatin1String("../../data/7z/corrupted1.7z"),
                QLatin1String("../../data/zip/corrupted3.zip"),
                QLatin1String("../../data/zip/corrupted.truncated.zip"),
                QLatin1String("../../data/7z/corrupted2.7z"),
                // 7-Zip and Unarchiver do not always report a missing part error
                // but rather a corrupted data error
                QLatin1String("../../data/7z/missing_part.7z.001"),
                QLatin1String("../../data/zip/missing_part.scheme2.zip.001")
    };

    for (const QLatin1String& path : paths)
        QTest::newRow(path.data())
                << path.data()
                << BackendHandlerError::DataError;
}


void BackendHandler7ZipUnarExtract::extractError()
{
    const auto handler = bhFactory.create();

    QFETCH(QString, archivePath);
    QFETCH(BackendHandlerError, expectedError);

    TestArchiveExtractParams params;
    params.handler = handler.get();
    params.archivePath = archivePath;
    params.expectedEndState = BackendHandlerState::Errors;
    params.expectedError = expectedError;

    testArchiveExtract(params);
}


void BackendHandler7ZipUnarExtract::extractEncrypted_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<QFileInfoChecker>("itemChecker");

    const auto checker1 = [] (const QFileInfoMap& map)
    {
        QCOMPARE(map.size(), static_cast<std::size_t>(4));

        const QFileInfo* item = getItem(map, "files");
        QVERIFY(item);
        QVERIFY(item->isDir());

        QVERIFY(item = getItem(map, "files/blob100b"));
        QCOMPARE(item->size(), 100);

        QVERIFY(item = getItem(map, "files/file1.txt"));
        QCOMPARE(item->size(), 4);

        QVERIFY(item = getItem(map, "files/file2.txt"));
        QCOMPARE(item->size(), 4);
    };

    const std::initializer_list<QLatin1String> linuxPaths
    {
        QLatin1String("../../data/7z/encrypted.solid.7z"),
                QLatin1String("../../data/7z/encrypted_content.solid.7z"),
                QLatin1String("../../data/zip/encrypted_content.zip")
    };

    for (const QLatin1String& path : linuxPaths)
        QTest::newRow(path.data())
                << path.data()
                << QFileInfoChecker(checker1);
}


void BackendHandler7ZipUnarExtract::extractEncrypted()
{
    QFETCH(QString, archivePath);
    QFETCH(QFileInfoChecker, itemChecker);

    auto handler = bhFactory.create();

    TestArchiveExtractParams params;
    params.handler = handler.get();
    params.archivePath = archivePath;
    params.password = validPassword;
    params.itemChecker = itemChecker;

    // Test with password provided in the command-line
    testArchiveExtract(params);

    // BackendHandlers must be used only once
    handler = bhFactory.create();

    params.handler = handler.get();
    params.passwordTestMode = PasswordTestMode::PromptPasswordEntry;

    // Test with password provided in the prompt
    testArchiveExtract(params);
}


void BackendHandler7ZipUnarExtract::extractEncryptedError_data()
{
    QTest::addColumn<QString>("archivePath");
    QTest::addColumn<QString>("password");
    QTest::addColumn<BackendHandlerError>("expectedError");

    const std::initializer_list<QLatin1String> validArchivePaths
    {
        QLatin1String("../../data/7z/encrypted.solid.7z"),
                QLatin1String("../../data/7z/encrypted_content.solid.7z"),
                QLatin1String("../../data/zip/encrypted_content.zip")
    };

    for (const QLatin1String& path : validArchivePaths)
        QTest::newRow(path.data())
                << path.data()
                << invalidPassword
                << BackendHandlerError::InvalidPasswordOrDataError;

    const std::initializer_list<QLatin1String> corruptedArchivePaths
    {
        QLatin1String("../../data/7z/corrupted.encrypted_content.solid.7z"),
                QLatin1String("../../data/7z/corrupted.encrypted2.7z"),
                QLatin1String("../../data/7z/corrupted.encrypted1.7z"),
                QLatin1String("../../data/7z/corrupted.encrypted_content.non_solid.7z"),
    };

    for (const QLatin1String& path : corruptedArchivePaths)
        QTest::newRow(path.data())
                << path.data()
                << validPassword
                << BackendHandlerError::InvalidPasswordOrDataError;
}


void BackendHandler7ZipUnarExtract::extractEncryptedError()
{
    QFETCH(QString, archivePath);
    QFETCH(QString, password);
    QFETCH(BackendHandlerError, expectedError);

    auto handler = bhFactory.create();

    TestArchiveExtractParams params;
    params.handler = handler.get();
    params.archivePath = archivePath;
    params.password = password;
    params.expectedEndState = BackendHandlerState::Errors;
    params.expectedError = expectedError;

    // Test with password provided in the command-line
    testArchiveExtract(params);

    // BackendHandlers must be used only once
    handler = bhFactory.create();

    params.handler = handler.get();
    params.passwordTestMode = PasswordTestMode::PromptPasswordEntry;

    // Test with password provided in the prompt
    testArchiveExtract(params);
}


void BackendHandler7ZipUnarExtract::extractEncryptedAbort_data()
{
    QTest::addColumn<QString>("archivePath");

    const char* path = "../../data/7z/encrypted.solid.7z";
    QTest::newRow(path)
            << path;
}


void BackendHandler7ZipUnarExtract::extractEncryptedAbort()
{
    const auto handler = bhFactory.create();

    QFETCH(QString, archivePath);

    TestArchiveExtractParams params;
    params.handler = handler.get();
    params.archivePath = archivePath;
    params.passwordTestMode = PasswordTestMode::PromptAbort;
    params.expectedEndState = BackendHandlerState::Aborted;

    testArchiveExtract(params);
}

}
