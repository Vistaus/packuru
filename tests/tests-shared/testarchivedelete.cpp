// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QtTest>
#include <QTemporaryDir>
#include <QStorageInfo>

#include "src/utils/qvariant_utils.h"

#include "src/core/private/archivetypedetector.h"
#include "src/core/private/plugin-api/archivetype.h"
#include "src/core/private/plugin-api/backendhandlererror.h"
#include "src/core/private/plugin-api/backendhandlerstate.h"
#include "src/core/private/plugin-api/backenddeletiondata.h"
#include "src/core/private/plugin-api/abstractbackendhandler.h"

#include "testarchivedelete.h"
#include "checkbackendexpectederror.h"


using namespace Packuru::Core;
using namespace Packuru::Utils;

namespace
{
const ArchiveTypeDetector typeDetector;
}

namespace Packuru::Tests::Shared
{

TestArchiveDeleteParams::TestArchiveDeleteParams()
    : handler(nullptr),
      expectedEndState(BackendHandlerState::Success),
      expectedError(BackendHandlerError::___INVALID)
{

}


void testArchiveDelete(const TestArchiveDeleteParams& params)
{
    BackendDeletionData data;

    const QTemporaryDir tempDir;
    if (!tempDir.isValid())
        QSKIP("Cannot create temporary directory: " + tempDir.path().toLatin1());

    const QStorageInfo info(tempDir.path());

    if (info.isReadOnly())
        QSKIP("No write permission for directory " + tempDir.path().toLatin1());

    if (info.bytesAvailable() < 10'000)
        QSKIP("Insufficient disk space: " + tempDir.path().toLatin1());

    const QString sourceArchive = QFINDTESTDATA(params.archivePath);

    // Operate on temporary copy
    QVERIFY(!sourceArchive.isEmpty());
    const QFileInfo archiveInfo(sourceArchive);
    QVERIFY(archiveInfo.exists());
    const QString tempArchivePath = tempDir.path() + QLatin1Char('/') + archiveInfo.fileName();
    QVERIFY(QFile::copy(sourceArchive, tempArchivePath));
    data.archiveInfo = tempArchivePath;

    data.archiveType = typeDetector.detectType(data.archiveInfo.absoluteFilePath());
    data.absoluteFilePaths = params.absoluteFilePaths;
    data.password = params.password;
    data.pluginPrivateData = params.pluginPrivateData;

    params.handler->deleteFiles(data);

    QTRY_VERIFY2(params.handler->isFinished() == true,
                 (QLatin1String("Actual state=") + toString(params.handler->getState())).toLatin1());

    const BackendHandlerState actualState = params.handler->getState();
    QCOMPARE(actualState, params.expectedEndState);

    const auto errors = params.handler->getErrors();
    const auto warnings = params.handler->getWarnings();

    QVERIFY(actualState != BackendHandlerState::Success || (errors.empty() && warnings.empty()));
    QVERIFY(actualState != BackendHandlerState::Warnings || errors.empty());

    if (params.expectedError != BackendHandlerError::___INVALID)
    {
        QVERIFY2(const auto check = checkBackendExpectedError(errors, params.expectedError),
                 check.errorString.toLatin1());
    }
}

}
