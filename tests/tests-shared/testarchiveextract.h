// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <functional>

#include <QString>
#include <QFileInfo>

#include "src/core/private/plugin-api/backendarchiveproperties.h"

#include "itemchecker.h"
#include "symbol_export.h"


namespace Packuru::Core
{
class AbstractBackendHandler;
enum class ArchiveType;
enum class BackendHandlerState;
enum class BackendHandlerError;
}


namespace Packuru::Tests::Shared
{

enum class PasswordTestMode;
using QFileInfoMap = ItemMap<QFileInfo>;
using QFileInfoChecker = ItemChecker<QFileInfo>;

struct PACKURU_TESTS_SHARED_EXPORT TestArchiveExtractParams
{
    TestArchiveExtractParams();

    Packuru::Core::AbstractBackendHandler* handler;
    QString archivePath;
    PasswordTestMode passwordTestMode;
    QString password;
    QVariant pluginPrivateData;
    Packuru::Core::BackendHandlerState expectedEndState;
    Packuru::Core::BackendHandlerError expectedError;
    std::vector<QString> filesToExtract;  // Dir paths must end with '/'
    QFileInfoChecker itemChecker;
};


PACKURU_TESTS_SHARED_EXPORT void testArchiveExtract(const TestArchiveExtractParams& params);

}

Q_DECLARE_METATYPE(Packuru::Tests::Shared::QFileInfoChecker);
