// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QObject>

#include "symbol_export.h"


namespace Packuru::Core
{
struct ArchiveHandlerInterface;
}


namespace Packuru::Tests::Shared
{

PACKURU_TESTS_SHARED_EXPORT Packuru::Core::ArchiveHandlerInterface* loadPlugin(const QString& name);

}
