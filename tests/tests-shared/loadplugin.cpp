// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QCoreApplication>
#include <QtTest>

#include "src/core/private/pluginmanager.h"
#include "src/core/private/plugin-api/archivehandlerinterface.h"

#include "loadplugin.h"


using namespace Packuru::Core;


namespace Packuru::Tests::Shared
{

ArchiveHandlerInterface* loadPlugin(const QString& name)
{
    PluginManager pm(QCoreApplication::applicationDirPath() + "/../../" + PROJECT_PLUGIN_DIR_NAME,
                     name);

    const auto plugins = pm.getPlugins<ArchiveHandlerInterface>();

    if (plugins.empty())
        return nullptr;
    else
        return plugins.front();
}

}
