// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QtTest>
#include <QTemporaryDir>
#include <QStorageInfo>

#include "src/utils/qvariant_utils.h"

#include "src/core/private/archivetypedetector.h"
#include "src/core/private/plugin-api/archivetype.h"
#include "src/core/private/plugin-api/backendhandlererror.h"
#include "src/core/private/plugin-api/backendhandlerstate.h"
#include "src/core/private/plugin-api/backendextractiondata.h"
#include "src/core/private/plugin-api/abstractbackendhandler.h"

#include "passwordtestmode.h"
#include "testarchiveextract.h"
#include "checkbackendexpectederror.h"


using namespace Packuru::Core;
using namespace Packuru::Utils;

namespace
{
const ArchiveTypeDetector typeDetector;
}

namespace Packuru::Tests::Shared
{

TestArchiveExtractParams::TestArchiveExtractParams()
    : handler(nullptr),
      passwordTestMode(PasswordTestMode::StartUpPasswordEntry),
      expectedEndState(BackendHandlerState::Success),
      expectedError(BackendHandlerError::___INVALID)
{


}


void testArchiveExtract(const TestArchiveExtractParams& params)
{
    BackendExtractionData data;
    data.archiveInfo.setFile(QFINDTESTDATA(params.archivePath));

    if (data.archiveInfo.fileName().isEmpty())
        QSKIP("");

    data.archiveType = typeDetector.detectType(data.archiveInfo.absoluteFilePath());

    const QTemporaryDir tempDir;

    if (!tempDir.isValid())
        QSKIP("Cannot create temporary directory: " + tempDir.path().toLatin1());

    const QStorageInfo info(tempDir.path());

    if (info.isReadOnly())
        QSKIP("No write permission for directory: " + tempDir.path().toLatin1());

    if (info.bytesAvailable() < 10'000)
        QSKIP("Insufficient disk space: " + tempDir.path().toLatin1());

    data.tempDestinationPath = tempDir.path();
    data.filesToExtract = params.filesToExtract;
    data.pluginPrivateData = params.pluginPrivateData;

    if (params.passwordTestMode == PasswordTestMode::StartUpPasswordEntry)
    {
        qDebug()<<"Startup password entry";
        data.password = params.password;
        params.handler->extractArchive(data);
    }
    else
    {
        qDebug()<<"Prompt password entry";
        params.handler->extractArchive(data);

        QTRY_VERIFY2(params.handler->getState() == BackendHandlerState::WaitingForPassword,
                     (QLatin1String("Actual state=") + toString(params.handler->getState())).toLatin1());

        if (params.passwordTestMode == PasswordTestMode::PromptAbort)
        {
            params.handler->stop();
            QTRY_VERIFY2(params.handler->getState() == BackendHandlerState::Aborted,
                         (QLatin1String("Actual state=") + toString(params.handler->getState())).toLatin1());
            return;
        }
        else
            params.handler->enterPassword(params.password);
    }

    QTRY_VERIFY2(params.handler->isFinished() == true,
                 (QLatin1String("Actual state=") + toString(params.handler->getState())).toLatin1());

    const BackendHandlerState actualState = params.handler->getState();
    QCOMPARE(actualState, params.expectedEndState);

    const auto errors = params.handler->getErrors();
    const auto warnings = params.handler->getWarnings();

    QVERIFY(actualState != BackendHandlerState::Success || (errors.empty() && warnings.empty()));
    QVERIFY(actualState != BackendHandlerState::Warnings || errors.empty());

    if (params.expectedError != BackendHandlerError::___INVALID)
    {
        QVERIFY2(const auto check = checkBackendExpectedError(errors, params.expectedError),
                 check.errorString.toLatin1());
    }

    qDebug()<<params.handler->clearLog();
    if (params.itemChecker)
    {
        ItemMap<QFileInfo> map;
        QDirIterator it(tempDir.path(),
                        QDir::AllDirs | QDir::Files | QDir::NoDotAndDotDot,
                        QDirIterator::Subdirectories);

        const int tempDirPathLength = tempDir.path().length() + 1;

        while(it.hasNext())
        {
            // Use archive's item paths as key
            const QString cleanedPath = it.next().remove(0, tempDirPathLength);
            map[cleanedPath] = QFileInfo(it.filePath());
        }

        params.itemChecker(map);
    }
}

}
