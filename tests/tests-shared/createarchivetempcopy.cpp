// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QStorageInfo>
#include <QDebug>

#include "createarchivetempcopy.h"


namespace Packuru::Tests::Shared
{

ArchiveTempCopyResult createArchiveTempCopy(const QFileInfo& archiveInfo)
{
    if (!archiveInfo.exists())
    {
        qDebug() << "Source archive does not exist:" << archiveInfo.absoluteFilePath();
        return ArchiveTempCopyResult();
    }

    auto tempDir = std::make_unique<QTemporaryDir>();

    if (!tempDir->isValid())
    {
        qDebug() << "Cannot create temporary directory:" << tempDir->path();
        return ArchiveTempCopyResult();
    }

    const QStorageInfo diskInfo(tempDir->path());

    if (diskInfo.isReadOnly())
    {
        qDebug() << "No write permission for directory " + tempDir->path();
        return ArchiveTempCopyResult();
    }

    if (diskInfo.bytesAvailable() < archiveInfo.size())
    {
        qDebug() << "Insufficient disk space: " + tempDir->path();
        return ArchiveTempCopyResult();
    }

    const QString tempArchivePath = tempDir->path() + QLatin1Char('/') + archiveInfo.fileName();

    if (!QFile::copy(archiveInfo.absoluteFilePath(), tempArchivePath))
    {
        qDebug() << "Cannot copy archive:" << archiveInfo.absoluteFilePath();
        return ArchiveTempCopyResult();
    }

    ArchiveTempCopyResult result;
    result.success = true;
    result.tempDir = std::move(tempDir);
    result.tempArchiveInfo = tempArchivePath;

    return result;
}


}
