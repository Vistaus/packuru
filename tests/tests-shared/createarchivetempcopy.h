// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QFileInfo>
#include <QTemporaryDir>

#include "symbol_export.h"


namespace Packuru::Tests::Shared
{

struct ArchiveTempCopyResult
{
    explicit operator bool() { return success; }

    bool success = false;
    std::unique_ptr<QTemporaryDir> tempDir;
    QFileInfo tempArchiveInfo;
};


PACKURU_TESTS_SHARED_EXPORT ArchiveTempCopyResult createArchiveTempCopy(const QFileInfo& archiveInfo);

}
