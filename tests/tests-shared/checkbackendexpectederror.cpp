// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../src/core/private/plugin-api/backendhandlererror.h"

#include "checkbackendexpectederror.h"


namespace Packuru::Tests::Shared
{

using namespace Packuru::Core;
BackendExpectedErrorCheck checkBackendExpectedError(const std::unordered_set<BackendHandlerError>& errors,
                                                    BackendHandlerError expectedError)
{
    BackendExpectedErrorCheck check;

    check.success = errors.find(expectedError) != errors.end();

    if (!check)
    {
        check.errorString = QString("Expected error %1 not encountered. Errors(")
                .arg(toLatin1(expectedError));

        check.errorString.append(QString::number(errors.size()) + QLatin1String("):"));

        for (const auto & err : errors)
            check.errorString.append(QLatin1String(" ") + toString(err));
    }

    return check;
}

}
