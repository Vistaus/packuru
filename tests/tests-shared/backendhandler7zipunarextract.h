// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QObject>

#include "clibackendhandlerfactory.h"
#include "symbol_export.h"


namespace Packuru::Tests::Shared
{

// Common archive extraction tests for 7-Zip and The Unarchiver
class PACKURU_TESTS_SHARED_EXPORT BackendHandler7ZipUnarExtract : public QObject
{
    Q_OBJECT
public:
    explicit BackendHandler7ZipUnarExtract(const CLIBackendHandlerFactory& factory,
                                           QObject *parent = nullptr);

    ~BackendHandler7ZipUnarExtract();

private slots:
    void initTestCase();

    void extractEntireArchive_data();
    void extractEntireArchive();

    void extractSelectedFiles_data();
    void extractSelectedFiles();

    void extractSelectedFolder_data();
    void extractSelectedFolder();

    void extractError_data();
    void extractError();

    void extractEncrypted_data();
    void extractEncrypted();

    void extractEncryptedError_data();
    void extractEncryptedError();

    void extractEncryptedAbort_data();
    void extractEncryptedAbort();

private:
    CLIBackendHandlerFactory bhFactory;
};

}
