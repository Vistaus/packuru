// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <unordered_map>

#include <QString>
#include <QMetaType>

#include "src/utils/private/qttypehasher.h"


namespace Packuru::Tests::Shared
{

// ItemMap and ItemChecker are used to inspect archive entries (ArchiveEntry)
// after reading or extracted files (QFileInfo) after extraction

template <typename ItemType>
using ItemMap = std::unordered_map<QString, ItemType, Packuru::Utils::QtTypeHasher<QString>>;

template <typename ItemType>
using ItemChecker = std::function<void (const ItemMap<ItemType>& items)>;

template <typename ItemType>
const ItemType* getItem(const  ItemMap<ItemType>& map, const QString& path)
{
    const auto it = map.find(path);

    if (it == map.end())
        return nullptr;

    return &it->second;
}

}
