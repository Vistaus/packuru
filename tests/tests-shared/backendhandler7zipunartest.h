// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QObject>

#include "clibackendhandlerfactory.h"
#include "symbol_export.h"


namespace Packuru::Tests::Shared
{

// Common archive test tests for 7-Zip and The Unarchiver
class PACKURU_TESTS_SHARED_EXPORT BackendHandler7ZipUnarTest : public QObject
{
    Q_OBJECT
public:
    explicit BackendHandler7ZipUnarTest(const CLIBackendHandlerFactory& factory,
                                        QObject *parent = nullptr);

private slots:
    void initTestCase();

    void testArchive_data();
    void testArchive();

    void testError_data();
    void testError();

    void testEncrypted_data();
    void testEncrypted();

    void testEncryptedError_data();
    void testEncryptedError();

    void testEncryptedAbort_data();
    void testEncryptedAbort();

private:
    CLIBackendHandlerFactory bhFactory;
};

}
