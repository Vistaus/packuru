# SPDX-FileCopyrightText: The Packuru Developers
#
# SPDX-License-Identifier: 0BSD

TEMPLATE = subdirs

SUBDIRS += \
    core \
    plugin-7zip \
    plugin-gnutar \
    tests-shared \
    plugin-unarchiver \
    plugin-zpaq

core.depends = tests-shared
plugin-7zip.depends = tests-shared
plugin-gnutar.depends = tests-shared
plugin-unarchiver.depends = tests-shared
plugin-zpaq.depends = tests-shared
