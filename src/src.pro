# SPDX-FileCopyrightText: The Packuru Developers
#
# SPDX-License-Identifier: 0BSD

TEMPLATE = subdirs

SUBDIRS += \
    core \
    core-browser \
    core-queue \
    desktop-core \
    desktop-browser \
    desktop-queue \
    mobile-core \
    mobile-browser \
    mobile-queue \
    plugin-7zip \
    plugin-7zip-core \
    plugin-gnutar \
    plugin-gnutar-core \
    plugin-unarchiver \
    plugin-unarchiver-core \
    plugin-zpaq \
    plugin-zpaq-core \
    qcs-core \
    qcs-qqcontrols \
    qcs-qtwidgets \
    utils

core.depends = utils qcs-core
core-browser.depends = core
core-queue.depends = core

desktop-core.depends = core
desktop-browser.depends = desktop-core core-browser qcs-qtwidgets
desktop-queue.depends = desktop-core core-queue qcs-qtwidgets

mobile-core.depends = core
mobile-browser.depends = mobile-core core-browser qcs-qqcontrols
mobile-queue.depends = mobile-core core-queue qcs-qqcontrols

plugin-7zip-core.depends = core qcs-core
plugin-7zip.depends = plugin-7zip-core core qcs-core

plugin-gnutar-core.depends = core qcs-core
plugin-gnutar.depends = plugin-gnutar-core core qcs-core

plugin-unarchiver-core.depends = core qcs-core
plugin-unarchiver.depends = plugin-unarchiver-core core qcs-core

plugin-zpaq-core.depends = core qcs-core
plugin-zpaq.depends = plugin-zpaq-core core qcs-core

qcs-core.depends = utils
qcs-qqcontrols.depends = qcs-core
qcs-qtwidgets.depends = qcs-core
