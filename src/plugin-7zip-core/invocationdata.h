// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <vector>

#include <QString>


namespace Packuru::Plugins::_7Zip::Core
{

struct InvocationData
{
    QString command;
    std::vector<QString> switches;
    QString archiveFilePath;
    std::vector<QString> items;
    QString password;
};

}
