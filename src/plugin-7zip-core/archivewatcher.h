// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QObject>


namespace Packuru::Plugins::_7Zip::Core
{

/* ArchiveWatcher checks whether an archive was created and then deleted,
 * which happens when 7-Zip detects full disk (I don't know any other possible
 * cause). ArchiveWatcher sometimes misses an event in which case DiskSpaceWatcher can
 * detect full disk or at least low disk space. Finally ArchiveWatcher cannot monitor a
 * temporary archive which 7-Zip creates when adding or deleting files. This would
 * require precise event detection (file moved vs file deleted) by platform-native
 * tools e.g. inotify (QFileSystemWatcher doesn't provide event types). So when
 * adding/deleting files ArchiveWatcher monitors all events in the archive's parent
 * directory and checks disk space when event occured.
 */
class ArchiveWatcher : public QObject
{
    Q_OBJECT
public:
    enum class OperationMode { WatchArchive, WatchParentDirectory };

    explicit ArchiveWatcher(const QString& archivePath,
                            OperationMode mode,
                            QObject *parent = nullptr);
    ~ArchiveWatcher();

    void start();
    void stop();

    bool fullDiskDetected() const;

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}
