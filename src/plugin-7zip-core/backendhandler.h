// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QProcess>
#include <QFileInfo>

#include "../utils/boolflag.h"
#include "../utils/private/qstringunorderedset.h"

#include "../core/private/plugin-api/clibackendhandler.h"


namespace Packuru::Plugins::_7Zip::Core
{

class InvocationData;
class ArchiveWatcher;


class BackendHandler : public Packuru::Core::CLIBackendHandler
{
public:
    BackendHandler(QObject* parent = nullptr);

    static bool executableAvailable();

    QString createSinglePartArchiveName(const QString& archiveBaseName, Packuru::Core::ArchiveType archiveType) const override;
    QString createMultiPartArchiveName(const QString& archiveBaseName, Packuru::Core::ArchiveType archiveType) const override;
    QString createMultiPartArchiveRegexNamePattern(const QString& archiveBaseName, Packuru::Core::ArchiveType archiveType) const override;

private:
    void readArchiveImpl(const Packuru::Core::BackendReadData& data) override;
    void extractArchiveImpl(const Packuru::Core::BackendExtractionData& data) override;
    void addToArchiveImpl(const Packuru::Core::BackendArchivingData& data) override;
    void deleteFilesImpl(const Packuru::Core::BackendDeletionData& data) override;
    void testArchiveImpl(const Packuru::Core::BackendTestData& data) override;

    void startBackend(const InvocationData& data);
    QStringList composeCommand(const InvocationData& data, bool includePassword) const;
    void passwordPromptHandler(const QString& line);
    void progressLineHandler(QString& line, int newDataPosition);
    void errorStreamErrorLineHandler(const std::vector<QStringRef>& lines);
    void outputStreamErrorLineHandler(const std::vector<QStringRef>& lines);
    void checkStdOutLineForErrors(QStringRef line);
    void listInitHandler(const std::vector<QStringRef>& lines);
    void listHeaderHandler(const std::vector<QStringRef>& lines, std::size_t firstLine);
    void listEntriesHandler(const std::vector<QStringRef>& lines, std::size_t firstLine);

    void onProcessFinished(int exitCode, QProcess::ExitStatus exitStatus);
    void onListProcessFinished(int exitCode, QProcess::ExitStatus exitStatus);

    QFileInfo archiveInfo;
    /*
     + adding
     - removing/extracting
     = copying files to temp archive
     T testing
     U updating
     R ???
     . ???
     */
    const QString progressMarkers = "+-=TUR.";
    Packuru::Core::ArchiveItem tempItem;
    Packuru::Utils::QStringUnorderedSet methodsSet;
    Packuru::Core::ArchiveType archiveType;
    Packuru::Utils::BoolFlag<false> encryptedFiles;
    QString readPassword;
    bool splitType = false;
    ArchiveWatcher* archiveWatcher = nullptr;
};

}
