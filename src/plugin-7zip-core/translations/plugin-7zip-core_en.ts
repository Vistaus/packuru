<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>ArchivingCompressionLevel</name>
    <message>
        <location filename="../archiving/archivingcompressionlevel.cpp" line="18"/>
        <source>No compression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archiving/archivingcompressionlevel.cpp" line="20"/>
        <source>Fastest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archiving/archivingcompressionlevel.cpp" line="22"/>
        <source>Fast</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archiving/archivingcompressionlevel.cpp" line="24"/>
        <source>Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archiving/archivingcompressionlevel.cpp" line="26"/>
        <source>Maximum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archiving/archivingcompressionlevel.cpp" line="28"/>
        <source>Ultra</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ArchivingSolidBlockSize</name>
    <message>
        <location filename="../archiving/archivingsolidblocksize.cpp" line="26"/>
        <source>Non solid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archiving/archivingsolidblocksize.cpp" line="28"/>
        <source>Solid</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ArchivingUpdateMode</name>
    <message>
        <location filename="../archiving/archivingupdatemode.cpp" line="18"/>
        <source>Create new archive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archiving/archivingupdatemode.cpp" line="20"/>
        <source>Add absent files and replace existing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archiving/archivingupdatemode.cpp" line="22"/>
        <source>Add absent files and update existing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archiving/archivingupdatemode.cpp" line="24"/>
        <source>Add absent files and skip existing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archiving/archivingupdatemode.cpp" line="26"/>
        <source>Update existing files only</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Packuru::Plugins::_7Zip::Core::ArchivingParametersHandler</name>
    <message>
        <location filename="../archiving/archivingparametershandler.cpp" line="96"/>
        <source>Compression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archiving/archivingparametershandler.cpp" line="100"/>
        <source>Level</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archiving/archivingparametershandler.cpp" line="105"/>
        <source>Method</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archiving/archivingparametershandler.cpp" line="110"/>
        <source>Dictionary size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archiving/archivingparametershandler.cpp" line="115"/>
        <source>Word size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archiving/archivingparametershandler.cpp" line="120"/>
        <source>Solid block size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archiving/archivingparametershandler.cpp" line="125"/>
        <source>Thread count</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archiving/archivingparametershandler.cpp" line="139"/>
        <source>Update mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archiving/archivingparametershandler.cpp" line="145"/>
        <source>Encryption method</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
