// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../core/private/datastream/unordered_map_datastream.h"

#include "registerstreamoperators.h"
#include "archiving/privatearchivingparameter.h"
#include "archiving/privatearchivingparametermap.h"
#include "archiving/archivingupdatemode.h"
#include "archiving/archivingsolidblocksize.h"
#include "archiving/archivingcompressionmethod.h"
#include "archiving/archivingcompressionlevel.h"
#include "archiving/archivingencryptionmethod.h"


using Packuru::Core::operator<<;
using Packuru::Core::operator>>;


namespace Packuru::Plugins::_7Zip::Core
{

void registerStreamOperators()
{
    qRegisterMetaTypeStreamOperators<PrivateArchivingParameter>("PrivateArchivingParameter7Zip");
    qRegisterMetaTypeStreamOperators<PrivateArchivingParameterMap>("PrivateArchivingParameterMap7Zip");
    qRegisterMetaTypeStreamOperators<ArchivingUpdateMode>("ArchivingUpdateMode");
    qRegisterMetaTypeStreamOperators<ArchivingSolidBlockSize>("ArchivingSolidBlockSize");
    qRegisterMetaTypeStreamOperators<ArchivingCompressionMethod>("ArchivingCompressionMethod");
    qRegisterMetaTypeStreamOperators<ArchivingCompressionLevel>("ArchivingCompressionLevel");
    qRegisterMetaTypeStreamOperators<ArchivingEncryptionMethod>("ArchivingEncryptionMethod");
}

}
