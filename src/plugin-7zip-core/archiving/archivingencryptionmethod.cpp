// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QString>

#include "archivingencryptionmethod.h"


namespace Packuru::Plugins::_7Zip::Core
{

QString toString(ArchivingEncryptionMethod value)
{
    switch (value)
    {
    case ArchivingEncryptionMethod::AES_128: return "AES-128";
    case ArchivingEncryptionMethod::AES_256: return  "AES-256";
    case ArchivingEncryptionMethod::ZipCrypto: return  "ZipCrypto";
    default: Q_ASSERT(false); return QString();
    };
}

}
