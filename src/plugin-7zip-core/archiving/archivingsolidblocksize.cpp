// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QCoreApplication>

#include "../utils/private/tounderlyingtype.h"

#include "../core/private/digitalsize.h"

#include "archivingsolidblocksize.h"


using namespace Packuru::Core;
using namespace Packuru::Utils;


namespace Packuru::Plugins::_7Zip::Core
{

QString toString(ArchivingSolidBlockSize value)
{
    switch (value)
    {
    case ArchivingSolidBlockSize::NonSolid:
        return QCoreApplication::translate("ArchivingSolidBlockSize", "Non solid");
    case ArchivingSolidBlockSize::Solid:
        return QCoreApplication::translate("ArchivingSolidBlockSize", "Solid");
    case ArchivingSolidBlockSize::MB1:
    case ArchivingSolidBlockSize::MB2:
    case ArchivingSolidBlockSize::MB4:
    case ArchivingSolidBlockSize::MB8:
    case ArchivingSolidBlockSize::MB16:
    case ArchivingSolidBlockSize::MB32:
    case ArchivingSolidBlockSize::MB64:
    case ArchivingSolidBlockSize::MB128:
    case ArchivingSolidBlockSize::MB256:
    case ArchivingSolidBlockSize::MB512:
    case ArchivingSolidBlockSize::GB1:
    case ArchivingSolidBlockSize::GB2:
    case ArchivingSolidBlockSize::GB4:
    case ArchivingSolidBlockSize::GB8:
    case ArchivingSolidBlockSize::GB16:
    case ArchivingSolidBlockSize::GB32:
    case ArchivingSolidBlockSize::GB64:
        return DigitalSize::toString(toUnderlyingType(value), SizeUnits::Decimal);
    default:
        Q_ASSERT(false); return QString();
    }
}

}
