// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QMetaType>


namespace Packuru::Plugins::_7Zip::Core
{

enum class ArchivingUpdateMode
{
    NewArchive,
    AddAndReplace,
    AddAndUpdate,
    AddAndSkip,
    UpdateExisting
};

QString toString(ArchivingUpdateMode value);

}

Q_DECLARE_METATYPE(Packuru::Plugins::_7Zip::Core::ArchivingUpdateMode);


