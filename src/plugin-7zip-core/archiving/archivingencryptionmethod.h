// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QMetaType>


namespace Packuru::Plugins::_7Zip::Core
{

// Used only for zip format, 7z uses AES-256
enum class ArchivingEncryptionMethod
{
    AES_128,
    AES_256,
    ZipCrypto,

    ___INVALID
};

QString toString(ArchivingEncryptionMethod value);

}

Q_DECLARE_METATYPE(Packuru::Plugins::_7Zip::Core::ArchivingEncryptionMethod);


