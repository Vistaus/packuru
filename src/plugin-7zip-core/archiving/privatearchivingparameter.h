// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QMetaType>


namespace Packuru::Plugins::_7Zip::Core
{

enum class PrivateArchivingParameter
{
    CompressionLevel, // ArchivingCompressionLevel; required
    CompressionMethod, // ArchivingCompressionMethod; required when compressing
    DictionarySize, // qulonglong; required when compressing
    EncryptionMethod, // ArchivingEncryptionMethod; required for zip only
    UpdateMode, // ArchivingUpdateMode; required
    SolidBlockSize, // ArchivingSolidBlockSize; required when compressing to 7z
    ThreadCount, // int; required when compressing
    WordSize // int; required when compressing (except bzip2 method)
};

}

Q_DECLARE_METATYPE(Packuru::Plugins::_7Zip::Core::PrivateArchivingParameter);
