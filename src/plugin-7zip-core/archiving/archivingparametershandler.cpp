// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../qcs-core/controller.h"
#include "../qcs-core/controllerengine.h"
#include "../qcs-core/controlleridrowmapper.h"

#include "../core/private/plugin-api/encryptionstate.h"
#include "../core/private/plugin-api/archivingcompressioninfo.h"
#include "../core/private/plugin-api/encryptionsupport.h"
#include "../core/private/plugin-api/archivingfilepaths.h"
#include "../core/private/plugin-api/archivetype.h"
#include "../core/private/privatestrings.h"

#include "controllers/encryptionmethodctrl.h"
#include "controllers/compressionlevelctrl.h"
#include "controllers/compressionmethodctrl.h"
#include "controllers/dictionarysizectrl.h"
#include "controllers/solidblocksizectrl.h"
#include "controllers/threadcountctrl.h"
#include "controllers/wordsizectrl.h"
#include "controllers/archivingupdatemodectrl.h"
#include "privatearchivingparameter.h"
#include "privatearchivingparametermap.h"
#include "archivingparametershandler.h"


using namespace qcs::core;
using namespace Packuru::Core;


namespace Packuru::Plugins::_7Zip::Core
{

ArchivingParametersHandler::ArchivingParametersHandler(const ArchivingParameterHandlerInitData& initData,
                                                       QObject *parent)
    : ArchivingParameterHandlerBase(initData, parent)
{

    const ArchiveType archiveType = initData.archiveType;

    compressionLevelCtrl
            = controllerEngine->createTopController<CompressionLevelCtrl>(PrivateArchivingParameter
                                                                          ::CompressionLevel);

    auto compresionMethodCtrl
            = controllerEngine->createController<CompressionMethodCtrl>(PrivateArchivingParameter
                                                                        ::CompressionMethod,
                                                                        archiveType);
    auto solidBlockSizeCtrl
            = controllerEngine->createController<SolidBlockSizeCtrl>(PrivateArchivingParameter
                                                                     ::SolidBlockSize,
                                                                     archiveType);
    auto dictionarySizeCtrl
            = controllerEngine->createController<DictionarySizeCtrl>(PrivateArchivingParameter
                                                                     ::DictionarySize,
                                                                     archiveType);
    auto wordSizeCtrl
            = controllerEngine->createController<WordSizeCtrl>(PrivateArchivingParameter
                                                               ::WordSize,
                                                               archiveType);

    auto threadCountCtrl
            = controllerEngine->createController<ThreadCountCtrl>(PrivateArchivingParameter
                                                                  ::ThreadCount);

    controllerEngine->createTopController<EncryptionMethodCtrl>(PrivateArchivingParameter
                                                                ::EncryptionMethod,
                                                                archiveType);

    controllerEngine->createTopController<ArchivingUpdateModeCtrl>(PrivateArchivingParameter
                                                                   ::UpdateMode);

    compressionLevelCtrl->addDependant(compresionMethodCtrl);
    compresionMethodCtrl->addDependant(solidBlockSizeCtrl);
    compresionMethodCtrl->addDependant(dictionarySizeCtrl);
    compresionMethodCtrl->addDependant(wordSizeCtrl);
    compresionMethodCtrl->addDependant(threadCountCtrl);

    controllerEngine->updateAllControllers();

    connect(controllerEngine, &ControllerEngine::sigControllerSyncedToWidget,
            this, [publ = this] (auto id)
    {
        const auto controllerId = static_cast<PrivateArchivingParameter>(id);

        if (controllerId == PrivateArchivingParameter::CompressionLevel)
            emit publ->sigCompressionInfoChanged();
        else if (controllerId == PrivateArchivingParameter::EncryptionMethod)
            emit publ->sigEncryptionMethodChanged(publ->getEncryptionMethod());
    });

    controllerRowMapper
            ->addHeader(Packuru::Plugins::_7Zip::Core::ArchivingParametersHandler
                        ::tr("Compression"));

    controllerRowMapper
            ->addController(Packuru::Plugins::_7Zip::Core::ArchivingParametersHandler
                            ::tr("Level"),
                            PrivateArchivingParameter::CompressionLevel);

    controllerRowMapper
            ->addController(Packuru::Plugins::_7Zip::Core::ArchivingParametersHandler
                            ::tr("Method"),
                            PrivateArchivingParameter::CompressionMethod);

    controllerRowMapper
            ->addController(Packuru::Plugins::_7Zip::Core::ArchivingParametersHandler
                            ::tr("Dictionary size"),
                            PrivateArchivingParameter::DictionarySize);

    controllerRowMapper
            ->addController(Packuru::Plugins::_7Zip::Core::ArchivingParametersHandler
                            ::tr("Word size"),
                            PrivateArchivingParameter::WordSize);

    if(initData.archiveType == ArchiveType::_7z)
        controllerRowMapper->addController(Packuru::Plugins::_7Zip::Core::ArchivingParametersHandler
                                           ::tr("Solid block size"),
                                           PrivateArchivingParameter::SolidBlockSize);

    controllerRowMapper
            ->addController(Packuru::Plugins::_7Zip::Core::ArchivingParametersHandler
                            ::tr("Thread count"),
                            PrivateArchivingParameter::ThreadCount);

    const bool miscOptions = (initData.creatingNewArchive == false
                              || initData.currentArchiveEncryption != EncryptionState::EntireArchive);

    if (miscOptions)
    {
        controllerRowMapper
                ->addHeader(PrivateStrings::getString(PrivateStrings::UserString::Miscellaneous));

        if (!initData.creatingNewArchive)
            controllerRowMapper
                    ->addController(Packuru::Plugins::_7Zip::Core::ArchivingParametersHandler
                                    ::tr("Update mode"),
                                    PrivateArchivingParameter::UpdateMode);

        if (initData.currentArchiveEncryption != EncryptionState::EntireArchive)
            controllerRowMapper
                    ->addController(Packuru::Plugins::_7Zip::Core::ArchivingParametersHandler
                                    ::tr("Encryption method"),
                                    PrivateArchivingParameter::EncryptionMethod);
    }
}


ArchivingParametersHandler::~ArchivingParametersHandler()
{

}


bool ArchivingParametersHandler::archiveSplittingSupport() const
{
    switch (initData.archiveType)
    {
    case ArchiveType::_7z:
    case ArchiveType::zip:
        return true;
    default:
        Q_ASSERT(false); return false;
    }
}


ArchivingCompressionInfo ArchivingParametersHandler::getCompressionInfo() const
{
    const auto variant = controllerEngine
            ->getControllerProperty(PrivateArchivingParameter::CompressionLevel,
                                    ControllerProperty::PrivateCurrentValue);

    const auto level = Utils::getValueAs<ArchivingCompressionLevel>(variant);


    return { static_cast<int>(ArchivingCompressionLevel::NoCompression),
                static_cast<int>(ArchivingCompressionLevel::Ultra),
                static_cast<int>(level) };
}


EncryptionSupport ArchivingParametersHandler::getEncryptionSupport() const
{
    const auto type = initData.archiveType;
    if (type == ArchiveType::_7z)
        return EncryptionSupport::FilesContentAndEntireArchive;
    else if (type == ArchiveType::zip)
        return EncryptionSupport::FilesContentOnly;
    else
    {
        Q_ASSERT(false);
        return EncryptionSupport::NoSupport;
    }
}


QString ArchivingParametersHandler::getEncryptionMethod() const
{
    return controllerEngine
            ->getControllerPropertyAs<QString>(PrivateArchivingParameter::EncryptionMethod,
                                               ControllerProperty::PublicCurrentValue);
}


void ArchivingParametersHandler::setCompressionLevel(int value)
{
    Q_ASSERT(value >= static_cast<int>(ArchivingCompressionLevel::NoCompression));
    Q_ASSERT(value < static_cast<int>(ArchivingCompressionLevel::___COUNT));

    const auto level = static_cast<ArchivingCompressionLevel>(value);
    compressionLevelCtrl->externalLevelInput(level);
}


QVariant ArchivingParametersHandler::getPrivateArchivingData() const
{
    const auto values = controllerEngine->getAvailableCurrentValues<PrivateArchivingParameter>();
    QVariant result;
    result.setValue(values);
    return result;
}


std::unordered_set<ArchivingFilePaths> ArchivingParametersHandler::getAdditionalSupportedFilePaths() const
{
    return {ArchivingFilePaths::AbsolutePaths, ArchivingFilePaths::FullPaths};
}

}
