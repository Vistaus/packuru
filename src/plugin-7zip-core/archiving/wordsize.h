// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QtGlobal>


namespace Packuru::Plugins::_7Zip::Core
{

enum class WordSizeDeflate
{
    _8 = 8,
    _12 = 12,
    _16 = 16,
    _24 = 24,
    _32 = 32,
    _48 = 48,
    _64 = 64,
    _96 = 96,
    _128 = 128,
    _192 = 192,
    _256 = 256,
    _258 = 258,
};


enum class WordSizeDeflate64
{
    _8 = 8,
    _12 = 12,
    _16 = 16,
    _24 = 24,
    _32 = 32,
    _48 = 48,
    _64 = 64,
    _96 = 96,
    _128 = 128,
    _192 = 192,
    _256 = 256,
    _257 = 257,
};


enum class WordSizeLZMA
{
    _2 = 2,
    _3 = 3,
    _4 = 4,
    _5 = 5,
    _6 = 6,
    _7 = 7,
    _8 = 8,
    _10 = 10,
    _12 = 12,
    _14 = 14,
    _16 = 16,
    _20 = 20,
    _24 = 24,
    _28 = 28,
    _32 = 32,
    _48 = 48,
    _64 = 64,
    _96 = 96,
    _128 = 128,
    _192 = 192,
    _256 = 256,
    _273 = 273,
};


enum class WordSizePPMd7z
{
    _2 = 2,
    _3 = 3,
    _4 = 4,
    _5 = 5,
    _6 = 6,
    _7 = 7,
    _8 = 8,
    _10 = 10,
    _12 = 12,
    _14 = 14,
    _16 = 16,
    _20 = 20,
    _24 = 24,
    _28 = 28,
    _32 = 32,
};


enum class WordSizePPMdZip
{
    _2 = 2,
    _3 = 3,
    _4 = 4,
    _5 = 5,
    _6 = 6,
    _7 = 7,
    _8 = 8,
    _9 = 9,
    _10 = 10,
    _11 = 11,
    _12 = 12,
    _13 = 13,
    _14 = 14,
    _15 = 15,
    _16 = 16,
};

}
