// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QLatin1String>


namespace Packuru::Core
{
enum class ArchiveType;
enum class ArchivingFilePaths;
enum class EncryptionState;
}

namespace Packuru::Plugins::_7Zip::Core
{

enum class ArchivingCompressionLevel;
enum class ArchivingCompressionMethod;
enum class ArchivingEncryptionMethod;
enum class ArchivingSolidBlockSize : qulonglong;
enum class ArchivingUpdateMode;

QString archivingOptionToCLIarg(Packuru::Core::ArchiveType value);

QString archivingOptionToCLIarg(Packuru::Core::ArchivingFilePaths value);

QString archivingOptionToCLIarg(Packuru::Core::EncryptionState value);

QString archivingOptionToCLIarg(ArchivingUpdateMode value);

QString archivingOptionToCLIarg(ArchivingCompressionLevel value);

QString archivingOptionToCLIarg(ArchivingSolidBlockSize value);

QString threadCountToCLIarg(int count);

QString archivingOptionToCLIarg(ArchivingEncryptionMethod value);

std::vector<QString> archivingOptionToCLIarg(Packuru::Core::ArchiveType archiveType,
                                             ArchivingCompressionMethod method,
                                             qulonglong dictionarySize,
                                             int wordSize);

QString archivingOptionToCLIarg(qulonglong partSize);

}
