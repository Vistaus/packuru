// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QCoreApplication>

#include "archivingcompressionlevel.h"


namespace Packuru::Plugins::_7Zip::Core
{

QString toString(ArchivingCompressionLevel value)
{
    switch (value)
    {
    case ArchivingCompressionLevel::NoCompression:
        return QCoreApplication::translate("ArchivingCompressionLevel", "No compression");
    case ArchivingCompressionLevel::Fastest:
        return QCoreApplication::translate("ArchivingCompressionLevel", "Fastest");
    case ArchivingCompressionLevel::Fast:
        return QCoreApplication::translate("ArchivingCompressionLevel", "Fast");
    case ArchivingCompressionLevel::Normal:
        return QCoreApplication::translate("ArchivingCompressionLevel", "Normal");
    case ArchivingCompressionLevel::Maximum:
        return QCoreApplication::translate("ArchivingCompressionLevel", "Maximum");
    case ArchivingCompressionLevel::Ultra:
        return QCoreApplication::translate("ArchivingCompressionLevel", "Ultra");
    default:
        Q_ASSERT(false); return QString();
    };
}

}
