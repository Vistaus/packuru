// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QMetaType>


namespace Packuru::Plugins::_7Zip::Core
{

enum class ArchivingSolidBlockSize : qulonglong
{
    NonSolid,
    Solid,
    MB1 = 1'000'000,
    MB2 = MB1 * 2,
    MB4 = MB1 * 4,
    MB8 = MB1 * 8,
    MB16 = MB1 * 16,
    MB32 = MB1 * 32,
    MB64 = MB1 * 64,
    MB128 = MB1 * 128,
    MB256 = MB1 * 256,
    MB512 = MB1 * 512,
    GB1 = MB1 * 1000,
    GB2 = GB1 * 2,
    GB4 = GB1 * 4,
    GB8 = GB1 * 8,
    GB16 = GB1 * 16,
    GB32 = GB1 * 32,
    GB64 = GB1 * 64,
};

QString toString(ArchivingSolidBlockSize value);

}

Q_DECLARE_METATYPE(Packuru::Plugins::_7Zip::Core::ArchivingSolidBlockSize);



