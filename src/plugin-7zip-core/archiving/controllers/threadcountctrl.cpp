// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QThread>

#include "core/private/plugin-api/archivetype.h"
#include "core/private/plugin-api/generatethreadcountlist.h"

#include "../archivingcompressionlevel.h"
#include "../archivingcompressionmethod.h"
#include "../privatearchivingparameter.h"
#include "threadcountctrl.h"


using Packuru::Core::ArchiveType;
using namespace qcs::core;


namespace Packuru::Plugins::_7Zip::Core
{

ThreadCountCtrl::ThreadCountCtrl()
{
    setControllerType(ControllerType::Type::ComboBox);

    // Don't use all threads by default as the system can become unusable
    userValue = QThread::idealThreadCount() / 2;
}


void ThreadCountCtrl::update()
{
    const auto level
            = getControllerPropertyAs<ArchivingCompressionLevel>(PrivateArchivingParameter::CompressionLevel,
                                                                 ControllerProperty::PrivateCurrentValue);

    if (level == ArchivingCompressionLevel::NoCompression)
    {
        setEnabled(false);
        setValueLists({}, {});
        clearCurrentValue();
        return;
    }

    const auto method = getControllerPropertyAs<ArchivingCompressionMethod>(PrivateArchivingParameter::CompressionMethod,
                                                                          ControllerProperty::PrivateCurrentValue);

    const int minThreadCount = 1;
    int maxThreadCount = 1;

    switch (method)
    {
    case ArchivingCompressionMethod::BZip2:
    case ArchivingCompressionMethod::LZMA2:
        maxThreadCount = QThread::idealThreadCount();
        break;
    case ArchivingCompressionMethod::LZMA:
        maxThreadCount = 2;
        break;
    default:
        break;
    }

    PrivateValueList privateList = Packuru::Core::generateThreadCountList(maxThreadCount);
    PublicValueList publicList;
    publicList.reserve(privateList.size());

    for (int value : privateList)
        publicList.push_back(QString::number(value));

    setValueLists(publicList, privateList);

    int setValue = userValue;

    // When user selects higher thread count in one method and than switches to another
    // with lower max thread count.
    if (setValue > maxThreadCount)
        setValue = maxThreadCount;

    setCurrentValue(QString::number(setValue), setValue);

    setEnabled(maxThreadCount - minThreadCount > 0);
}


void ThreadCountCtrl::onValueSyncedToWidget(const QVariant& value)
{
    Q_UNUSED(value)
    userValue = getPropertyAs<int>(ControllerProperty::PrivateCurrentValue);
}

}
