// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "../qcs-core/controller.h"

#include "../archivingcompressionmethod.h"


namespace Packuru::Core
{
enum class ArchiveType;
}

namespace Packuru::Plugins::_7Zip::Core
{

class CompressionMethodCtrl : public qcs::core::Controller<QString, ArchivingCompressionMethod>
{
public:
    CompressionMethodCtrl(Packuru::Core::ArchiveType archiveType);

    void update() override;

protected:
    void onValueSyncedToWidget(const QVariant& value) override;

private:
    PublicValueList publicList;
    PrivateValueList privateList;
    Packuru::Core::ArchiveType archiveType_;
    ArchivingCompressionMethod userValue;
};

}
