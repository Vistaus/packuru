// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "utils/private/tounderlyingtype.h"

#include "core/private/plugin-api/archivetype.h"
#include "core/private/digitalsize.h"
#include "core/private/sizeunits.h"

#include "../archivingcompressionlevel.h"
#include "../archivingcompressionmethod.h"
#include "../privatearchivingparameter.h"
#include "../dictionarysize.h"
#include "dictionarysizectrl.h"


using namespace qcs::core;
using namespace Packuru::Core;
using Packuru::Utils::toUnderlyingType;


namespace
{

template<typename Enum>
std::enable_if_t<std::is_enum_v<Enum>, std::pair<QString, std::underlying_type_t<Enum>>>
createPair(Enum value)
{
    const auto uv = toUnderlyingType(value);
    return {DigitalSize::toString(uv, SizeUnits::Binary), uv};
}

}


namespace Packuru::Plugins::_7Zip::Core
{

DictionarySizeCtrl::DictionarySizeCtrl(ArchiveType archiveType)
    : archiveType_(archiveType)
{
    setControllerType(ControllerType::Type::ComboBox);

    // bzip2
    const auto listsBZip2
            = createLists({ createPair(DictionarySizeBZip2::_100KiB),
                            createPair(DictionarySizeBZip2::_200KiB),
                            createPair(DictionarySizeBZip2::_300KiB),
                            createPair(DictionarySizeBZip2::_400KiB),
                            createPair(DictionarySizeBZip2::_500KiB),
                            createPair(DictionarySizeBZip2::_600KiB),
                            createPair(DictionarySizeBZip2::_700KiB),
                            createPair(DictionarySizeBZip2::_800KiB),
                            createPair(DictionarySizeBZip2::_900KiB), });

    publicListBZip2 = listsBZip2.first;
    privateListBZip2 = listsBZip2.second;

    // lzma
    const auto listsLZMA
            = createLists({ createPair(DictionarySizeLZMA::_64KiB),
                            createPair(DictionarySizeLZMA::_1MiB),
                            createPair(DictionarySizeLZMA::_2MiB),
                            createPair(DictionarySizeLZMA::_3MiB),
                            createPair(DictionarySizeLZMA::_4MiB),
                            createPair(DictionarySizeLZMA::_6MiB),
                            createPair(DictionarySizeLZMA::_8MiB),
                            createPair(DictionarySizeLZMA::_12MiB),
                            createPair(DictionarySizeLZMA::_16MiB),
                            createPair(DictionarySizeLZMA::_24MiB),
                            createPair(DictionarySizeLZMA::_32MiB),
                            createPair(DictionarySizeLZMA::_48MiB),
                            createPair(DictionarySizeLZMA::_64MiB),
                            createPair(DictionarySizeLZMA::_96MiB),
                            createPair(DictionarySizeLZMA::_128MiB),
                            createPair(DictionarySizeLZMA::_192MiB),
                            createPair(DictionarySizeLZMA::_256MiB),
                            createPair(DictionarySizeLZMA::_384MiB),
                            createPair(DictionarySizeLZMA::_512MiB),
                            createPair(DictionarySizeLZMA::_768MiB),
                            createPair(DictionarySizeLZMA::_1024MiB),
                            createPair(DictionarySizeLZMA::_1536MiB), });

    publicListLZMABoth = listsLZMA.first;
    privateListLZMABoth = listsLZMA.second;

    // ppmd
    std::pair<QList<QString>,QList<qulonglong>> listsPPMd;

    if (archiveType_ == ArchiveType::_7z)
        listsPPMd = createLists({ createPair(DictionarySizePPMd7z::_1MiB),
                                  createPair(DictionarySizePPMd7z::_2MiB),
                                  createPair(DictionarySizePPMd7z::_3MiB),
                                  createPair(DictionarySizePPMd7z::_4MiB),
                                  createPair(DictionarySizePPMd7z::_6MiB),
                                  createPair(DictionarySizePPMd7z::_8MiB),
                                  createPair(DictionarySizePPMd7z::_12MiB),
                                  createPair(DictionarySizePPMd7z::_16MiB),
                                  createPair(DictionarySizePPMd7z::_24MiB),
                                  createPair(DictionarySizePPMd7z::_32MiB),
                                  createPair(DictionarySizePPMd7z::_48MiB),
                                  createPair(DictionarySizePPMd7z::_64MiB),
                                  createPair(DictionarySizePPMd7z::_96MiB),
                                  createPair(DictionarySizePPMd7z::_128MiB),
                                  createPair(DictionarySizePPMd7z::_192MiB),
                                  createPair(DictionarySizePPMd7z::_256MiB),
                                  createPair(DictionarySizePPMd7z::_384MiB),
                                  createPair(DictionarySizePPMd7z::_512MiB),
                                  createPair(DictionarySizePPMd7z::_768MiB),
                                  createPair(DictionarySizePPMd7z::_1024MiB), });
    else if (archiveType_ == ArchiveType::zip)
        listsPPMd = createLists({ createPair(DictionarySizePPMdZip::_1MiB),
                                  createPair(DictionarySizePPMdZip::_2MiB),
                                  createPair(DictionarySizePPMdZip::_4MiB),
                                  createPair(DictionarySizePPMdZip::_8MiB),
                                  createPair(DictionarySizePPMdZip::_16MiB),
                                  createPair(DictionarySizePPMdZip::_32MiB),
                                  createPair(DictionarySizePPMdZip::_64MiB),
                                  createPair(DictionarySizePPMdZip::_128MiB),
                                  createPair(DictionarySizePPMdZip::_256MiB), });

    publicListPPMd = listsPPMd.first;
    privateListPPMd = listsPPMd.second;
}


void DictionarySizeCtrl::update()
{
    const auto level
            = getControllerPropertyAs<ArchivingCompressionLevel>(PrivateArchivingParameter::CompressionLevel,
                                                                 ControllerProperty::PrivateCurrentValue);

    if (level == ArchivingCompressionLevel::NoCompression)
    {
        setEnabled(false);
        setValueLists({}, {});
        clearCurrentValue();
        return;
    }

    setEnabled(true);

    const auto method
            = getControllerPropertyAs<ArchivingCompressionMethod>(PrivateArchivingParameter::CompressionMethod,
                                                                  ControllerProperty::PrivateCurrentValue);

    switch (method)
    {
    case ArchivingCompressionMethod::BZip2:
        setValueLists(publicListBZip2,privateListBZip2); break;
    case ArchivingCompressionMethod::Deflate:
    {
        const auto value = toUnderlyingType(DictionarySizeDeftate::value);
        setValueLists({DigitalSize::toString(value, SizeUnits::Binary)}, {value});
        setEnabled(false);
        break;
    }
    case ArchivingCompressionMethod::Deflate64:
    {
        const auto value = toUnderlyingType(DictionarySizeDeftate64::value);
        setValueLists({DigitalSize::toString(value, SizeUnits::Binary)}, {value});
        setEnabled(false);
        break;
    }
    case ArchivingCompressionMethod::LZMA:
    case ArchivingCompressionMethod::LZMA2:
        setValueLists(publicListLZMABoth,privateListLZMABoth); break;
    case ArchivingCompressionMethod::PPMd:
        setValueLists(publicListPPMd,privateListPPMd); break;
    default:
        Q_ASSERT(false); break;
    }

    switch (method)
    {
    case ArchivingCompressionMethod::BZip2:
        switch (level)
        {
        case ArchivingCompressionLevel::Fastest:
            setCurrentValueFromPrivate(toUnderlyingType(DictionarySizeBZip2::_100KiB)); return;
        case ArchivingCompressionLevel::Fast:
            setCurrentValueFromPrivate(toUnderlyingType(DictionarySizeBZip2::_500KiB)); return;
        case ArchivingCompressionLevel::Normal:
        case ArchivingCompressionLevel::Maximum:
        case ArchivingCompressionLevel::Ultra:
            setCurrentValueFromPrivate(toUnderlyingType(DictionarySizeBZip2::_900KiB)); return;
        default:
            Q_ASSERT(false); return;
        }

    case ArchivingCompressionMethod::Deflate:
        switch (level)
        {
        case ArchivingCompressionLevel::Fastest:
        case ArchivingCompressionLevel::Fast:
        case ArchivingCompressionLevel::Normal:
        case ArchivingCompressionLevel::Maximum:
        case ArchivingCompressionLevel::Ultra:
            setCurrentValueFromPrivate(toUnderlyingType(DictionarySizeDeftate::value)); return;
        default:
            Q_ASSERT(false); return;
        }

    case ArchivingCompressionMethod::Deflate64:
        switch (level)
        {
        case ArchivingCompressionLevel::Fastest:
        case ArchivingCompressionLevel::Fast:
        case ArchivingCompressionLevel::Normal:
        case ArchivingCompressionLevel::Maximum:
        case ArchivingCompressionLevel::Ultra:
            setCurrentValueFromPrivate(toUnderlyingType(DictionarySizeDeftate64::value)); return;
        default:
            Q_ASSERT(false); return;
        }

    case ArchivingCompressionMethod::LZMA:
    case ArchivingCompressionMethod::LZMA2:
        switch (level)
        {
        case ArchivingCompressionLevel::Fastest:
            setCurrentValueFromPrivate(toUnderlyingType(DictionarySizeLZMA::_64KiB)); return;
        case ArchivingCompressionLevel::Fast:
            setCurrentValueFromPrivate(toUnderlyingType(DictionarySizeLZMA::_1MiB)); return;
        case ArchivingCompressionLevel::Normal:
            setCurrentValueFromPrivate(toUnderlyingType(DictionarySizeLZMA::_16MiB)); return;
        case ArchivingCompressionLevel::Maximum:
            setCurrentValueFromPrivate(toUnderlyingType(DictionarySizeLZMA::_32MiB)); return;
        case ArchivingCompressionLevel::Ultra:
            setCurrentValueFromPrivate(toUnderlyingType(DictionarySizeLZMA::_64MiB)); return;
        default:
            Q_ASSERT(false); return;
        }

    case ArchivingCompressionMethod::PPMd:
        switch (archiveType_)
        {

        case ArchiveType::_7z:
            switch (level)
            {
            case ArchivingCompressionLevel::Fastest:
            case ArchivingCompressionLevel::Fast:
                setCurrentValueFromPrivate(toUnderlyingType(DictionarySizePPMd7z::_4MiB)); return;
            case ArchivingCompressionLevel::Normal:
                setCurrentValueFromPrivate(toUnderlyingType(DictionarySizePPMd7z::_16MiB)); return;
            case ArchivingCompressionLevel::Maximum:
                setCurrentValueFromPrivate(toUnderlyingType(DictionarySizePPMd7z::_64MiB)); return;
            case ArchivingCompressionLevel::Ultra:
                setCurrentValueFromPrivate(toUnderlyingType(DictionarySizePPMd7z::_192MiB)); return;
            default:
                Q_ASSERT(false); return;
            }

        case ArchiveType::zip:
            switch (level)
            {
            case ArchivingCompressionLevel::Fastest:
                setCurrentValueFromPrivate(toUnderlyingType(DictionarySizePPMdZip::_1MiB)); return;
            case ArchivingCompressionLevel::Fast:
                setCurrentValueFromPrivate(toUnderlyingType(DictionarySizePPMdZip::_4MiB)); return;
            case ArchivingCompressionLevel::Normal:
                setCurrentValueFromPrivate(toUnderlyingType(DictionarySizePPMdZip::_16MiB)); return;
            case ArchivingCompressionLevel::Maximum:
                setCurrentValueFromPrivate(toUnderlyingType(DictionarySizePPMdZip::_64MiB)); return;
            case ArchivingCompressionLevel::Ultra:
                setCurrentValueFromPrivate(toUnderlyingType(DictionarySizePPMdZip::_128MiB)); return;
            default:
                Q_ASSERT(false); return;
            }

        default:
            Q_ASSERT(false); return;
        }
    default:
        Q_ASSERT(false); return;
    }
}

}
