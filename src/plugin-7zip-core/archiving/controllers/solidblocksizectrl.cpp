// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../core/private/plugin-api/archivetype.h"

#include "../archivingcompressionlevel.h"
#include "../archivingcompressionmethod.h"
#include "../privatearchivingparameter.h"
#include "solidblocksizectrl.h"


using namespace qcs::core;
using Packuru::Core::ArchiveType;


namespace Packuru::Plugins::_7Zip::Core
{

SolidBlockSizeCtrl::SolidBlockSizeCtrl(ArchiveType archiveType)
    : archiveType_(archiveType)
{
    setControllerType(ControllerType::Type::ComboBox);

    if (archiveType_ == ArchiveType::zip)
        return;

    const auto lists = createListsFrom({ ArchivingSolidBlockSize::NonSolid,
                                         ArchivingSolidBlockSize::Solid,
                                         ArchivingSolidBlockSize::MB1,
                                         ArchivingSolidBlockSize::MB2,
                                         ArchivingSolidBlockSize::MB4,
                                         ArchivingSolidBlockSize::MB8,
                                         ArchivingSolidBlockSize::MB16,
                                         ArchivingSolidBlockSize::MB32,
                                         ArchivingSolidBlockSize::MB64,
                                         ArchivingSolidBlockSize::MB128,
                                         ArchivingSolidBlockSize::MB256,
                                         ArchivingSolidBlockSize::MB512,
                                         ArchivingSolidBlockSize::GB1,
                                         ArchivingSolidBlockSize::GB2,
                                         ArchivingSolidBlockSize::GB4,
                                         ArchivingSolidBlockSize::GB8,
                                         ArchivingSolidBlockSize::GB16,
                                         ArchivingSolidBlockSize::GB32,
                                         ArchivingSolidBlockSize::GB64 });

    publicList = lists.first;
    privateList = lists.second;
}


void SolidBlockSizeCtrl::update()
{
    if (archiveType_ == ArchiveType::zip)
    {
        setEnabled(false);
        return;
    }

    const auto level
            = getControllerPropertyAs<ArchivingCompressionLevel>(PrivateArchivingParameter::CompressionLevel,
                                                                 ControllerProperty::PrivateCurrentValue);

    if (level == ArchivingCompressionLevel::NoCompression)
    {
        setEnabled(false);
        setValueLists({},{});
        clearCurrentValue();
        return;
    }

    setEnabled(true);
    setValueLists(publicList,privateList);

    const auto method
            = getControllerPropertyAs<ArchivingCompressionMethod>(PrivateArchivingParameter::CompressionMethod,
                                                                  ControllerProperty::PrivateCurrentValue);

    switch (level)
    {
    case ArchivingCompressionLevel::Fastest:
        switch (method)
        {
        case ArchivingCompressionMethod::BZip2:
        case ArchivingCompressionMethod::LZMA:
        case ArchivingCompressionMethod::LZMA2:
            setCurrentValueFromPrivate(ArchivingSolidBlockSize::MB8); return;
        case ArchivingCompressionMethod::PPMd:
            setCurrentValueFromPrivate(ArchivingSolidBlockSize::MB512); return;
        default:
            Q_ASSERT(false); return;
        }

    case ArchivingCompressionLevel::Fast:
        switch (method)
        {
        case ArchivingCompressionMethod::BZip2:
            setCurrentValueFromPrivate(ArchivingSolidBlockSize::MB32); return;
        case ArchivingCompressionMethod::LZMA:
        case ArchivingCompressionMethod::LZMA2:
            setCurrentValueFromPrivate(ArchivingSolidBlockSize::MB128); return;
        case ArchivingCompressionMethod::PPMd:
            setCurrentValueFromPrivate(ArchivingSolidBlockSize::MB512); return;
        default:
            Q_ASSERT(false); return;
        }

    case ArchivingCompressionLevel::Normal:
        switch (method)
        {
        case ArchivingCompressionMethod::BZip2:
            setCurrentValueFromPrivate(ArchivingSolidBlockSize::MB64); return;
        case ArchivingCompressionMethod::LZMA:
        case ArchivingCompressionMethod::LZMA2:
        case ArchivingCompressionMethod::PPMd:
            setCurrentValueFromPrivate(ArchivingSolidBlockSize::GB2); return;
        default:
            Q_ASSERT(false); return;
        }

    case ArchivingCompressionLevel::Maximum:
    case ArchivingCompressionLevel::Ultra:
        switch (method)
        {
        case ArchivingCompressionMethod::BZip2:
            setCurrentValueFromPrivate(ArchivingSolidBlockSize::MB64); return;
        case ArchivingCompressionMethod::LZMA:
        case ArchivingCompressionMethod::LZMA2:
        case ArchivingCompressionMethod::PPMd:
            setCurrentValueFromPrivate(ArchivingSolidBlockSize::GB4); return;
        default:
            Q_ASSERT(false); return;
        }
    default:
        Q_ASSERT(false); return;
    }
}

}
