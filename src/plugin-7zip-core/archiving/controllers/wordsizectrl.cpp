// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "utils/private/tounderlyingtype.h"

#include "core/private/plugin-api/archivetype.h"

#include "../archivingcompressionlevel.h"
#include "../archivingcompressionmethod.h"
#include "../privatearchivingparameter.h"
#include "../wordsize.h"
#include "wordsizectrl.h"


using namespace qcs::core;
using namespace Packuru::Utils;
using Packuru::Core::ArchiveType;


namespace
{

template<typename Enum>
std::enable_if_t<std::is_enum_v<Enum>, std::pair<QString, std::underlying_type_t<Enum>>>
createPair(Enum value)
{
    const auto uv = toUnderlyingType(value);
    return {QString::number(uv), uv};
}

}


namespace Packuru::Plugins::_7Zip::Core
{

WordSizeCtrl::WordSizeCtrl(ArchiveType archiveType)
    : archiveType_(archiveType)
{
    setControllerType(ControllerType::Type::ComboBox);

    const auto listsDeflate
            = createLists({ createPair(WordSizeDeflate::_8),
                            createPair(WordSizeDeflate::_12),
                            createPair(WordSizeDeflate::_16),
                            createPair(WordSizeDeflate::_24),
                            createPair(WordSizeDeflate::_32),
                            createPair(WordSizeDeflate::_48),
                            createPair(WordSizeDeflate::_64),
                            createPair(WordSizeDeflate::_96),
                            createPair(WordSizeDeflate::_128),
                            createPair(WordSizeDeflate::_192),
                            createPair(WordSizeDeflate::_256),
                            createPair(WordSizeDeflate::_258), });

    listDeflatePublic = listsDeflate.first;
    listDeflatePrivate = listsDeflate.second;

    const auto listsDeflate64
            = createLists({ createPair(WordSizeDeflate64::_8),
                            createPair(WordSizeDeflate64::_12),
                            createPair(WordSizeDeflate64::_16),
                            createPair(WordSizeDeflate64::_24),
                            createPair(WordSizeDeflate64::_32),
                            createPair(WordSizeDeflate64::_48),
                            createPair(WordSizeDeflate64::_64),
                            createPair(WordSizeDeflate64::_96),
                            createPair(WordSizeDeflate64::_128),
                            createPair(WordSizeDeflate64::_192),
                            createPair(WordSizeDeflate64::_256),
                            createPair(WordSizeDeflate64::_257), });

    listDeflate64Public = listsDeflate64.first;
    listDeflate64Private = listsDeflate64.second;

    const auto listsLZMA
            = createLists({ createPair(WordSizeLZMA::_8),
                            createPair(WordSizeLZMA::_12),
                            createPair(WordSizeLZMA::_16),
                            createPair(WordSizeLZMA::_24),
                            createPair(WordSizeLZMA::_32),
                            createPair(WordSizeLZMA::_48),
                            createPair(WordSizeLZMA::_64),
                            createPair(WordSizeLZMA::_96),
                            createPair(WordSizeLZMA::_128),
                            createPair(WordSizeLZMA::_192),
                            createPair(WordSizeLZMA::_256),
                            createPair(WordSizeLZMA::_273), });

    listLZMAPublic = listsLZMA.first;
    listLZMAPrivate = listsLZMA.second;

    if (archiveType == ArchiveType::_7z)
    {
        const auto listsPPMd
                = createLists({ createPair(WordSizePPMd7z::_2),
                                createPair(WordSizePPMd7z::_3),
                                createPair(WordSizePPMd7z::_4),
                                createPair(WordSizePPMd7z::_5),
                                createPair(WordSizePPMd7z::_6),
                                createPair(WordSizePPMd7z::_7),
                                createPair(WordSizePPMd7z::_8),
                                createPair(WordSizePPMd7z::_10),
                                createPair(WordSizePPMd7z::_12),
                                createPair(WordSizePPMd7z::_14),
                                createPair(WordSizePPMd7z::_16),
                                createPair(WordSizePPMd7z::_20),
                                createPair(WordSizePPMd7z::_24),
                                createPair(WordSizePPMd7z::_28),
                                createPair(WordSizePPMd7z::_32), });

        listPPMdPublic = listsPPMd.first;
        listPPMdPrivate = listsPPMd.second;
    }
    else
    {
        const auto listsPPMd
                = createLists({ createPair(WordSizePPMdZip::_2),
                                createPair(WordSizePPMdZip::_3),
                                createPair(WordSizePPMdZip::_4),
                                createPair(WordSizePPMdZip::_5),
                                createPair(WordSizePPMdZip::_6),
                                createPair(WordSizePPMdZip::_7),
                                createPair(WordSizePPMdZip::_8),
                                createPair(WordSizePPMdZip::_9),
                                createPair(WordSizePPMdZip::_10),
                                createPair(WordSizePPMdZip::_11),
                                createPair(WordSizePPMdZip::_12),
                                createPair(WordSizePPMdZip::_13),
                                createPair(WordSizePPMdZip::_14),
                                createPair(WordSizePPMdZip::_15),
                                createPair(WordSizePPMdZip::_16), });

        listPPMdPublic = listsPPMd.first;
        listPPMdPrivate = listsPPMd.second;
    }
}


void WordSizeCtrl::update()
{
    const auto level
            = getControllerPropertyAs<ArchivingCompressionLevel>(PrivateArchivingParameter::CompressionLevel,
                                                                 ControllerProperty::PrivateCurrentValue);

    ArchivingCompressionMethod method = ArchivingCompressionMethod::___INVALID;

    // Method is not set when level == NoCompression
    if (level != ArchivingCompressionLevel::NoCompression)
    {
        method = getControllerPropertyAs<ArchivingCompressionMethod>(PrivateArchivingParameter::CompressionMethod,
                                                                     ControllerProperty::PrivateCurrentValue);
    }

    if (level == ArchivingCompressionLevel::NoCompression || method == ArchivingCompressionMethod::BZip2)
    {
        setEnabled(false);
        setValueLists({}, {});
        clearCurrentValue();
        return;
    }

    setEnabled(true);

    switch (method)
    {
    case ArchivingCompressionMethod::Deflate:
        setValueLists(listDeflatePublic, listDeflatePrivate); break;
    case ArchivingCompressionMethod::Deflate64:
        setValueLists(listDeflate64Public, listDeflate64Private); break;
    case ArchivingCompressionMethod::LZMA:
    case ArchivingCompressionMethod::LZMA2:
        setValueLists(listLZMAPublic, listLZMAPrivate); break;
    case ArchivingCompressionMethod::PPMd:
        setValueLists(listPPMdPublic, listPPMdPrivate); break;
    default:
        Q_ASSERT(false);
        break;
    }

    switch (method)
    {
    case ArchivingCompressionMethod::Deflate:
    case ArchivingCompressionMethod::Deflate64:
        switch (level)
        {
        case ArchivingCompressionLevel::Fastest:
        case ArchivingCompressionLevel::Fast:
        case ArchivingCompressionLevel::Normal:
            setCurrentValueFromPrivate(toUnderlyingType(WordSizeDeflate::_32)); return;
        case ArchivingCompressionLevel::Maximum:
            setCurrentValueFromPrivate(toUnderlyingType(WordSizeDeflate::_64)); return;
        case ArchivingCompressionLevel::Ultra:
            setCurrentValueFromPrivate(toUnderlyingType(WordSizeDeflate::_128)); return;
        default:
            Q_ASSERT(false); return;
        }

    case ArchivingCompressionMethod::LZMA:
    case ArchivingCompressionMethod::LZMA2:
        switch (level)
        {
        case ArchivingCompressionLevel::Fastest:
        case ArchivingCompressionLevel::Fast:
        case ArchivingCompressionLevel::Normal:
            setCurrentValueFromPrivate(toUnderlyingType(WordSizeLZMA::_32)); return;
        case ArchivingCompressionLevel::Maximum:
        case ArchivingCompressionLevel::Ultra:
            setCurrentValueFromPrivate(toUnderlyingType(WordSizeLZMA::_64)); return;
        default:
            Q_ASSERT(false); return;
        }

    case ArchivingCompressionMethod::PPMd:
        switch (archiveType_)
        {
        case ArchiveType::_7z:
            switch (level)
            {
            case ArchivingCompressionLevel::Fastest:
            case ArchivingCompressionLevel::Fast:
                setCurrentValueFromPrivate(toUnderlyingType(WordSizePPMd7z::_4)); return;
            case ArchivingCompressionLevel::Normal:
                setCurrentValueFromPrivate(toUnderlyingType(WordSizePPMd7z::_6)); return;
            case ArchivingCompressionLevel::Maximum:
                setCurrentValueFromPrivate(toUnderlyingType(WordSizePPMd7z::_16)); return;
            case ArchivingCompressionLevel::Ultra:
                setCurrentValueFromPrivate(toUnderlyingType(WordSizePPMd7z::_32)); return;
            default:
                Q_ASSERT(false); return;
            }

        case ArchiveType::zip:
            switch (level)
            {
            case ArchivingCompressionLevel::Fastest:
                setCurrentValueFromPrivate(toUnderlyingType(WordSizePPMdZip::_4)); return;
            case ArchivingCompressionLevel::Fast:
                setCurrentValueFromPrivate(toUnderlyingType(WordSizePPMdZip::_6)); return;
            case ArchivingCompressionLevel::Normal:
                setCurrentValueFromPrivate(toUnderlyingType(WordSizePPMdZip::_8)); return;
            case ArchivingCompressionLevel::Maximum:
                setCurrentValueFromPrivate(toUnderlyingType(WordSizePPMdZip::_10)); return;
            case ArchivingCompressionLevel::Ultra:
                setCurrentValueFromPrivate(toUnderlyingType(WordSizePPMdZip::_12)); return;
            default:
                Q_ASSERT(false); return;
            }
        default:
            Q_ASSERT(false); return;
        }
    default:
        Q_ASSERT(false); return;
    }
}

}
