// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "../qcs-core/controller.h"

#include "../archivingencryptionmethod.h"


namespace Packuru::Core
{
enum class ArchiveType;
}

namespace Packuru::Plugins::_7Zip::Core
{

class EncryptionMethodCtrl : public qcs::core::Controller<QString, ArchivingEncryptionMethod>
{
public:
    EncryptionMethodCtrl(Packuru::Core::ArchiveType archiveType);
};

}
