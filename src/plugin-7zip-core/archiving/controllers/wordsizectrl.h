// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "../qcs-core/controller.h"


namespace Packuru::Core
{
enum class ArchiveType;
}

namespace Packuru::Plugins::_7Zip::Core
{

class WordSizeCtrl : public qcs::core::Controller<QString, int>
{
public:
    WordSizeCtrl(Packuru::Core::ArchiveType archiveType);

    void update() override;

private:
    QList<QString> listDeflatePublic;
    QList<int> listDeflatePrivate;

    QList<QString> listDeflate64Public;
    QList<int> listDeflate64Private;

    QList<QString> listLZMAPublic;
    QList<int> listLZMAPrivate;


    QList<QString> listPPMdPublic;
    QList<int> listPPMdPrivate;

    Packuru::Core::ArchiveType archiveType_;
};

}
