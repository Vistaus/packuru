// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../core/private/plugin-api/archivetype.h"

#include "encryptionmethodctrl.h"


using Packuru::Core::ArchiveType;
using namespace qcs::core;


namespace Packuru::Plugins::_7Zip::Core
{

EncryptionMethodCtrl::EncryptionMethodCtrl(ArchiveType archiveType)
{
    setControllerType(ControllerType::Type::ComboBox);

    setValueListsFromPrivate({ //ArchivingEncryptionMethod::ZipCrypto, // legacy and weak
                               ArchivingEncryptionMethod::AES_128,
                               ArchivingEncryptionMethod::AES_256 });

    setCurrentValueFromPrivate(ArchivingEncryptionMethod::AES_256);

    // 7z uses only AES-256
    if (archiveType == ArchiveType::_7z)
        setEnabled(false);
}

}
