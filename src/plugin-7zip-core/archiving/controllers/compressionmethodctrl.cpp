// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../core/private/plugin-api/archivetype.h"

#include "../archivingcompressionlevel.h"
#include "../privatearchivingparameter.h"
#include "compressionmethodctrl.h"


using Packuru::Core::ArchiveType;
using namespace qcs::core;


namespace Packuru::Plugins::_7Zip::Core
{

CompressionMethodCtrl::CompressionMethodCtrl(ArchiveType archiveType)
    : archiveType_(archiveType)
{
    setControllerType(ControllerType::Type::ComboBox);

    std::pair<QList<QString>, QList<ArchivingCompressionMethod>> lists;

    if (archiveType_ == ArchiveType::_7z)
    {
        lists = createListsFrom({ ArchivingCompressionMethod::BZip2,
                                  ArchivingCompressionMethod::LZMA,
                                  ArchivingCompressionMethod::LZMA2,
                                  ArchivingCompressionMethod::PPMd });
        userValue = ArchivingCompressionMethod::LZMA2;
    }
    else if (archiveType_ == ArchiveType::zip)
    {
        lists = createListsFrom({ ArchivingCompressionMethod::BZip2,
                                  ArchivingCompressionMethod::Deflate,
                                  ArchivingCompressionMethod::Deflate64,
                                  ArchivingCompressionMethod::LZMA,
                                  ArchivingCompressionMethod::PPMd });
        userValue = ArchivingCompressionMethod::Deflate;
    }

    publicList = lists.first;
    privateList = lists.second;
}


void CompressionMethodCtrl::update()
{
    const auto level = getControllerPropertyAs<ArchivingCompressionLevel>(PrivateArchivingParameter::CompressionLevel,
                                                                          ControllerProperty::PrivateCurrentValue);

    if (level == ArchivingCompressionLevel::NoCompression)
    {
        setEnabled(false);
        setValueLists({}, {});
        clearCurrentValue();
    }
    else
    {
        setEnabled(true);
        setValueLists(publicList, privateList);
        setCurrentValueFromPrivate(userValue);
    }
}


void CompressionMethodCtrl::onValueSyncedToWidget(const QVariant& value)
{
    Q_UNUSED(value)
    userValue = getPropertyAs<ArchivingCompressionMethod>(ControllerProperty::PrivateCurrentValue);
}

}
