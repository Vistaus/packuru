// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "archivingupdatemodectrl.h"


using namespace qcs::core;


namespace Packuru::Plugins::_7Zip::Core
{

ArchivingUpdateModeCtrl::ArchivingUpdateModeCtrl()
{
    setControllerType(ControllerType::Type::ComboBox);

    setValueListsFromPrivate({ ArchivingUpdateMode::AddAndReplace,
                               ArchivingUpdateMode::AddAndUpdate,
                               ArchivingUpdateMode::AddAndSkip,
                               ArchivingUpdateMode::UpdateExisting,
                               ArchivingUpdateMode::NewArchive });

    setCurrentValueFromPrivate(ArchivingUpdateMode::AddAndReplace);

}

}
