// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "compressionlevelctrl.h"


using namespace qcs::core;


namespace Packuru::Plugins::_7Zip::Core
{

CompressionLevelCtrl::CompressionLevelCtrl()
{
    setControllerType(ControllerType::Type::ComboBox);

    setValueListsFromPrivate({ ArchivingCompressionLevel::NoCompression,
                               ArchivingCompressionLevel::Fastest,
                               ArchivingCompressionLevel::Fast,
                               ArchivingCompressionLevel::Normal,
                               ArchivingCompressionLevel::Maximum,
                               ArchivingCompressionLevel::Ultra });

    setCurrentValueFromPrivate(ArchivingCompressionLevel::Normal);
}


void CompressionLevelCtrl::externalLevelInput(ArchivingCompressionLevel level)
{
    setCurrentValueFromPrivate(level);
    externalValueInput();
}

}
