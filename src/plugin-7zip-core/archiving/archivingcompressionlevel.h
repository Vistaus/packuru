// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QMetaType>


namespace Packuru::Plugins::_7Zip::Core
{

enum class ArchivingCompressionLevel
{
    NoCompression,
    Fastest,
    Fast,
    Normal,
    Maximum,
    Ultra,

    ___COUNT
};


QString toString(ArchivingCompressionLevel value);

}

Q_DECLARE_METATYPE(Packuru::Plugins::_7Zip::Core::ArchivingCompressionLevel);


