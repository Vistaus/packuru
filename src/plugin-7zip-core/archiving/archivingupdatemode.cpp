// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QCoreApplication>

#include "archivingupdatemode.h"


namespace Packuru::Plugins::_7Zip::Core
{

QString toString(ArchivingUpdateMode value)
{
    switch (value)
    {
    case ArchivingUpdateMode::NewArchive:
        return QCoreApplication::translate("ArchivingUpdateMode", "Create new archive");
    case ArchivingUpdateMode::AddAndReplace:
        return QCoreApplication::translate("ArchivingUpdateMode", "Add absent files and replace existing");
    case ArchivingUpdateMode::AddAndUpdate:
        return QCoreApplication::translate("ArchivingUpdateMode", "Add absent files and update existing");
    case ArchivingUpdateMode::AddAndSkip:
        return QCoreApplication::translate("ArchivingUpdateMode", "Add absent files and skip existing");
    case ArchivingUpdateMode::UpdateExisting:
        return QCoreApplication::translate("ArchivingUpdateMode", "Update existing files only");
    default:
        Q_ASSERT(false); return QString();
    };
}

}
