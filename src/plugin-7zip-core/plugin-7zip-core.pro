# SPDX-FileCopyrightText: The Packuru Developers
#
# SPDX-License-Identifier: 0BSD

include(../common-src.pri)
include(../common-lib.pri)

MODULE_BUILD_NAME_SUFFIX = plugin-7zip-core
MODULE_BUILD_NAME = $${PROJECT_BUILD_NAME}-$${MODULE_BUILD_NAME_SUFFIX}

include(../embed-qm-files.pri)

TARGET = $$qtLibraryTarget($$MODULE_BUILD_NAME)

LIBS += -L$${PROJECT_TARGET_LIB_DIR} -l$$QCS_CORE_BUILD_NAME -l$${CORE_BUILD_NAME}

TRANSLATIONS += \
    translations/$${MODULE_BUILD_NAME_SUFFIX}_en.ts \

HEADERS += \
    archivewatcher.h \
    archiving/archivingcompressionlevel.h \
    archiving/archivingcompressionmethod.h \
    archiving/archivingencryptionmethod.h \
    archiving/archivingoptiontocliarg.h \
    archiving/archivingparametershandler.h \
    archiving/archivingsolidblocksize.h \
    archiving/archivingupdatemode.h \
    archiving/controllers/archivingupdatemodectrl.h \
    archiving/controllers/compressionlevelctrl.h \
    archiving/controllers/compressionmethodctrl.h \
    archiving/controllers/dictionarysizectrl.h \
    archiving/controllers/encryptionmethodctrl.h \
    archiving/controllers/solidblocksizectrl.h \
    archiving/controllers/threadcountctrl.h \
    archiving/controllers/wordsizectrl.h \
    archiving/dictionarysize.h \
    archiving/privatearchivingparameter.h \
    archiving/privatearchivingparametermap.h \
    archiving/wordsize.h \
    backendhandler.h \
    invocationdata.h \
    registerstreamoperators.h

SOURCES += \
    archivewatcher.cpp \
    archiving/archivingcompressionlevel.cpp \
    archiving/archivingcompressionmethod.cpp \
    archiving/archivingencryptionmethod.cpp \
    archiving/archivingoptiontocliarg.cpp \
    archiving/archivingparametershandler.cpp \
    archiving/archivingsolidblocksize.cpp \
    archiving/archivingupdatemode.cpp \
    archiving/controllers/archivingupdatemodectrl.cpp \
    archiving/controllers/compressionlevelctrl.cpp \
    archiving/controllers/compressionmethodctrl.cpp \
    archiving/controllers/dictionarysizectrl.cpp \
    archiving/controllers/encryptionmethodctrl.cpp \
    archiving/controllers/solidblocksizectrl.cpp \
    archiving/controllers/threadcountctrl.cpp \
    archiving/controllers/wordsizectrl.cpp \
    backendhandler.cpp \
    registerstreamoperators.cpp

