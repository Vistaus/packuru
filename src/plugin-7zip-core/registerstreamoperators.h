// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once


namespace Packuru::Plugins::_7Zip::Core
{

void registerStreamOperators();

}
