// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QObject>
#include <QtPlugin>

#include "../core/private/plugin-api/archivehandlerinterface.h"


namespace Packuru::Plugins::GnuTar
{

class ArchiveHandlerGnuTar : public QObject, public Packuru::Core::ArchiveHandlerInterface
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "ArchiveHandlerGnuTar" FILE "pluginmetadata.json")
    Q_INTERFACES(Packuru::Core::ArchiveHandlerInterface)

public:
    ArchiveHandlerGnuTar();

    std::unordered_set<Packuru::Core::ArchiveType> supportedTypesForReading() const override;
    std::unordered_set<Packuru::Core::ArchiveType> supportedTypesForWriting() const override;

    bool canAddFiles(Packuru::Core::ArchiveType archiveType, const Packuru::Core::BackendArchiveProperties& properties) const override;
    bool canDeleteFiles(Packuru::Core::ArchiveType archiveType, const Packuru::Core::BackendArchiveProperties& properties) const override;

    std::unique_ptr<Packuru::Core::AbstractBackendHandler> createBackendHandler() const override;

    std::unique_ptr<Packuru::Core::ArchivingParameterHandlerBase>
    createArchivingParameterHandler(const Packuru::Core::ArchivingParameterHandlerInitData& initData) const override;

private:
    std::unordered_set<Packuru::Core::ArchiveType> supportedArchives;
};

}
