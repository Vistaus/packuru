// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <unordered_map>
#include <unordered_set>

#include <QWidget>

#include "../qcs-core/controllerengine.h"
#include "../qcs-core/controllerproperty.h"

#include "widgetmapper.h"
#include "private/widgethandler.h"


using namespace qcs::core;


namespace qcs::qtw
{

struct WidgetMapper::Private
{
    void addLabelsAndWidgets(int internalId,
                             std::initializer_list<QLabel*> labels,
                             std::initializer_list<QWidget*> widgets);
    void addBuddies(InternalControllerIdType internalId,
                    const std::initializer_list<QWidget*>& widgets);
    void updateWidgets();

    void onUserChangedWidgetValue(QWidget* widget, const QVariant& value);
    void syncWidgetsToControllers(InternalControllerIdType controllerId,
                                  const ControllerPropertyMap& properties);

    using LabelSet = WidgetHandler::LabelSet;
    using WidgetSet = WidgetHandler::WidgetSet;

    std::unordered_map<InternalControllerIdType, WidgetSet> idToWidgets;
    std::unordered_map<QWidget*, InternalControllerIdType> widgetToId;
    std::unordered_map<InternalControllerIdType, WidgetSet> idToBuddies;
    std::unordered_map<InternalControllerIdType, LabelSet> idToLabels;
    std::unordered_set<InternalControllerIdType> widgetsToUpdate;
    WidgetMapper* publ = nullptr;
    ControllerEngine* engine_ = nullptr;
    WidgetHandler* handler;
};


WidgetMapper::WidgetMapper(QObject *parent)
    : QObject(parent),
      priv(new Private)
{
    priv->publ = this;
    priv->handler = new WidgetHandler(this);

    connect(priv->handler, &WidgetHandler::sigCurrentValueChanged,
            this, [priv = priv.get()] (QWidget* widget, const QVariant& value)
    { priv->onUserChangedWidgetValue(widget, value); });
}


WidgetMapper::~WidgetMapper()
{

}


void WidgetMapper::setEngine(ControllerEngine* engine)
{
    Q_ASSERT(engine);

    if (!priv->engine_)
    {
        priv->engine_ = engine;

        connect(engine, &ControllerEngine::sigControllerUpdated,
                this, [priv = priv.get()] (InternalControllerIdType id, const ControllerPropertyMap& properties)
        { priv->syncWidgetsToControllers(id, properties); });
    }

    updateWidgets();
}


void WidgetMapper::updateWidgets()
{
    priv->updateWidgets();
}


void WidgetMapper::addLabelsAndWidgetsImpl(int internalId,
                                           std::initializer_list<QLabel*> labels,
                                           std::initializer_list<QWidget*> widgets)
{
    priv->addLabelsAndWidgets(internalId, labels, widgets);
}


void WidgetMapper::addBuddiesImpl(int internalId, const std::initializer_list<QWidget*>& widgets)
{
    priv->addBuddies(internalId, widgets);
}


bool WidgetMapper::isSetVisibleImpl(int internalId) const
{
    return priv->engine_->getControllerPropertyAs<bool>(internalId,
                                                        qcs::core::ControllerProperty::Visible);
}


bool WidgetMapper::isSetEnabledImpl(int internalId) const
{
    return priv->engine_->getControllerPropertyAs<bool>(internalId,
                                                        qcs::core::ControllerProperty::Enabled);
}


void WidgetMapper::Private::addLabelsAndWidgets(int internalId,
                                                std::initializer_list<QLabel*> labels,
                                                std::initializer_list<QWidget*> widgets)
{
    if (labels.size() != 0)
    {
        auto& labelsForId = idToLabels[internalId];

        for (auto label : labels)
        {
            if (labelsForId.find(label) == labelsForId.end())
            {
                labelsForId.insert(label);

                QObject::connect(label, &QObject::destroyed,
                                 publ, [priv = this, controllerId = internalId, label = label] ()
                {
                    LabelSet& labelsForId = priv->idToLabels[controllerId];
                    labelsForId.erase(label);
                });
            }
        }
    }

    if (widgets.size() != 0)
    {
        auto& widgetsForId = idToWidgets[internalId];

        for (auto widget : widgets)
        {
            if (widgetsForId.find(widget) == widgetsForId.end())
            {
                handler->connectWidget(widget);

                widgetsForId.insert(widget);
                widgetToId[widget] = internalId;

                QObject::connect(widget, &QObject::destroyed,
                                 publ, [priv = this, widget = widget] ()
                {
                    const InternalControllerIdType controllerId = priv->widgetToId[widget];
                    WidgetSet& widgetsForId = priv->idToWidgets[controllerId];
                    widgetsForId.erase(widget);
                    priv->widgetToId.erase(widget);
                });
            }
        }
    }

    widgetsToUpdate.insert(internalId);
}


void WidgetMapper::Private::addBuddies(InternalControllerIdType internalId,
                                       const std::initializer_list<QWidget*>& widgets)
{
    WidgetSet& buddiesForId = idToBuddies[internalId];

    for (auto widget : widgets)
    {
        if (buddiesForId.find(widget) == buddiesForId.end())
        {
            buddiesForId.insert(widget);

            QObject::connect(widget, &QObject::destroyed,
                             publ, [priv = this, widget = widget, internalId = internalId] ()
            {
                WidgetSet& buddiesForId = priv->idToBuddies[internalId];
                buddiesForId.erase(widget);
            });
        }
    }

    widgetsToUpdate.insert(internalId);
}


void WidgetMapper::Private::updateWidgets()
{
    if (!engine_)
        return;

    for (InternalControllerIdType id : widgetsToUpdate)
        syncWidgetsToControllers(id, engine_->getControllerProperties(id));

    widgetsToUpdate.clear();
}


void WidgetMapper::Private::onUserChangedWidgetValue(QWidget* widget, const QVariant& value)
{
    const auto it = widgetToId.find(widget);
    Q_ASSERT(it != widgetToId.end());

    const InternalControllerIdType id = it->second;

    const auto it2 = idToWidgets.find(id);
    Q_ASSERT(it2 != idToWidgets.end());

    WidgetSet widgets = it2->second; // Copy
    widgets.erase(widget);

    for (auto widget : widgets)
        handler->changeCurrentValue(widget, value);

    engine_->syncControllerToWidget(id, value);
}


void WidgetMapper::Private::syncWidgetsToControllers(InternalControllerIdType controllerId,
                                                     const ControllerPropertyMap& properties)
{
    WidgetHandler::UpdateData data(properties);

    auto labelIt = idToLabels.find(controllerId);
    if (labelIt != idToLabels.end())
        data.labels = &labelIt->second;

    auto widgetIt = idToWidgets.find(controllerId);
    if (widgetIt != idToWidgets.end())
        data.widgets = &widgetIt->second;

    widgetIt = idToBuddies.find(controllerId);
    if (widgetIt != idToBuddies.end())
        data.buddies = &widgetIt->second;

    handler->updateWidgets(data);
}

}
