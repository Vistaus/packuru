// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <unordered_map>

#include <QGridLayout>
#include <QLabel>
#include <QFrame>
#include <QSpinBox>
#include <QSlider>
#include <QLineEdit>
#include <QCheckBox>
#include <QComboBox>

#include "../utils/localholder.h"
#include "../utils/private/unordered_map_utils.h"
#include "../utils/qvariant_utils.h"

#include "../qcs-core/controlleridrowmapper.h"
#include "../qcs-core/controllerproperty.h"
#include "../qcs-core/controllertype.h"

#include "generatedpage.h"
#include "private/widgethandler.h"


using namespace Packuru;
using namespace qcs::core;


namespace qcs::qtw
{

struct GeneratedPage::Private
{
    void setup(GeneratedPage* publ, ControllerIdRowMapper* mapper);
    void syncWidgetToController(int row, const ControllerPropertyMap& properties);
    void onUserChangedValue(QWidget* widget, const QVariant& value);

    struct RowData{
        QLabel* label;
        QWidget* widget;
    };

    GeneratedPage* publ_ = nullptr;
    ControllerIdRowMapper* mapper_ = nullptr;
    WidgetHandler* widgetHandler;
    std::unordered_map<int, RowData> rowToData;
    std::unordered_map<QWidget*, int> widgetToRow;
    bool updatingWidgets = false;
};


GeneratedPage::GeneratedPage(ControllerIdRowMapper* mapper, QWidget *parent)
    : QWidget(parent),
      priv(new Private)

{
    priv->setup(this, mapper);
}


GeneratedPage::~GeneratedPage()
{

}


void GeneratedPage::Private::setup(GeneratedPage* publ,ControllerIdRowMapper* mapper)
{
    publ_ = publ;
    mapper_ = mapper;
    widgetHandler = new WidgetHandler(publ);

    connect(mapper_, &ControllerIdRowMapper::sigControllerPropertiesChanged,
            publ_, [priv = this] (int row, const ControllerPropertyMap& properties)
    { priv->syncWidgetToController(row, properties); });

    connect(widgetHandler, &WidgetHandler::sigCurrentValueChanged,
            publ_, [priv = this] (QWidget* widget, const QVariant& value)
    { priv->onUserChangedValue(widget, value); });

    auto gridLayout = std::make_unique<QGridLayout>();

    for (int mapperRow = 0, gridRow = 0; mapperRow < mapper_->size(); ++mapperRow, ++gridRow)
    {

        auto labelData = mapper_->getLabel(mapperRow);

        auto label = new QLabel(mapper_->getLabel(mapperRow));
        auto alignement = Qt::AlignVCenter | Qt::AlignRight;
        label->setAlignment(alignement);

        if (mapper_->isHeader(mapperRow))
        {
            if (gridRow > 0)
            {
                auto line = new QFrame;
                line->setFrameShape(QFrame::HLine);
                line->setFrameShadow(QFrame::Sunken);
                gridLayout->addWidget(line, gridRow, 0, 1, 2);
                ++gridRow;
            }

            QFont font;
            font.setBold(true);
            label->setFont(font);
            gridLayout->addWidget(label, gridRow, 0);
            continue;
        }

        gridLayout->addWidget(label, gridRow, 0);

        auto& data = rowToData[mapperRow];
        data.label = label;

        const auto properties = mapper_->getControllerProperties(mapperRow);
        const auto variant = Utils::getValue(properties, ControllerProperty::ControllerType);

        const auto type = Utils::getValueAs<ControllerType::Type>(variant);

        QWidget* widget = nullptr;

        switch(type)
        {
        case ControllerType::Type::CheckBox:
        {
            auto w = new QCheckBox;
            widget = w;
            break;
        }
        case ControllerType::Type::ComboBox:
        {
            auto w = new QComboBox;
            widget = w;
            break;
        }
        case ControllerType::Type::DoubleSpinBox:
        {
            Q_ASSERT(false);
            break;
        }
        case ControllerType::Type::IntSpinBox:
        {
            auto w = new QSpinBox;
            widget = w;
            break;
        }
        case ControllerType::Type::LineEdit:
        {
            auto w = new QLineEdit;
            widget = w;
            break;
        }
        case ControllerType::Type::Label:
        {
            auto w = new QLabel;
            QFont f;
            f.setItalic(true);
            w->setFont(f);
            widget = w;
            break;
        }
        case ControllerType::Type::Slider:
        {
            auto w = new QSlider(Qt::Horizontal);
            w->setTickInterval(1);
            w->setTickPosition(QSlider::TicksAbove);
            w->setPageStep(1);
            widget = w;
            break;
        }
        default:
            Q_ASSERT(false);
            break;
        }

        Q_ASSERT(widget);

        widgetHandler->connectWidget(widget);
        gridLayout->addWidget(widget, gridRow, 1);
        widgetToRow[widget] = mapperRow;
        data.widget = widget;
        syncWidgetToController(mapperRow, properties);
    }

    auto spacer = new QSpacerItem(0, 0, QSizePolicy::Fixed, QSizePolicy::Expanding);
    gridLayout->addItem(spacer, gridLayout->rowCount(), 0);

    publ->setLayout(gridLayout.get());
    gridLayout.release();
}


void GeneratedPage::Private::syncWidgetToController(int row, const ControllerPropertyMap& properties)
{
    const Utils::LocalHolder<bool, false> lh(&updatingWidgets, true);

    auto it = rowToData.find(row);
    Q_ASSERT(it != rowToData.end());

    const auto& data = it->second;

    Q_ASSERT(data.widget);
    Q_ASSERT(data.label);


    using WidgetSet = WidgetHandler::WidgetSet;
    WidgetSet buddies {data.label}; // Labels are currently only set as buddies and text is set separately
    WidgetSet widgets {data.widget};

    WidgetHandler::UpdateData updateData(properties);
    updateData.buddies = &buddies;
    updateData.widgets = &widgets;
    widgetHandler->updateWidgets(updateData);
}


void GeneratedPage::Private::onUserChangedValue(QWidget* widget, const QVariant& value)
{
    auto it = widgetToRow.find(widget);
    Q_ASSERT(it != widgetToRow.end());
    auto row = it->second;
    mapper_->onUserChangedValue(row, value);
}

}
