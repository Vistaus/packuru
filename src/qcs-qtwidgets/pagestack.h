// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QStackedWidget>

#include "symbol_export.h"

/// @file

namespace qcs::core
{
class ControllerIdRowMapper;
}


namespace qcs::qtw
{

/**
 @brief Creates %GeneratedPage pages and displays in a stack.

 @headerfile "src/qcs-qtwidgets/pagestack.h" "qcs-qtwidgets/pagestack.h"
 @sa GeneratedPage, WidgetMapper
 */
class QCS_QTWIDGETS_EXPORT PageStack : public QStackedWidget
{
    Q_OBJECT
public:
    explicit PageStack(QWidget* parent = nullptr);
    ~PageStack() override;

    /**
     @brief Sets as current the page corresponding to the given mapper.

     Constructs the page if it doesn't exist.
     @param mapper ControllerIdRowMapper delivers logic for the page.
     */
    void setCurrentPage(qcs::core::ControllerIdRowMapper* mapper);

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}
