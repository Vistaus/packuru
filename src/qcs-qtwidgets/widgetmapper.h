// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QObject>
#include <QCheckBox>
#include <QComboBox>
#include <QLabel>
#include <QLineEdit>
#include <QSlider>
#include <QSpinBox>

#include "../utils/typetraits.h"

#include "symbol_export.h"

/// @file

namespace qcs::core
{
class ControllerEngine;
}


namespace qcs::qtw
{

/**
 @brief Maps existing input widgets to an ID and updates their state.

 %WidgetMapper makes development of widgets such as dialogs faster by removing the need to
 handle input widgets' interdependencies and state changes (visible, enabled, current value,
 value range, value set) on the UI side.
 The widgets are just mapped to an ID and updated by a %WidgetMapper connected to
 a ControllerEngine object which handles all the logic.

 The usual workflow is to map widgets, labels and buddies first and then set the engine, which will
 also update the widgets with values from ControllerEngine. A "buddy" widget will automatically
 follow enabled/visible properties of an input widget mapped to the same ID. A label
 widget can also be mapped and it will automatically received its text, in addition to following
 enable/visible states of the input widget.

 Input widgets receive only a visual representation of data (e.g. QString, bool, int).
 This shields the UI from low-level details like enums, which are handled on the logic side.

 Setting values is only supported for:
 * * Qt Widgets:
   * * QCheckBox, QComboBox, QLabel, QLineEdit, QSlider, QSpinBox.
 * * Qt Quick Controls 2:
   * * CheckBox, ComboBox, Label, Slider, SpinBox, TextField.

 For the rest of widgets (QWidget, QQC2 Control) only Enabled/Visible/ToolTip properties are supported.

 When a dialog containing %WidgetMapper is accepted the values are dumped from ControllerEngine,
 which typically is encapsulated in some core object. This allows to write dialog widgets
 only as a thin layer on top of a core logic object.

 GeneratedPage is similar to %WidgetMapper, however it autogenerates widgets before mapping
 them instead of mapping existing widgets.

 @par C++/Qt Widgets Example

 @code
 Dialog::Dialog(DialogCore* core, QWidget* parent)
    : QWidget(parent),
      dialogCore(core)
 {
    mapper = new WidgetMapper(this);

    // Given: enum class Control { ArchiveType, CompressionLevel, DestinationPath }

    // The mapper will set labels' text and enabled/visible properties
    mapper->addLabelAndWidget(Control::ArchiveType,
                              ui->archiveTypeLabel,
                              ui->archiveTypeComboBox);

    mapper->addLabelAndWidget(Control::DestinationPath,
                              ui->destinationPathLabel,
                              ui->destinationPathLineEdit);

    // compressionLevelSlider will be updated automatically depending on the archive type
    // (controlled on the core/logic side)
    mapper->addLabelAndWidget(Control::CompressionLevel,
                              ui->compressionLevelLabel,
                              ui->compressionLevelSlider);

    mapper->setEngine(dialogCore->getControllerEngine());
 }

 void Dialog::accept()
 {
    // The core dumps values from ControllerEngine and does all the low-level work.
    // Everything is hidden from the UI.
    dialogCore->accept();
 }
 @endcode

 @par QML/Qt Quick Controls Example

 @code
 import QtQuick 2.15
 import QtQuick.Controls 2.15

 import ControllerSystem.QQC 1.0 as QCS_QQC

 Dialog {
     id: root

     property QtObject dialogCore // Set at construction time; contains ControllerEngine

     QCS_QQC.WidgetMapper {
         id: mapper
         engine: root.dialogCore.controllerEngine
     }

     Component.onCompleted: {
         // Given: enum Control { ArchiveType, CompressionLevel, DestinationPath }

         // The mapper will set labels' text and enabled/visible properties
         mapper.addLabelAndWidget(Control.ArchiveType,
                                  archiveTypeLabel,
                                  archiveTypeComboBox)

         mapper.addLabelAndWidget(Control.DestinationPath,
                                  destinationPathLabel,
                                  destinationPathLineEdit)

         // compressionLevelSlider will be updated automatically depending on the archive type
         // (controlled on the core/logic side)
         mapper.addLabelAndWidget(Control.CompressionLevel,
                                  compressionLevelLabel,
                                  compressionLevelSlider)

         mapper.updateWidgets()
     }

     onAccepted: {
         // The core dumps values from ControllerEngine and does all the low-level work.
         // Everything is hidden from the UI.
         dialogCore.accept()
     }
 }
 @endcode

 @sa GeneratedPage, PageStack
 @headerfile "src/qcs-qtwidgets/widgetmapper.h" "qcs-qtwidgets/widgetmapper.h"
 */

class QCS_QTWIDGETS_EXPORT WidgetMapper : public QObject
{
    Q_OBJECT

public:
    WidgetMapper(QObject *parent = nullptr);
    ~WidgetMapper() override;

    /** @brief Maps existing label and widget to an ID.
     *
     * The label will automatically receive its text and it will follow the widget's
     * enabled/visible properties.
     *
     */
    template <typename ControllerId>
    void addLabelAndWidget(ControllerId id, QLabel* label, QWidget* widget)
    {
        const auto internalId = static_cast<int>(id);
        addLabelsAndWidgetsImpl(internalId, {label}, {widget});
    }

    /** @brief Maps existing labels and widgets to an ID.
     *
     * This can be useful if two widgets and their corresponding labels are mapped
     * to the same ID but are located on different pages inside the dialog.
     * @sa addLabelAndWidget()
     */
    template <typename ControllerId>
    void addLabelsAndWidgets(ControllerId id,
                             std::initializer_list<QLabel*> labels,
                             std::initializer_list<QWidget*> widgets)
    {
        const auto internalId = static_cast<int>(id);
        addLabelsAndWidgetsImpl(internalId, labels, widgets);
    }

    /**
     * @brief Maps existing widgets to an ID.
     *
     * Mapping multiple widgets can be useful if two widgets are mapped
     * to the same ID but are located on different pages inside the dialog.
     */
    template <typename ControllerId, typename ... WidgetType>
    void addWidgets(ControllerId id, WidgetType* ... widgets)
    {
        static_assert(Packuru::Utils::IsBaseOf<QWidget, WidgetType...>::value);

        const auto internalId = static_cast<int>(id);
        const std::initializer_list<QWidget*> widgetList {widgets...};
        addLabelsAndWidgetsImpl(internalId, {}, widgetList);
    }

    /**
     * @brief Adds buddies for corresponding input widgets.
     *
     * The main use case for a "buddy" widget is for additional widgets inside dialogs
     * to follow state of the input widget mapped to the same ID, so e.g. entire dialog section
     * can be hidden or disabled.
     */
    template <typename ControllerId, typename ... WidgetType>
    void addBuddies(ControllerId id, WidgetType* ... widgets)
    {
        static_assert(Packuru::Utils::IsBaseOf<QWidget, WidgetType...>::value);

        const auto internalId = static_cast<int>(id);
        const std::initializer_list<QWidget*> widgetList {widgets...};
        addBuddiesImpl(internalId, widgetList);
    }

    /**
     @brief Connects to ControllerEngine and fetches state for all widgets that
     have been mapped so far.

     @note The engine can be set only once.
     @warning Passing an invalid pointer is not supported.

     @sa updateWidgets()
     */
    void setEngine(qcs::core::ControllerEngine* engine);

    /**
     @brief Fetches state for all widgets that haven't been updated yet.

     Sometimes it might not be certain if all widgets have already been mapped when the setEngine()
     function is called. In such cases it is possible to first set the ControllerEngine and then
     map and update widgets in batches.
     */
    void updateWidgets();

    /** @brief Checks whether the Visible property for widgets associated with specified ID is set.

      This information can be useful when dealing with UI issues not covered by Controller System
      e.g. keyboard focus.
     */
    template <typename ControllerId>
    bool isSetVisible(ControllerId id) const
    {
        const auto internalId = static_cast<int>(id);
        return isSetVisibleImpl(internalId);
    }

    /** @brief Checks whether the Enabled property for widgets associated with specified ID is set.

      This information can be useful when dealing with UI issues not covered by Controller System
      e.g. keyboard focus.
     */
    template <typename ControllerId>
    bool isSetEnabled(ControllerId id) const
    {
        const auto internalId = static_cast<int>(id);
        return isSetVisibleImpl(internalId);
    }

private:
    void addLabelsAndWidgetsImpl(int internalId,
                                 std::initializer_list<QLabel*> labels,
                                 std::initializer_list<QWidget*> widgets);
    void addBuddiesImpl(int internalId,
                        const std::initializer_list<QWidget*>& widgets);

    bool isSetVisibleImpl(int internalId) const;
    bool isSetEnabledImpl(int internalId) const;

    struct Private;
    std::unique_ptr<Private> priv;
};

}
