// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QLineEdit>
#include <QSlider>
#include <QLabel>
#include <QCheckBox>
#include <QComboBox>
#include <QSpinBox>

#include "utils/localholder.h"
#include "utils/private/unordered_map_utils.h"
#include "utils/qvariant_utils.h"

#include "qcs-core/controllerproperty.h"

#include "widgethandler.h"


using namespace Packuru;
using namespace qcs::core;


namespace qcs::qtw
{

struct WidgetHandler::Private
{
    void onWidgetValueChanged(const QVariant& value);

    WidgetHandler* publ = nullptr;
    bool updatingWidgets = false;
};


WidgetHandler::WidgetHandler(QObject *parent)
    : QObject(parent),
      priv(new Private)
{
    priv->publ = this;
}


WidgetHandler::~WidgetHandler()
{

}


void WidgetHandler::connectWidget(QWidget* widget)
{
    const auto onWidgetValueChanged = [priv = priv.get()] (const QVariant& value)
    { priv->onWidgetValueChanged(value); };

    if (auto w = qobject_cast<QLineEdit*>(widget))
        connect(w, &QLineEdit::textChanged,
                this, onWidgetValueChanged);

    else if (auto w = qobject_cast<QComboBox*>(widget))
        connect(w, &QComboBox::currentTextChanged,
                this, onWidgetValueChanged);

    else if (auto w = qobject_cast<QCheckBox*>(widget))
        connect(w, &QCheckBox::toggled,
                this, onWidgetValueChanged);

    else if (auto w = qobject_cast<QSlider*>(widget))
        connect(w, &QSlider::valueChanged,
                this, onWidgetValueChanged);

    else if (auto w = qobject_cast<QSpinBox*>(widget))
        connect(w, static_cast<void (QSpinBox::*)(int)>(&QSpinBox::valueChanged),
                this, onWidgetValueChanged);
}


void WidgetHandler::updateWidgets(const UpdateData& updateData)
{
    const Utils::LocalHolder<bool, false> lh(&priv->updatingWidgets, true);

    const auto& properties = updateData.properties;

    const QVariant currentValue = Utils::tryValue(properties, ControllerProperty::PublicCurrentValue);

    if (updateData.widgets)
    {
        for (auto widget : *updateData.widgets)
        {
            if (auto w = qobject_cast<QLineEdit*>(widget))
            {
                if(currentValue.canConvert<QString>())
                    w->setText(currentValue.toString());

                const QVariant text = Utils::tryValue(properties, ControllerProperty::PlaceholderText);
                if (Utils::containsType<QString>(text))
                    w->setPlaceholderText(text.toString());
            }
            else if (auto w = qobject_cast<QComboBox*>(widget))
            {
                const QVariant valueSet = Utils::tryValue(properties, ControllerProperty::PublicValueList);

                if(valueSet.canConvert<QList<QString>>())
                {
                    w->clear();
                    w->insertItems(0, valueSet.value<QList<QString>>());
                }

                if(currentValue.canConvert<QString>())
                    w->setCurrentText(currentValue.toString());
            }
            else if (auto w = qobject_cast<QCheckBox*>(widget))
            {
                if(Utils::containsType<bool>(currentValue))
                    w->setChecked(currentValue.toBool());

                const QVariant text = Utils::tryValue(properties, ControllerProperty::Text);
                if (Utils::containsType<QString>(text))
                    w->setText(text.toString());
            }
            else if (auto w = qobject_cast<QSlider*>(widget))
            {
                const QVariant min = Utils::tryValue(properties, ControllerProperty::MinValue);
                const QVariant max = Utils::tryValue(properties, ControllerProperty::MaxValue);
                if (Utils::containsType<int>(min) && Utils::containsType<int>(max))
                    w->setRange(min.toInt(), max.toInt());

                if(currentValue.canConvert<int>())
                    w->setValue(currentValue.toInt());
            }
            else if (auto w = qobject_cast<QSpinBox*>(widget))
            {
                const QVariant min = Utils::tryValue(properties, ControllerProperty::MinValue);
                const QVariant max = Utils::tryValue(properties, ControllerProperty::MaxValue);
                if (Utils::containsType<int>(min) && Utils::containsType<int>(max))
                    w->setRange(min.toInt(), max.toInt());

                if(currentValue.canConvert<int>())
                    w->setValue(currentValue.toInt());

                const QVariant suffix = Utils::tryValue(properties, ControllerProperty::SpinBoxSuffix);
                if (Utils::containsType<QString>(suffix))
                    w->setSuffix(suffix.toString());
            }
            else if (auto w = qobject_cast<QLabel*>(widget))
            {
                if(currentValue.canConvert<QString>())
                    w->setText(currentValue.toString());
            }
            else if (auto w = qobject_cast<QAbstractButton*>(widget))
            {
                const QVariant text = Utils::tryValue(properties, ControllerProperty::Text);
                if (Utils::containsType<QString>(text))
                    w->setText(text.toString());
            }
        }
    }

    if (updateData.labels)
    {
        const QVariant labelText = Utils::tryValue(properties, ControllerProperty::LabelText);
        if (Utils::containsType<QString>(labelText))
        {
            const auto v = labelText.toString();
            for (auto label : *updateData.labels)
                label->setText(v);
        }
    }

    if (updateData.widgets)
    {
        const QVariant toolTip = Utils::tryValue(properties, ControllerProperty::ToolTip);
        if (Utils::containsType<QString>(toolTip))
        {
            const auto v = toolTip.toString();
            for (auto widget : *updateData.widgets)
                widget->setToolTip(v);
        }
    }

    const QVariant visible = Utils::tryValue(properties, ControllerProperty::Visible);
    if (Utils::containsType<bool>(visible))
    {
        const bool v = visible.toBool();

        if (updateData.labels)
            for (auto label : *updateData.labels)
                label->setVisible(v);

        if (updateData.widgets)
            for (auto widget : *updateData.widgets)
                widget->setVisible(v);

        if (updateData.buddies)
            for (auto buddy : *updateData.buddies)
                buddy->setVisible(v);
    }

    const QVariant enabled = Utils::tryValue(properties, ControllerProperty::Enabled);
    if (Utils::containsType<bool>(enabled))
    {
        const bool e = enabled.toBool();

        if (updateData.labels)
            for (auto label : *updateData.labels)
                label->setEnabled(e);

        if (updateData.widgets)
            for (auto widget : *updateData.widgets)
                widget->setEnabled(e);

        if (updateData.buddies)
            for (auto buddy : *updateData.buddies)
                buddy->setEnabled(e);
    }
}


void WidgetHandler::changeCurrentValue(QWidget* widget, const QVariant& value)
{
    const Utils::LocalHolder<bool, false> lh(&priv->updatingWidgets, true);

    if (auto w = qobject_cast<QLineEdit*>(widget))
        w->setText(value.toString());
    else if (auto w = qobject_cast<QComboBox*>(widget))
        w->setCurrentText(value.toString());
    else if (auto w = qobject_cast<QCheckBox*>(widget))
        w->setChecked(value.toBool());
    else if (auto w = qobject_cast<QSlider*>(widget))
        w->setValue(value.toInt());
    else if (auto w = qobject_cast<QSpinBox*>(widget))
        w->setValue(value.toInt());
    else if (auto w = qobject_cast<QLabel*>(widget))
        w->setText(value.toString());
}


void WidgetHandler::Private::onWidgetValueChanged(const QVariant& value)
{
    if (updatingWidgets)
        return;

    auto widget = qobject_cast<QWidget*>(publ->sender());
    Q_ASSERT(widget);

    emit publ->sigCurrentValueChanged(widget, value);
}

}
