// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>
#include <unordered_set>

#include <QObject>

#include "qcs-core/controllerpropertymap.h"


class QLabel;


namespace qcs::qtw
{

class WidgetHandler : public QObject
{
    Q_OBJECT
public:
    using LabelSet = std::unordered_set<QLabel*>;
    using WidgetSet = std::unordered_set<QWidget*>;

    struct UpdateData
    {
        UpdateData(const qcs::core::ControllerPropertyMap& properties)
            : properties(properties) {}

        LabelSet* labels = nullptr;
        WidgetSet* widgets = nullptr;
        WidgetSet* buddies = nullptr;
        const qcs::core::ControllerPropertyMap& properties;
    };

    explicit WidgetHandler(QObject *parent = nullptr);
    ~WidgetHandler() override;

    void connectWidget(QWidget* widget);

    void updateWidgets(const UpdateData& updateData);

    void changeCurrentValue(QWidget* widget, const QVariant& value);

signals:
    void sigCurrentValueChanged(QWidget*, const QVariant&);

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}
