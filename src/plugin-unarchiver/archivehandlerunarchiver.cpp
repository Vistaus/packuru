// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../core/private/plugin-api/archivetype.h"

#include "../plugin-unarchiver-core/backendhandler.h"

#include "archivehandlerunarchiver.h"


using Packuru::Core::ArchiveType;


namespace Packuru::Plugins::Unarchiver
{

ArchiveHandlerUnarchiver::ArchiveHandlerUnarchiver()
{

}


std::unordered_set<ArchiveType> ArchiveHandlerUnarchiver::supportedTypesForReading() const
{
    return
    {
        ArchiveType::_7z,
                ArchiveType::ace,
                ArchiveType::alz,
                ArchiveType::ar,
                ArchiveType::arc,
                ArchiveType::arj,
                ArchiveType::bzip2,
                ArchiveType::cab,
                ArchiveType::cpio,
                ArchiveType::deb,
                ArchiveType::gzip,
                ArchiveType::iso9660,
                ArchiveType::lha,
                ArchiveType::lzip,
                ArchiveType::lzma,
                ArchiveType::lzop,
                ArchiveType::mdx,
                ArchiveType::msi,
                ArchiveType::nrg,
                ArchiveType::pak,
                ArchiveType::pdf,
                ArchiveType::rar,
                ArchiveType::raw_optical,
                ArchiveType::rpm,
                ArchiveType::sit,
                ArchiveType::swf,
                ArchiveType::tar,
                ArchiveType::tar_bzip2,
                ArchiveType::tar_gzip,
                ArchiveType::tar_lzip,
                ArchiveType::tar_lzma,
                ArchiveType::tar_lzop,
                ArchiveType::tar_xz,
                ArchiveType::tar_compress,
                ArchiveType::xar,
                ArchiveType::xz,
                ArchiveType::compress,
                ArchiveType::zip,
    };
}


std::unique_ptr<Packuru::Core::AbstractBackendHandler> ArchiveHandlerUnarchiver::createBackendHandler() const
{
    return std::make_unique<Core::BackendHandler>();
}

}
