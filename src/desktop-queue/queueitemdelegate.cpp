// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QApplication>
#include <QPainter>

#include "../core-queue/taskqueuemodel.h"

#include "queueitemdelegate.h"


QueueItemDelegate::QueueItemDelegate(QObject* parent)
    : QStyledItemDelegate(parent)
{

}


void QueueItemDelegate::paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
{
    auto model = qobject_cast<const Packuru::Core::Queue::TaskQueueModel*>(index.model());

    if (!model || index.column() != model->progressColumn())
        return QStyledItemDelegate::paint(painter, option, index);

    QApplication::style()->drawControl(QStyle::CE_ItemViewItem, &option, painter);

    const int progress = index.data(Qt::DisplayRole).toInt();

    if (progress < 0)
        return;

    QStyleOptionProgressBar progressBarOption;
    progressBarOption.state = QStyle::State_Enabled;
    progressBarOption.direction = QApplication::layoutDirection();
    progressBarOption.rect = option.rect;
    progressBarOption.fontMetrics = QApplication::fontMetrics();
    progressBarOption.minimum = 0;
    progressBarOption.maximum = 100;
    progressBarOption.textAlignment = Qt::AlignCenter;
    progressBarOption.textVisible = true;

    progressBarOption.progress = progress;
    progressBarOption.text = QString::number(progress) + QLatin1Char('%');

    QApplication::style()->drawControl(QStyle::CE_ProgressBar, &progressBarOption, painter);
}
