// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QCloseEvent>
#include <QDesktopServices>
#include <QSettings>
#include <QLabel>
#include <QTimer>

#include "utils/makeqpointer.h"

#include "core/aboutdialogcore.h"
#include "core/commonstrings.h"
#include "core/extractionfileoverwritepromptcore.h"
#include "core/extractionfolderoverwritepromptcore.h"
#include "core/extractiontypemismatchpromptcore.h"
#include "core/passwordpromptcore.h"
#include "core/creationnameerrorpromptcore.h"

#include "core-queue/mainwindowcore.h"
#include "core-queue/taskqueuemodel.h"

#include "desktop-core/errorlogwidget.h"
#include "desktop-core/aboutdialog.h"
#include "desktop-core/extractionfileoverwriteprompt.h"
#include "desktop-core/extractionfolderoverwriteprompt.h"
#include "desktop-core/extractiontypemismatchprompt.h"

#include "queuemainwindow.h"
#include "ui_queuemainwindow.h"
#include "queueitemdelegate.h"
#include "archivecreationnameerrorprompt.h"
#include "queuepasswordprompt.h"


using namespace Packuru::Core::Queue;
using namespace Packuru::Core;
using Packuru::Utils::makeQPointer;


struct QueueMainWindow::Private
{
    void updateStatusBar(bool queueRunning);
    void updateWindowTitle(const QString& queueDescription);
    void updateViewTaskLogAction();

    QueueMainWindow* publ = nullptr;
    std::unique_ptr<Ui::QueueMainWindow> ui;

    MainWindowCore* windowCore = nullptr;
    QLabel* statusLabel = nullptr;

};


QueueMainWindow::QueueMainWindow(Packuru::Core::Queue::MainWindowCore* core, QWidget *parent)
    : QMainWindow(parent),
      priv(new Private)
{
    auto& ui = priv->ui;

    ui.reset(new Ui::QueueMainWindow);
    ui->setupUi(this);

    priv->publ = this;
    priv->windowCore = core;

    auto queueModel = priv->windowCore->getTaskQueueModel();

    const auto description = queueModel->getQueueDescription();
    priv->updateWindowTitle(description);
    connect(queueModel, &TaskQueueModel::queueDescriptionChanged,
            this, [publ = this] (const auto& description)
    {
        publ->priv->updateWindowTitle(description);
    });

    ui->taskQueueView->setModel(queueModel);
    ui->taskQueueView->setItemDelegate(new QueueItemDelegate(ui->taskQueueView));

    ui->taskQueueView->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->taskQueueView, &QTreeView::customContextMenuRequested,
            [ui = ui.get()] (const QPoint &pos)
    {
        const auto index = ui->taskQueueView->indexAt(pos);

        if (!index.isValid())
            ui->taskQueueView->selectionModel()->clear();

        const auto rows = ui->taskQueueView->selectionModel()->selectedRows();

        QMenu menu;

        if (rows.size() == 0)
        {
            menu.addAction(ui->actionRemove_completed);
        }
        else
        {
            menu.addAction(ui->actionReset_task);
            menu.addAction(ui->actionRemove_task);
            menu.addAction(ui->actionStop_here);
            menu.addAction(ui->actionError_log);
        }

        menu.exec(ui->taskQueueView->mapToGlobal(pos));
    });

    ui->actionStart_queue->setVisible(!queueModel->isRunning() && queueModel->unfinishedTaskCount() > 0);
    ui->actionStop_queue->setVisible(queueModel->isRunning());

    connect(ui->actionStart_queue, &QAction::triggered,
            queueModel, &TaskQueueModel::startQueue);

    connect(ui->actionStop_queue, &QAction::triggered,
            queueModel, &TaskQueueModel::stopQueue);

    connect(queueModel, &TaskQueueModel::runningChanged,
            ui->actionStart_queue, [action = ui->actionStart_queue] (bool value)
    {
        action->setVisible(!value);
    });


    priv->statusLabel = new QLabel(this);
    statusBar()->addWidget(priv->statusLabel);

    priv->updateStatusBar(queueModel->isRunning());

    connect(queueModel, &TaskQueueModel::runningChanged,
            this, [publ = this] (auto running)
    {
        publ->priv->updateStatusBar(running);
    });

    connect(ui->actionSettings, &QAction::triggered,
            this, &QueueMainWindow::settingsRequested);

    connect(queueModel, &TaskQueueModel::runningChanged,
            ui->actionStop_queue, &QAction::setVisible);

    connect(ui->actionRemove_completed, &QAction::triggered,
            queueModel, &TaskQueueModel::removeCompletedTasks);

    ui->actionRemove_completed->setEnabled(queueModel->getCompletedTaskCount() > 0);

    connect(queueModel, &TaskQueueModel::completedTaskCountChanged,
            ui->actionRemove_completed, &QAction::setEnabled);

    ui->actionReset_task->setEnabled(ui->taskQueueView->selectionModel()->hasSelection());
    connect(ui->actionReset_task, &QAction::triggered,
            [model = queueModel, view = ui->taskQueueView] ()
    {
        const auto indexes = view->selectionModel()->selectedRows();
        model->resetTasks(indexes);
    });

    connect(ui->taskQueueView->selectionModel(), &QItemSelectionModel::selectionChanged,
            [model = ui->taskQueueView->selectionModel(), action = ui->actionReset_task] ()
    { action->setEnabled(model->hasSelection()); });

    connect(queueModel, &TaskQueueModel::modelReset,
            this, [publ = this] ()
    {
        auto model = publ->priv->ui->taskQueueView->selectionModel();
        publ->priv->ui->actionReset_task->setEnabled(model->hasSelection());
    });

    ui->actionRemove_task->setEnabled(ui->taskQueueView->selectionModel()->hasSelection());
    connect(ui->actionRemove_task, &QAction::triggered,
            queueModel, [model = queueModel, view = ui->taskQueueView] ()
    {
        auto selection = view->selectionModel()->selection();

        std::sort(selection.begin(), selection.end(),
                  [] (const auto& range1, const auto& range2)
        { return range1.top() > range2.top(); });

        for (const auto& range : qAsConst(selection))
            model->removeRows(range.top(), range.height(), QModelIndex());
    });

    connect(ui->taskQueueView->selectionModel(), &QItemSelectionModel::selectionChanged,
            [model = ui->taskQueueView->selectionModel(), action = ui->actionRemove_task] ()
    { action->setEnabled(model->hasSelection()); });

    connect(queueModel, &TaskQueueModel::modelReset,
            this, [publ = this] ()
    {
        auto ui = publ->priv->ui.get();
        auto model = publ->priv->ui->taskQueueView->selectionModel();
        ui->actionRemove_task->setEnabled(model->hasSelection());

        const auto currentIndex = ui->taskQueueView->currentIndex();

        ui->actionStop_here->setEnabled(currentIndex.isValid());

        auto queueModel = publ->priv->windowCore->getTaskQueueModel();
        ui->actionError_log->setEnabled(currentIndex.isValid() && queueModel->hasErrorLog(currentIndex));
    });

    connect(ui->actionStop_here, &QAction::triggered,
            [model = queueModel, selectionModel = ui->taskQueueView->selectionModel()] ()
    { model->stopAfterTask(selectionModel->currentIndex()); });

    ui->actionStop_here->setEnabled(ui->taskQueueView->currentIndex().isValid());
    connect(ui->taskQueueView->selectionModel(), &QItemSelectionModel::currentChanged,
            [action = ui->actionStop_here, model = ui->taskQueueView]
            (const QModelIndex&)
    {  action->setEnabled(model->currentIndex().isValid()); });

    connect(ui->actionError_log, &QAction::triggered,
            queueModel, [model = queueModel, window = this] ()
    {
        const auto index = window->priv->ui->taskQueueView->selectionModel()->currentIndex();
        ErrorLogWidget w(window);
        w.setText(model->getErrorLog(index));
        w.exec();
    });

    priv->updateViewTaskLogAction();

    const auto currentIndex = ui->taskQueueView->currentIndex();
    ui->actionError_log->setEnabled(ui->taskQueueView->currentIndex().isValid()
                                    && queueModel->hasErrorLog(currentIndex));

    connect(ui->taskQueueView->selectionModel(), &QItemSelectionModel::currentChanged,
            this, [publ = this] ()
    {
        publ->priv->updateViewTaskLogAction();
    });

    connect(queueModel, &TaskQueueModel::taskStateChanged,
            this, [publ = this] ()
    {
        publ->priv->updateViewTaskLogAction();
    });

    connect(queueModel, &TaskQueueModel::openDestinationFolder,
            [] (const auto& url) { QDesktopServices::openUrl(url); });

    connect(queueModel, &TaskQueueModel::openNewArchive,
            [] (const QUrl& url) { QDesktopServices::openUrl(url); });

    connect(queueModel, &TaskQueueModel::allTasksCompleted,
            this, &QueueMainWindow::allTasksCompleted);

    connect(queueModel, &TaskQueueModel::passwordNeeded,
            this, [window = this] (auto promptCore)
    {
        auto prompt = new QueuePasswordPrompt(promptCore, window);

        const auto width = prompt->width();
        const auto height = prompt->height();

        prompt->setFixedSize({width, height});
        prompt->show();

    });

    connect(queueModel, &TaskQueueModel::archiveCreationNameError,
            this, [window = this] (auto promptCore)
    {
        auto prompt = new ArchiveCreationNameErrorPrompt(promptCore, window);

        // Wait before showing it to the user; previous prompt might not have yet disappeared
        QTimer::singleShot(100, prompt, &QWidget::show);
    });

    connect(queueModel, &TaskQueueModel::fileExists,
            this, [window = this] (auto promptCore)
    {
        auto prompt = new ExtractionFileOverwritePrompt(promptCore, false, window);

        // Wait before showing it to the user; previous prompt might not have yet disappeared
        QTimer::singleShot(100, prompt, &ExtractionFileOverwritePrompt::showAndSetup);
    });

    connect(queueModel, &TaskQueueModel::folderExists,
            this, [window = this] (auto promptCore)
    {
        auto prompt = new ExtractionFolderOverwritePrompt(promptCore, false, window);

        // Wait before showing it to the user; previous prompt might not have yet disappeared
        QTimer::singleShot(100, prompt, &ExtractionFolderOverwritePrompt::showAndSetup);
    });

    connect(queueModel, &TaskQueueModel::itemTypeMismatch,
            this, [window = this] (auto promptCore)
    {
        auto prompt = new ExtractionTypeMismatchPrompt(promptCore, false, window);

        // Wait before showing it to the user; previous prompt might not have yet disappeared
        QTimer::singleShot(100, prompt, &ExtractionTypeMismatchPrompt::showAndSetup);
    });

    connect(ui->actionNew, &QAction::triggered,
            this, &QueueMainWindow::newArchiveDialogRequested);

    connect(ui->actionExtract, &QAction::triggered,
            this, &QueueMainWindow::extractionDialogRequested);

    connect(ui->actionTest, &QAction::triggered,
            this, &QueueMainWindow::testDialogRequested);

    connect(ui->actionAbout, &QAction::triggered,
            [win = this] ()
    {
        auto d = new AboutDialog(win);
        connect(d, &QDialog::rejected, &QObject::deleteLater);
        d->open();
    });

    connect(ui->actionQuit, &QAction::triggered,
            this, &QueueMainWindow::closeRequested);

    QSettings s;
    const QSize size = s.value("queue_main_window_size").toSize();
    if (size.isValid())
        resize(size);

    ui->taskQueueView->header()->restoreState(s.value("queue_main_view_header_state").toByteArray());

    ui->actionAbout->setText(core->getString(MainWindowCore::UserString::ActionAbout));
    ui->actionSettings->setText(core->getString(MainWindowCore::UserString::ActionSettings));
    ui->actionError_log->setText(TaskQueueModel::getString(TaskQueueModel::UserString::ActionErrorLog));
    ui->actionQuit->setText(core->getString(MainWindowCore::UserString::ActionQuit));
    ui->actionExtract->setText(core->getString(MainWindowCore::UserString::ActionExtract));
    ui->actionNew->setText(core->getString(MainWindowCore::UserString::ActionNew));
    ui->actionRemove_completed->setText(TaskQueueModel::getString(TaskQueueModel::UserString::ActionRemoveCompleted));
    ui->actionRemove_task->setText(TaskQueueModel::getString(TaskQueueModel::UserString::ActionRemoveTask));
    ui->actionReset_task->setText(TaskQueueModel::getString(TaskQueueModel::UserString::ActionResetTask));
    ui->actionStart_queue->setText(TaskQueueModel::getString(TaskQueueModel::UserString::ActionStartQueue));
    ui->actionStop_here->setText(TaskQueueModel::getString(TaskQueueModel::UserString::ActionStopHere));
    ui->actionStop_queue->setText(TaskQueueModel::getString(TaskQueueModel::UserString::ActionStopQueue));
    ui->actionTest->setText(core->getString(MainWindowCore::UserString::ActionTest));
}


QueueMainWindow::~QueueMainWindow()
{
    QSettings s;
    s.setValue("queue_main_window_size", size());
    s.setValue("queue_main_view_header_state", priv->ui->taskQueueView->header()->saveState());
}


void QueueMainWindow::closeEvent(QCloseEvent *event)
{
    event->ignore();
    emit closeRequested();
}


void QueueMainWindow::showEvent(QShowEvent *event)
{
    event->accept();
}


void QueueMainWindow::moveEvent(QMoveEvent *event)
{
    event->accept();
}


void QueueMainWindow::Private::updateStatusBar(bool queueRunning)
{
    if (queueRunning)
        statusLabel->setText(QueueMainWindow::tr("Queue is running"));
    else
        statusLabel->setText(QueueMainWindow::tr("Queue stopped"));
}


void QueueMainWindow::Private::updateWindowTitle(const QString& queueDescription)
{
    const auto name = windowCore->getString(MainWindowCore::UserString::WindowTitle);
    if (queueDescription.isEmpty())
        publ->setWindowTitle(name);
    else
        publ->setWindowTitle(queueDescription + QLatin1String(" - ") + name);
}


void QueueMainWindow::Private::updateViewTaskLogAction()
{
    const auto currentIndex = ui->taskQueueView->currentIndex();

    ui->actionError_log->setEnabled(currentIndex.isValid()
                                    && windowCore->getTaskQueueModel()->hasErrorLog(currentIndex));
}
