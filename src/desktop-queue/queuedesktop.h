// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QObject>


namespace Packuru::Core::Queue
{
class QueueCore;
}

namespace Packuru::Core
{
class ArchivingDialogCore;
class ExtractionDialogCore;
class TestDialogCore;
}

class QueueDesktop : public QObject
{
    Q_OBJECT
public:
    QueueDesktop(Packuru::Core::Queue::QueueCore* queueCore, QObject* parent = nullptr);
    ~QueueDesktop() override;

signals:
    void quitApplication();

private:
    void onCommandLineError(const QString& info);
    void onCommandLineHelpRequested(const QString& info);
    void onArchivingDialogCoreCreated(Packuru::Core::ArchivingDialogCore* dialogCore);
    void onExtractionDialogCoreCreated(Packuru::Core::ExtractionDialogCore* dialogCore);
    void onTestDialogCoreCreated(Packuru::Core::TestDialogCore* dialogCore);
    void onCloseMainWindowRequested();
    void onLastDialogClosed();
    void onSettingsRequested();
    void onDefaultStartUp();
    void onAnotherInstanceDefaultStartUp();
    void showMainWindow();

    Packuru::Core::Queue::QueueCore* queueCore;
    std::unique_ptr<class QueueMainWindow> mainWindow;
    int openedDialogs = 0;
};
