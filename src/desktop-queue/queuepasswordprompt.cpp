// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QIcon>
#include <QCloseEvent>
#include <QAction>

#include "../utils/makeqpointer.h"

#include "../core/passwordpromptcore.h"

#include "../desktop-core/passwordedithandler.h"

#include "queuepasswordprompt.h"
#include "ui_queuepasswordprompt.h"


using Packuru::Utils::makeQPointer;
using namespace Packuru::Core;


QueuePasswordPrompt::QueuePasswordPrompt(PasswordPromptCore* promptCore, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::QueuePasswordPrompt)
{
    ui->setupUi(this);

    setWindowFlag(Qt::Dialog);
    setWindowTitle(promptCore->getString(PasswordPromptCore::UserString::DialogTitle));
    ui->abortButton->setText(promptCore->getString(PasswordPromptCore::UserString::Abort));

    ui->okButton->setIcon(QIcon::fromTheme("dialog-ok"));
    ui->abortButton->setIcon(QIcon::fromTheme("dialog-cancel"));

    const auto enterPassword = [publ = this, promptCore = makeQPointer(promptCore)] ()
    {
        const auto password = publ->ui->passwordEdit->text();
        if (password.isEmpty())
            return;

        publ->setEnabled(false);
        if (promptCore)
            promptCore->enterPassword(password);
    };

    connect(ui->passwordEdit, &QLineEdit::returnPressed,
            this, enterPassword);

    connect(ui->okButton, &QPushButton::clicked,
            this, enterPassword);

    ui->okButton->setEnabled(false);

    connect(ui->passwordEdit, &QLineEdit::textChanged,
            this, [publ = this] (const QString& text)
    { publ->ui->okButton->setEnabled(!text.isEmpty()); });

    new PasswordEditHandler(ui->passwordEdit, PasswordEditHandler::WidgetRole::Main, this);

    const auto abortPrompt = [prompt = this, promptCore = makeQPointer(promptCore)] ()
    {
        prompt->setEnabled(false);
        if (promptCore)
            promptCore->abort();
    };

    connect(ui->abortButton, &QPushButton::clicked,
            this, abortPrompt);

    auto abortAction = new QAction(this);
    abortAction->setShortcut(Qt::Key_Escape);
    connect(abortAction, &QAction::triggered,
            this, abortPrompt);
    addAction(abortAction);

    connect(promptCore, &QObject::destroyed,
            this, &QObject::deleteLater);

    const auto icon = QIcon::fromTheme("document-encrypted");
    ui->iconLabel->setPixmap(icon.pixmap({48, 48}));
    setWindowIcon(icon);
}


QueuePasswordPrompt::~QueuePasswordPrompt()
{
    delete ui;
}


void QueuePasswordPrompt::closeEvent(QCloseEvent *event)
{
    event->ignore();
}
