<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl">
<context>
    <name>QueueMainWindow</name>
    <message>
        <location filename="../queuemainwindow.ui" line="60"/>
        <source>&amp;Queue</source>
        <translation>Wa&amp;chtrij</translation>
    </message>
    <message>
        <location filename="../queuemainwindow.ui" line="73"/>
        <source>&amp;Archive</source>
        <translation>&amp;Archief</translation>
    </message>
    <message>
        <location filename="../queuemainwindow.ui" line="81"/>
        <source>A&amp;pplication</source>
        <translation>&amp;Programma</translation>
    </message>
    <message>
        <location filename="../queuemainwindow.ui" line="127"/>
        <source>Ctrl+Return</source>
        <translation>Ctrl+Enter</translation>
    </message>
    <message>
        <location filename="../queuemainwindow.ui" line="139"/>
        <source>Ctrl+Backspace</source>
        <translation>Ctrl+Backspace</translation>
    </message>
    <message>
        <location filename="../queuemainwindow.ui" line="151"/>
        <source>F5</source>
        <translation>F5</translation>
    </message>
    <message>
        <location filename="../queuemainwindow.ui" line="163"/>
        <source>Ctrl+Shift+Del</source>
        <translation>Ctrl+Shift+Del</translation>
    </message>
    <message>
        <location filename="../queuemainwindow.ui" line="175"/>
        <source>Del</source>
        <translation>Del</translation>
    </message>
    <message>
        <location filename="../queuemainwindow.ui" line="187"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="../queuemainwindow.ui" line="199"/>
        <source>F11</source>
        <translation>F11</translation>
    </message>
    <message>
        <location filename="../queuemainwindow.ui" line="211"/>
        <source>Shift+S</source>
        <translation>Shift+S</translation>
    </message>
    <message>
        <location filename="../queuemainwindow.ui" line="223"/>
        <source>Shift+L</source>
        <translation>Shift+L</translation>
    </message>
    <message>
        <location filename="../queuemainwindow.ui" line="235"/>
        <source>Ctrl+N</source>
        <translation>Ctrl+N</translation>
    </message>
    <message>
        <location filename="../queuemainwindow.ui" line="247"/>
        <source>F6</source>
        <translation>F6</translation>
    </message>
    <message>
        <location filename="../queuemainwindow.ui" line="259"/>
        <source>F8</source>
        <translation>F8</translation>
    </message>
    <message>
        <location filename="../queuemainwindow.cpp" line="371"/>
        <source>Queue is running</source>
        <translation>Er staan items in de wachtrij</translation>
    </message>
    <message>
        <location filename="../queuemainwindow.cpp" line="373"/>
        <source>Queue stopped</source>
        <translation>Er staan geen items in de wachtrij</translation>
    </message>
</context>
</TS>
