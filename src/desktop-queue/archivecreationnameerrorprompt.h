// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QDialog>


namespace Ui {
class ArchiveCreationNameErrorPrompt;
}

namespace Packuru::Core
{
class CreationNameErrorPromptCore;
}

class ArchiveCreationNameErrorPrompt : public QDialog
{
    Q_OBJECT
public:
    explicit ArchiveCreationNameErrorPrompt(Packuru::Core::CreationNameErrorPromptCore* promptCore,
                                            QWidget *parent = nullptr);
    ~ArchiveCreationNameErrorPrompt();

private:
    Ui::ArchiveCreationNameErrorPrompt *ui;
    Packuru::Core::CreationNameErrorPromptCore* promptCore;
};

