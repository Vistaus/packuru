// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QApplication>

#include "../core/appinfo.h"

#include "../core-queue/queuecore.h"
#include "../core-queue/init.h"

#include "queuedesktop.h"


using namespace Packuru::Core;
using namespace Packuru::Core::Queue;


int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QApplication app(argc, argv);
    app.setApplicationVersion(AppInfo::getAppVersion());
    app.setOrganizationName(AppInfo::getAppName());
    app.setApplicationName(AppInfo::getAppName() + " Desktop");

    Init coreInit(QUEUE_EXEC_NAME);

    if (coreInit.messagePrimaryInstance())
        return 0;

    QueueDesktop queueDesktop(coreInit.getQueueCore());

    QObject::connect(&queueDesktop, &QueueDesktop::quitApplication,
                     &app, &QApplication::quit);

    coreInit.processArguments();

    return app.exec();
}
