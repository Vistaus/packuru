// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QIcon>

#include "../core-queue/settingsdialogcore.h"

#include "queuesettingsdialog.h"
#include "queuesettingspage.h"


using namespace Packuru::Core;
using namespace Packuru::Core::Queue;

QueueSettingsDialog::QueueSettingsDialog(AppSettingsDialogCore* appSettingsCore,
                                         SettingsDialogCore* queueSettingsCore,
                                         QWidget *parent)
    : SettingsDialog(appSettingsCore, parent)
{
    const QString queuePageTitle = SettingsDialogCore::getString(SettingsDialogCore::UserString::PageTitle);

    auto queuePage = new QueueSettingsPage(queueSettingsCore->getControllerEngine(),
                                           queuePageTitle);

    insertSettingsPage(1,
                       QIcon::fromTheme("media-playback-start"),
                       queuePageTitle,
                       queuePage);

    connect(this, &QDialog::accepted,
            queueSettingsCore, &SettingsDialogCore::accept);
}

QueueSettingsDialog::~QueueSettingsDialog()
{

}

