// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QMainWindow>


namespace Packuru::Core::Queue
{
class MainWindowCore;
}


class QueueMainWindow : public QMainWindow
{
    Q_OBJECT
public:
    explicit QueueMainWindow(Packuru::Core::Queue::MainWindowCore* core, QWidget *parent = nullptr);
    ~QueueMainWindow() override;

signals:
    void newArchiveDialogRequested();
    void extractionDialogRequested();
    void testDialogRequested();
    void closeRequested();
    void settingsRequested();
    void allTasksCompleted();

private:
    void closeEvent(QCloseEvent *event) override;
    void showEvent(QShowEvent *event) override;
    void moveEvent(QMoveEvent *event) override;

    struct Private;
    std::unique_ptr<Private> priv;
};
