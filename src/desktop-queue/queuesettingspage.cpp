// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../qcs-qtwidgets/widgetmapper.h"

#include "../core-queue/settingscontrollertype.h"
#include "../core-queue/settingsdialogcore.h"

#include "queuesettingspage.h"
#include "ui_queuesettingspage.h"


using Packuru::Core::Queue::SettingsDialogCore;


QueueSettingsPage::QueueSettingsPage(qcs::core::ControllerEngine* engine,
                                     const QString& pageTitle,
                                     QWidget *parent)
    : QWidget(parent),
      ui(new Ui::QueueSettingsPage),
      engine(engine)
{
    ui->setupUi(this);

    ui->queuePageTitle->setText(pageTitle);

    QFont font;
    font.setPointSizeF(font.pointSizeF() * 1.2);
    font.setBold(true);

    ui->queuePageTitle->setFont(font);

    using Type = Packuru::Core::Queue::SettingsControllerType::Type;
    mapper = new qcs::qtw::WidgetMapper(this);

    mapper->addLabelAndWidget(Type::AbortTasksRequiringPassword,
                              ui->abortEncryptedLabel,
                              ui->abortEncryptedCheck);
    mapper->addLabelAndWidget(Type::AutoCloseOnAllTasksCompletion,
                              ui->quitIfAllCompletedLabel,
                              ui->autoCloseCheckBox);
    mapper->addLabelAndWidget(Type::AutoRemoveCompletedTasks,
                              ui->autoRemoveCompletedLabel,
                              ui->autoRemoveCompletedCheck);
    mapper->addLabelAndWidget(Type::MaxErrorCount,
                              ui->maxErrorCountLabel,
                              ui->maxErrorCountSpinBox);

    mapper->setEngine(engine);
}


QueueSettingsPage::~QueueSettingsPage()
{
    delete ui;
}
