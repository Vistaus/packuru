// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QWidget>


namespace Ui {
class QueueSettingsPage;
}


namespace qcs::core
{
class ControllerEngine;
}

namespace qcs::qtw
{
class WidgetMapper;
}


class QueueSettingsPage : public QWidget
{
    Q_OBJECT

public:
    explicit QueueSettingsPage(qcs::core::ControllerEngine* engine,
                               const QString& pageTitle,
                               QWidget *parent = nullptr);
    ~QueueSettingsPage();

private:
    Ui::QueueSettingsPage *ui;
    qcs::core::ControllerEngine* engine;
    qcs::qtw::WidgetMapper* mapper;
};
