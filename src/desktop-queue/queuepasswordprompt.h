// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QDialog>


namespace Ui {
class QueuePasswordPrompt;
}


namespace Packuru::Core
{
class PasswordPromptCore;
}

class QueuePasswordPrompt : public QWidget
{
    Q_OBJECT

public:
    explicit QueuePasswordPrompt(Packuru::Core::PasswordPromptCore* promptCore, QWidget *parent = nullptr);
    ~QueuePasswordPrompt() override;

private:
    void closeEvent(QCloseEvent* event) override;

    Ui::QueuePasswordPrompt *ui;
};

