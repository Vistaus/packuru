// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

import PackuruMobileCore 1.0 as PackuruMobileCore
import PackuruCore 1.0


PackuruMobileCore.AutoResizableDialog {
    id: root

    property QtObject promptCore

    Connections {
        target: promptCore
        function onDeleted() {
            root.close()
        }
    }

    headerText: promptCore ? promptCore.getString(PasswordPromptCore.DialogTitle) : ""
    closePolicy: Popup.NoAutoClose

    contentItem: ColumnLayout {
        spacing: 20
        ColumnLayout {
            spacing: 0
            Label {
                text: qsTr("Archive")
                opacity: 0.5
            }
            Label {
                text: promptCore ? promptCore.getArchiveName() : ""
            }
        }
        PackuruMobileCore.PasswordField {
            id: passwordField
            Layout.fillWidth: true
            echoMode: TextInput.Password
            onAccepted: {
                if (text.length > 0)
                    promptCore.enterPassword(text)
            }
            focus: true
        }
    }

    footer: DialogButtonBox {
        implicitWidth: contentWidth

        Button {
            text: "OK"
            enabled: passwordField.text.length > 0
            onClicked: promptCore.enterPassword(passwordField.text)
            DialogButtonBox.buttonRole: DialogButtonBox.AcceptRole
        }
        Button {
            text: promptCore ? promptCore.getString(PasswordPromptCore.Abort) : ""
            onClicked: promptCore.abort()
            DialogButtonBox.buttonRole: DialogButtonBox.RejectRole
        }
    }

}
