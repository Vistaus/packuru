// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

import PackuruMobileCore 1.0 as PackuruMobileCore
import PackuruCore 1.0

PackuruMobileCore.AutoResizableDialog {
    id: root

    property QtObject promptCore

    Connections {
        target: promptCore
        function onDeleted() {
            root.close()
        }
    }

    headerText: promptCore ? promptCore.getString(ExtractionFolderOverwritePromptCore.FolderAlreadyExists) : ""
    closePolicy: Popup.NoAutoClose
    contentItem: PackuruMobileCore.ExtractionFolderOverwritePrompt {
        promptCore: root.promptCore
        headerVisible: false
        maxContentWidth: root.maxContentWidth
    }

    footer.visible:false
}
