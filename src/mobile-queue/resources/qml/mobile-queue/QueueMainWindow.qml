// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.4

import org.kde.kirigami 2.5 as Kirigami

import KirigamiActivities 1.0 as KirigamiActivities

import PackuruCore 1.0
import PackuruCoreQueue 1.0
import PackuruMobileCore 1.0 as PackuruMobileCore


Kirigami.AbstractApplicationWindow {
    id: root
    visible: true
    width: 600
    height: 1000
    title: (queueDescription.length === 0 ? "" : queueDescription + " - ")
           + mainWindowCore.getString(MainWindowCore.WindowTitle)

    readonly property MainWindowCore mainWindowCore: queueCore.getMainWindowCore()
    readonly property string queueDescription: mainWindowCore.getTaskQueueModel().description
    property int openedDialogs: 0

    onClosing: {
        var unfinishedTaskCount = mainWindowCore.getTaskQueueModel().unfinishedTaskCount()

        if (unfinishedTaskCount > 0 || openedDialogs > 0)
        {
            close.accepted = false
            if (closingDialogLoader.active)
                closingDialogLoader.active = false
            closingDialogLoader.active = true
            closingDialogLoader.item.unfinishedTasks = unfinishedTaskCount
            closingDialogLoader.item.openedDialogs = openedDialogs
            closingDialogLoader.item.open()
        }
    }

    Loader {
        id: closingDialogLoader
        active: false
        sourceComponent: QueueClosingDialog {
            parent: root.contentItem
            onClosed: closingDialogLoader.active = false
        }
    }

    KirigamiActivities.SwitchableActivity {
        id: mainActivity
        active: true
        initialActivity: TaskQueueActivity {
            id: queueActivity
            taskQueueModel: mainWindowCore.getTaskQueueModel()
            onViewTaskLogRequested: {
                openInfoDialog(commonStrings.getString(CommonStrings.ErrorLogDialogTitle), log);
            }
        }

        onActivityRemoved: {
            activity.destroy()
        }

        property QtObject settingsActivity

        function showSettings() {
            if (settingsActivity)
                return
            var comp = Qt.createComponent("QueueSettingsActivity.qml")
            settingsActivity = comp.createObject(mainActivity,
                                                 {
                                                     "appSettingsDialogCore" : queueCore.createAppSettingsDialogCore(),
                                                     "queueSettingsDialogCore" : queueCore.createQueueSettingsDialogCore()
                                                 })
            pushActivity(settingsActivity, true)
        }
    }

    wideScreen: pageStack ? width >= pageStack.defaultColumnWidth * 1.5 : false
    pageStack: mainActivity.pageRow

    Component {
        id: archivingDialogComp
        PackuruMobileCore.ArchivingDialog {}
    }

    Component {
        id: extractionDialogComp
        PackuruMobileCore.ExtractionDialog {}
    }

    Component {
        id: testDialogComp
        PackuruMobileCore.TestDialog {}
    }

    Component {
        id: infoDialogComp
        PackuruMobileCore.InformationDialog {}
    }

    Component {
        id: aboutDialogComp
        PackuruMobileCore.AboutDialog {}
    }

    Connections {
        target: queueCore

        function onCommandLineError(info) {
            openInfoDialog(commonStrings.getString(CommonStrings.CommandLineErrorDialogTitle), info);

        }
        function onCommandLineHelpRequested(info) {
            openInfoDialog(commonStrings.getString(CommonStrings.CommandLineHelpDialogTitle), info);
        }

        function onDefaultStartUp() {}
        function onNewTasks() {}
        function onArchivingDialogCoreCreated(core) {
            var dialog = archivingDialogComp.createObject(root, {"dialogCore" : core})
            dialog.destroyOnClose = true
            dialog.closed.connect(function() { if (root) --root.openedDialogs })
            dialog.headerText = core.getString(ArchivingDialogCore.DialogTitleCreate)
            dialog.open()
            ++root.openedDialogs
        }
        function onExtractionDialogCoreCreated(core) {
            var dialog = extractionDialogComp.createObject(root, {"dialogCore" : core})
            dialog.destroyOnClose = true
            dialog.closed.connect(function() { if (root) --root.openedDialogs })
            dialog.headerText = core.getString(ExtractionDialogCore.DialogTitleMulti)
            dialog.open()
            ++root.openedDialogs
        }
        function onTestDialogCoreCreated(core) {
            var dialog = testDialogComp.createObject(root, {"dialogCore" : core})
            dialog.destroyOnClose = true
            dialog.closed.connect(function() { if (root) --root.openedDialogs })
            dialog.headerText = core.getString(TestDialogCore.DialogTitleMulti)
            dialog.open()
            ++root.openedDialogs
        }
    }

    Connections {
        target: mainWindowCore
        function onAllTasksCompletedReadyToClose() {
            if (queueActivity.current && root.openedDialogs == 0)
            {
                root.contentItem.enabled = false
                globalDrawer.enabled = false
                contextDrawer.enabled = false
                shutDownTimer.start()
            }
        }
    }

    Connections {
        target: mainWindowCore.getTaskQueueModel()
        function onPasswordNeeded(promptCore) {
            var comp = Qt.createComponent("QueuePasswordPrompt.qml")
            var dialog = comp.createObject(root, {"promptCore" : promptCore})
            dialog.destroyOnClose = true
            dialog.open();
        }
        function onArchiveCreationNameError(promptCore) {
            var comp = Qt.createComponent("ArchiveCreationNameErrorPrompt.qml")
            var dialog = comp.createObject(root, {"promptCore" : promptCore})
            dialog.destroyOnClose = true
            dialog.open();
        }
        function onFileExists(promptCore) {
            var comp = Qt.createComponent("QueueExtractionFileOverwritePrompt.qml")
            var dialog = comp.createObject(root, {"promptCore" : promptCore})
            dialog.destroyOnClose = true
            dialog.open();
        }
        function onFolderExists(promptCore) {
            var comp = Qt.createComponent("QueueExtractionFolderOverwritePrompt.qml")
            var dialog = comp.createObject(root, {"promptCore" : promptCore})
            dialog.destroyOnClose = true
            dialog.open();
        }
        function onItemTypeMismatch(promptCore) {
            var comp = Qt.createComponent("QueueExtractionTypeMismatchPrompt.qml")
            var dialog = comp.createObject(root, {"promptCore" : promptCore})
            dialog.destroyOnClose = true
            dialog.open();
        }
    }

    Timer {
        id: shutDownTimer
        interval: 1000
        onTriggered: Qt.quit()
    }

    function openInfoDialog(title, text) {
        var dialog = infoDialogComp.createObject(root)
        dialog.closed.connect(function() { --root.openedDialogs })
        dialog.headerText = title
        dialog.text = text
        dialog.open();
        ++root.openedDialogs
    }

    globalDrawer: Kirigami.GlobalDrawer {
        id: globalDrawer
        title: mainWindowCore.getString(MainWindowCore.WindowTitle)
        enabled: mainActivity.globalDrawerAvailable
        contentItem.enabled: mainActivity.globalDrawerAvailable

        actions: [
            Kirigami.Action {
                text: mainWindowCore.getString(MainWindowCore.ActionNew)
                iconName: "document-new"
                onTriggered: {
                    queueCore.createArchivingDialogCore()
                }
            },
            Kirigami.Action {
                text: mainWindowCore.getString(MainWindowCore.ActionExtract)
                iconName: "archive-extract"
                onTriggered: {
                    queueCore.createExtractionDialogCore()
                }
            },
            Kirigami.Action {
                text: mainWindowCore.getString(MainWindowCore.ActionTest)
                iconName: "dialog-ok"
                onTriggered: {
                    queueCore.createTestDialogCore()
                }
            },
            Kirigami.Action {
                text: mainWindowCore.getString(MainWindowCore.ActionSettings)
                iconName: "configure"
                onTriggered: {
                    mainActivity.showSettings()
                }
            },
            Kirigami.Action {
                text: mainWindowCore.getString(MainWindowCore.ActionAbout)
                iconName: "help-about-symbolic"
                onTriggered: {
                    var dialog = aboutDialogComp.createObject(root)
                    dialog.destroyOnClose = true
                    dialog.open()
                }
            },
            Kirigami.Action {
                text: mainWindowCore.getString(MainWindowCore.ActionQuit)
                iconName: "application-exit"
                onTriggered: {
                    root.close()
                }
            }
        ]
    }

    contextDrawer: Kirigami.ContextDrawer{

    }

}
