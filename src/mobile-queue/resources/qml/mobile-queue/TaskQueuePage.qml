// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3

import org.kde.kirigami 2.3 as Kirigami

import PackuruCoreQueue 1.0


Kirigami.ScrollablePage {
    property QtObject taskQueueModel

    Connections {
        target: taskQueueModel

        function onOpenDestinationFolder(url) {
            Qt.openUrlExternally(url)
        }

        function onOpenNewArchive(url) {
            Qt.openUrlExternally(url)
        }
    }

    title: "Queue"
    signal viewTaskLogRequested(string log)

    actions.main: Kirigami.Action {
        iconName: taskQueueModel.running ? "media-playback-stop" : "media-playback-start"
        text: taskQueueModel.running ? taskQueueModel.getString(TaskQueueModel.ActionStopQueue)
                                     : taskQueueModel.getString(TaskQueueModel.ActionStartQueue)
        onTriggered: {
            if (taskQueueModel.running)
                taskQueueModel.stopQueue()
            else
                taskQueueModel.startQueue()
        }
    }

    actions.left: Kirigami.Action {
        enabled: taskQueueModel.completedTaskCount > 0
        text: taskQueueModel.getString(TaskQueueModel.ActionRemoveCompleted)
        iconName: "dialog-ok"
        onTriggered: {
            taskQueueModel.removeCompletedTasks()
        }
    }

   /* contextualActions: [
        Kirigami.Action {
            text: qsTr("Reset selected")
            iconName: "view-refresh"
            onTriggered: {
            }
        },
        Kirigami.Action {
            text: qsTr("Remove selected")
            iconName: "edit-delete"
            onTriggered: {
            }
        }

    ]
*/
    ListView {
        anchors.fill: parent
        id: view
        model: taskQueueModel
        delegate: delegate1

        Component {
            id: delegate1
            Kirigami.SwipeListItem {
                id: listItem

                actions: [
                    Kirigami.Action {
                        text: taskQueueModel.getString(TaskQueueModel.ActionResetTask)
                        iconName: "view-refresh"
                        tooltip: text
                        onTriggered: {
                            var i = taskQueueModel.index(index, 0)
                            taskQueueModel.resetTasks([i])
                        }
                    },
                    Kirigami.Action {
                        text: taskQueueModel.getString(TaskQueueModel.ActionRemoveTask)
                        iconName: "edit-delete"
                        tooltip: text
                        onTriggered: {
                            taskQueueModel.removeRows(index, 1)
                        }
                    },
                    Kirigami.Action {
                        text: taskQueueModel.getString(TaskQueueModel.ActionStopHere)
                        iconName: "media-playback-stop"
                        tooltip: text
                        onTriggered: {
                            var i = taskQueueModel.index(index, 0)
                            taskQueueModel.stopAfterTask(i)
                        }
                    },
                    Kirigami.Action {
                        text: taskQueueModel.getString(TaskQueueModel.ActionErrorLog)
                        iconName: "viewlog"
                        tooltip: text
                        enabled: taskLogAvailable
                        onTriggered: {
                            var i = taskQueueModel.index(index, 0)
                            root.viewTaskLogRequested(taskQueueModel.getErrorLog(i))
                        }
                    }
                ]

                Item{
                    implicitHeight: topLayout.implicitHeight
                    RowLayout {
                        id: topLayout
                        anchors.fill: parent

                        Kirigami.Icon {
                            id: iconItem
                            source: decoration
                            Layout.preferredHeight: 50
                            Layout.preferredWidth: Layout.preferredHeight
                        }

                        ColumnLayout
                        {
                            RowLayout {
                                Label {
                                    id: labelItem
                                    Layout.fillWidth: true
                                    text: taskInfo
                                }

                                Label {
                                    opacity: 0.5
                                    text: taskStateInfo
                                }
                            }

                            RowLayout {
                                Label {
                                    opacity: 0.5
                                    Layout.fillWidth: true
                                    text: archiveName
                                }

                                Label {
                                    text: taskProgress + "%"
                                    opacity: 0.5
                                    visible: taskProgress > 0
                                }
                            }

                            Label {
                                text: taskError ? taskError : ""
                                opacity: 0.5
                                visible: text != ""
                            }
                        }
                    }
                }
            }
        }
    }
}
