// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

import PackuruMobileCore 1.0 as PackuruMobileCore
import PackuruCore 1.0


PackuruMobileCore.AutoResizableDialog {
    id: root

    property QtObject promptCore

    Connections {
        target: promptCore
        function onDeleted() {
            root.close()
        }
    }

    headerText: promptCore.getString(CreationNameErrorPromptCore.DialogTitle)
    closePolicy: Popup.NoAutoClose

    contentItem: ColumnLayout {
        Label {
            text: promptCore.getString(CreationNameErrorPromptCore.CannotCreateArchive) + " " + promptCore.archiveName() + "."
        }
        Label {
            text: promptCore.errorInfo()
        }
        Label {
            text: promptCore.getString(CreationNameErrorPromptCore.YouCanChangeArchiveName)
        }

        ColumnLayout {
            spacing: 0
            Label {
                text: promptCore.getString(CreationNameErrorPromptCore.Name)
                opacity: 0.5
            }
            TextField {
                id: nameField
                text: promptCore.archiveName()
                focus: true
                Layout.fillWidth: true
            }
        }
    }

    footer: DialogButtonBox {
            implicitWidth: contentWidth

        Button {
            text: promptCore.getString(CreationNameErrorPromptCore.Retry)
            onClicked: promptCore.retry(nameField.text)
            DialogButtonBox.buttonRole: DialogButtonBox.AcceptRole
        }
        Button {
            text: promptCore.getString(CreationNameErrorPromptCore.Abort)
            onClicked: promptCore.abort()
            DialogButtonBox.buttonRole: DialogButtonBox.RejectRole
        }
    }
}
