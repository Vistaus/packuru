# SPDX-FileCopyrightText: The Packuru Developers
#
# SPDX-License-Identifier: 0BSD

include(../common-src.pri)
include(../common-mobile.pri)
include(../common-exec.pri)

MODULE_BUILD_NAME = $$QUEUE_BUILD_NAME
MODULE_BUILD_NAME_SUFFIX = $$QUEUE_BUILD_NAME_SUFFIX

include(../embed-qm-files.pri)

TARGET = $$MODULE_BUILD_NAME

DEFINES += MODULE_QML_DIR=\\\"$${QML_ROOT_DIR}/$${MODULE_BUILD_NAME_SUFFIX}\\\"

LIBS += -L$${PROJECT_TARGET_LIB_DIR}
LIBS += -l$$QCS_QQCONTROLS_BUILD_NAME
LIBS += -l$${CORE_BUILD_NAME} -l$${CORE_QUEUE_BUILD_NAME} -l$${MOBILE_CORE_BUILD_NAME}

TRANSLATIONS += \
    translations/$${MODULE_BUILD_NAME_SUFFIX}_en.ts \

RESOURCES += resources/qml/$${MODULE_BUILD_NAME_SUFFIX}_qml.qrc

SOURCES += \
        main.cpp
