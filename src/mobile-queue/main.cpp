// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "../core/globalsettingsmanager.h"
#include "../core/creationnameerrorpromptcore.h"
#include "../core/appinfo.h"
#include "../core/commonstrings.h"

#include "../core-queue/queuecore.h"
#include "../core-queue/taskqueuemodel.h"
#include "../core-queue/mainwindowcore.h"
#include "../core-queue/settingsdialogcore.h"
#include "../core-queue/settingscontrollertype.h"
#include "../core-queue/init.h"

#include "../mobile-core/registertypes.h"


using namespace Packuru::Core::Queue;
using namespace Packuru::Core;


void registerTypes();

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);
    app.setApplicationVersion(AppInfo::getAppVersion());
    app.setOrganizationName(AppInfo::getAppName());
    app.setApplicationName(AppInfo::getAppName() + " Mobile");

    Init coreInit(QUEUE_EXEC_NAME);

    if (coreInit.messagePrimaryInstance())
        return 0;

    Packuru::Mobile::Core::registerTypes();

    registerTypes();

    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("globalSettingsManager", GlobalSettingsManager::instance());
    engine.rootContext()->setContextProperty("queueCore", coreInit.getQueueCore());
    CommonStrings commonStrings;
    engine.rootContext()->setContextProperty("commonStrings", &commonStrings);

    engine.load(QLatin1String(MODULE_QML_DIR) + "/QueueMainWindow.qml");
    if (engine.rootObjects().isEmpty())
        return -1;

    coreInit.processArguments();

    return app.exec();
}


void registerTypes()
{
    qmlRegisterUncreatableType<SettingsControllerType>("PackuruCoreQueue", 1, 0, "SettingsControllerType", "");
    qmlRegisterUncreatableType<QueueCore>("PackuruCoreQueue", 1, 0, "QueueCore", "");
    qmlRegisterUncreatableType<MainWindowCore>("PackuruCoreQueue", 1, 0, "MainWindowCore", "");
    qmlRegisterUncreatableType<TaskQueueModel>("PackuruCoreQueue", 1, 0, "TaskQueueModel", "");
    qmlRegisterUncreatableType<SettingsDialogCore>("PackuruCoreQueue", 1, 0, "SettingsDialogCore", "");
    qmlRegisterUncreatableType<CreationNameErrorPromptCore>("PackuruCoreQueue", 1, 0, "CreationNameErrorPromptCore", "");

}
