// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QFileDialog>
#include <QVBoxLayout>
#include <QSettings>
#include <QAction>

#include "../qcs-qtwidgets/pagestack.h"
#include "../qcs-qtwidgets/widgetmapper.h"

#include "../core/archivingdialogcore.h"
#include "../core/archivingdialogcontrollertype.h"
#include "../core/commonstrings.h"

#include "archivingdialog.h"
#include "ui_archivingdialog.h"
#include "private/filelistwidget.h"
#include "private/archivepage.h"
#include "passwordedithandler.h"


using namespace Packuru::Core;


namespace Ui {
class ArchivingDialog;
}

struct ArchivingDialogPrivate
{
    ArchivingDialogPrivate(ArchivingDialog* publ, ArchivingDialogCore* core);
    ~ArchivingDialogPrivate();

    void onAddFolderRequested();
    void onAddFilesRequested();
    void onRemoveItemsRequested();

    Ui::ArchivingDialog *ui;
    ArchivingDialog* publ;
    ArchivingDialogCore* core;
    qcs::qtw::WidgetMapper* mapper;
    ArchivePage* archivePage;
    qcs::qtw::PageStack* pluginPageStack;
    FileListWidget* fileListWidget;
    QIcon updatedPageIcon;
    QString updatedPageToolTip;
};


ArchivingDialog::ArchivingDialog(ArchivingDialogCore* core, QWidget *parent)
    : QDialog(parent),
      priv(new ArchivingDialogPrivate(this, core))
{
    QSettings s;
    const QSize size = s.value("archiving_dialog_size").toSize();
    if (size.isValid())
        resize(size);
}


ArchivingDialog::~ArchivingDialog()
{
    QSettings s;
    s.setValue("archiving_dialog_size", size());
    delete priv;

}


ArchivingDialogPrivate::ArchivingDialogPrivate(ArchivingDialog* publ, ArchivingDialogCore* core)
    : ui(new Ui::ArchivingDialog),
      publ(publ),
      core(core),
      mapper(new qcs::qtw::WidgetMapper(publ)),
      archivePage(nullptr),
      pluginPageStack(new qcs::qtw::PageStack(publ)),
      fileListWidget(nullptr),
      updatedPageIcon(QIcon::fromTheme("dialog-warning")),
      updatedPageToolTip(ArchivingDialog::tr("Page updated"))
{
    ui->setupUi(publ);

    core->setParent(publ);

    ui->compressionHeaderLabel->setText(core->getString(ArchivingDialogCore::UserString::Compression));
    ui->encryptionHeaderLabel->setText(core->getString(ArchivingDialogCore::UserString::Encryption));
    ui->filePathsHeaderLabel->setText(core->getString(ArchivingDialogCore::UserString::FilePaths));
    ui->pluginSettingsHeader->setText(core->getString(ArchivingDialogCore::UserString::PluginSettings));
    ui->tabWidget->setTabText(0, core->getString(ArchivingDialogCore::UserString::TabOptions));
    ui->tabWidget->setTabText(1, core->getString(ArchivingDialogCore::UserString::TabAdvanced));
    ui->rejectButton->setText(core->getString(ArchivingDialogCore::UserString::Cancel));

    QObject::connect(ui->acceptButton, &QPushButton::clicked,
                     publ, &ArchivingDialog::accept);

    QObject::connect(ui->rejectButton, &QPushButton::clicked,
                     publ, &ArchivingDialog::reject);

    QObject::connect(publ, &QDialog::accepted,
                     core, &ArchivingDialogCore::accept);

    ui->advancedGlobalParameters->hide();

    using Type = ArchivingDialogControllerType::Type;

    mapper->addLabelAndWidget(Type::CompressionLevel,
                              ui->compressionLevelLabel,
                              ui->compressionSlider);
    mapper->addLabelAndWidget(Type::CompressionState,
                              ui->compressionStateLabel,
                              ui->compressionStatus);
    mapper->addLabelAndWidget(Type::InternalPath,
                              ui->internalPathLabel,
                              ui->internalPathEdit);
    mapper->addLabelAndWidget(Type::FilePaths,
                              ui->filePathsModeLabel,
                              ui->filePathsModeCombo);
    mapper->addWidgets(Type::RunInQueue, ui->runInQueueCheck);

    mapper->addBuddies(Type::CompressionState,
                       ui->compressionHeaderLabel);

    mapper->addBuddies(Type::CompressionLevel,
                       ui->compressionLevelMinValue,
                       ui->compressionLevelMaxValue);

    const auto icon = QIcon::fromTheme("dialog-error");
    ui->errorLabel->setPixmap(icon.pixmap({22, 22}));

    fileListWidget = new FileListWidget(core, mapper, true, publ);

    ui->tabWidget->insertTab(0, fileListWidget, core->getString(ArchivingDialogCore::UserString::TabFiles));

    QObject::connect(fileListWidget, &FileListWidget::addFolderRequested,
                     [priv = this] () { priv->onAddFolderRequested(); });

    QObject::connect(fileListWidget, &FileListWidget::addFilesRequested,
                     [priv = this] () { priv->onAddFilesRequested(); });

    QObject::connect(fileListWidget, &FileListWidget::removeItemsRequested,
                     [priv = this] () { priv->onRemoveItemsRequested(); });

    if (core->getMode() == ArchivingDialogCore::DialogMode::CreateArchive)
    {
        archivePage = new ArchivePage(publ, core, mapper);
        ui->tabWidget->insertTab(1, archivePage, core->getString(ArchivingDialogCore::UserString::TabArchive));

        // If no files provided
        if (core->getFileModel()->rowCount() == 0)
        {
            ui->tabWidget->setCurrentIndex(0); // File list
            fileListWidget->setFocus();
        }
        else
        {
            ui->tabWidget->setCurrentIndex(1); // Archive
            archivePage->setFocus();
        }

        QObject::connect(archivePage, &ArchivePage::archiveTypeChanged,
                         [ui = ui, icon = updatedPageIcon, toolTip = updatedPageToolTip] ()
        {
            int index = ui->tabWidget->indexOf(ui->optionsPage);
            ui->tabWidget->setTabIcon(index, icon);
            ui->tabWidget->setTabToolTip(index, toolTip);
            index =  ui->tabWidget->indexOf(ui->advancedPage);
            ui->tabWidget->setTabIcon(index, icon);
            ui->tabWidget->setTabToolTip(index, toolTip);
        });

        QObject::connect(ui->tabWidget, &QTabWidget::currentChanged,
                         [ui = ui] (int index)
        {
            ui->tabWidget->setTabIcon(index, QIcon());
            ui->tabWidget->setTabToolTip(index, "");
        });
    }
    else
    {
        ui->tabWidget->setCurrentIndex(0); // Files
        ui->runInQueueCheck->setFocus();
    }

    mapper->addLabelAndWidget(Type::EncryptionState,
                              ui->encryptionStateLabel,
                              ui->encryptionStateInfo);
    mapper->addLabelAndWidget(Type::EncryptionMode,
                              ui->encryptionModeLabel,
                              ui->encryptionModeCombo);
    mapper->addLabelAndWidget(Type::Password,
                              ui->passwordLabel,
                              ui->passwordEdit);
    mapper->addLabelAndWidget(Type::PasswordRepeat,
                              ui->repeatLabel,
                              ui->repeatEdit);
    mapper->addWidgets(Type::PasswordMatch, ui->passwordMatchLabel);
    mapper->addWidgets(Type::DialogErrors, ui->errorLabel);
    mapper->addWidgets(Type::AcceptButton, ui->acceptButton);

    mapper->addBuddies(Type::EncryptionState,
                       ui->encryptionHeaderLabel);

    mapper->setEngine(core->getControllerEngine());

    auto hnd = new PasswordEditHandler(ui->passwordEdit, PasswordEditHandler::WidgetRole::Main, publ);
    auto hnd2 = new PasswordEditHandler(ui->repeatEdit, PasswordEditHandler::WidgetRole::Confirm, publ);

    hnd->interconnect(hnd2);

    ui->compressionLevelMinValue->setText(QString::number(ui->compressionSlider->minimum()));
    ui->compressionLevelMaxValue->setText(QString::number(ui->compressionSlider->maximum()));

    QObject::connect(ui->compressionSlider, &QAbstractSlider::rangeChanged,
                     [minLabel = ui->compressionLevelMinValue, maxLabel = ui->compressionLevelMaxValue]
                     (auto min, auto max)
    {
        minLabel->setText(QString::number(min));
        maxLabel->setText(QString::number(max));
    });

    if (auto mapper = core->getPluginRowMapper())
    {
        auto layout = new QVBoxLayout;
        layout->addWidget(pluginPageStack);
        ui->stackWidgetContainer->setLayout(layout);
        pluginPageStack->setCurrentPage(mapper);
    }

    QObject::connect(core, &ArchivingDialogCore::pluginRowMapperChanged,
                     [priv = this] ()
    {
        auto mapper = priv->core->getPluginRowMapper();
        priv->pluginPageStack->setCurrentPage(mapper);
    });
}


ArchivingDialogPrivate::~ArchivingDialogPrivate()
{
    delete ui;
}


void ArchivingDialogPrivate::onAddFolderRequested()
{
    const QString folder = QFileDialog::getExistingDirectory(publ);

    if (folder.isEmpty())
        return;

    core->addItems({QUrl(folder)});
}


void ArchivingDialogPrivate::onAddFilesRequested()
{
    const auto files = QFileDialog::getOpenFileUrls(publ);
    core->addItems(files);
}


void ArchivingDialogPrivate::onRemoveItemsRequested()
{
    const auto indexes = fileListWidget->getSelectedIndexes();
    core->removeItems(indexes);
}
