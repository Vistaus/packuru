// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QWidget>


namespace Ui {
class ExtractionFileOverwritePrompt;
}

namespace Packuru::Core
{
class ExtractionFileOverwritePromptCore;
}


class ExtractionFileOverwritePrompt : public QWidget
{
    Q_OBJECT

public:
    explicit ExtractionFileOverwritePrompt(Packuru::Core::ExtractionFileOverwritePromptCore* promptCore,
                                           bool browserMode,
                                           QWidget *parent = nullptr);
    ~ExtractionFileOverwritePrompt() override;

    void showAndSetup();

private:
    void closeEvent(QCloseEvent* event) override;
    bool eventFilter(QObject *watched, QEvent *event) override;

    Ui::ExtractionFileOverwritePrompt *ui;
    Packuru::Core::ExtractionFileOverwritePromptCore* promptCore;
};
