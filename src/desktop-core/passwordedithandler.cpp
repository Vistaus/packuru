// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QIcon>
#include <QAction>

#include "passwordedithandler.h"


struct PasswordEditHandler::Private
{
    Private(PasswordEditHandler* publ, QLineEdit* edit, WidgetRole role);

    void interconnect(PasswordEditHandler* handler);
    void setEchoMode(QLineEdit::EchoMode mode, bool silent);

    PasswordEditHandler* publ;
    const WidgetRole role;
    QLineEdit* edit = nullptr;
    PasswordEditHandler* connectedTo =nullptr;
    QAction* toggleEchoModeAction;
    const QIcon visibleIcon = QIcon::fromTheme(QStringLiteral("visibility"));
    const QIcon hiddenIcon = QIcon::fromTheme(QStringLiteral("hint"));
};


PasswordEditHandler::PasswordEditHandler(QLineEdit* edit, WidgetRole role, QObject* parent)
    : QObject(parent),
      priv(new Private(this, edit, role))

{

}


PasswordEditHandler::~PasswordEditHandler()
{

}


void PasswordEditHandler::interconnect(PasswordEditHandler* handler)
{
    priv->interconnect(handler);
}


PasswordEditHandler::Private::Private(PasswordEditHandler* publ, QLineEdit* edit, WidgetRole role)
    : publ(publ),
      role(role),
      edit(edit),
      toggleEchoModeAction(new QAction(edit))
{
    QKeySequence shortcut(Qt::CTRL + Qt::Key_H);
    toggleEchoModeAction->setShortcut(shortcut);
    toggleEchoModeAction->setShortcutContext(Qt::WidgetShortcut);
    toggleEchoModeAction->setToolTip(tr("Toggle password visibility (") + shortcut.toString() + ")");

    if (role == WidgetRole::Main)
        edit->addAction(toggleEchoModeAction, QLineEdit::TrailingPosition);
    else
        edit->addAction(toggleEchoModeAction);

    setEchoMode(QLineEdit::Password, false);

    QObject::connect(toggleEchoModeAction, &QAction::triggered,
                     publ, [priv = this, edit] ()
    {
        auto mode = edit->echoMode();

        if (mode == QLineEdit::EchoMode::Password)
            mode = QLineEdit::EchoMode::Normal;
        else
            mode = QLineEdit::EchoMode::Password;

        priv->setEchoMode(mode, false);
    });
}


void PasswordEditHandler::Private::interconnect(PasswordEditHandler* handler)
{
    if (connectedTo || handler->priv->connectedTo || role == handler->priv->role)
        return;

    connectedTo = handler;
    handler->priv->connectedTo = publ;

    connect(publ, &PasswordEditHandler::echoModeChanged,
            handler, [handler] (auto mode) { handler->priv->setEchoMode(mode, true); } );

    connect(handler, &PasswordEditHandler::echoModeChanged,
            publ, [handler = publ] (auto mode) { handler->priv->setEchoMode(mode, true); } );
}


void PasswordEditHandler::Private::setEchoMode(QLineEdit::EchoMode mode, bool silent)
{
    switch (mode)
    {
    case QLineEdit::Password: toggleEchoModeAction->setIcon(hiddenIcon); break;
    default: toggleEchoModeAction->setIcon(visibleIcon); break;
    }

    auto echoMode = edit->echoMode();
    if (mode != echoMode)
    {
        edit->setEchoMode(mode);
        if (!silent)
            emit publ->echoModeChanged(mode);
    }
}
