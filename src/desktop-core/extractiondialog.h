// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QDialog>


namespace Ui {
class ExtractionDialog;
}


namespace Packuru::Core
{
class ExtractionDialogCore;
}


class ExtractionDialog : public QDialog
{
    Q_OBJECT
public:
    ExtractionDialog(Packuru::Core::ExtractionDialogCore* core, QWidget *parent = nullptr);
    ~ExtractionDialog();

private:
    Ui::ExtractionDialog *ui;

    struct Private;
    std::unique_ptr<Private> priv;
};
