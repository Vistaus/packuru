// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QAction>

#include "core/commonstrings.h"

#include "archiveslistwidget.h"
#include "ui_archiveslistwidget.h"


using namespace Packuru::Core;


ArchivesListWidget::ArchivesListWidget(QAbstractItemModel* model, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ArchivesListWidget)
{
    ui->setupUi(this);

    ui->addButton->setText(CommonStrings::getString(CommonStrings::UserString::Add));
    ui->removeButton->setText(CommonStrings::getString(CommonStrings::UserString::Remove));

    ui->archivesView->header()->setSectionResizeMode(QHeaderView::ResizeToContents);
    ui->archivesView->setModel(model);

    connect(ui->archivesView->selectionModel(), &QItemSelectionModel::selectionChanged,
            this, &ArchivesListWidget::onSelectionChanged);

    connect(ui->addButton, &QPushButton::clicked,
            this, &ArchivesListWidget::sigAddArchivesRequested);

    connect(ui->removeButton, &QPushButton::clicked,
            this, &ArchivesListWidget::sigRemoveArchivesRequested);

    auto addAction = new QAction(this);
    addAction->setShortcut(Qt::Key_Insert);
    connect (addAction, &QAction::triggered,
             this, &ArchivesListWidget::sigAddArchivesRequested);
    ui->archivesView->addAction(addAction);

    auto removeAction = new QAction(this);
    removeAction->setShortcut(QKeySequence::Delete);
    connect (removeAction, &QAction::triggered,
             this, &ArchivesListWidget::sigRemoveArchivesRequested);
    ui->archivesView->addAction(removeAction);
}


ArchivesListWidget::~ArchivesListWidget()
{
    delete ui;
}


void ArchivesListWidget::onSelectionChanged(const QItemSelection &selected, const QItemSelection &)
{
    const bool selection = !selected.indexes().empty();
    ui->removeButton->setEnabled(selection);
}


QList<QModelIndex> ArchivesListWidget::getSelectedIndexes() const
{
    return ui->archivesView->selectionModel()->selectedRows();
}
