// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QWidget>


class QAbstractItemModel;
class QItemSelection;


namespace Ui {
class ArchivesListWidget;
}


class ArchivesListWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ArchivesListWidget(QAbstractItemModel* model, QWidget *parent = nullptr);
    ~ArchivesListWidget();

    QList<QModelIndex> getSelectedIndexes() const;

signals:
    void sigAddArchivesRequested();
    void sigRemoveArchivesRequested();

private:
    void onSelectionChanged(const QItemSelection& selected, const QItemSelection&);

    Ui::ArchivesListWidget *ui;
};
