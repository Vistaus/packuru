// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QWidget>


class QDialog;


namespace Ui {
class ArchivePage;
}


namespace qcs::qtw
{
class WidgetMapper;
}


namespace Packuru::Core
{
class ArchivingDialogCore;
}


class ArchivePage : public QWidget
{
    Q_OBJECT
public:
    explicit ArchivePage(QDialog* dialog,
                         Packuru::Core::ArchivingDialogCore* dialogCore,
                         qcs::qtw::WidgetMapper* mapper,
                         QWidget* parent = nullptr);
    ~ArchivePage();

signals:
    void archiveTypeChanged();

private:
    void onDestinationPathButtonClicked();

    Ui::ArchivePage *ui;
    QDialog* dialog;
    Packuru::Core::ArchivingDialogCore* dialogCore;
    qcs::qtw::WidgetMapper* mapper;
};

