// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QWidget>
#include <QModelIndexList>


class QAbstractItemModel;
class QItemSelection;


namespace Ui {
class FileListWidget;
}


namespace Packuru::Core
{
class ArchivingDialogCore;
}

namespace qcs::qtw
{
class WidgetMapper;
}

class FileListWidget : public QWidget
{
    Q_OBJECT
public:
    explicit FileListWidget(Packuru::Core::ArchivingDialogCore* dialogCore,
                            qcs::qtw::WidgetMapper* mapper,
                            bool foldersAllowed,
                            QWidget *parent = nullptr);
    ~FileListWidget();

    QModelIndexList getSelectedIndexes() const;

signals:
    void addFolderRequested();
    void addFilesRequested();
    void removeItemsRequested();

private:
    void onSelectionChanged(const QItemSelection& selected, const QItemSelection&);

    Ui::FileListWidget *ui;
};

