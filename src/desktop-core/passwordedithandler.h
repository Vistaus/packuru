// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QObject>
#include <QLineEdit>


class PasswordEditHandler : public QObject
{
    Q_OBJECT
public:
    enum class WidgetRole {Main, Confirm};

    PasswordEditHandler(QLineEdit* edit,
                        WidgetRole role,
                        QObject* parent = nullptr);
    ~PasswordEditHandler();

    void interconnect(PasswordEditHandler* handler);

signals:
    void echoModeChanged(QLineEdit::EchoMode mode);

private:
    struct Private;
    std::unique_ptr<Private> priv;
};
