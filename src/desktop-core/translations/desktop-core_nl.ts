<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl">
<context>
    <name>ArchivePage</name>
    <message>
        <location filename="../private/archivepage.ui" line="350"/>
        <source>Do not add file extension</source>
        <translation>Geen bestandsextensie gebruiken</translation>
    </message>
</context>
<context>
    <name>ArchivingDialog</name>
    <message>
        <location filename="../archivingdialog.cpp" line="81"/>
        <source>Page updated</source>
        <translation>De pagina is bijgewerkt</translation>
    </message>
</context>
<context>
    <name>PasswordEditHandler</name>
    <message>
        <location filename="../passwordedithandler.cpp" line="58"/>
        <source>Toggle password visibility (</source>
        <translation>Wachtwoord tonen/verbergen (</translation>
    </message>
</context>
</TS>
