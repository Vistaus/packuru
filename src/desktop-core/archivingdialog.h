// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QDialog>


namespace Packuru::Core
{
class ArchivingDialogCore;
}

class ArchivingDialog : public QDialog
{
    Q_OBJECT
public:
    explicit ArchivingDialog(Packuru::Core::ArchivingDialogCore* core, QWidget *parent = nullptr);
    ~ArchivingDialog();

private:
    friend struct ArchivingDialogPrivate;
    struct ArchivingDialogPrivate* priv;
};

