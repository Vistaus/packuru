// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QMessageBox>


class AppClosingDialog : public QMessageBox
{
    Q_OBJECT
public:
    AppClosingDialog(QWidget *parent = nullptr);

    bool quitRequested() const { return quitRequested_; }

private:
    bool quitRequested_ = false;
};

