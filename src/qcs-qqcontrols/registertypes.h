// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "symbol_export.h"

///@file

namespace qcs::qqc
{

/** @brief Registers Controller System's C++ and QML types
  */
QCS_QQCONTROLS_EXPORT void registerTypes();

}
