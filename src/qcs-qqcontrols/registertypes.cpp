// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QQmlEngine>

#include "../qcs-core/controllertype.h"
#include "../qcs-core/controllerengine.h"
#include "../qcs-core/controlleridrowmapper.h"

#include "private/widgetmapper.h"
#include "private/generatedpagecore.h"
#include "private/pagestackcore.h"

#include "registertypes.h"


namespace qcs::qqc
{

void registerTypes()
{
    qmlRegisterType<qcs::qqc::WidgetMapper>("ControllerSystem.QQC", 1, 0, "WidgetMapper");
    qmlRegisterType<qcs::qqc::GeneratedPageCore>("ControllerSystem.QQC", 1, 0, "GeneratedPageCore");
    qmlRegisterType<qcs::qqc::PageStackCore>("ControllerSystem.QQC", 1, 0, "PageStackCore");

    qmlRegisterUncreatableType<qcs::core::ControllerEngine>("ControllerSystem.Core", 1, 0, "ControllerEngine", "");
    qmlRegisterUncreatableType<qcs::core::ControllerIdRowMapper>("ControllerSystem.Core", 1, 0, "ControllerIdRowMapper", "");

    qmlRegisterUncreatableMetaObject(qcs::core::ControllerType::staticMetaObject,  // static meta object
                                     "QCS_ControllerType", // import statement (can be any string)
                                     1, 0,                   // major and minor version of the import
                                     "QCS_ControllerType", // name in QML (does not have to match C++ name)
                                     "" );                   // error in case someone tries to create an object

    const QString qmlDir(QCS_QQCONTROLS_QML_DIR);

    qmlRegisterType(QUrl(qmlDir + "/GeneratedPage.qml"), "ControllerSystem.QQC", 1, 0, "GeneratedPage");
    qmlRegisterType(QUrl(qmlDir + "/PageStack.qml"), "ControllerSystem.QQC", 1, 0, "PageStack");
}

}
