# SPDX-FileCopyrightText: The Packuru Developers
#
# SPDX-License-Identifier: 0BSD

include(../common-src.pri)
include(../common-qcs.pri)

QT += qml quick

MODULE_BUILD_NAME_SUFFIX = $$QCS_QQCONTROLS_BUILD_NAME_SUFFIX
MODULE_BUILD_NAME = $$QCS_QQCONTROLS_BUILD_NAME

QML_ROOT_DIR=qrc:/$${QCS_BASE_BUILD_NAME}/qml
DEFINES += QCS_QQCONTROLS_QML_DIR=\\\"$${QML_ROOT_DIR}/$${MODULE_BUILD_NAME_SUFFIX}\\\"

TARGET = $$qtLibraryTarget($$MODULE_BUILD_NAME)

LIBS += -L$${PROJECT_TARGET_LIB_DIR} -l$$QCS_CORE_BUILD_NAME

DEFINES += $${QCS_MACRO_NAME}_QQCONTROLS_LIBRARY

public_headers.files = $$files(*.h)
public_headers.path = $${PROJECT_API_TARGET_DIR}/$${MODULE_BUILD_NAME}

RESOURCES += resources/qml/$${MODULE_BUILD_NAME_SUFFIX}_qml.qrc

HEADERS += \
    private/generatedpagecore.h \
    private/widgethandler.h \
    private/widgetmapper.h \
    private/pagestackcore.h \
    registertypes.h \
    symbol_export.h

SOURCES += \
    private/generatedpagecore.cpp \
    private/widgethandler.cpp \
    private/widgetmapper.cpp \
    private/pagestackcore.cpp \
    registertypes.cpp

 
