// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.5
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.4

import ControllerSystem.QQC 1.0 as CS_QQC

/**
 @copydoc qcs::qtw::PageStack
 @ingroup ControllerSystemQML
 */
StackLayout {
    id: root

    /// @copydoc qcs::qtw::PageStack::setCurrentPage
    function setCurrentPage(rowMapper) {
        var page = stackCore.getPage(rowMapper)

        if (!page) {
            var comp = Qt.createComponent("GeneratedPage.qml")
            page = comp.createObject(root)
            page.rowMapper = rowMapper
            stackCore.addPage(rowMapper, page)
        }

        priv.changePage(page)
    }

    CS_QQC.PageStackCore {
        id: stackCore
    }

    QtObject {
        id: priv

        function getPageIndex(page) {
            var index = -1

            for (var i = 0; i < root.children.length; ++i)
                if (root.children[i] === page) {
                    index = i
                    break
                }
            return index
        }

        function changePage(page) {
            var index = getPageIndex(page)
            if (index >= 0)
                root.currentIndex = index
        }
    }

}
