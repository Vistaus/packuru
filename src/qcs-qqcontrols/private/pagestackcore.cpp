// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <unordered_map>

#include "pagestackcore.h"


using namespace qcs::core;


namespace qcs::qqc
{

struct PageStackCore::Private
{
    std::unordered_map<ControllerIdRowMapper*, QObject*> mapperToItem;
};


PageStackCore::PageStackCore(QObject *parent)
    : QObject(parent),
      priv(new Private)
{

}

PageStackCore::~PageStackCore()
{

}

void PageStackCore::addPage(ControllerIdRowMapper* mapper, QObject* page)
{
    Q_ASSERT(priv->mapperToItem.find(mapper) == priv->mapperToItem.end());

    priv->mapperToItem[mapper] = page;
}


QObject* PageStackCore::getPage(ControllerIdRowMapper* mapper)
{
    const auto it = priv->mapperToItem.find(mapper);

    if (it == priv->mapperToItem.end())
        return nullptr;
    else {
        return it->second;
    }
}


}
