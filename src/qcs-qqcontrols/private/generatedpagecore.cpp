// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <unordered_map>

#include "utils/localholder.h"

#include "qcs-core/controlleridrowmapper.h"

#include "generatedpagecore.h"
#include "widgethandler.h"


using namespace Packuru;
using namespace qcs::core;


namespace qcs::qqc
{

struct GeneratedPageCore::Private
{
    void addItem(int mapperRow, QObject* label, QObject* item);
    void setMapper(ControllerIdRowMapper* mapper);

    void syncItemToController(int row, const ControllerPropertyMap& properties);
    void onUserChangedValue(QObject* item, const QVariant& value);

    struct RowData{
        QObject* label;
        QObject* item;
    };

    GeneratedPageCore* publ = nullptr;
    ControllerIdRowMapper* mapper_ = nullptr;
    class WidgetHandler* widgetHandler;
    std::unordered_map<int, RowData> rowToData;
    std::unordered_map<QObject*, int> itemToRow;
    bool updatingItems = false;
};


GeneratedPageCore::GeneratedPageCore(QObject *parent)
    : QObject(parent),
      priv(new Private)
{
    priv->publ = this;
    priv->widgetHandler = new WidgetHandler(this);

    connect(priv->widgetHandler, &WidgetHandler::sigCurrentValueChanged,
            this, [priv = priv.get()] (QObject* item, const QVariant& value)
    { priv->onUserChangedValue(item, value); });
}


GeneratedPageCore::~GeneratedPageCore()
{

}


void GeneratedPageCore::addItem(int mapperRow, QObject* label, QObject* item)
{
    priv->addItem(mapperRow, label, item);
}


void GeneratedPageCore::setMapper(ControllerIdRowMapper* mapper)
{
    priv->setMapper(mapper);
}


ControllerIdRowMapper* GeneratedPageCore::getMapper()
{
    return priv->mapper_;
}


void GeneratedPageCore::Private::addItem(int mapperRow, QObject* label, QObject* item)
{
    if (item)
        widgetHandler->connectItem(item);

    itemToRow[item] = mapperRow;
    rowToData[mapperRow] = {label, item};

    Q_ASSERT(mapper_);
    const auto& properties = mapper_->getControllerProperties(mapperRow);

    label->setProperty("text", mapper_->getLabel(mapperRow));
    syncItemToController(mapperRow, properties);
}


void GeneratedPageCore::Private::setMapper(ControllerIdRowMapper* mapper)
{
    if (mapper != mapper_)
    {
        if (mapper_)
            mapper_->disconnect(publ);

        mapper_ = mapper;

        connect(mapper_, &ControllerIdRowMapper::sigControllerPropertiesChanged,
                publ, [priv = this] (int row, const ControllerPropertyMap& properties)
        {priv->syncItemToController(row, properties); });

        emit publ->mapperChanged();
    }
}


void GeneratedPageCore::Private::syncItemToController(int row, const ControllerPropertyMap& properties)
{
    const Utils::LocalHolder<bool, false> lh(&updatingItems, true);

    const auto it = rowToData.find(row);
    Q_ASSERT(it != rowToData.end());

    const auto& data = it->second;

    Q_ASSERT(data.item);
    Q_ASSERT(data.label);


    using ItemSet = WidgetHandler::ItemSet;
    ItemSet buddies {data.label}; // Labels are currently only set as buddies and text is set separately
    ItemSet widgets {data.item};

    WidgetHandler::UpdateData updateData(properties);
    updateData.buddies = &buddies;
    updateData.widgets = &widgets;

    widgetHandler->updateItems(updateData);
}


void GeneratedPageCore::Private::onUserChangedValue(QObject* item, const QVariant& value)
{
    const auto it = itemToRow.find(item);
    Q_ASSERT(it != itemToRow.end());
    const auto row = it->second;

    Q_ASSERT(mapper_);
    mapper_->onUserChangedValue(row, value);
}

}
