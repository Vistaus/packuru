// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QQmlComponent>
#include <QQuickItem>

#include "utils/localholder.h"
#include "utils/private/unordered_map_utils.h"
#include "utils/qvariant_utils.h"

#include "widgethandler.h"
#include "qcs-core/controllerproperty.h"


using namespace Packuru;
using qcs::core::ControllerProperty;
using qcs::core::ControllerPropertyMap;


namespace
{

enum class WidgetType
{
    Button,
    CheckBox,
    ComboBox,
    Item, // Generic QML Item
    Label,
    Slider,
    SpinBox,
    TextField,
    ToolTip,

    ___INVALID
};

WidgetType toWidgetType(QObject* object);

const QLatin1String itemValueTypeKey{"qcs_value_type"};
const QString qmlDir = QCS_QQCONTROLS_QML_DIR;

}

namespace qcs::qqc
{

struct WidgetHandler::Private
{
    static void setObjectsProperty(ItemSet* objects, const char* property, const QVariant& value);

    bool updatingItems = false;
};


WidgetHandler::WidgetHandler(QObject *parent)
    : QObject(parent),
      priv(new Private)
{

}


WidgetHandler::~WidgetHandler()
{

}


void WidgetHandler::connectItem(QObject* item)
{
    const WidgetType controllerType = toWidgetType(item);

    switch (controllerType)
    {
    case WidgetType::CheckBox:
        QObject::connect(item, SIGNAL(checkedChanged()), this, SLOT(onCheckBoxToggled()));
        break;
    case WidgetType::ComboBox:
        QObject::connect(item, SIGNAL(activated(int)), this, SLOT(onComboBoxIndexActivated()));
        break;
    case WidgetType::Slider:
        QObject::connect(item, SIGNAL(valueChanged()), this, SLOT(onSliderValueChanged()));
        break;
    case WidgetType::SpinBox:
        QObject::connect(item, SIGNAL(valueChanged()), this, SLOT(onSpinBoxValueChanged()));
        break;
    case WidgetType::TextField:
        QObject::connect(item, SIGNAL(textChanged()), this, SLOT(onTextFieldChanged()));
        break;
    default:
        break;
    }
}


void WidgetHandler::updateItems(const UpdateData& updateData)
{
    const Utils::LocalHolder<bool, false> lh(&priv->updatingItems, true);

    const auto& properties = updateData.properties;

    const QVariant currentValue = Utils::tryValue(properties, ControllerProperty::PublicCurrentValue);

    if (updateData.widgets)
    {
        for (auto widget : *updateData.widgets)
        {
            const WidgetType controllerType = toWidgetType(widget);

            if (controllerType == WidgetType::Button)
            {
                const QVariant text = Utils::tryValue(properties, ControllerProperty::Text);
                if (Utils::containsType<QString>(text))
                    widget->setProperty("text", text);
            }
            else if (controllerType == WidgetType::TextField)
            {
                if (currentValue.canConvert<QString>())
                    widget->setProperty("text", currentValue);

                const QVariant text = Utils::tryValue(properties, ControllerProperty::PlaceholderText);
                if (Utils::containsType<QString>(text))
                    widget->setProperty("placeholderText", text);
            }
            else if (controllerType == WidgetType::Label)
            {
                if (currentValue.canConvert<QString>())
                    widget->setProperty("text",currentValue);
            }
            else if (controllerType == WidgetType::ComboBox)
            {
                const QVariant valueSet = Utils::tryValue(properties, ControllerProperty::PublicValueList);

                if(valueSet.canConvert<QList<QString>>())
                {
                    QStringList list(valueSet.value<QList<QString>>());
                    widget->setProperty("model", list);
                }

                if (currentValue.canConvert<QString>())
                {
                    int index = -1;
                    QMetaObject::invokeMethod(widget,
                                              "find",
                                              Qt::DirectConnection,
                                              Q_RETURN_ARG(int, index),
                                              Q_ARG(QString, currentValue.toString()) );

                    if (index >= 0)
                        widget->setProperty("currentIndex", index);
                }
            }
            else if (controllerType == WidgetType::CheckBox)
            {
                if(currentValue.canConvert<bool>())
                    widget->setProperty("checked", currentValue);

                const QVariant text = Utils::tryValue(properties, ControllerProperty::Text);
                if (Utils::containsType<QString>(text))
                    widget->setProperty("text", text);
            }
            else if (controllerType == WidgetType::SpinBox)
            {
                const QVariant min = Utils::tryValue(properties, ControllerProperty::MinValue);
                const QVariant max = Utils::tryValue(properties, ControllerProperty::MaxValue);

                if(min.canConvert<double>() && max.canConvert<double>())
                {
                    widget->setProperty("from", min);
                    widget->setProperty("to", max);
                }

                widget->setProperty(itemValueTypeKey.latin1(), currentValue.userType());

                if(currentValue.canConvert<double>())
                    widget->setProperty("value", currentValue);
            }
            else if (controllerType == WidgetType::Slider)
            {
                const QVariant min = Utils::tryValue(properties, ControllerProperty::MinValue);
                const QVariant max = Utils::tryValue(properties, ControllerProperty::MaxValue);

                if(min.canConvert<double>() && max.canConvert<double>())
                {
                    widget->setProperty("from", min);
                    widget->setProperty("to", max);
                }

                widget->setProperty(itemValueTypeKey.latin1(), currentValue.userType());

                if(currentValue.canConvert<double>())
                    widget->setProperty("value",currentValue);
            }
        }
    }

    if (updateData.labels)
    {
        const QVariant labelText = Utils::tryValue(properties, ControllerProperty::LabelText);
        if (Utils::containsType<QString>(labelText))
        {
            const auto v = labelText.toString();
            for (auto label : *updateData.labels)
                label->setProperty("text", v);
        }
    }

    if (updateData.widgets)
    {
        const QVariant toolTip = Utils::tryValue(properties, ControllerProperty::ToolTip);
        if (Utils::containsType<QString>(toolTip))
        {
            const QLatin1String toolTipPropertyName("qcs_tooltip");

            for (auto item : *updateData.widgets)
            {
                /* If a tooltip object has already been created for this Item,
               we only update the tooltip text and move on to the next Item. */
                auto temp1 = item->property(toolTipPropertyName.data());
                if (Utils::containsType<QObject*>(temp1))
                {
                    const auto obj = Utils::getValueAs<QObject*>(temp1);
                    const bool isToolTip = toWidgetType(obj) == WidgetType::ToolTip;
                    Q_ASSERT(isToolTip);
                    obj->setProperty("text", toolTip);
                    continue;
                }

                QQmlComponent toolTipComponent(qmlEngine(item),
                                               QUrl(qmlDir + "/private/ItemToolTip.qml"),
                                               item);

                const auto toolTipObject = toolTipComponent.create(qmlContext(item));
                toolTipObject->setParent(item);
                toolTipObject->setProperty("text", toolTip);
                // Set visual parent for ToolTip popup Item
                toolTipObject->setProperty("parent", QVariant::fromValue(item));
                item->setProperty(toolTipPropertyName.data(), QVariant::fromValue(toolTipObject));

                temp1 = item->property("hovered");
                auto temp2 = item->property("hoverEnabled");
                if (Utils::containsType<bool>(temp1) && Utils::containsType<bool>(temp2))
                    item->setProperty("hoverEnabled", true);
            }
        }
    }

    const QVariant visible = Utils::tryValue(properties, ControllerProperty::Visible);
    if (visible.canConvert<bool>())
    {
        const bool v = visible.toBool();

        const char* property = "visible";
        Private::setObjectsProperty(updateData.labels, property, v);
        Private::setObjectsProperty(updateData.widgets, property, v);
        Private::setObjectsProperty(updateData.buddies, property, v);
    }

    const QVariant enabled = Utils::tryValue(properties, ControllerProperty::Enabled);
    if (enabled.canConvert<bool>())
    {
        const bool e = enabled.toBool();

        const char* property = "enabled";
        Private::setObjectsProperty(updateData.labels, property, e);
        Private::setObjectsProperty(updateData.widgets, property, e);
        Private::setObjectsProperty(updateData.buddies, property, e);
    }
}


void WidgetHandler::changeCurrentValue(QObject* item, const QVariant& value)
{
    const Utils::LocalHolder<bool, false> lh(&priv->updatingItems, true);

    const WidgetType controllerType = toWidgetType(item);

    switch (controllerType)
    {
    case WidgetType::CheckBox:
        item->setProperty("checked", value);
        break;
    case WidgetType::ComboBox:
        item->setProperty("currentText", value);
        break;
    case WidgetType::Slider:
        item->setProperty("value", value);
        break;
    case WidgetType::SpinBox:
        item->setProperty("value", value);
        break;
    case WidgetType::TextField:
        item->setProperty("text", value);
        break;
    default:
        Q_ASSERT(false);
    }
}


void WidgetHandler::onCheckBoxToggled()
{
    if (priv->updatingItems)
        return;

    auto item = qobject_cast<QObject*>(sender());
    const auto value = item->property("checked");

    emit sigCurrentValueChanged(item, value);
}


void WidgetHandler::onComboBoxIndexActivated()
{
    if (priv->updatingItems)
        return;

    auto item = qobject_cast<QObject*>(sender());
    const auto value = item->property("currentText");

    emit sigCurrentValueChanged(item, value);
}


void WidgetHandler::onSpinBoxValueChanged()
{
    if (priv->updatingItems)
        return;

    auto item = qobject_cast<QObject*>(sender());
    auto value = item->property("value");
    const auto valueType = item->property(itemValueTypeKey.latin1()).toInt();
    const auto result = value.convert(valueType);
    Q_ASSERT(result);

    emit sigCurrentValueChanged(item, value);
}


void WidgetHandler::onSliderValueChanged()
{
    if (priv->updatingItems)
        return;

    auto item = sender();
    auto value = item->property("value");
    const auto valueType = item->property(itemValueTypeKey.latin1()).toInt();
    const auto result = value.convert(valueType);
    Q_ASSERT(result);

    emit sigCurrentValueChanged(item, value);
}


void WidgetHandler::onTextFieldChanged()
{
    if (priv->updatingItems)
        return;

    auto item = qobject_cast<QObject*>(sender());
    const auto value = item->property("text");

    emit sigCurrentValueChanged(item, value);
}


void WidgetHandler::Private::setObjectsProperty(ItemSet* objects,
                                                const char* property,
                                                const QVariant& value)
{
    if (!objects)
        return;

    for (auto obj : *objects)
        obj->setProperty(property, value);
}

}


namespace
{

WidgetType toWidgetType(QObject* object)
{
    const QString name = object->metaObject()->className();

    if (name.contains(QLatin1String("Button")))
        return WidgetType::Button;
    else if (name.contains(QLatin1String("CheckBox")))
        return WidgetType::CheckBox;
    else if (name.contains(QLatin1String("ComboBox")))
        return WidgetType::ComboBox;
    else if (name.contains(QLatin1String("Label")))
        return WidgetType::Label;
    else if (name.contains(QLatin1String("Slider")))
        return WidgetType::Slider;
    else if (name.contains(QLatin1String("SpinBox")))
        return WidgetType::SpinBox;
    // Support also Kirigami PasswordField and Packuru PasswordField as they inherit from QQC2 TextField.
    // TODO: There should be a better way of handling widgets, based on whether a class has
    // required properties/functions/signals for a given widget type, instead of looking at class name.
    // The values for QQC2 widgets are set anyway by calling QObject::setProperty() and
    // for Qt Widgets QMetaObject::invokeMethod() could be used.
    else if (name.contains(QLatin1String("TextField")) || name.contains(QLatin1String("PasswordField")))
        return WidgetType::TextField;
    else if (name.contains(QLatin1String("ToolTip")))
        return WidgetType::ToolTip;
    else if (qobject_cast<QQuickItem*>(object))
        return WidgetType::Item;

    Q_ASSERT(false);
    return WidgetType::___INVALID;
}

}
