// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QObject>

#include "symbol_export.h"


namespace qcs::core
{
class ControllerEngine;
}


namespace qcs::qqc
{

/**
 @copydoc qcs::qtw::WidgetMapper
*/

class QCS_QQCONTROLS_EXPORT WidgetMapper : public QObject
{
    Q_OBJECT
    Q_PROPERTY(qcs::core::ControllerEngine* engine READ getEngine WRITE setEngine NOTIFY engineChanged)
public:
    explicit WidgetMapper(QObject *parent = nullptr);
    virtual ~WidgetMapper() override;

    /// @copydoc qcs::qtw::WidgetMapper::addLabelAndWidget()
    Q_INVOKABLE void addLabelAndWidget(int controllerId, QObject* label, QObject* widget);

    /// @copydoc qcs::qtw::WidgetMapper::addLabelsAndWidgets()
    Q_INVOKABLE void addLabelsAndWidgets(int controllerId,
                             const QList<QObject*>& labels,
                             const QList<QObject*>& widgets);

    /// @brief Maps existing widget to an ID.
    Q_INVOKABLE void addWidget(int controllerId, QObject* widget);

    /// @copydoc qcs::qtw::WidgetMapper::addWidgets()
    Q_INVOKABLE void addWidgets(int controllerId, const QList<QObject*>& widgets);

    /**
     * @brief Adds buddy for corresponding input widgets.
     * @sa addBuddies()
     */
    Q_INVOKABLE void addBuddy(int controllerId, QObject* buddy);

    /// @copydoc qcs::qtw::WidgetMapper::addBuddies
    Q_INVOKABLE void addBuddies(int controllerId, const QList<QObject*>& buddies);

    /// @copydoc qcs::qtw::WidgetMapper::setEngine
    Q_INVOKABLE void setEngine(qcs::core::ControllerEngine* engine);

    Q_INVOKABLE qcs::core::ControllerEngine* getEngine();

    /// @copydoc qcs::qtw::WidgetMapper::updateWidgets
    Q_INVOKABLE void updateWidgets();

    /// @copydoc qcs::qtw::WidgetMapper::isSetVisible
    Q_INVOKABLE bool isSetVisible(int controllerId) const;

    /// @copydoc qcs::qtw::WidgetMapper::isSetEnabled
    Q_INVOKABLE bool isSetEnabled(int controllerId) const;

signals:
    void engineChanged();

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}
