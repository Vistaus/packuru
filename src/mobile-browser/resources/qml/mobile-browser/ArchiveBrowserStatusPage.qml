// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.11
import QtQuick.Controls 2.5
import QtQuick.Layouts 1.3

import org.kde.kirigami 2.0 as Kirigami

import ControllerSystem.QQC 1.0 as CS_QQC

import PackuruCore 1.0
import PackuruCoreBrowser 1.0
import PackuruMobileCore 1.0 as PackuruMobileCore


Kirigami.Page {
    id: root
    property QtObject statusPageCore
    property alias img: image.source

    globalToolBarStyle: Kirigami.ApplicationHeaderStyle.None

    Rectangle {
        anchors.fill: parent
        color: Kirigami.Theme.backgroundColor
    }

    QtObject {
        id: priv
        property string taskErrorLog
    }

    Component {
        id: fileOverwritePromptComponent
        PackuruMobileCore.ExtractionFileOverwritePrompt { }
    }

    Component {
        id: folderOverwritePromptComponent
        PackuruMobileCore.ExtractionFolderOverwritePrompt { }
    }

    Component {
        id: typeMismatchPromptComponent
        PackuruMobileCore.ExtractionTypeMismatchPrompt { }
    }

    Connections {
        target: statusPageCore
        function onShowBasicControl() {
            basicControlWidget.visible = true
        }

        function onShowPasswordPrompt(promptCore) {
            basicControlWidget.visible = false
            var prompt = passwordPromptComponent.createObject(this, {"promptCore" : promptCore})
            promptCore.deleted.connect(prompt.destroy)
            prompt.parent = widget
            prompt.focus = Qt.binding( function() { return root.focus } )
        }

        function onShowFileExistsPrompt(promptCore) {
            basicControlWidget.visible = false
            var prompt = fileOverwritePromptComponent.createObject(this,
                                                                   {"promptCore" : promptCore,
                                                                       "focus" : root.focus})
            prompt.parent = widget
            prompt.width = Qt.binding(function() { return prompt.parent.width })
        }

        function onShowFolderExistsPrompt(promptCore) {
            basicControlWidget.visible = false
            var prompt = folderOverwritePromptComponent.createObject(this,
                                                                     {"promptCore" : promptCore,
                                                                         "focus" : root.focus})
            prompt.parent = widget
            prompt.width = Qt.binding(function() { return prompt.parent.width })
        }

        function onShowItemTypeMismatchPrompt(promptCore) {
            basicControlWidget.visible = false
            var prompt = typeMismatchPromptComponent.createObject(this,
                                                                  {"promptCore" : promptCore,
                                                                      "focus" : root.focus})
            prompt.parent = widget
            prompt.width = Qt.binding(function() { return prompt.parent.width })
        }
    }


    Rectangle {
        id: statusRectangle
        anchors.verticalCenter: parent.verticalCenter
        width: Math.min(Kirigami.Units.gridUnit * 30, parent.width)
        radius: 5
        anchors.horizontalCenter: parent.horizontalCenter

        height: widget.height
        Behavior on height {
            SpringAnimation {
                spring: 100; damping: 0.8
                epsilon: 0.25
            }
        }

        color: Kirigami.Theme.viewBackgroundColor

        Column {
            id: widget
            width: parent.width
            spacing: 10
            topPadding: 10
            bottomPadding: 10

            Item {
                anchors.horizontalCenter: parent.horizontalCenter
                height: image.height > taskInfo.height ? image.height : taskInfo.height
                width: image.width + taskInfo.width
                Kirigami.Icon {
                    id: image
                    anchors.verticalCenter: parent.verticalCenter
                    isMask: true
                    anchors.left: parent.left
                    width: 50
                    height: 50
                }

                Column {
                    id: taskInfo
                    anchors.right: parent.right
                    anchors.verticalCenter: parent.verticalCenter
                    Label {
                        id: description
                        text: statusPageCore ? statusPageCore.taskDescription : null
                        font.pointSize: Kirigami.Units.fontMetrics.font.pointSize * 1.5
                        font.bold: true
                    }
                    Label {
                        id: state
                        text: statusPageCore ? statusPageCore.taskState : null
                    }
                }
            }

            ProgressBar {
                id: progressBar
                anchors.horizontalCenter: parent.horizontalCenter
                width: statusRectangle.width * 0.75
                /* It looks like when ProgressBar is in indeterminate mode it doesn't update
                 * properly the moving rectangle minimum/maximum positions, so currently
                 * an extra condition to show progess bar is that task progress is >= 0
                 * in addition to statusPageCore.progressBarVisible value. */
                visible: statusPageCore.taskProgress >= 0 && statusPageCore.progressBarVisible
                value: statusPageCore.taskProgress
                // indeterminate: statusPageCore.taskProgress < 0
                from: 0
                to: 100
            }

            Label {
                anchors.horizontalCenter: parent.horizontalCenter
                text: progressBar.value + "%"
                visible: progressBar.visible && !progressBar.indeterminate

            }

            Rectangle {
                width: parent.width
                height: 1
                color: Kirigami.Theme.backgroundColor
            }

            FocusScope {
                id: basicControlWidget
                focus: root.focus
                anchors.horizontalCenter: parent.horizontalCenter
                width: basicControlContent.width
                height: basicControlContent.height
                property QtObject basicControl: root.statusPageCore ? root.statusPageCore.basicControl : null

                Connections {
                    target: basicControlWidget.basicControl

                    function onDefaultButtonChanged(button) {
                        switch (button) {
                        case BasicControl.DefaultButton.Abort: abortButton.focus = true; break;
                        case BasicControl.DefaultButton.BrowseArchive: browseButton.focus = true; break;
                        case BasicControl.DefaultButton.Retry: retryButton.focus = true; break;
                        case BasicControl.DefaultButton.ViewLog: viewLogButton.focus = true; break;
                        }
                    }
                }

                CS_QQC.WidgetMapper {
                    id: mapper
                }

                Column {
                    id: basicControlContent
                    spacing: 10
                    Label {
                        id: errorLabel
                        anchors.horizontalCenter: parent.horizontalCenter
                        color: Kirigami.Theme.textColor
                    }

                    RowLayout {
                        Button {
                            id: retryButton
                            icon.name: "view-refresh"
                            onClicked: basicControlWidget.basicControl.retry()
                        }
                        Button {
                            id: abortButton
                            icon.name: "dialog-cancel"
                            onClicked: basicControlWidget.basicControl.abort()
                        }
                        Button {
                            id: browseButton
                            icon.name: "view-list-details"
                            onClicked: basicControlWidget.basicControl.browse()
                        }
                        Button {
                            id: viewLogButton
                            icon.name: "viewlog"
                            onClicked:  {
                                var dialog = logDialogComp.createObject(applicationWindow())
                                dialog.destroyOnClose = true
                                dialog.headerText = commonStrings.getString(CommonStrings.ErrorLogDialogTitle)
                                dialog.text = basicControlWidget.basicControl.getLog()
                                dialog.open();
                            }
                        }
                    }
                }

                Component.onCompleted: {
                    mapper.addWidgets(BasicControlControllerType.AbortButton, abortButton)
                    mapper.addWidgets(BasicControlControllerType.BrowserButton, browseButton)
                    mapper.addWidgets(BasicControlControllerType.ErrorsWarningsLabel, errorLabel)
                    mapper.addWidgets(BasicControlControllerType.RetryButton, retryButton)
                    mapper.addWidgets(BasicControlControllerType.ViewLogButton, viewLogButton)
                    mapper.setEngine(basicControlWidget.basicControl.getControllerEngine())
                }
            }
        }
    }

    Component {
        id: logDialogComp
        PackuruMobileCore.InformationDialog {}
    }

    Component {
        id: passwordPromptComponent

        FocusScope{
            property QtObject promptCore
            width: passwordControl.width
            height: passwordControl.height
            anchors.horizontalCenter: parent ? parent.horizontalCenter : undefined
            Column {
                id: passwordControl
                spacing: 10

                PackuruMobileCore.PasswordField {
                    id: passwordField
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: statusRectangle.width * 0.75
                    placeholderText: promptCore ? promptCore.getString(PasswordPromptCore.DialogTitle) : ""
                    echoMode: TextInput.Password
                    onAccepted: {
                        if (text.length > 0)
                            promptCore.enterPassword(text)
                    }
                    focus: true

                }

                RowLayout {
                    anchors.horizontalCenter: parent.horizontalCenter
                    Button {
                        text: "OK"
                        enabled: passwordField.text.length > 0
                        Layout.fillWidth: true
                        icon.name: "dialog-ok"
                        onClicked: promptCore.enterPassword(passwordField.text)
                    }
                    Button {
                        text: promptCore ? promptCore.getString(PasswordPromptCore.Abort) : ""
                        Layout.fillWidth: true
                        icon.name: "dialog-cancel"
                        onClicked: promptCore.abort()
                    }
                }
            }
        }
    }
}
