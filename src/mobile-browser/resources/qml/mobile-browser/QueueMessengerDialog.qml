// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

import org.kde.kirigami 2.5 as Kirigami

import PackuruMobileCore 1.0 as PackuruMobileCore
import PackuruCoreBrowser 1.0
import PackuruCore 1.0


PackuruMobileCore.AutoResizableDialog {
    id: root
    property QtObject queueMessenger

    Connections {
        target: queueMessenger
        function onDeactivated() {
            root.enabled = false
            // Only delay close when messenger's state changed without user action; otherwise close instantly.
            closeTimer.start()
        }

        function onStatusChanged(info) {
            statusLabel.text = info
        }
    }

    Timer {
        id: closeTimer
        interval: 1000
        onTriggered: root.close()
    }

    // When tapped outside dialog
    onRejected: queueMessenger.abort()

    headerText: queueMessenger.getString(QueueMessenger.DialogTitle)

    contentItem: Label {
        id: statusLabel
        wrapMode: Text.Wrap
    }

    footer: DialogButtonBox {
        alignment: Qt.AlignHCenter
        Button {
            text: queueMessenger.getString(QueueMessenger.RunInBrowser)
            visible: queueMessenger.canRunInBrowser
            onClicked: {
                queueMessenger.runInBrowser()
                root.close()
            }
        }
        Button {
            text: queueMessenger.getString(QueueMessenger.Abort)
            onClicked: {
                queueMessenger.abort()
                root.close()
            }
        }
    }
}
