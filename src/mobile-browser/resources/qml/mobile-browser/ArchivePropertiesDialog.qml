// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

import org.kde.kirigami 2.5 as Kirigami

import PackuruCore 1.0
import PackuruMobileCore 1.0 as PackuruMobileCore


PackuruMobileCore.InformationDialog {
    id: root
    property string absArchiveFilePath
    property QtObject propertiesModel
    property string archiveComment

    contentItem: PackuruMobileCore.TabWidget {
        id: tabWidget
        focus: true
    }

    Component {
        id: propertiesPageComponent

        ListView {
            id: view
            clip: true
            boundsBehavior: Flickable.StopAtBounds

            property string title: commonStrings.getString(CommonStrings.ArchivePropertiesDialogTitle)
            property int delegateMaxImplicitWidth: 0
            property int delegateImplicitHeight: 0

            implicitWidth: delegateMaxImplicitWidth
            implicitHeight: contentHeight

            delegate: Kirigami.AbstractListItem {
                hoverEnabled: false
                separatorVisible: false
                highlighted: false
                Column {
                    Layout.fillWidth: true
                    Layout.fillHeight: true

                    Label {
                        Component.onCompleted: text = view.model.headerData(index, Qt.Vertical, Qt.DisplayRole)
                        opacity: 0.5
                    }
                    Label {
                        text: Aux.containsUrlType(display) ? Aux.htmlLink(display) : display
                        onLinkActivated: Qt.openUrlExternally(display)
                    }
                }

                onImplicitWidthChanged: {
                    if (implicitWidth + leftPadding + rightPadding > view.delegateMaxImplicitWidth)
                        view.delegateMaxImplicitWidth = implicitWidth  + leftPadding + rightPadding
                }
                onImplicitHeightChanged: {
                    if (implicitHeight > view.delegateImplicitHeight)
                        view.delegateImplicitHeight = implicitHeight
                }
            }
        }
    }

    Component {
        id: commentPageComponent

        Flickable {
            property string title: commonStrings.getString(CommonStrings.Comment)
            property alias comment: edit.text
            clip:true
            implicitWidth: contentWidth
            implicitHeight: contentHeight
            contentWidth: edit.contentWidth
            contentHeight: edit.contentHeight
            boundsBehavior: Flickable.StopAtBounds

            TextEdit {
                id: edit
                font.family: "monospace"
                readOnly: true
            }
        }
    }

    Component.onCompleted: {
        var page1 = propertiesPageComponent.createObject(tabWidget.swipeView, {"model" : root.propertiesModel})
        tabWidget.swipeView.addPage(page1)

        if (root.archiveComment.length > 0)
        {
            var page2 = commentPageComponent.createObject(tabWidget.swipeView, {"comment" : root.archiveComment})
            tabWidget.swipeView.addPage(page2)
        }

    }
}
