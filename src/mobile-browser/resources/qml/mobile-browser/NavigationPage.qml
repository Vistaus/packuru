// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.0
import QtQml.Models 2.11
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.12
import Qt.labs.settings 1.0

import org.kde.kirigami 2.5 as Kirigami

import PackuruCore 1.0
import PackuruCoreBrowser 1.0
import PackuruMobileCore 1.0 as PackuruMobileCore

import ControllerSystem.QQC 1.0 as CS_QQC


Kirigami.ScrollablePage {
    id: page

    property string archiveName
    property ArchiveNavigator navigator

    readonly property alias actionAdd: __actionAdd
    readonly property alias actionDelete: __actionDelete
    readonly property alias actionExtract: __actionExtract
    readonly property alias actionFilter: __actionFilter
    readonly property alias actionPreview: __actionPreview
    readonly property alias actionReload: __actionReload
    readonly property alias actionTest: __actionTest
    readonly property alias actionProperties: __actionProperties

    title: navigator.currentPathIndex.valid ? navigator.currentPath : archiveName

    supportsRefreshing: true
    onRefreshingChanged: {
        if (refreshing) {
            actionReload.trigger()
            refreshing = false
        }
    }

    header: ColumnLayout {

        Kirigami.Heading {
            Layout.fillWidth: true
            id: selectionInfo
            text: visible ? qsTr("Selected:") + " " + navigator.selectionModel.selectedIndexes.length : ""
            visible: false
        }

        RowLayout {
            id: filterStatus
            visible: false

            function close() {
                filterEdit.text = ""
                filterStatus.visible = false
            }

            TextField {
                id: filterEdit
                Layout.fillWidth: true
            }
            Button {
                text: qsTr("Edit")
                icon.name: "document-edit"
                flat: true
                onClicked: filterLoader.showFilterDialog()
            }
            Button {
                text: navigator.getString(ArchiveNavigator.Close)
                icon.name: "dialog-close"
                flat: true
                onClicked: filterStatus.close()
            }
        }
    }

    Loader {
        id: filterLoader
        active: false

        function showFilterDialog() {
            item.open()
        }

        sourceComponent: PackuruMobileCore.AutoResizableDialog {
            headerText: actionFilter.text
            parent: applicationWindow().contentItem
            contentItem: GridLayout {
                columns: 2
                Label { id: filterScopeLabel}
                ComboBox { id: filterScopeCombo }
                Label { id: filterFilesLabel }
                ComboBox { id: filterFilesCombo }
                Label { id: filterFoldersLabel}
                ComboBox { id: filterFoldersCombo }

                CS_QQC.WidgetMapper {
                    id: filterWidgetMapper
                }
            }
            Component.onCompleted: {
                filterWidgetMapper.setEngine(navigator.getFilterControllerEngine())

                filterWidgetMapper.addWidget(ViewFilterControllerType.FilterText, filterEdit)
                filterWidgetMapper.addLabelAndWidget(ViewFilterControllerType.FilterFiles,
                                                     filterFilesLabel,
                                                     filterFilesCombo)
                filterWidgetMapper.addLabelAndWidget(ViewFilterControllerType.FilterFolders,
                                                     filterFoldersLabel,
                                                     filterFoldersCombo)
                filterWidgetMapper.addLabelAndWidget(ViewFilterControllerType.FilterScope,
                                                     filterScopeLabel,
                                                     filterScopeCombo)

                filterWidgetMapper.updateWidgets()
            }
        }
    }


    contextualActions: [
        Kirigami.Action {
            id: __actionReload
            iconName: "view-refresh"
        },
        Kirigami.Action {
            id: __actionExtract
            iconName: "archive-extract"
        },
        Kirigami.Action {
            id: __actionPreview
            iconName: "document-preview"
        },
        Kirigami.Action {
            id: __actionTest
            iconName: "dialog-ok"
        },
        Kirigami.Action {
            id: __actionAdd
            iconName: "archive-insert"
        },
        Kirigami.Action {
            id: __actionDelete
            iconName: "archive-remove"
        },
        Kirigami.Action {
            id: __actionProperties
            iconName: "documentinfo"
        },
        Kirigami.Action {
            id: actionSort
            iconName: "view-sort"
            text: qsTr("Sorting")
            onTriggered: {
                var comp = Qt.createComponent("SortingDialog.qml")
                var list = []
                for (var i = 0; i < navigator.model.columnCount(); ++i) {
                    var label= navigator.model.headerData(i, Qt.Horizontal)
                    list.push(label)
                }

                var dialog = comp.createObject(applicationWindow(),
                                               {"columnList" : list,
                                                   "sortColumn" : priv.sortColumn,
                                                   "sortOrder" : priv.sortOrder})
                dialog.destroyOnClose = true
                dialog.headerText = actionSort.text
                dialog.userSortConfig.connect(function(sortColumn, sortOrder) {
                    var configChanged = false
                    if (sortColumn !== priv.sortColumn) {
                        priv.sortColumn = sortColumn
                        configChanged = true
                    }

                    if (sortOrder !== priv.sortOrder) {
                        priv.sortOrder = sortOrder
                        configChanged = true
                    }

                    if (configChanged)
                        navigator.sortModel(sortColumn, sortOrder)


                })
                dialog.open()
            }
        },
        Kirigami.Action {
            id: __actionFilter
            iconName: "search"
            onTriggered: {
                filterLoader.active = true
                filterStatus.visible = true
            }
        }

    ]

    actions.main: Kirigami.Action {
        id: actionGoUp
        iconName: "go-up"
        enabled: navigator.canGoUp
        onTriggered: navigator.goUp()
    }

    Connections {
        target: navigator

        function onCurrentPathIndexChanged() {
            /* This is a workaround for a shortcoming in DelegateModel:
               If DelegateModel's rootIndex is removed from its source model (e.g. it is
               filtered out) it doesn't clear its delegates so they remain visible. This is
               in contrast to QtWidgets views which can handle this situation gracefully.
               So we have to reset the model to get desired behavior. */
            if (!navigator.currentPathIndex.valid)
                delegateModel.model = navigator.model
        }
    }

    Connections {
        target: navigator.model
        function onModelReset() {
            navigator.sortModel(priv.sortColumn, priv.sortOrder)
            selectionInfo.visible = false
            filterStatus.close()
        }
        /* Connections set in QML Connections object do not follow signal/slot execution order
           and are invoked first even if there were some other slots connected first. So even
           though selection model was the first object connected to layoutChanged() signal, it has
           not yet updated its selection when we receive this signal here. The workaround is to use
           a timer to give the selection model a chance to update before we use it. */
        function onLayoutChanged() {
            delayedSelectionTimer.restart()
        }
    }

    Timer {
        id: delayedSelectionTimer
        interval: 0;
        onTriggered: delegateModel.updateDelegateSelection()
    }

    Connections {
        target: navigator.selectionModel
        // ArchiveNavigator changes current index when changing dir
        function onCurrentChanged(current) {
            /* Do nothing if the signal was actually caused by the view changing current index in
               response to mouse or keyboard navigation. */
            if (view.updatingCurrent)
                return

            if (current.row >= 0)
                view.currentIndex = current.row
            else
                view.currentIndex = 0
        }

        // This doesn't handle the case when the item model has been reset as selection model
        // doesn't emit any signal in this case. So it should be handled separately in
        // QAbstractItemModel::modelReset() signal handler.
        function onHasSelectionChanged () {
            selectionInfo.visible = navigator.selectionModel.hasSelection
        }
    }

    Settings {
        id: settings
        property alias sortColumn: priv.sortColumn
        property alias sortOrder: priv.sortOrder

    }

    QtObject {
        id: priv
        property int sortColumn: 0
        property int sortOrder: Qt.AscendingOrder
    }

    Keys.onPressed: {
        if (event.key == Qt.Key_Return) {
            if (view.currentItem)
                view.currentItem.openItem()
            event.accepted = true;
        }
        else if (event.key == Qt.Key_Backspace || event.key == Qt.Key_Back) {
            actionGoUp.trigger()
            event.accepted = true;
        }
        else if (event.key == Qt.Key_PageDown) {
            var current = view.currentIndex + view.pageScrollStep
            if (current >= delegateModel.count)
                current = delegateModel.count - 1
            view.currentIndex = current
            event.accepted = true;
        }
        else if (event.key == Qt.Key_PageUp) {
            var current = view.currentIndex - view.pageScrollStep
            if (current < 0)
                current = 0
            view.currentIndex = current
            event.accepted = true;
        }
        else if (event.key == Qt.Key_Home) {
            view.currentIndex = 0
            event.accepted = true;
        }
        else if (event.key == Qt.Key_End) {
            view.currentIndex = Math.max(0, delegateModel.count - 1)
            event.accepted = true;
        }
    }

    ListView {
        anchors.fill: parent
        id: view
        model: DelegateModel {
            id: delegateModel
            model: navigator.model
            rootIndex: navigator.currentPathIndex
            delegate: delegateComp

            function updateDelegateSelection() {
                if (navigator.selectionModel.currentIndex.row >= 0)
                    view.currentIndex = navigator.selectionModel.currentIndex.row

                for (var i = 0; i < view.contentItem.children.length; ++i)
                {
                    var item = view.contentItem.children[i]
                    if (item instanceof Kirigami.SwipeListItem) {
                        var modelIndex = delegateModel.modelIndex(item.row)
                        item.checked = navigator.selectionModel.isSelected(modelIndex)
                    }
                }
            }
        }

        readonly property int delegatesPerPage: height / (view.contentHeight / delegateModel.count)
        readonly property int pageScrollStep: delegatesPerPage > 1 ? delegatesPerPage - 1 : 1

        highlightMoveDuration: 100
        currentIndex: 0

        /* This flags informs that the view is now changing current index in the selection
           model in response to keyboard/mouse navigation, so when the selection model signals
           currentChanged() we check this flag and if its true we do nothing. */
        property bool updatingCurrent: false

        onCurrentIndexChanged: {
            updatingCurrent = true

            var modelIndex = delegateModel.modelIndex(currentIndex)
            navigator.selectionModel.setCurrentIndex(modelIndex, ItemSelectionModel.NoUpdate);

            updatingCurrent = false
        }

        Component {
            id: delegateComp

            Kirigami.SwipeListItem {
                id: listItem
                highlighted: view.currentIndex == index
                checkable: navigator.selectionModel.hasSelection

                property int row: index

                onClicked: {
                    setAsCurrent()
                    if (navigator.selectionModel.hasSelection) {
                        toggleItemSelection()
                        return
                    }
                    openItem()
                }
                onPressAndHold: {
                    setAsCurrent()
                    toggleItemSelection()
                    toggle()
                }
                function toggleItemSelection() {
                    var modelIndex = delegateModel.modelIndex(index)
                    navigator.selectionModel.select(modelIndex, ItemSelectionModel.Toggle)
                }
                function setAsCurrent() {
                    view.currentIndex = index
                }
                function openItem() {
                    var modelIndex = delegateModel.modelIndex(index)
                    navigator.openItem(modelIndex)
                }

                Component.onCompleted: {
                    var modelIndex = delegateModel.modelIndex(index)
                    if (navigator.selectionModel.isSelected(modelIndex))
                        checked = true
                }

                Item{
                    implicitHeight: topLayout.implicitHeight
                    RowLayout {
                        id: topLayout
                        anchors.fill: parent

                        MouseArea {
                            Layout.minimumHeight: 50
                            Layout.maximumHeight: 50
                            Layout.minimumWidth: height

                            Kirigami.Icon {
                                id: iconItem
                                height: parent.height
                                width: parent.width
                                source: decoration
                            }
                        }

                        ColumnLayout
                        {
                            Text {
                                id: labelItem
                                Layout.fillWidth: true
                                color: listItem.checked || listItem.pressed ? listItem.activeTextColor : listItem.textColor
                                text: name
                                font.italic: encrypted
                            }

                            RowLayout {
                                Text {
                                    opacity: 0.5
                                    Layout.fillWidth: true
                                    color: listItem.checked || listItem.pressed ? listItem.activeTextColor : listItem.textColor
                                    text: originalSize
                                    font.italic: encrypted
                                }

                                Text {
                                    opacity: 0.5
                                    color: listItem.checked || listItem.pressed ? listItem.activeTextColor : listItem.textColor
                                    text: modified
                                    font.italic: encrypted
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
