// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "../core/globalsettingsmanager.h"
#include "../core/appinfo.h"
#include "../core/commonstrings.h"

#include "../core-browser/mainwindowcore.h"
#include "../core-browser/archivebrowsercore.h"
#include "../core-browser/archivenavigator.h"
#include "../core-browser/statuspagecore.h"
#include "../core-browser/basiccontrol.h"
#include "../core-browser/basiccontrolcontrollertype.h"
#include "../core-browser/deletiondialogcore.h"
#include "../core-browser/deletiondialogcontrollertype.h"
#include "../core-browser/queuemessenger.h"
#include "../core-browser/settingsdialogcore.h"
#include "../core-browser/settingscontrollertype.h"
#include "../core-browser/init.h"
#include "../core-browser/tabclosingdialogcore.h"
#include "../core-browser/viewfiltercontrollertype.h"

#include "../mobile-core/registertypes.h"


using namespace Packuru::Core::Browser;
using namespace Packuru::Core;


void registerTypes();


int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QGuiApplication app(argc, argv);
    app.setApplicationVersion(AppInfo::getAppVersion());
    app.setOrganizationName(AppInfo::getAppName());
    app.setApplicationName(AppInfo::getAppName() + " Mobile");

    Init coreInit(QUEUE_EXEC_NAME, QUEUE_EXEC_NAME);

    if (coreInit.messagePrimaryInstance())
        return 0;

    Packuru::Mobile::Core::registerTypes();

    registerTypes();

    QQmlApplicationEngine engine;

    CommonStrings commonStrings;
    engine.rootContext()->setContextProperty("mainWindowCore", coreInit.getMainWindowCore());
    engine.rootContext()->setContextProperty("commonStrings", &commonStrings);

    engine.load(QLatin1String(MODULE_QML_DIR) + "/BrowserMainWindow.qml");
    if (engine.rootObjects().isEmpty())
        return -1;

    coreInit.processArguments();

    return app.exec();
}


void registerTypes()
{
    qmlRegisterUncreatableType<SettingsControllerType>("PackuruCoreBrowser",
                                                       1, 0,
                                                       "SettingsControllerType",
                                                       "");

    qmlRegisterUncreatableType<ViewFilterControllerType>("PackuruCoreBrowser",
                                                         1, 0,
                                                         "ViewFilterControllerType",
                                                         "");

    qmlRegisterUncreatableType<MainWindowCore>("PackuruCoreBrowser",
                                               1, 0,
                                               "MainWindowCore",
                                               "");

    qmlRegisterUncreatableType<ArchiveBrowserCore>("PackuruCoreBrowser",
                                                   1, 0,
                                                   "ArchiveBrowserCore",
                                                   "");

    qmlRegisterUncreatableType<StatusPageCore>("PackuruCoreBrowser",
                                               1, 0,
                                               "StatusPageCore",
                                               "");

    qmlRegisterUncreatableType<BasicControl>("PackuruCoreBrowser",
                                             1, 0,
                                             "BasicControl",
                                             "");

    qmlRegisterUncreatableType<BasicControlControllerType>("PackuruCoreBrowser",
                                                           1, 0,
                                                           "BasicControlControllerType",
                                                           "");

    qmlRegisterUncreatableType<ArchiveNavigator>("PackuruCoreBrowser", 1, 0, "ArchiveNavigator", "");

    qmlRegisterUncreatableType<DeletionDialogCore>("PackuruCoreBrowser",
                                                   1, 0,
                                                   "DeletionDialogCore",
                                                   "");

    qmlRegisterUncreatableType<DeletionDialogControllerType>("PackuruCoreBrowser",
                                                             1, 0,
                                                             "DeletionDialogControllerType",
                                                             "");

    qmlRegisterUncreatableType<SettingsDialogCore>("PackuruCoreBrowser",
                                                   1, 0,
                                                   "SettingsDialogCore",
                                                   "");

    qmlRegisterUncreatableType<QueueMessenger>("PackuruCoreBrowser",
                                               1, 0,
                                               "QueueMessenger",
                                               "");

    qmlRegisterType<TabClosingDialogCore>("PackuruCoreBrowser", 1, 0, "TabClosingDialogCore");

    qmlRegisterInterface<QAbstractItemModel>("QAbstractItemModel", 1);
    qmlRegisterInterface<QAbstractTableModel>("QAbstractTableModel", 1);
}
