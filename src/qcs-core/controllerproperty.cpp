// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../utils/private/enumhash.h"

#include "controllerproperty.h"


namespace qcs::core
{

uint qHash(ControllerProperty value, uint seed)
{
   return Packuru::Utils::enumHash(value, seed);
}

}
