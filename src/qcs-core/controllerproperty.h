// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QHash>

#include "symbol_export.h"


namespace qcs::core
{

enum class ControllerProperty
{
    Callback,               // std::function<void()>; Button callback; Not used yet
    ControllerType,         // ControllerType
    Enabled,                // bool
    LabelText,              // QString, for accompanying label widget text
    MaxValue,               // int or double
    MinValue,               // int or double
    PlaceholderText,        // QString
    PrivateCurrentValue,    // Can be anything, likely an enum, bool, int etc
    PrivateValueList,       // QList of anything, likely an enum, bool, int etc
    PublicCurrentValue,     // bool, int, double or QString
    PublicValueList,        // QList<QString>
    SpinBoxSuffix,          // QString
    Text,                   // QString, for Checkbox or Button text
    ToolTip,                // QString
    Validator,              // Not used yet
    Visible                 // bool
};


QCS_CORE_EXPORT uint qHash(ControllerProperty value, uint seed = 0);

}
