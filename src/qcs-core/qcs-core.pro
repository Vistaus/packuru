# SPDX-FileCopyrightText: The Packuru Developers
#
# SPDX-License-Identifier: 0BSD

include(../common-src.pri)
include(../common-qcs.pri)

QT -= gui

MODULE_BUILD_NAME_SUFFIX = $$QCS_CORE_BUILD_NAME_SUFFIX
MODULE_BUILD_NAME = $$QCS_CORE_BUILD_NAME

TARGET = $$qtLibraryTarget($$MODULE_BUILD_NAME)

DEFINES += $${QCS_MACRO_NAME}_CORE_LIBRARY

HEADERS += \
    callback.h \
    controller.h \
    controllerbase.h \
    controllerengine.h \
    controlleridrowmapper.h \
    controllerproperty.h \
    controllerpropertymap.h \
    controllertype.h \
    internalcontrolleridtype.h \
    symbol_export.h

SOURCES += \
    controllerbase.cpp \
    controllerengine.cpp \
    controlleridrowmapper.cpp \
    controllerproperty.cpp
