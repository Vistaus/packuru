// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <unordered_map>

#include <QVariant>


namespace qcs::core
{

enum class ControllerProperty;

using ControllerPropertyMap = std::unordered_map<ControllerProperty, QVariant>;

}

Q_DECLARE_METATYPE(qcs::core::ControllerPropertyMap);
