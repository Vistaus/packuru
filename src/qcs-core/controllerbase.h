// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <unordered_set>

#include <QVariant>

#include "../utils/qvariant_utils.h"
#include "../utils/private/qvariantunorderedmap.h"

#include "controllertype.h"
#include "controllerproperty.h"
#include "controllerpropertymap.h"
#include "internalcontrolleridtype.h"
#include "callback.h"
#include "symbol_export.h"


namespace qcs::core
{

class QCS_CORE_EXPORT ControllerBase
{
    friend class ControllerEngine;

public:
    virtual ~ControllerBase() {}

    InternalControllerIdType getId() const { return id; }
    void addDependant(ControllerBase* controller) { dependants.insert(controller); }

protected:
    ControllerBase()
        : id(0) {}

    void setControllerType(ControllerType::Type type)
    {
        properties[ControllerProperty::ControllerType].setValue(type);
    }

    virtual void update();

    // When a user changes a widget's value these methods are called to sync a corresponding controller
    // 'value' is public value
    virtual void aboutToSyncToWidget(const QVariant& value) { Q_UNUSED(value) }
    virtual void onValueSyncedToWidget(const QVariant& value) { Q_UNUSED(value) }
    virtual void syncCurrentValueToWidget(const QVariant& value) { Q_UNUSED(value) }

    // Get current value: private if available, else public if available, else QVariant()
    virtual QVariant tryGetCurrentValue() const { return QVariant(); }

    void setEnabled(bool value) { properties[ControllerProperty::Enabled] = value; }
    void setVisible(bool value) { properties[ControllerProperty::Visible] = value; }

    void clearEnabled() { properties[ControllerProperty::Enabled].clear(); }
    void clearVisible() { properties[ControllerProperty::Visible].clear(); }

    void externalValueInput() { sigExternalUpdate(this, properties); }

    QVariant getProperty(ControllerProperty property) const;

    template <typename T>
    T getPropertyAs(ControllerProperty property) const;

    template <typename ControllerId>
    const ControllerPropertyMap& getControllerProperties(ControllerId id) const;

    template <typename T, typename ControllerId>
    T getControllerPropertyAs(ControllerId id, ControllerProperty property) const;

    ControllerPropertyMap properties;

private:
    std::unordered_set<ControllerBase*> getDependants() const { return dependants; }

    InternalControllerIdType id;
    std::unordered_set<ControllerBase*> dependants;
    Callback<const ControllerPropertyMap&, InternalControllerIdType> getControllerProperties_;
    Callback<void, ControllerBase*, const ControllerPropertyMap&> sigExternalUpdate;
};


template<typename T>
T ControllerBase::ControllerBase::getPropertyAs(ControllerProperty property) const
{
    return Packuru::Utils::getValueAs<T>(getProperty(property));
}


template<typename ControllerId>
const ControllerPropertyMap& ControllerBase::getControllerProperties(ControllerId id) const
{
    return getControllerProperties_(static_cast<InternalControllerIdType>(id));
}


template<typename T, typename ControllerId>
T ControllerBase::getControllerPropertyAs(ControllerId id, ControllerProperty property) const
{
    const auto& map = getControllerProperties_(static_cast<InternalControllerIdType>(id));

    return Packuru::Utils::getValueAs<T>(map, property);
}

}
