// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <unordered_map>
#include <vector>

#include "controlleridrowmapper.h"
#include "controllerengine.h"
#include "controllerproperty.h"
#include "controllertype.h"


namespace qcs::core
{

using RowNumber = int;

struct ControllerIdRowMapper::Private
{
    void onControllerPropertiesChanged(InternalControllerIdType id, const ControllerPropertyMap& properties);

    ControllerIdRowMapper* publ = nullptr;
    static const InternalControllerIdType invalidControllerId = -1;
    std::vector<LabelData> rows;
    std::unordered_map<InternalControllerIdType, RowNumber> idToRow;
    ControllerEngine* engine_ = nullptr;
};


ControllerIdRowMapper::ControllerIdRowMapper(QObject *parent)
    : QObject(parent),
      priv(new Private)
{
    priv->publ = this;
}


ControllerIdRowMapper::~ControllerIdRowMapper()
{

}


void ControllerIdRowMapper::setEngine(ControllerEngine* engine)
{
    Q_ASSERT(!priv->engine_);

    priv->engine_ = engine;

    connect(engine, &ControllerEngine::sigControllerUpdated,
            this, [priv = priv.get()] (InternalControllerIdType id, const ControllerPropertyMap& properties)
    { priv->onControllerPropertiesChanged(id, properties); });

    connect(this, &ControllerIdRowMapper::sigUserChangedValue,
            engine, &ControllerEngine::syncControllerToWidget<InternalControllerIdType>);
}


void ControllerIdRowMapper::addHeader(const QString& label)
{
    priv->rows.push_back( LabelData{label, true, priv->invalidControllerId } );
}


QString ControllerIdRowMapper::getLabel(int row) const
{
    const auto index = static_cast<std::size_t>(row);
    Q_ASSERT(index < priv->rows.size());
    return priv->rows[index].text;
}


bool ControllerIdRowMapper::isHeader(int row) const
{
    const auto index = static_cast<std::size_t>(row);
    Q_ASSERT(index < priv->rows.size());
    return priv->rows[index].isHeader;

}


ControllerType::Type ControllerIdRowMapper::getControllerType(int row) const
{
    const auto index = static_cast<std::size_t>(row);
    Q_ASSERT(index < priv->rows.size());

    const InternalControllerIdType controllerId = priv->rows[index].controllerId;
    Q_ASSERT(controllerId != priv->invalidControllerId);

    return priv->engine_->getControllerPropertyAs<ControllerType::Type>(controllerId, ControllerProperty::ControllerType);
}


const ControllerPropertyMap& ControllerIdRowMapper::getControllerProperties(int row) const
{
    const auto index = static_cast<std::size_t>(row);
    Q_ASSERT(index < priv->rows.size());

    const InternalControllerIdType controllerId = priv->rows[index].controllerId;
    Q_ASSERT(controllerId != priv->invalidControllerId);

    return priv->engine_->getControllerProperties(controllerId);
}


int ControllerIdRowMapper::size() const
{
     return static_cast<int>(priv->rows.size());
}


void ControllerIdRowMapper::onUserChangedValue(int row, const QVariant& value)
{
    const auto index = static_cast<std::size_t>(row);
    Q_ASSERT(index < priv->rows.size());

    const InternalControllerIdType controllerId = priv->rows[index].controllerId;
    emit sigUserChangedValue(controllerId, value);
}


void ControllerIdRowMapper::addControllerImpl(const QString& label, InternalControllerIdType id)
{
    Q_ASSERT(id != priv->invalidControllerId);

    priv->rows.push_back( LabelData{label, false, id });
    priv->idToRow[id] = static_cast<int>(priv->rows.size()) - 1;
}


void ControllerIdRowMapper::Private::onControllerPropertiesChanged(InternalControllerIdType id, const ControllerPropertyMap& properties)
{
    const auto it = idToRow.find(id);
    if (it == idToRow.end())
        return;

    const RowNumber row = it->second;
    publ->sigControllerPropertiesChanged(row, properties);
}

}
