// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "plugindatamodel.h"
#include "private/plugin-api/archivehandlerinterface.h"
#include "private/plugindata.h"
#include "private/plugindatamodelaux.h"


namespace Packuru::Core
{

struct PluginDataModel::Private
{
    struct PluginDataExtended : public PluginData
    {
        PluginDataExtended(PluginData&& data)
            : PluginData(data) {}

        QString supportedReadArchives;
        QString supportedWriteArchives;

    };

    const PluginDataExtended& getData(const QModelIndex& index) const;

    PluginDataModel* publ = nullptr;
    std::vector<PluginDataExtended> pluginData;
};


PluginDataModel::PluginDataModel(PluginDataModelAux::Init& initData, QObject* parent)
    : QAbstractListModel(parent),
      priv(new Private)
{
    priv->publ = this;

    for (auto& d : initData.dataList)
    {
        priv->pluginData.emplace_back(std::move(d));
    }

    std::sort(priv->pluginData.begin(), priv->pluginData.end(),
              [] (const auto& data1, const auto& data2)
    { return data1.pluginName < data2.pluginName; } );

    for (auto& data : priv->pluginData)
    {
        const auto plugin = qobject_cast<ArchiveHandlerInterface*>(data.plugin);
        Q_ASSERT(plugin);

        QStringList list;
        for (const auto& type : plugin->supportedTypesForReading())
            list.append(toString(type));

        std::sort(list.begin(), list.end());

        data.supportedReadArchives = list.join('\n');

        list.clear();

        for (const auto& type : plugin->supportedTypesForWriting())
            list.append(toString(type));

        std::sort(list.begin(), list.end());

        data.supportedWriteArchives = list.join('\n');
    }
}


PluginDataModel::~PluginDataModel()
{

}


int PluginDataModel::rowCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent)
    return static_cast<int>(priv->pluginData.size());
}


QVariant PluginDataModel::data(const QModelIndex& index, int role) const
{
    if (role == Qt::DisplayRole)
    {
        const auto& data = priv->getData(index);
        return data.pluginName;
    }

    return QVariant();
}


QString PluginDataModel::getDescription(int row) const
{   
    return priv->pluginData[static_cast<std::size_t>(row)].pluginDescription;
}


QString PluginDataModel::getHomepage(int row) const
{
    return priv->pluginData[static_cast<std::size_t>(row)].backendHomepage;
}


QString PluginDataModel::getPath(int row) const
{
    return priv->pluginData[static_cast<std::size_t>(row)].absFilePath;
}


QString PluginDataModel::supportedReadArchives(int row) const
{
    return priv->pluginData[static_cast<std::size_t>(row)].supportedReadArchives;
}


QString PluginDataModel::supportedWriteArchives(int row) const
{
    return priv->pluginData[static_cast<std::size_t>(row)].supportedWriteArchives;
}


const PluginDataModel::Private::PluginDataExtended& PluginDataModel::Private::getData(const QModelIndex& index) const
{
    Q_ASSERT(publ->checkIndex(index,
                              QAbstractItemModel::CheckIndexOption::IndexIsValid
                              | QAbstractItemModel::CheckIndexOption::ParentIsInvalid));

    return pluginData[static_cast<std::size_t>(index.row())];
}

}
