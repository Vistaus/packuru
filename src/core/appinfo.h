// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QString>

#include "symbol_export.h"

/// @file

#define PACKURU_VERSION_MAJOR 0
#define PACKURU_VERSION_MINOR 6
#define PACKURU_VERSION_PATCH 0


namespace Packuru::Core
{

/**
 @brief Contains basic application information.

 @note For About Dialog use AboutDialogCore class.

 @headerfile "core/appinfo.h"
 */
struct PACKURU_CORE_EXPORT AppInfo
{
    static QString getAppName();
    static QString getAppVersion();

    /**
     @brief Returns full application info, including 3rd-party code (Markdown format).
     */
    static QString getAppInfo();

    static constexpr int getVersionMajor() { return PACKURU_VERSION_MAJOR; }
    static constexpr int getVersionMinor() { return PACKURU_VERSION_MINOR; }
    static constexpr int getVersionPatch() { return PACKURU_VERSION_PATCH; }

    static constexpr bool versionEquals(int major, int minor, int patch)
    {
        return major == getVersionMajor()
                && minor == getVersionMinor()
                && patch == getVersionPatch();
    }
};

}
