// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "appclosingdialogcore.h"
#include "commonstrings.h"


namespace Packuru::Core
{


AppClosingDialogCore::AppClosingDialogCore(QObject *parent)
    : QObject(parent)
{

}


QString AppClosingDialogCore::getString(UserString value, int count)
{
    switch (value)
    {
    case UserString::BrowserBusyBrowsersInfo:
        return Packuru::Core::AppClosingDialogCore::tr("There are %n busy tabs.", "", count);
    case UserString::Cancel:
        return CommonStrings::getString(CommonStrings::UserString::Cancel);
    case UserString::DialogTitle:
        return Packuru::Core::AppClosingDialogCore::tr("Confirm quit");
    case UserString::QueueOpenedDialogsInfo:
        return Packuru::Core::AppClosingDialogCore::tr("%n opened dialog windows will be closed.", "", count);
    case UserString::QueueUnfinishedTasksInfo:
        return Packuru::Core::AppClosingDialogCore::tr("%n unfinished tasks will be aborted.", "" , count);
    case UserString::QuitQuestion:
        return Packuru::Core::AppClosingDialogCore::tr("Do you want to quit?");
    case UserString::Quit:
        return Packuru::Core::AppClosingDialogCore::tr("Quit");
    default:
        Q_ASSERT(false); return QString();
    }
}

}
