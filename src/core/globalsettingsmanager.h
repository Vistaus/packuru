// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QObject>
#include <QVariant>

#include "../utils/qvariant_utils.h"

#include "symbol_export.h"

/// @file

namespace Packuru::Core
{

namespace GlobalSettingsManagerAux
{
struct Init;
}

class GlobalSettingsConverter;

/**
 * @brief Loads and saves settings and notifies about changes.
 *
 * The rationale for creating this class is to allow accessing settings and signal
 * their changes without using string-based keys all over the place.
 * The values are stored at run-time using int as a key and accessed by the user using
 * enum key. The values are also serialized to QSettings.
 *
 * Using this class for frontend development is not required;
 * it only allows to use additional frontend-specific settings in the application.
 *
 * To convert enum-based keys to string-based keys used by QSettings GlobalSettingsConverter
 * subclasses are used.
 * They allow the user to store custom settings in %GlobalSettingsManager.
 * Call addCustomSettingsConverter() to add your converters.
 *
 * %GlobalSettingsManager instance is created by Packuru::Core::Browser::Init and
 * Packuru::Core::Queue::Init objects.
 * To access the instance call instance() function.
 *
 * @sa GlobalSettingsConverter
 * @headerfile "core/globalsettingsmanager.h"
 */
class PACKURU_CORE_EXPORT GlobalSettingsManager : public QObject
{
    Q_OBJECT

public:
    ~GlobalSettingsManager();

    static GlobalSettingsManager* createSingleInstance(GlobalSettingsManagerAux::Init& initData,
                                                       QObject* parent);

    /**
     * @brief Returns the instance.
     */
    static GlobalSettingsManager* instance();

    /** @brief Adds custom converter.
     * @warning Each converters must support unique keys, they must not overlap.
     */
    void addCustomSettingsConverter(std::unique_ptr<GlobalSettingsConverter> converter);

    /** @brief Returns as type T the value specified by the key.
     * @warning There must be a converter set for this key.
     */
    template <typename ValueType, typename KeyType>
    ValueType getValueAs(KeyType key) const
    {
        return Packuru::Utils::getValueAs<ValueType>(getValue(static_cast<int>(key)));
    }

    /** @brief Sets the value specified by the key.
     * @warning There must be a converter set for this key.
     */
    template <typename ValueType, typename KeyType>
    void setValue(KeyType key, const ValueType& value)
    {
        setValue(static_cast<int>(key), QVariant::fromValue(value));
    }

    /** @brief Returns as T the value specified by the key.
     * @note This function is intended for QML use.
     * @warning There must be a converter set for this key.
     */
    Q_INVOKABLE QVariant getValue(int key) const;

    /** @brief Sets the value specified by the key.
     * @note This function is intended for QML use.
     * @warning There must be a converter set for this key.
     */
    Q_INVOKABLE void setValue(int key, const QVariant& value);

signals:
    /** @brief Notifies that the value associated with specified key has changed.
     */
    void valueChanged(int key, const QVariant& value);

private:
    explicit GlobalSettingsManager(QObject *parent = nullptr);

    struct Private;
    std::unique_ptr<Private> priv;
};

}
