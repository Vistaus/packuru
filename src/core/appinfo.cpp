// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QFile>
#include <QTextStream>

#include "appinfo.h"


namespace Packuru::Core
{

QString AppInfo::getAppName()
{
    return "Packuru";
}


QString AppInfo::getAppVersion()
{
    return QString::number(getVersionMajor()) + QLatin1Char('.')
            + QString::number(getVersionMinor()) + QLatin1Char('.')
            + QString::number(getVersionPatch());
}


QString AppInfo::getAppInfo()
{
    QFile file(":/appinfo.md");
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
        return QString{};

    QTextStream in(&file);
    QString s;
    QString temp;

    do
    {
        temp = in.readLine();
        if (temp == QLatin1String("__version__"))
            temp = QLatin1String("Version: ") + getAppVersion() + QLatin1Char('\n');
        else
            temp += QLatin1Char('\n');

        s += temp;
    }
    while (!in.atEnd());

    return s;
}

}
