// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QtCore/qglobal.h>

#if defined(PACKURU_CORE_LIBRARY)
#  define PACKURU_CORE_EXPORT Q_DECL_EXPORT
#else
#  define PACKURU_CORE_EXPORT Q_DECL_IMPORT
#endif
