// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QString>
#include <QCoreApplication>

#include "extractionfileoverwritemode.h"


namespace Packuru::Core
{

QString toString(ExtractionFileOverwriteMode value)
{
    switch (value)
    {
    case ExtractionFileOverwriteMode::AskBeforeOverwrite:
        return QCoreApplication::translate("ExtractionFileOverwriteMode", "Ask before overwrite");
    case ExtractionFileOverwriteMode::OverwriteExisting:
        return QCoreApplication::translate("ExtractionFileOverwriteMode", "Overwrite existing");
    case ExtractionFileOverwriteMode::SkipExisting:
        return QCoreApplication::translate("ExtractionFileOverwriteMode", "Skip existing");
    case ExtractionFileOverwriteMode::UpdateExisting:
        return QCoreApplication::translate("ExtractionFileOverwriteMode", "Update existing");
    default:
        Q_ASSERT(false); return QString();
    }

}

}
