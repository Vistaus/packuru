// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QObject>


namespace Packuru::Core
{

// Detects full disk
class DiskSpaceWatcher : public QObject
{
    Q_OBJECT
public:
    explicit DiskSpaceWatcher(QObject *parent = nullptr);

    explicit DiskSpaceWatcher(const QString& path,
                              QObject *parent = nullptr);
    ~DiskSpaceWatcher();

    // Calls refresh() immediately which can cause diskFull() signal emission from this function.
    void setPath(const QString& path);
    void refresh();
    // Starts timer.
    void start();
    void stop();
    bool diskFullDetected() const;
    bool lowDiskSpaceDetected() const;
    quint64 lowDiskSpaceTreshold() const;

signals:
    void diskFull();
    void lowDiskSpace();

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}
