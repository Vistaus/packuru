// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <vector>

#include <QString>
#include <QFileInfo>
#include <QVariant>

#include "archivetype.h"
#include "core/symbol_export.h"
#include "encryptionstate.h"


namespace Packuru::Core
{

struct BackendExtractionData
{
    QFileInfo archiveInfo;
    ArchiveType archiveType = ArchiveType::___NOT_SUPPORTED;
    QString tempDestinationPath;
    std::vector<QString> filesToExtract; // Dir paths always end with '/'
    bool extractionForPreview = false;
    /* Disabled encryption means that the archive is not encrypted or encryption status
       is unknown because the task has been run from queue without opening the archive.
       If this is not enough, another value, Unknown, could be introduced to EncryptionState.
       This is useful for backends that fail if a password has been provided in the command-line
       but the archive is not encrypted, so BackendHandler can invoke backend without a password
       first and if it fails the backend can be started again with the password provided. */
    EncryptionState encryptionState = EncryptionState::Disabled;
    QString password;

    QVariant pluginPrivateData;
};

}


PACKURU_CORE_EXPORT QDataStream& operator<<(QDataStream& out, const Packuru::Core::BackendExtractionData& data);
PACKURU_CORE_EXPORT QDataStream& operator>>(QDataStream& in, Packuru::Core::BackendExtractionData& data);
