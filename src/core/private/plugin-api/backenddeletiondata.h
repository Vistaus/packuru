// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <vector>

#include <QString>
#include <QFileInfo>
#include <QVariant>
#include "archivetype.h"
#include "core/symbol_export.h"


namespace Packuru::Core
{

struct BackendDeletionData
{
    QFileInfo archiveInfo;
    ArchiveType archiveType = ArchiveType::___NOT_SUPPORTED;
    std::vector<QString> absoluteFilePaths; // Dir paths always end with '/'
    QString password;

    QVariant pluginPrivateData;
};

}


PACKURU_CORE_EXPORT QDataStream& operator<<(QDataStream& out, const Packuru::Core::BackendDeletionData& data);
PACKURU_CORE_EXPORT QDataStream& operator>>(QDataStream& in, Packuru::Core::BackendDeletionData& data);
