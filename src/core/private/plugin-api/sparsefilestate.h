// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once


namespace Packuru::Core
{

enum class SparseFileState : unsigned char
{
    Unknown,
    Sparse,
    NonSparse
};

}
