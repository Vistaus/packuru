// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "QDataStream"

#include "../datastream/container_datastream.h"
#include "../datastream/qfileinfo_datastream.h"

#include "backenddeletiondata.h"


using Packuru::Core::operator<<;
using Packuru::Core::operator>>;


QDataStream& operator<<(QDataStream& out, const Packuru::Core::BackendDeletionData& data)
{
    return out << data.archiveInfo.absoluteFilePath()
               << data.archiveType
               << data.absoluteFilePaths
               << data.password
               << data.pluginPrivateData;
}


QDataStream& operator>>(QDataStream& in, Packuru::Core::BackendDeletionData& data)
{
    return in >> data.archiveInfo
              >> data.archiveType
              >> data.absoluteFilePaths
              >> data.password
              >> data.pluginPrivateData;
}
