// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "backendhandlertasktype.h"


namespace Packuru::Core
{

bool isWritingTask(BackendHandlerTaskType type)
{
    switch (type)
    {
    case BackendHandlerTaskType::Add:
    case BackendHandlerTaskType::Delete:
    case BackendHandlerTaskType::Extract:
        return true;
    default:
        return false;
    }
}

}
