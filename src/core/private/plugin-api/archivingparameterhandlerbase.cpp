// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QVariant>

#include "qcs-core/controlleridrowmapper.h"
#include "qcs-core/controllerengine.h"

#include "archivingparameterhandlerbase.h"
#include "archivingcompressioninfo.h"
#include "encryptionsupport.h"
#include "archivingfilepaths.h"
#include "archivetype.h"
#include "archivingparameterhandlerinitdata.h"


using namespace qcs::core;


namespace Packuru::Core
{

ArchivingParameterHandlerBase::ArchivingParameterHandlerBase(const ArchivingParameterHandlerInitData& initData,
                                                     QObject *parent)
    : QObject(parent),
      initData(initData),
      controllerEngine(new ControllerEngine(this)),
      controllerRowMapper(new ControllerIdRowMapper(this))
{
    controllerRowMapper->setEngine(controllerEngine);
}


ArchivingParameterHandlerBase::~ArchivingParameterHandlerBase()
{

}


ArchiveType ArchivingParameterHandlerBase::getArchiveType() const
{
    return initData.archiveType;
}


ControllerIdRowMapper* ArchivingParameterHandlerBase::getMapper()
{
    return controllerRowMapper;
}


bool ArchivingParameterHandlerBase::archiveSplittingSupport() const
{
    return false;
}


std::unordered_set<ArchivingFilePaths> ArchivingParameterHandlerBase::getSupportedFilePaths() const
{
    auto set = getAdditionalSupportedFilePaths();
    set.insert(ArchivingFilePaths::RelativePaths); // Relative paths must be supported by all plugins
    // set.erase(ArchivingFilePaths::AbsolutePaths); // Absolute paths are evil
    return set;
}


bool ArchivingParameterHandlerBase::customInternalPathSupport() const
{
    return false;
}


ArchivingCompressionInfo ArchivingParameterHandlerBase::getCompressionInfo() const
{
    return ArchivingCompressionInfo{};
}


void ArchivingParameterHandlerBase::setCompressionLevel(int value)
{
    Q_UNUSED(value);
}


EncryptionSupport ArchivingParameterHandlerBase::getEncryptionSupport() const
{
    return EncryptionSupport::NoSupport;
}


QString ArchivingParameterHandlerBase::getEncryptionMethod() const
{
    return QString();
}


std::unordered_set<ArchivingFilePaths> ArchivingParameterHandlerBase::getAdditionalSupportedFilePaths() const
{
    return std::unordered_set<ArchivingFilePaths>();
}


QVariant ArchivingParameterHandlerBase::getPrivateArchivingData() const
{
    return QVariant();
}

}
