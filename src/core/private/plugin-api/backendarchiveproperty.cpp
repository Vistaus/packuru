// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "backendarchiveproperty.h"


namespace Packuru::Core
{

QLatin1String toLatin1(BackendArchiveProperty value)
{
    switch (value)
    {
    case BackendArchiveProperty::Comment: return QLatin1String("Comment");
    case BackendArchiveProperty::EncryptionState: return QLatin1String("EncryptionState");
    case BackendArchiveProperty::Locked: return QLatin1String("Locked");
    case BackendArchiveProperty::Methods: return QLatin1String("Methods");
    case BackendArchiveProperty::Multipart: return QLatin1String("Multipart");
    case BackendArchiveProperty::RecoveryData: return QLatin1String("RecoveryData");
    case BackendArchiveProperty::Snapshots: return QLatin1String("Snapshots");
    case BackendArchiveProperty::Solid: return QLatin1String("Solid");
    case BackendArchiveProperty::PartCount: return QLatin1String("PartCount");
    case BackendArchiveProperty::TotalSize: return QLatin1String("TotalSize");
    default: Q_ASSERT(false); return QLatin1String("");
    }
}


char* toString(BackendArchiveProperty value)
{
    const QLatin1String str = toLatin1(value);
    return qstrdup(str.data());
}

}
