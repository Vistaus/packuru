// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <memory>

#include <QFileInfo>
#include <QRegularExpression>

#include "multipartarchivehelper.h"
#include "archivetype.h"


namespace Packuru::Core
{

namespace
{

// Exclude *.lz0.lz, *.lz00.lz, *.lz000.lz...
const QRegularExpression lzipPattern("(^.+)(?!0+\\.lz$)(\\d+\\.lz$)");

// Exclude *.tar*0.lz, *.tar*00.lz, *.tar*000.lz...
// Include e.g. *.tar00001.lz, *.tar.00001.lz, *.tar.part00001.lz
const QRegularExpression lzipTarPattern("(^.+\\.tar.*)(?!0+\\.lz$)(\\d+\\.lz$)");

// Exclude *.part.rar, *.part0.rar, *.part00.rar...
// *.part1.rar, *.part2.rar...
// *.part01.rar, *.part02.rar...
const QRegularExpression rarNewPattern("(^.+\\.part)(?!0*\\.rar$)(\\d+\\.rar$)");

// Exclude new rar pattern (in case they are processed in different order)
// Exclude *.sar and *.tar
// *.rar, *.r00...*.r99, *.s00...*.s99, *.t00...*.t99
const QRegularExpression rarOldPattern("(?!^.+\\.part[1-9](\\d*).rar$)(^.+\\.(rar|[rstuvwxyz{|]\\d\\d)$)");

// Exclude *.7z.000
// *.7z.001, *.7z.002...*.7z.999
const QRegularExpression split7zPattern("^.+\\.7z\\.(?!000)(\\d{3}$)");

// Exclude *.000
// *.001, *.002...*.999
const QRegularExpression splitGeneralPattern("^.+\\.(?!000)(\\d{3}$)");

// Exclude *.7z.000
// *.7z.001, *.7z.002...*.7z.999
const QRegularExpression splitZipPattern("^.+\\.zip\\.(?!000)\\d{3}$");

// Exclude *.z00:
// *.zip, *.z01, *.z02...*.z99
const QRegularExpression zipPattern("^.+\\.z(?!00)(ip|\\d\\d)$");


struct PartIterator
{
    PartIterator()
        : null(true),
          partCount_(0),
          currentIndex(0) {}

    virtual ~PartIterator() {}

    bool isNull() const { return null; }

    virtual QString next() { return  QString(); }

    bool null;
    QString partName;
    const QString archivePath;
    int partCount_;
    int currentIndex;

protected:
    PartIterator(const QFileInfo& archiveInfo, int partCount, int maxPartCount, int startIndex)
        : null(false),
          partName(archiveInfo.fileName()),
          archivePath(archiveInfo.absolutePath()),
          partCount_(partCount),
          currentIndex(startIndex)
    {
        if (partCount_ == 0 || partCount_ > maxPartCount)
            partCount_ = maxPartCount;
    }
};


struct LzipIterator : public PartIterator
{
    LzipIterator(const QFileInfo& archiveInfo, int partCount)
        : PartIterator(archiveInfo, partCount, 99999, 1) // Artificial max limit
    {
        partId.resize(digitCount, '0');
    }

    QString next() override
    {
        if (currentIndex > partCount_)
            return QString();

        const auto partNumberStr = QString::number(currentIndex);
        partId.truncate(partId.length() - partNumberStr.length());
        partId.append(partNumberStr);

        partName.truncate(pos);
        partName.append(partId + QLatin1String(".lz"));
        ++currentIndex;

        return archivePath + QLatin1Char('/') + partName;
    }

    const QRegularExpression pattern{"(\\d+)\\.lz"};
    const int pos = partName.lastIndexOf(pattern);
    const int digitCount = pattern.match(partName, pos).captured(1).length();
    QString partId;
};


struct RarNewIterator : public PartIterator
{
    RarNewIterator(const QFileInfo& archiveInfo, int partCount)
        : PartIterator(archiveInfo, partCount, 32768, 1) // Artificial max limit
    {
        partId.resize(digitCount, '0');
    }

    QString next() override
    {
        if (currentIndex > partCount_)
            return QString();

        const auto partNumberStr = QString::number(currentIndex);
        partId.truncate(partId.length() - partNumberStr.length());
        partId.append(partNumberStr);

        partName.truncate(pos);
        partName.append(QLatin1String(".part") + partId + QLatin1String(".rar"));
        ++currentIndex;

        return archivePath + QLatin1Char('/') + partName;
    }

    const QRegularExpression pattern{"(\\.part)(\\d+)\\.rar"};
    const int pos = partName.lastIndexOf(pattern);
    const int digitCount = pattern.match(partName, pos).captured(2).length();
    QString partId;
};


struct RarOldIterator : public PartIterator
{
    RarOldIterator(const QFileInfo& archiveInfo, int partCount)
        : PartIterator(archiveInfo, partCount, 1001, 0), // Old rar extension limit
          letters("rstuvwxyz{|")
    {
    }

    QString next() override
    {
        if (checkedParts == partCount_ || currentLetterIndex == letters.size())
            return QString();

        return (this->*currentStep)();
    }

private:
    QString step1()
    {
        partName.chop(3);
        partName.append(QLatin1String("rar"));
        ++checkedParts;
        currentStep = &RarOldIterator::step2;
        return archivePath + QLatin1Char('/') + partName;
    }

    QString step2()
    {
        if (currentIndex == 0)
        {
            // Start with r00, s00, t00...
            partName.replace(partName.size() - 3, 3, letters[currentLetterIndex] + QLatin1String("00"));
        }
        else
        {
            auto partNumberStr = QString::number(currentIndex);
            partName.chop(partNumberStr.size());
            partName.append(partNumberStr);
        }

        ++currentIndex;
        ++checkedParts;

        if (currentIndex == 100)
        {
            ++currentLetterIndex;
            currentIndex = 0;
        }

        return archivePath + QLatin1Char('/') + partName;
    }

    using StepCallback = QString(RarOldIterator::*)();
    StepCallback currentStep = &RarOldIterator::step1;
    const QLatin1String letters;
    int currentLetterIndex = 0;
    int checkedParts = 0;
};


struct SplitIterator : public PartIterator
{
    SplitIterator(const QFileInfo& archiveInfo, int partCount)
        : PartIterator(archiveInfo, partCount, 999, 1)
    {
        partName.replace(partName.size() - 3, 3, "000"); // Change extension to 000
    }

    QString next() override
    {
        if (currentIndex > partCount_)
            return QString();

        auto partNumberStr = QString::number(currentIndex);
        partName.chop(partNumberStr.size());
        partName.append(partNumberStr);
        ++currentIndex;
        return archivePath + QLatin1Char('/') + partName;
    }
};


struct ZipIterator : public PartIterator
{
    ZipIterator(const QFileInfo& archiveInfo, int partCount)
        : PartIterator(archiveInfo, partCount, 100, 0) // Zip extension limit: first *.zip, then *.z01 to z99
    {
    }

    QString next() override
    {
        if (currentIndex == partCount_)
            return QString();
        return (this->*currentStep)();
    }

private:
    QString step1()
    {
        partName.chop(2);
        partName.append(QLatin1String("ip"));
        currentStep = &ZipIterator::step2;
        ++currentIndex;
        return archivePath + QLatin1Char('/') + partName;
    }

    QString step2()
    {
        if (currentIndex == 1)
            partName.replace(partName.size() - 2, 2, "00"); // Change extension to z00 (not used)

        auto partNumberStr = QString::number(currentIndex);
        partName.chop(partNumberStr.size());
        partName.append(partNumberStr);
        ++currentIndex;
        return archivePath + QLatin1Char('/') + partName;
    }

    using StepCallback = QString(ZipIterator::*)();
    StepCallback currentStep = &ZipIterator::step1;
};


std::unique_ptr<PartIterator> createPartIterator(const QString& filePath, ArchiveType type, int partCount = 0)
{
    const QFileInfo archiveInfo(filePath);
    const QString archiveName = archiveInfo.fileName();

    if ((type == ArchiveType::lzip && lzipPattern.match(archiveName).hasMatch())
            || (type == ArchiveType::tar_lzip && lzipTarPattern.match(archiveName).hasMatch()))
        return std::make_unique<LzipIterator>(archiveInfo, partCount);

    else if (type == ArchiveType::rar && rarNewPattern.match(archiveName).hasMatch())
        return std::make_unique<RarNewIterator>(archiveInfo, partCount);

    // Ensure the type is rar because both rar and zip archives can have z01...z99 extension
    else if (type == ArchiveType::rar && rarOldPattern.match(archiveName).hasMatch())
        return std::make_unique<RarOldIterator>(archiveInfo, partCount);

    else if (splitGeneralPattern.match(archiveName).hasMatch())
        return std::make_unique<SplitIterator>(archiveInfo, partCount);

    // Ensure the type is zip because both rar and zip archives can have z01...z99 extension
    else if (type == ArchiveType::zip && zipPattern.match(archiveName).hasMatch())
        return std::make_unique<ZipIterator>(archiveInfo, partCount);

    return std::make_unique<PartIterator>();
}


std::vector<QString> getArchiveExistingPartPaths(PartIterator* iter)
{
    Q_ASSERT(iter);

    std::vector<QString> paths;

    if (iter->isNull())
        return paths;

    while (true)
    {
        const QString path = iter->next();

        if (path.isEmpty())
            break;

        const QFileInfo info(path);
        if (!info.exists())
            break;

        paths.push_back(path);
    }

    return paths;
}


int getArchiveExistingPartCount(PartIterator* iter)
{
    Q_ASSERT(iter);

    int result = 0;

    if (iter->isNull())
        return result;

    while (true)
    {
        const QString path = iter->next();

        if (path.isEmpty())
            break;

        const QFileInfo info(path);
        if (!info.exists())
            break;

        ++result;
    }

    return result;
}


quint64 computeArchiveSize(PartIterator* iter)
{
    Q_ASSERT(iter);

    quint64 result = 0;

    if (iter->isNull())
        return result;

    while (true)
    {
        const QString path = iter->next();

        if (path.isEmpty())
            break;

        const QFileInfo info(path);
        if (!info.exists())
            break;

        result += static_cast<quint64>(info.size());
    }

    return result;
}

}


bool isMultiPartArchiveName(const QString& filePath, ArchiveType type)
{
    const QFileInfo archiveInfo(filePath);

    if (type == ArchiveType::lzip && lzipPattern.match(archiveInfo.fileName()).hasMatch())
        return true;

    else if (type == ArchiveType::tar_lzip && lzipTarPattern.match(archiveInfo.fileName()).hasMatch())
        return true;

    else if (type == ArchiveType::rar
             && (rarNewPattern.match(archiveInfo.fileName()).hasMatch()
                 || rarOldPattern.match(archiveInfo.fileName()).hasMatch()))
        return true;

    else if (type == ArchiveType::_7z && split7zPattern.match(archiveInfo.fileName()).hasMatch())
        return true;

    else if (type == ArchiveType::zip
             && (splitZipPattern.match(archiveInfo.fileName()).hasMatch()
                 || zipPattern.match(archiveInfo.fileName()).hasMatch()))
        return true;

    return false;
}


std::vector<QString> getArchiveExistingPartPaths(const QString& filePath, ArchiveType type)
{
    std::unique_ptr<PartIterator> iter = createPartIterator(filePath, type);
    return getArchiveExistingPartPaths(iter.get());
}


int getArchiveExistingPartCount(const QString& filePath, ArchiveType type)
{
    std::unique_ptr<PartIterator> iter = createPartIterator(filePath, type);
    return getArchiveExistingPartCount(iter.get());
}


quint64 computeArchiveSize(const QString& filePath, ArchiveType type, int partCount)
{
    std::unique_ptr<PartIterator> iter = createPartIterator(filePath, type, partCount);
    return computeArchiveSize(iter.get());
}


QString getFirstRarPart(const QString& filePath)
{
    const QFileInfo archiveInfo(filePath);
    QString archiveName = archiveInfo.fileName();

    if (rarNewPattern.match(archiveName).hasMatch())
    {
        RarNewIterator it(archiveInfo, 1);
        return it.next();
    }
    else if (rarOldPattern.match(archiveName).hasMatch())
    {
        RarOldIterator it(archiveInfo, 1);
        return it.next();
    }

    return QString();
}

}
