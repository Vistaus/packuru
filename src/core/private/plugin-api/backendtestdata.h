// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QFileInfo>
#include <QString>
#include <QVariant>

#include "archivetype.h"
#include "core/symbol_export.h"
#include "encryptionstate.h"


namespace Packuru::Core
{

struct BackendTestData
{
    QFileInfo archiveInfo;
    ArchiveType archiveType = ArchiveType::___NOT_SUPPORTED;
    /* Disabled encryption means that the archive is not encrypted or encryption status
       is unknown because the task has been run from queue without opening the archive.
       If this is not enough, another value, Unknown, could be introduced to EncryptionState. */
    EncryptionState encryptionState = EncryptionState::Disabled;
    QString password;

    QVariant pluginPrivateData;
};

}


PACKURU_CORE_EXPORT QDataStream& operator<<(QDataStream& out, const Packuru::Core::BackendTestData& data);
PACKURU_CORE_EXPORT QDataStream& operator>>(QDataStream& in, Packuru::Core::BackendTestData& data);
