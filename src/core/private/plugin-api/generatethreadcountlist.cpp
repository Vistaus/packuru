// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "generatethreadcountlist.h"


namespace Packuru::Core
{

QList<int> generateThreadCountList(int maxThreadCount)
{
    const int maxStepCount = 8;
    const int stepSize = std::max(maxThreadCount / maxStepCount, 2);
    const int resultStepCount = maxThreadCount / stepSize + 1;

    QList<int> list {1}; // Allow single thread
    list.reserve(resultStepCount);

    for (int i = stepSize; i <= maxThreadCount; i += stepSize)
        list.push_back(i);

    return list;
}

}
