// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QCoreApplication>

#include "archivingfilepaths.h"


namespace Packuru::Core
{

QString toString(ArchivingFilePaths value)
{
    switch (value)
    {
    case ArchivingFilePaths::AbsolutePaths:
        return QCoreApplication::translate("ArchivingFilePaths", "Absolute paths");
    case ArchivingFilePaths::FullPaths:
        return QCoreApplication::translate("ArchivingFilePaths", "Full paths");
    case ArchivingFilePaths::RelativePaths:
        return QCoreApplication::translate("ArchivingFilePaths", "Relative paths");
    default:
        Q_ASSERT(false); return QString();
    }
}

}
