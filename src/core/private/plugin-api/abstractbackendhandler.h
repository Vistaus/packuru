// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>
#include <unordered_set>

#include <QObject>

#include "archiveitem.h"
#include "backendhandlertasktype.h"
#include "backendhandlererror.h"
#include "backendhandlerstate.h"
#include "backendhandlerwarning.h"
#include "backendarchiveproperties.h"
#include "core/symbol_export.h"


namespace Packuru::Core
{


class BackendExtractionData;
class BackendArchivingData;
class BackendDeletionData;
class BackendTestData;
class BackendReadData;
enum class ArchiveType;
class DiskSpaceWatcher;

class PACKURU_CORE_EXPORT AbstractBackendHandler : public QObject
{
    Q_OBJECT

public:
    explicit AbstractBackendHandler(QObject *parent = nullptr);
    ~AbstractBackendHandler() override;

    void readArchive(const BackendReadData& data);
    void extractArchive(const BackendExtractionData& data);
    void addToArchive(const BackendArchivingData& data);
    void deleteFiles(const BackendDeletionData& data);
    void testArchive(const BackendTestData& data);

    void stop();
    bool stopRequested() const;

    BackendHandlerTaskType getTaskType() const;
    BackendHandlerState getState() const;
    bool isReady() const;
    bool isBusy() const;
    bool isFinished() const;
    bool isCompleted() const;

    std::unordered_set<BackendHandlerError> getErrors() const;
    bool hasErrors() const;
    bool hasError(BackendHandlerError error) const;

    std::unordered_set<BackendHandlerWarning> getWarnings() const;
    bool hasWarnings() const;
    bool hasWarning(BackendHandlerWarning warning) const;

    QString clearLog();

    // Password must not be empty as it might result in inconsistent behavior across backend handlers
    virtual void enterPassword(const QString&) {}

    virtual BackendArchiveProperties getArchiveProperties() const { return archiveProperties; }

    virtual QString createSinglePartArchiveName(const QString& archiveBaseName, ArchiveType archiveType) const;
    virtual QString createMultiPartArchiveName(const QString& archiveBaseName, ArchiveType archiveType) const;
    virtual QString createMultiPartArchiveRegexNamePattern(const QString& archiveBaseName, ArchiveType archiveType) const;

signals:
    void progressChanged(int value);
    void stateChanged(BackendHandlerState state);
    // Items will be cleared by the receiver
    void newItems(std::vector<ArchiveItem>& items);

protected:
    virtual void readArchiveImpl(const BackendReadData&) {}
    virtual void extractArchiveImpl(const BackendExtractionData&) {}
    virtual void addToArchiveImpl(const BackendArchivingData&) {}
    virtual void deleteFilesImpl(const BackendDeletionData&) {}
    virtual void testArchiveImpl(const BackendTestData&) {}
    virtual void stopImpl() = 0;

    virtual void onNewError(BackendHandlerError error) { Q_UNUSED(error) }

    void changeState(BackendHandlerState newState);
    void changeProgress(int value);
    void addError(BackendHandlerError e);
    void addError(BackendHandlerError e, const QString& line);
    void addError(BackendHandlerError e, QStringRef line);
    void addWarning(BackendHandlerWarning w, const QString& line);
    void addWarning(BackendHandlerWarning w, QStringRef line);
    void removeWarning(BackendHandlerWarning w);
    void addLogLine(const QString& line);
    void addLogLine(QStringRef line);
    void addLogLines(const std::vector<QStringRef>& lines);

    BackendArchiveProperties archiveProperties;
    std::vector<ArchiveItem> items;
    DiskSpaceWatcher* diskWatcher;

private:
    struct Private;
    friend struct Private;
    std::unique_ptr<Private> priv;
};

}
