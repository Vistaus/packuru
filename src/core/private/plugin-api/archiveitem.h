// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <unordered_map>

#include <QDateTime>

#include "utils/boolflag.h"

#include "../fundamental.h"
#include "../integral.h"

#include "sparsefilestate.h"
#include "nodetype.h"


namespace Packuru::Core
{

struct ArchiveItem
{
    ArchiveItem() = default;
    ArchiveItem(const ArchiveItem&) = delete;
    ArchiveItem(ArchiveItem&& other) = default;

    ArchiveItem& operator=(const ArchiveItem&) = delete;
    ArchiveItem& operator=(ArchiveItem&& other) = default;

    /* Keep dates in local time (QDateTime default) as some archive formats might neither store time zone
       information nor UTC time. And we show local time to the user so want to avoid converting it from
       UTC on every access in ArchiveContentModel::data(). */
    QDateTime accessed;
    QDateTime created;
    QDateTime modified;

    QString attributes;
    QString block;
    QString comment;
    QString checksum;
    QString group;
    QString hostOS;
    QString method;
    QString name;
    QString user;

    Fundamental<qulonglong> originalSize;
    Fundamental<qulonglong> packedSize;

    Integral<NodeType, NodeType::File> nodeType;
    Integral<SparseFileState, SparseFileState::Unknown> sparseState;

    Packuru::Utils::BoolFlag<false> encrypted;
};

}
