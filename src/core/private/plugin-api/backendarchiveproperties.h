// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <unordered_map>

#include <QVariant>


namespace Packuru::Core
{

enum class BackendArchiveProperty;
using BackendArchiveProperties = std::unordered_map<BackendArchiveProperty, QVariant>;

}

Q_DECLARE_METATYPE(Packuru::Core::BackendArchiveProperties)
