// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QFileInfo>
#include <QVariant>

#include "archivetype.h"
#include "core/symbol_export.h"


namespace Packuru::Core
{

struct PACKURU_CORE_EXPORT BackendReadData
{
    QFileInfo archiveInfo;
    ArchiveType archiveType = ArchiveType::___NOT_SUPPORTED;
    QString password;

    QVariant pluginPrivateData;
};

}
