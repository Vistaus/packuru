// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "backendhandlererror.h"


namespace Packuru::Core
{

QLatin1String toLatin1(BackendHandlerError value)
{
    switch (value)
    {
    case BackendHandlerError::AccessDenied: return QLatin1String("AccessDenied");
    case BackendHandlerError::ArchivePartNotFound: return QLatin1String("ArchivePartNotFound");
    case BackendHandlerError::ArchivingMethodNotSupported: return QLatin1String("ArchivingMethodNotSupported");
    case BackendHandlerError::Crash: return QLatin1String("Crash");
    case BackendHandlerError::DangerousLink: return QLatin1String("DangerousLink");
    case BackendHandlerError::DataError: return QLatin1String("DataError");
    case BackendHandlerError::DiskFull: return QLatin1String("DiskFull");
    case BackendHandlerError::ExecutableNotFound: return QLatin1String("ExecutableNotFound");
    case BackendHandlerError::FileNotFound: return QLatin1String("FileNotFound");
    case BackendHandlerError::InvalidCommandLine: return QLatin1String("InvalidCommandLine");
    case BackendHandlerError::InvalidPasswordOrDataError: return QLatin1String("InvalidPasswordOrDataError");
    case BackendHandlerError::ItemTypeMismatch: return QLatin1String("ItemTypeMismatch");
    case BackendHandlerError::NotEnoughMemory: return QLatin1String("NotEnoughMemory");
    case BackendHandlerError::SubmoduleNotFound: return QLatin1String("SubmoduleNotFound");
    case BackendHandlerError::TemporaryArchiveError: return QLatin1String("TemporaryArchiveError");
    case BackendHandlerError::TemporaryFolderNotEmpty: return QLatin1String("TemporaryFolderNotEmpty");
    case BackendHandlerError::TemporaryFolderNotFound: return QLatin1String("TemporaryFolderNotFound");
    case BackendHandlerError::WriteError: return QLatin1String("WriteError");
    case BackendHandlerError::___INVALID: return QLatin1String("___INVALID");
    default: Q_ASSERT(false); return QLatin1String("");
    }
}

char* toString(BackendHandlerError value)
{
    const QLatin1String str = toLatin1(value);
    return qstrdup(str.data());
}


}
