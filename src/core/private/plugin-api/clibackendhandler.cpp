// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QProcess>
#include <QProcessEnvironment>
#include <QStandardPaths>

#include "clibackendhandler.h"
#include "textstreampreprocessor.h"
#include "backendexitcodeinterpretation.h"
#include "diskspacewatcher.h"


namespace Packuru::Core
{

struct CLIBackendHandler::Private
{
    std::vector<QStringList> backendArgs;
};


CLIBackendHandler::CLIBackendHandler(QObject* parent)
    : AbstractBackendHandler(parent),
      backendProcess (new QProcess(this)),
      outPreprocessor(new TextStreamPreprocessor(this)),
      errPreprocessor(new TextStreamPreprocessor(this)),
      priv(new Private)
{
    backendProcess->setProcessChannelMode(QProcess::SeparateChannels);

    // Ensure that backends print in English
    QProcessEnvironment env(QProcessEnvironment::systemEnvironment());
    env.insert("LANG", "en_US.UTF-8");
    env.insert("LANGUAGE", "en_US:en");
    backendProcess->setProcessEnvironment(env);

    connect(backendProcess, &QProcess::errorOccurred,
            [hnd = this, proc = backendProcess] (auto err)
    {
        if (err == QProcess::FailedToStart)
        {
            if (QStandardPaths::findExecutable(proc->program()).isEmpty())
            {
                hnd->addError(BackendHandlerError::ExecutableNotFound,
                              proc->errorString() + QLatin1String(": ") + proc->program());
                hnd->changeState(BackendHandlerState::Errors);
            }
        }
    });

    connect(backendProcess, &QProcess::readyReadStandardOutput,
            outPreprocessor, [preprocessor = outPreprocessor, process = backendProcess] ()
    {
        preprocessor->processBytes(process->readAllStandardOutput());
    });

    connect(backendProcess, &QProcess::readyReadStandardError,
            errPreprocessor, [preprocessor = errPreprocessor, process = backendProcess] ()
    {
        preprocessor->processBytes(process->readAllStandardError());
    });

    connect(backendProcess, &QProcess::started,
            this, [handler = this] ()
    {
        auto args = handler->backendProcess->arguments();

        args.prepend(handler->backendProcess->program());

        handler->priv->backendArgs.push_back(args);
    });

}


CLIBackendHandler::~CLIBackendHandler()
{

}


void CLIBackendHandler::stopImpl()
{
    if (backendProcess->state() == QProcess::NotRunning)
        changeState(BackendHandlerState::Aborted);
    else
        backendProcess->close();
}


void CLIBackendHandler::onNewError(BackendHandlerError error)
{
    if (error == BackendHandlerError::DiskFull)
    {
        Q_ASSERT(isWritingTask(getTaskType()));
        if (backendProcess->state() != QProcess::NotRunning)
        {
            addLogLine("Disk full, aborting process " + backendProcess->program());
            backendProcess->kill();
        }
    }
}


void CLIBackendHandler::enterPassword(const QString& password)
{
    Q_ASSERT(!password.isEmpty());
    passwordReceived_ = true;
    backendProcess->write((password + "\n").toUtf8());
    changeState(BackendHandlerState::InProgress);
}


const std::vector<QStringList>& CLIBackendHandler::getBackendArgs() const
{
    return priv->backendArgs;
}


BackendHandlerState CLIBackendHandler::handleFinishedProcess(BackendExitCodeInterpretation exitCodeInter)
{
    /* Some backends do not report full disk error and return exit code 0.
     * By checking disk space here, even when process reported success, we
     * can catch this error.
     * We might also accidentally report false error, e.g. the process has successfully finished,
     * the archive has been successfully created and space left was 1KB but before
     * we check disk space here, another process fills the disk so we report error.
     * However I think its worth it, the user can free disk space and restart the task,
     * and the danger of corrupted data is lowered.
     * It is also possible that none of our checks detect full disk as another processes
     * running in parallel might free some space. In that case we hopefully can detect
     * at least low disk space.
     */
    if (isWritingTask(getTaskType()))
    {
        diskWatcher->refresh();

        if (!hasError(BackendHandlerError::DiskFull))
        {
            if (diskWatcher->diskFullDetected())
            {
                addError(BackendHandlerError::DiskFull, "Detected full disk on backend exit");
            }
            else if (!hasWarning(BackendHandlerWarning::LowDiskSpace))
            {
                if (diskWatcher->lowDiskSpaceDetected())
                    addWarning(BackendHandlerWarning::LowDiskSpace,
                               "Warning: Low disk space detected on backend exit");
            }
        }

        if (hasError(BackendHandlerError::DiskFull))
            removeWarning(BackendHandlerWarning::LowDiskSpace);
    }

    if (stopRequested())
        return BackendHandlerState::Aborted;

    addLogLine(backendProcess->program()
               + " exit code = "+ QString::number(backendProcess->exitCode()));

    auto state = BackendHandlerState::Success;

    if (backendProcess->exitStatus() == QProcess::NormalExit)
    {
        if (exitCodeInter == BackendExitCodeInterpretation::PasswordRequiredNotProvided)
            state = BackendHandlerState::WaitingForPassword;
        else if (hasErrors() || exitCodeInter == BackendExitCodeInterpretation::WarningsOrErrors)
            state = BackendHandlerState::Errors;
        else if (hasWarnings())
            state = BackendHandlerState::Warnings;
    }
    // CrashExit is also reported when QProcess is aborted while running
    else if (backendProcess->exitStatus() == QProcess::CrashExit)
    {
#ifdef Q_OS_UNIX
        const auto exitCode = backendProcess->exitCode();

        // 128 + 11 (SIGSEGV)
        // https://www.tldp.org/LDP/abs/html/exitcodes.html
        // It looks like it can be both 11 or 139
        if (exitCode == 11 || exitCode == 139)
            addError(BackendHandlerError::Crash, "Segmentation fault");
#endif

        state = BackendHandlerState::Errors;
    }

    return state;
}


void CLIBackendHandler::logCommand(const QString& executableName, const QStringList& args)
{
    QString command;
    command.append(executableName);

    for (const auto& arg : args)
        command.append(QLatin1String(" \"") + arg + QLatin1Char('\"'));

    addLogLine(command);
}

}
