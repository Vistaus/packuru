// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <unordered_map>
#include <type_traits>

#include <QString>

#include "utils/private/qttypehasher.h"

#include "archivetype.h"


namespace Packuru::Core
{

bool isValid(ArchiveType type)
{
    using UnderlyingType = std::underlying_type<ArchiveType>::type;
    return static_cast<UnderlyingType>(type) >= 0 && type < ArchiveType::___COUNT;
}


bool isTar(ArchiveType type)
{
    switch (type)
    {
    case ArchiveType::tar:
    case ArchiveType::tar_brotli:
    case ArchiveType::tar_bzip2:
    case ArchiveType::tar_compress:
    case ArchiveType::tar_gzip:
    case ArchiveType::tar_lrzip:
    case ArchiveType::tar_lz4:
    case ArchiveType::tar_lzip:
    case ArchiveType::tar_lzma:
    case ArchiveType::tar_lzop:
    case ArchiveType::tar_xz:
    case ArchiveType::tar_zstd:
        return true;
    default:
        return false;
    }
}


QLatin1String toString(ArchiveType type)
{
    static const std::unordered_map<ArchiveType, QLatin1String> types {
        {ArchiveType::_7z,                      QLatin1String("7z")},
        {ArchiveType::ace,                      QLatin1String("ace")},
        {ArchiveType::alz,                      QLatin1String("alz")},
        {ArchiveType::appimage,                 QLatin1String("appimage")},
        {ArchiveType::ar,                       QLatin1String("ar")},
        {ArchiveType::arc,                      QLatin1String("arc")},
        {ArchiveType::arj,                      QLatin1String("arj")},
        {ArchiveType::brotli,                   QLatin1String("brotli")},
        {ArchiveType::bzip2,                     QLatin1String("bzip2")},
        {ArchiveType::cab,                      QLatin1String("cab")},
        {ArchiveType::chm,                      QLatin1String("chm")},
        {ArchiveType::compress,                 QLatin1String("compress")},
        {ArchiveType::cpio,                     QLatin1String("cpio")},
        {ArchiveType::cramfs,                   QLatin1String("cramfs")},
        {ArchiveType::deb,                      QLatin1String("deb")},
        {ArchiveType::dmg,                      QLatin1String("dmg")},
        {ArchiveType::ext,                      QLatin1String("ext")},
        {ArchiveType::fat,                      QLatin1String("fat")},
        {ArchiveType::gzip,                     QLatin1String("gzip")},
        {ArchiveType::iso9660,                  QLatin1String("iso9660")},
        {ArchiveType::lha,                      QLatin1String("lha")},
        {ArchiveType::lrzip,                    QLatin1String("lrzip")},
        {ArchiveType::lz4,                      QLatin1String("lz4")},
        {ArchiveType::lzip,                     QLatin1String("lzip")},
        {ArchiveType::lzma,                     QLatin1String("lzma")},
        {ArchiveType::lzop,                     QLatin1String("lzop")},
        {ArchiveType::nrg,                      QLatin1String("nrg")},
        {ArchiveType::mbr,                      QLatin1String("mbr")},
        {ArchiveType::mdx,                      QLatin1String("mdx")},
        {ArchiveType::msi,                      QLatin1String("msi")},
        {ArchiveType::ntfs,                     QLatin1String("ntfs")},
        {ArchiveType::pak,                      QLatin1String("pak")},
        {ArchiveType::pdf,                      QLatin1String("pdf")},
        {ArchiveType::qcow,                     QLatin1String("qcow")},
        {ArchiveType::rar,                      QLatin1String("rar")},
        {ArchiveType::raw_optical,              QLatin1String("raw optical")},
        {ArchiveType::rpm,                      QLatin1String("rpm")},
        {ArchiveType::sit,                      QLatin1String("sit")},
        {ArchiveType::squashfs,                 QLatin1String("squashfs")},
        {ArchiveType::swf,                      QLatin1String("swf")},
        {ArchiveType::tar,                      QLatin1String("tar")},
        {ArchiveType::tar_brotli,               QLatin1String("tar/brotli")},
        {ArchiveType::tar_bzip2,                 QLatin1String("tar/bzip2")},
        {ArchiveType::tar_compress,             QLatin1String("tar/compress")},
        {ArchiveType::tar_gzip,                 QLatin1String("tar/gzip")},
        {ArchiveType::tar_lrzip,                QLatin1String("tar/lrzip")},
        {ArchiveType::tar_lz4,                  QLatin1String("tar/lz4")},
        {ArchiveType::tar_lzip,                 QLatin1String("tar/lzip")},
        {ArchiveType::tar_lzma,                 QLatin1String("tar/lzma")},
        {ArchiveType::tar_lzop,                 QLatin1String("tar/lzop")},
        {ArchiveType::tar_xz,                   QLatin1String("tar/xz")},
        {ArchiveType::tar_zstd,                 QLatin1String("tar/zstd")},
        {ArchiveType::udf,                      QLatin1String("udf")},
        {ArchiveType::vdi,                      QLatin1String("vdi")},
        {ArchiveType::vhd,                      QLatin1String("vhd")},
        {ArchiveType::vmdk,                     QLatin1String("vmdk")},
        {ArchiveType::wim,                      QLatin1String("wim")},
        {ArchiveType::xar,                      QLatin1String("xar")},
        {ArchiveType::xz,                       QLatin1String("xz")},
        {ArchiveType::zip,                      QLatin1String("zip")},
        {ArchiveType::zpaq,                     QLatin1String("zpaq")},
        {ArchiveType::zstd,                     QLatin1String("zstd")},

        {ArchiveType::___NOT_SUPPORTED,         QLatin1String("")}
    };

    const auto it = types.find(type);
    Q_ASSERT(it != types.end());
    return it->second;
}


void fromString(const QString& type, ArchiveType& result)
{
    static const auto types = [] (auto toString)
    {
        std::unordered_map<QString, ArchiveType, Utils::QtTypeHasher<QString>> types;

        for (int i = 0; i < static_cast<int>(ArchiveType::___COUNT); ++i)
        {
            auto type = static_cast<ArchiveType>(i);
            types[toString(type)] = type;
        }

        return types;
    } (toString);

    const auto it = types.find(type);

    if (it != types.end())
        result = it->second;
    else
        result = ArchiveType::___NOT_SUPPORTED;
}

}
