// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "archivingparameterhandlerfactory.h"


namespace Packuru::Core
{

ArchivingParameterHandlerFactory::ArchivingParameterHandlerFactory(ArchiveType archiveType,
                                                                   const QString& archiveTypeAlias,
                                                                   uint pluginId,
                                                                   const CreateHandlerFunction& createHandler)
    : archiveType_(archiveType),
      archiveTypeAlias_(archiveTypeAlias),
      pluginId(pluginId),
      createHandler_(createHandler)
{
}


ArchivingParameterHandlerBase*
ArchivingParameterHandlerFactory::createHandler(const ArchivingParameterHandlerInitData& initData)
{
    Q_ASSERT(!handler);
    handler = createHandler_(initData);
    return handler.get();
}

}
