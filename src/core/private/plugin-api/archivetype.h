// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QMetaType>

#include "core/symbol_export.h"


class QLatin1String;


namespace Packuru::Core
{

enum class ArchiveType
{
    _7z,
    ace,
    alz,
    appimage,
    ar,
    arc,
    arj,
    brotli,
    bzip2,
    cab,
    chm,
    compress,
    cpio,
    cramfs,
    deb,
    dmg,
    ext,
    fat,
    gzip,
    iso9660,
    lha,
    lrzip,
    lz4,
    lzip,
    lzma,
    lzop,
    mbr,
    mdx,
    msi,
    nrg,
    ntfs,
    pak,
    pdf,
    qcow,
    rar,
    raw_optical,
    rpm,
    sit,
    squashfs,
    swf,
    tar,
    tar_brotli,
    tar_bzip2,
    tar_compress,
    tar_gzip,
    tar_lrzip,
    tar_lz4,
    tar_lzip,
    tar_lzma,
    tar_lzop,
    tar_xz,
    tar_zstd,
    udf,
    vdi,
    vhd,
    vmdk,
    wim,
    xar,
    xz,
    zip,
    zpaq,
    zstd,

    ___COUNT,

    ___NOT_SUPPORTED
};

PACKURU_CORE_EXPORT bool isValid(ArchiveType type);
PACKURU_CORE_EXPORT bool isTar(ArchiveType type);
PACKURU_CORE_EXPORT QLatin1String toString(ArchiveType type);
PACKURU_CORE_EXPORT void fromString(const QString& type, ArchiveType& result);

}

Q_DECLARE_METATYPE(Packuru::Core::ArchiveType);
