// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QDataStream>
#include <QCoreApplication>
#include <QDir>

#include "commandlinehandlerbase.h"


namespace Packuru::Core
{

CommandLineHandlerBase::CommandLineHandlerBase() :
    appDir(QCoreApplication::instance()->applicationDirPath())
{
    parser.addPositionalArgument(Packuru::Core::CommandLineHandlerBase::tr("urls"),
                                 Packuru::Core::CommandLineHandlerBase::tr("URLs to open"),
                                 Packuru::Core::CommandLineHandlerBase::tr("[urls...]"));
}

CommandLineHandlerBase::~CommandLineHandlerBase()
{

}


std::vector<QFileInfo> CommandLineHandlerBase::createUrlList() const
{
    QStringList args = parser.positionalArguments();

    std::vector<QFileInfo> list;
    list.reserve(static_cast<std::size_t>(args.size()));
    QFileInfo temp;

    for (const auto& arg : args)
    {
        temp.setFile(arg);

        if (temp.isAbsolute())
            list.push_back(arg);
        else
            list.push_back(appDir + "/" + arg);
    }

    return list;
}

}
