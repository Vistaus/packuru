// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QMetaType>

#include "core/symbol_export.h"


namespace Packuru::Core
{

enum class CompressionRatioMode
{
    PackedToOriginal,
    OriginalToPacked,

    ___INVALID
};


PACKURU_CORE_EXPORT QString toString(CompressionRatioMode value);

PACKURU_CORE_EXPORT QString toSettingsValue(CompressionRatioMode value);

PACKURU_CORE_EXPORT void fromString(const QString& key, CompressionRatioMode& out);

PACKURU_CORE_EXPORT double computeSizeRatio(qulonglong originalSize, qulonglong packedSize, CompressionRatioMode mode);

}


Q_DECLARE_METATYPE(Packuru::Core::CompressionRatioMode);
