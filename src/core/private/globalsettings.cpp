// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "globalsettings.h"


namespace Packuru::Core
{

int toInt(GlobalSettings::Enum key)
{
    return static_cast<int>(key);
}

}
