// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>
#include <vector>

#include <QString>
#include <QObject>

#include "plugindata.h"
#include "core/symbol_export.h"


class QObject;


namespace Packuru::Core
{

class PACKURU_CORE_EXPORT PluginManager
{
public:
    // Loads plugins from pluginsDir.
    PluginManager(const QString& pluginsDir);

    // Specify pluginMetaDataName to only load a plugin whose metadata contains a given name.
    PluginManager(const QString& pluginsDir, const QString& pluginMetaDataName);

    ~PluginManager();

    template <typename T>
    std::vector<T*> getPlugins() const;

    std::vector<PluginData> getAllPlugins() const;

private:   
    std::vector<PluginData>::const_iterator begin() const;
    std::vector<PluginData>::const_iterator end() const;

    struct Private;
    std::unique_ptr<Private> priv;
};


template <typename T>
std::vector<T*> PluginManager::getPlugins() const
{
    std::vector<T*> returnPlugins;

    for (const auto& pluginData : *this)
    {
        if (auto p = qobject_cast<T*>(pluginData.plugin))
            returnPlugins.push_back(p);
    }

    return returnPlugins;
}

}
