// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <cstdint>

#include "timer.h"


using namespace std::chrono;


namespace Packuru::Core
{

Timer::Timer()
{

}


void Timer::start()
{
    if (!started)
    {
        started = true;
        startTime = system_clock::now();
    }
    else if (paused)
    {
        paused = false;
        startTime = system_clock::now() - (pauseTime - startTime);
        pauseTime = system_clock::time_point();
    }
}


void Timer::stop()
{
    if (!started || paused)
        return;

    paused = true;
    pauseTime = system_clock::now();
}


void Timer::reset()
{
    started = false;
    paused = false;
    startTime = system_clock::time_point();
    pauseTime = system_clock::time_point();
}


std::chrono::system_clock::duration Timer::getDuration() const
{
    if (started)
    {
        if (paused)
            return pauseTime - startTime;
        else
            return system_clock::now() - startTime;
    }

    return std::chrono::system_clock::duration();
}


std::chrono::system_clock::rep Timer::getMilliseconds() const
{
    return duration_cast<milliseconds>(getDuration()).count();
}


std::chrono::system_clock::rep Timer::getSeconds() const
{
    return duration_cast<seconds>(getDuration()).count();
}

}
