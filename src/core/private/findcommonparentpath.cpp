// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QFileInfoList>

#include "findcommonparentpath.h"


namespace Packuru::Core
{

QString findCommonParentPath(const std::vector<QFileInfo>& list)
{
    if (list.empty())
        return QString();

    QString currentParent = list.front().absolutePath();

    for (const auto& data : list)
    {
        QString itemParent = data.absolutePath();

        int shorterLength = itemParent.length() < currentParent.length() ? itemParent.length() : currentParent.length();

        QString newParent;

        for (int i = 0; i < shorterLength; ++i)
        {
            if (currentParent[i] == itemParent[i])
                newParent += currentParent[i];
            else
            {
                while (!newParent.isEmpty() and newParent.rightRef(1) != QLatin1String("/"))
                    newParent.chop(1);
                break;
            }
        }

        currentParent = newParent;
    }

    return currentParent;
}

}
