// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QDataStream>

#include <unordered_set>

#include "registermetatypes.h"
#include "plugin-api/archivingfilepaths.h"
#include "plugin-api/archivetype.h"
#include "plugin-api/encryptionstate.h"
#include "tasks/taskerror.h"
#include "tasks/taskwarning.h"
#include "tasks/taskstate.h"


namespace Packuru::Core
{
enum class TaskError;
}

Q_DECLARE_METATYPE(std::unordered_set<Packuru::Core::TaskError>);
Q_DECLARE_METATYPE(std::unordered_set<Packuru::Core::TaskWarning>);


namespace Packuru::Core
{

void registerMetaTypes()
{
    qRegisterMetaTypeStreamOperators<ArchiveType>("ArchiveType");
    qRegisterMetaTypeStreamOperators<ArchivingFilePaths>("ArchivingFilePaths");
    qRegisterMetaTypeStreamOperators<EncryptionState>("EncryptionState");
    /* TaskWorker emits signals errors(const std::unordered_set<TaskError>& errors)
       and stateChanged(TaskState)
       which are received by Task that lives in another thread so the signals are queued
       and the parameters needs qRegisterMetaType */
    qRegisterMetaType<std::unordered_set<TaskError>>();
    qRegisterMetaType<std::unordered_set<TaskWarning>>();
    qRegisterMetaType<TaskState>();
}

}
