// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QByteArray>
#include <QDataStream>


namespace Packuru::Core
{

template <typename IDType, typename DataType>
QByteArray createMessage(const IDType& type, const DataType& data, bool skipSize = false)
{
    QByteArray block;
    QDataStream out(&block, QIODevice::ReadWrite);

    if (!skipSize)
        out << static_cast<quint64>(0);

    out << type;
    out << data;

    if (!skipSize)
    {
        out.device()->seek(0);
        out << static_cast<quint64>(block.size()) - static_cast<quint64>(sizeof(quint64));
    }

    return block;
}

}
