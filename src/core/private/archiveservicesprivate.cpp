// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QSettings>
#include <QCoreApplication>

#include "archiveservicesprivate.h"
#include "appinfo.h"


namespace Packuru::Core
{

ArchiveServicesPrivate::ArchiveServicesPrivate(QObject* parent)
    : QObject(parent)
{

}


void ArchiveServicesPrivate::onPluginsConfigChanged(const ArchiveTypeModelAux::ChangedValuesMap& map)
{
    struct ChangeInfo
    {
        const QString& newPlugin;
        PluginMap& plugins;
    };

    for (const auto& it : map)
    {
        const QString& typeStr = it.first;
        const ArchiveTypeModelAux::ChangedValues& values = it.second;

        ArchiveType type;
        fromString(typeStr, type);

        std::array<ChangeInfo, 2> infoList { ChangeInfo{values.newReadPlugin, readingPlugins},
                                             ChangeInfo{values.newWritePlugin, writingPlugins} };

        for (const auto& info : infoList)
        {
            if (info.newPlugin.isEmpty())
                continue;

            const auto it2 = info.plugins.find(type);
            Q_ASSERT(it2 != info.plugins.end());

            std::vector<PluginData>& pluginList = it2->second;
            Q_ASSERT(!pluginList.empty());

            setCurrentPlugin(pluginList, info.newPlugin);
        }
    }

    savePluginConfig();

    emit publ->pluginConfigChanged();
    emit publ->pluginConfigChangedBroadcast(QCoreApplication::applicationPid());
}


ArchiveHandlerInterface* ArchiveServicesPrivate::getPlugin(const ArchiveServicesPrivate::PluginMap& pluginMap, ArchiveType archiveType) const
{
    const auto it = pluginMap.find(archiveType);

    if (it != pluginMap.end())
    {
        const std::vector<PluginData>& plugins = it->second;
        Q_ASSERT(!plugins.empty());

        const auto plugin = qobject_cast<ArchiveHandlerInterface*>(plugins[0].plugin);
        Q_ASSERT(plugin);
        return plugin;
    }

    return nullptr;
}


std::unique_ptr<AbstractBackendHandler>
ArchiveServicesPrivate::createBackendHandler(const ArchiveServicesPrivate::PluginMap& map, ArchiveType archiveType) const
{
    const ArchiveHandlerInterface* const plugin = getPlugin(map, archiveType);

    if (plugin)
        return plugin->createBackendHandler();

    return std::unique_ptr<AbstractBackendHandler>();
}


void ArchiveServicesPrivate::setCurrentPlugin(std::vector<PluginData>& pluginList, const QString& newPlugin)
{
    Q_ASSERT(!pluginList.empty());
    Q_ASSERT(!newPlugin.isEmpty());

    // Current plugin is always at index 0
    if (pluginList[0].pluginName != newPlugin)
    {
        const auto findIt = std::find_if(pluginList.begin(), pluginList.end(),
                                         [name = newPlugin] (const auto& data) { return data.pluginName == name; });

        if (findIt != pluginList.end())
            std::swap(*pluginList.begin(), *findIt);
    }
}


void ArchiveServicesPrivate::setDefaultPlugins()
{
    auto setPlugin = [] (PluginMap& map, ArchiveType type, const QLatin1String& name)
    {
        auto listIt = map.find(type);
        if (listIt == map.end())
            return;

        std::vector<PluginData>& plugins = listIt->second;

        auto pluginIt = std::find_if(plugins.begin(), plugins.end(), [&name = name] (const auto& data)
        { return data.pluginName == name; });

        if (pluginIt == plugins.end())
            return;

        if (pluginIt != plugins.begin())
            std::swap(*pluginIt, *plugins.begin());
    };

    const QLatin1String sevenZipName ("7-Zip");
    const QLatin1String gnuTarName ("GNU Tar");
    const QLatin1String unarchiverName ("The Unarchiver");

    setPlugin(readingPlugins, ArchiveType::gzip, sevenZipName);
    setPlugin(readingPlugins, ArchiveType::rar, unarchiverName);
    setPlugin(readingPlugins, ArchiveType::_7z, sevenZipName);
    setPlugin(readingPlugins, ArchiveType::tar_bzip2, gnuTarName);
    setPlugin(readingPlugins, ArchiveType::tar_gzip, gnuTarName);
    setPlugin(readingPlugins, ArchiveType::iso9660, sevenZipName);
    setPlugin(readingPlugins, ArchiveType::tar_lzip, gnuTarName);
    setPlugin(readingPlugins, ArchiveType::tar_lzma, gnuTarName);
    setPlugin(readingPlugins, ArchiveType::tar, gnuTarName);
    setPlugin(readingPlugins, ArchiveType::tar_compress, gnuTarName);
    setPlugin(readingPlugins, ArchiveType::tar_lzop, gnuTarName);
    setPlugin(readingPlugins, ArchiveType::tar_xz, gnuTarName);
    setPlugin(readingPlugins, ArchiveType::zip, sevenZipName);
}


void ArchiveServicesPrivate::savePluginConfig()
{
    // Shared plugin config desktop/mobile
    const auto appName = AppInfo::getAppName();
    QSettings s(appName, appName);

    struct PreferredPlugins
    {
        QString read;
        QString write;
    };

    std::unordered_map<ArchiveType, PreferredPlugins> pluginMap;

    for (const auto& it : readingPlugins)
    {
        const ArchiveType& type = it.first;
        const std::vector<PluginData>& plugins = it.second;
        Q_ASSERT(!plugins.empty());
        pluginMap[type].read = plugins[0].pluginName;
    }

    for (const auto& it : writingPlugins)
    {
        const ArchiveType& type = it.first;
        const std::vector<PluginData>& plugins = it.second;
        Q_ASSERT(!plugins.empty());
        pluginMap[type].write = plugins[0].pluginName;
    }

    s.beginWriteArray("plugin-preferences");

    int i = 0;
    for (const auto& it : pluginMap)
    {
        s.setArrayIndex(i);
        s.setValue("type", toString(it.first));
        s.setValue("read", it.second.read);
        s.setValue("write", it.second.write);
        ++i;
    }

    s.endArray();

}


void ArchiveServicesPrivate::loadPluginConfig()
{
    struct Info
    {
        PluginMap& pluginMap;
        const QString& plugin;
    };

    // Shared plugin config desktop/mobile
    const auto appName = AppInfo::getAppName();
    QSettings s(appName, appName);

    const int size = s.beginReadArray("plugin-preferences");

    for (int i = 0; i < size; ++i)
    {
        s.setArrayIndex(i);

        const QString typeStr = s.value("type").toString();
        ArchiveType type;
        fromString(typeStr, type);
        std::array<Info, 2> infoList { Info{readingPlugins, s.value("read").toString()},
                                       Info{writingPlugins, s.value("write").toString()} };

        for (const auto& info : infoList)
        {
            if (!info.plugin.isEmpty())
            {
                const auto it = info.pluginMap.find(type);
                if (it != info.pluginMap.end())
                {
                    auto& pluginList = it->second;
                    if (!pluginList.empty())
                        setCurrentPlugin(pluginList, info.plugin);
                }
            }
        }
    }

    s.endArray();

}

}
