// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QMetaType>

#include "core/symbol_export.h"


namespace Packuru::Core
{

enum class ExtractionFileOverwriteMode
{
    AskBeforeOverwrite,
    OverwriteExisting,
    SkipExisting,
    UpdateExisting
};

PACKURU_CORE_EXPORT QString toString(ExtractionFileOverwriteMode value);

}

Q_DECLARE_METATYPE(Packuru::Core::ExtractionFileOverwriteMode);
