// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "sizeunits.h"
#include "core/symbol_export.h"


class QString;

namespace Packuru::Core
{

struct PACKURU_CORE_EXPORT DigitalSize
{
    static QString toString(qulonglong byteCount, SizeUnits units);
    static bool exceedsByteTreshold(qulonglong byteCount, SizeUnits units);
};

}
