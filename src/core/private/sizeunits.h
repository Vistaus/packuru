// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QMetaType>
#include <QString>

#include "core/symbol_export.h"


namespace Packuru::Core
{

enum class SizeUnits
{
    Binary,
    Decimal,

    ___INVALID
};


PACKURU_CORE_EXPORT QString toString(SizeUnits value);

PACKURU_CORE_EXPORT QString toSettingsValue(SizeUnits value);

PACKURU_CORE_EXPORT void fromString(const QString& key, SizeUnits& out);

}

Q_DECLARE_METATYPE(Packuru::Core::SizeUnits);
