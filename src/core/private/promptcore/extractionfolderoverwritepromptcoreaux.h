// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QDir>
#include <QObject>

#include "core/extractionfolderoverwritepromptcore.h"


namespace Packuru::Core::ExtractionFolderOverwritePromptCoreAux
{

class Emitter : public QObject
{
    Q_OBJECT

public:
    explicit Emitter(QObject* parent = nullptr);

signals:
    void writeIntoRequested(bool applyToAll);
    void skipRequested(bool applyToAll);
    void retryRequested();
    void abortRequested();
};


struct Init
{
    QDir source;
    QDir destination;
};


struct Backdoor
{
    Emitter* emitter = nullptr;
};

}
