// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QFileInfo>
#include <QObject>

#include "core/extractionfileoverwritepromptcore.h"


namespace Packuru::Core::ExtractionFileOverwritePromptCoreAux
{

class Emitter : public QObject
{
    Q_OBJECT

public:
    explicit Emitter(QObject* parent = nullptr);

signals:
    void overwriteRequested(bool applyToAll);
    void skipRequested(bool applyToAll);
    void retryRequested();
    void abortRequested();
};


struct Init
{
    QFileInfo source;
    QFileInfo destination;
};


struct Backdoor
{
    Emitter* emitter = nullptr;
};

}
