// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "extractiontypemismatchpromptcoreaux.h"


namespace Packuru::Core::ExtractionTypeMismatchPromptCoreAux
{

Emitter::Emitter(QObject* parent)
    : QObject(parent)
{

}

}
