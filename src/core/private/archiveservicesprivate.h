// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <unordered_map>

#include <QObject>

#include "../archivetypemodel.h"
#include "plugin-api/archivehandlerinterface.h"
#include "plugin-api/abstractbackendhandler.h"
#include "plugin-api/archivetype.h"
#include "archiveservices.h"
#include "plugindata.h"
#include "archivetypemodelaux.h"


namespace Packuru::Core
{

enum class ArchiveType;


class ArchiveServicesPrivate : public QObject
{
    Q_OBJECT
public:
    using PluginMap = std::unordered_map<ArchiveType, std::vector<PluginData>>;

    ArchiveServicesPrivate(QObject* parent = nullptr);

    void onPluginsConfigChanged(const ArchiveTypeModelAux::ChangedValuesMap& map);
    ArchiveHandlerInterface* getPlugin(const ArchiveServicesPrivate::PluginMap& pluginMap, ArchiveType archiveType) const;
    std::unique_ptr<AbstractBackendHandler> createBackendHandler(const ArchiveServicesPrivate::PluginMap& map,
                                                         ArchiveType archiveType) const;
    void setCurrentPlugin(std::vector<PluginData>& pluginList, const QString& newPlugin);
    void setDefaultPlugins();
    void savePluginConfig();
    void loadPluginConfig();

    ArchiveServices* publ = nullptr;
    PluginMap writingPlugins;
    PluginMap readingPlugins;
    std::unordered_map<ArchiveHandlerInterface*, uint> pluginToId;
    std::unordered_map<uint, ArchiveHandlerInterface*> idToPlugin;
};

}
