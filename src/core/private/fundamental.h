// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <type_traits>


namespace Packuru::Core
{

/* Stores integral and floating point types. Value is initalized to a default value in contructor.
 * When moved from, the value is reset to default. */
template <typename T>
struct Fundamental
{
    using ThisType = Fundamental<T>;

    Fundamental(const ThisType& other) noexcept
        : value(other.value) {}

    Fundamental(ThisType&& other) noexcept
        : value(other.value) { other.reset(); }

    Fundamental(const T& value = T()) noexcept
        : value(value) {}

    ThisType& operator=(const ThisType& other) noexcept
    {
        if (&other != this)
            value = other.value;
        return *this;
    }

    ThisType& operator=(ThisType&& other) noexcept
    {
        if (&other != this)
        {
            value = other.value;
            other.reset();
        }
        return *this;
    }

    ThisType& operator=(const T& v) noexcept
    {
        value = v;
        return *this;
    }

    operator T() const noexcept { return value; }

    void reset() noexcept { value = T(); }

    std::enable_if_t<(std::is_integral<T>::value || std::is_floating_point<T>::value), T> value;
};

}
