// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QString>


class QObject;


namespace Packuru::Core
{

struct PluginData
{
    QString pluginName;
    QString pluginDescription;
    QString backendHomepage;
    QObject* plugin = nullptr;
    QString absFilePath;
    uint id = 0;
};

}
