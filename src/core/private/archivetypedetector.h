// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QString>


namespace Packuru::Core
{

enum class ArchiveType;


class ArchiveTypeDetector
{
public:
    ArchiveTypeDetector();
    ~ArchiveTypeDetector();

    ArchiveType detectType(const QString& absFilePath) const;

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}
