// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <unordered_set>

#include <QDirIterator>
#include <QTranslator>
#include <QCoreApplication>
#include <QLocale>

#include "utils/private/qttypehasher.h"

#include "loadtranslations.h"


namespace Packuru::Core
{

namespace
{

std::unordered_set<QString, Utils::QtTypeHasher<QString>> paths;

}


void loadTranslations()
{
    QDirIterator it(QM_FILES_DIR, QDir::Dirs | QDir::NoDotAndDotDot);

    while (it.hasNext())
    {
        const QFileInfo dir(it.next());

        if (paths.find(dir.absoluteFilePath()) != paths.end())
            continue;

        paths.insert(dir.absoluteFilePath());

        auto tr = new QTranslator(qApp);
        const auto loaded = tr->load(QLocale(), dir.fileName(), "_", dir.absoluteFilePath());
        if (loaded)
            qApp->installTranslator(tr);
    }
}

}
