// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QCoreApplication>
#include <QDBusConnection>

#include "utils/makeqpointer.h"

#include "archiveservices.h"
#include "archiveservicesprivate.h"
#include "tasks/extractiontask.h"
#include "tasks/readtask.h"
#include "tasks/archivingtask.h"
#include "tasks/deletiontask.h"
#include "tasks/tasktype.h"
#include "tasks/testtask.h"
#include "plugin-api/archivingparameterhandlerfactory.h"
#include "tasks/archivingtaskdata.h"
#include "tasks/deletiontaskdata.h"
#include "tasks/extractiontaskdata.h"
#include "tasks/readtaskdata.h"
#include "tasks/testtaskdata.h"
#include "registermetatypes.h"
#include "archiveservicesdbusadaptor.h"
#include "archivetypemodelaccessor.h"


using Packuru::Utils::makeQPointer;


namespace Packuru::Core
{

ArchiveServices::ArchiveServices(const std::vector<PluginData>& pluginData, QObject* parent)
    : QObject(parent),
      priv(new ArchiveServicesPrivate(this))
{
    priv->publ = this;
    registerMetaTypes();

    new ArchiveServicesDBusAdaptor(this);

    QDBusConnection::sessionBus().registerObject("/org/" PROJECT_BUILD_NAME "/archiveservices", this);

    QDBusConnection::sessionBus().connect("",
                                          "/org/" PROJECT_BUILD_NAME "/archiveservices",
                                          "org." PROJECT_BUILD_NAME ".archiveservices",
                                          "pluginConfigChangedBroadcast",
                                          this,
                                          SLOT(onSomeProcessPluginConfigChanged(qint64)));

    for (const auto& data : pluginData)
    {
        const auto plugin = qobject_cast<ArchiveHandlerInterface*>(data.plugin);
        priv->pluginToId[plugin] = data.id;
        priv->idToPlugin[data.id] = plugin;

        std::unordered_set<ArchiveType> archiveTypes = plugin->supportedTypesForReading();
        for (const auto& type : archiveTypes)
            priv->readingPlugins[type].push_back(data);

        archiveTypes = plugin->supportedTypesForWriting();
        for (const auto& type : archiveTypes)
            priv->writingPlugins[type].push_back(data);
    }

    /* At this point the plugins are in random order. The procedure is to first load some default
     * values and then override them with user values if there are any. */
    priv->setDefaultPlugins();
    priv->loadPluginConfig();
}


ArchiveServices::~ArchiveServices()
{

}


std::vector<std::unique_ptr<ArchivingParameterHandlerFactory>>
ArchiveServices::createArchivingParameterHandlerFactoryList() const
{
    std::vector<std::unique_ptr<ArchivingParameterHandlerFactory>> list;

    for (const auto& it : priv->writingPlugins)
    {
        const ArchiveType archiveType = it.first;
        const std::vector<PluginData> pluginsForType = it.second;
        Q_ASSERT(!pluginsForType.empty());
        const auto plugin = qobject_cast<ArchiveHandlerInterface*>(pluginsForType[0].plugin);
        const auto createHandler = [plugin = plugin] (const ArchivingParameterHandlerInitData& initData)
        { return plugin->createArchivingParameterHandler(initData); };

        const auto idIt = priv->pluginToId.find(plugin);
        Q_ASSERT(idIt != priv->pluginToId.end());

        const uint pluginId = idIt->second;
        auto factory = std::make_unique<ArchivingParameterHandlerFactory>(archiveType,
                                                                          toString(archiveType),
                                                                          pluginId,
                                                                          createHandler);

        list.push_back(std::move(factory));
    }

    return list;
}


std::unique_ptr<ArchivingParameterHandlerFactory>
ArchiveServices::createArchivingParameterHandlerFactory(ArchiveType archiveType) const
{
    std::unique_ptr<ArchivingParameterHandlerFactory> ptr;

    if (priv->writingPlugins.find(archiveType) != priv->writingPlugins.end())
    {
        const std::vector<PluginData>& pluginsForType = priv->writingPlugins[archiveType];

        if (!pluginsForType.empty())
        {
            const auto plugin = qobject_cast<ArchiveHandlerInterface*>(pluginsForType[0].plugin);

            const auto idIt = priv->pluginToId.find(plugin);
            Q_ASSERT(idIt != priv->pluginToId.end());

            const uint pluginId = idIt->second;

            const auto createHandler = [plugin = plugin] (const ArchivingParameterHandlerInitData& initData)
            { return plugin->createArchivingParameterHandler(initData); };

            ptr.reset(new ArchivingParameterHandlerFactory(archiveType,
                                                           toString(archiveType),
                                                           pluginId,
                                                           createHandler));
        }
    }

    return ptr;
}


QString ArchiveServices::getExtractionBackendNameForType(ArchiveType archiveType) const
{
    const auto it = priv->readingPlugins.find(archiveType);

    if (it == priv->readingPlugins.end())
        return QString();

    const std::vector<PluginData>& plugins = it->second;

    Q_ASSERT(!plugins.empty());

    return plugins[0].pluginName;
}


ReadTask* ArchiveServices::createTask(const ReadTaskData& data)
{
    auto factory = [priv = makeQPointer(priv), &map = priv->readingPlugins] (ArchiveType type)
    {
        if (!priv)
            return std::unique_ptr<AbstractBackendHandler>();
        return priv->createBackendHandler(map ,type);
    };

    return new ReadTask(data, std::move(factory), this);
}


ExtractionTask* ArchiveServices::createTask(const ExtractionTaskData& data)
{
    auto factory = [priv = makeQPointer(priv), &map = priv->readingPlugins] (ArchiveType type)
    {
        if (!priv)
            return std::unique_ptr<AbstractBackendHandler>();
        return priv->createBackendHandler(map ,type);
    };

    return new ExtractionTask(data, std::move(factory), this);
}


ArchivingTask* ArchiveServices::createTask(const ArchivingTaskData& data)
{
    Q_ASSERT(data.pluginId != 0);

    // It is not possible to change backend handler of ArchvingTask to another plugin because
    // BackendArchivingData contains data specific to particular plugin.
    BackendHandlerFactory factory;

    const auto it = priv->idToPlugin.find(data.pluginId);

    if (it == priv->idToPlugin.end())
        factory = [] (ArchiveType) { return nullptr; };
    else
    {
        const ArchiveHandlerInterface* const plugin = it->second;
        factory = [plugin = plugin] (ArchiveType)
        { return plugin->createBackendHandler(); };
    }

    return new ArchivingTask(data, std::move(factory), this);
}


DeletionTask* ArchiveServices::createTask(const DeletionTaskData& data)
{
    auto factory = [priv = makeQPointer(priv), &map = priv->writingPlugins] (ArchiveType type)
    {
        if (!priv)
            return std::unique_ptr<AbstractBackendHandler>();
        return priv->createBackendHandler(map ,type);
    };

    return new DeletionTask(data, std::move(factory), this);
}


TestTask* ArchiveServices::createTask(const TestTaskData& data)
{
    auto factory = [priv = makeQPointer(priv), &map = priv->readingPlugins] (ArchiveType type)
    {
        if (!priv)
            return std::unique_ptr<AbstractBackendHandler>();
        return priv->createBackendHandler(map ,type);
    };

    return new TestTask(data, std::move(factory), this);
}


ArchiveTypeModelAccessor ArchiveServices::getArchiveTypeModelAccessor()
{
    ArchiveTypeModelAux::InitData initData;

    std::array<ArchiveServicesPrivate::PluginMap, 2> pluginMaps {priv->readingPlugins, priv->writingPlugins };

    for (int i = 0; i < static_cast<int>(ArchiveType::___COUNT); ++i)
    {
        ArchiveTypeModelAux::Entry entry;
        auto archiveType = static_cast<ArchiveType>(i);
        entry.archiveType = toString(archiveType);

        auto it = priv->readingPlugins.find(archiveType);
        if (it != priv->readingPlugins.end())
        {
            const std::vector<PluginData>& pluginsForType = it->second;
            Q_ASSERT(!pluginsForType.empty());
            entry.read.currentPlugin = pluginsForType[0].pluginName;
            for (const auto& plugin : pluginsForType)
                entry.read.availablePlugins.push_back(plugin.pluginName);
        }

        it = priv->writingPlugins.find(archiveType);
        if (it != priv->writingPlugins.end())
        {
            const std::vector<PluginData>& pluginsForType = it->second;
            Q_ASSERT(!pluginsForType.empty());
            entry.write.currentPlugin = pluginsForType[0].pluginName;
            for (const auto& plugin : pluginsForType)
                entry.write.availablePlugins.push_back(plugin.pluginName);
        }

        // Hide types without any handlers
        if (entry.read.availablePlugins.size() > 0
                || entry.write.availablePlugins.size() > 0)
            initData.entries.emplace_back(std::move(entry));
    }

    initData.onValuesChanged = [priv = makeQPointer(priv)] (const ArchiveTypeModelAux::ChangedValuesMap& map)
    {
        if (priv)
            priv->onPluginsConfigChanged(map);
    };

    ArchiveTypeModelAux::BackdoorData backdoorData;
    auto model = new ArchiveTypeModel(initData, backdoorData, this);

    ArchiveTypeModelAccessor accessor;
    accessor.model = model;
    accessor.saveModelChanges = backdoorData.saveChanges;

    return accessor;
}


void ArchiveServices::onSomeProcessPluginConfigChanged(qint64 originProcessId)
{
    if (originProcessId == QCoreApplication::applicationPid())
        return;

    priv->loadPluginConfig();

    emit pluginConfigChanged();
}


std::unordered_set<TaskType> ArchiveServices::getSupportedWriteTasksForArchive(ArchiveType archiveType, const BackendArchiveProperties &properties) const
{
    std::unordered_set<TaskType> result;

    const auto it = priv->writingPlugins.find(archiveType);

    if (it != priv->writingPlugins.end())
    {
        const std::vector<PluginData>& plugins = it->second;
        if (plugins.size() > 0)
        {
            const auto plugin = qobject_cast<ArchiveHandlerInterface*>(plugins[0].plugin);
            if (plugin->canAddFiles(archiveType, properties))
                result.insert(TaskType::Add);
            if (plugin->canDeleteFiles(archiveType, properties))
                result.insert(TaskType::Delete);
        }
    }

    return result;
}

}
