// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QPluginLoader>
#include <QtPlugin>
#include <QDirIterator>

#include "pluginmanager.h"
#include "core/private/plugin-api/archivehandlerinterface.h"
#include "appinfo.h"
#include "loadtranslations.h"


namespace Packuru::Core
{

struct PluginManager::Private
{
    void load(const QString& pluginsDir, const QString& pluginMetaDataName);

    std::vector<PluginData> plugins;
};


PluginManager::PluginManager(const QString& pluginsDir)
    : PluginManager(pluginsDir, QString())
{
}


PluginManager::PluginManager(const QString& pluginsDir, const QString& pluginMetaDataName)
    : priv(new Private)
{
    priv->load(pluginsDir, pluginMetaDataName);
}


PluginManager::~PluginManager()
{

}


std::vector<PluginData> PluginManager::getAllPlugins() const
{
    return priv->plugins;
}


void PluginManager::Private::load(const QString& pluginsDir, const QString& pluginMetaDataName)
{
    QDirIterator it(pluginsDir, QDir::Files);

    while (it.hasNext())
    {
        const QFileInfo path = it.next();

        QPluginLoader loader(path.absoluteFilePath());

        const QJsonObject metaData = loader.metaData().value("MetaData").toObject();

        if (metaData.value("Application").toString() != AppInfo::getAppName()
                || (!pluginMetaDataName.isEmpty() && metaData.value("Name").toString() != pluginMetaDataName))
            continue;

        QObject* plugin = loader.instance();

        if (plugin)
        {
            if (qobject_cast<ArchiveHandlerInterface*>(plugin))
            {
                PluginData data;
                data.absFilePath = path.absoluteFilePath();
                data.backendHomepage = metaData.value("BackendHomepage").toString();
                data.plugin = plugin;
                data.pluginDescription = metaData.value("Description").toString();
                data.pluginName = metaData.value("Name").toString();
                data.id = qHash(data.absFilePath);
                plugins.push_back(std::move(data));

                // Only load one plugin with a given name
                if (!pluginMetaDataName.isEmpty())
                    break;
            }
        }
    }

    loadTranslations();
}


std::vector<PluginData>::const_iterator PluginManager::begin() const
{
    return priv->plugins.cbegin();
}


std::vector<PluginData>::const_iterator PluginManager::end() const
{
    return priv->plugins.cend();
}

}
