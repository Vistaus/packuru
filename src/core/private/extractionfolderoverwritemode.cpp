// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QString>
#include <QCoreApplication>

#include "extractionfolderoverwritemode.h"


namespace Packuru::Core
{

QString toString(ExtractionFolderOverwriteMode value)
{
    switch (value)
    {
    case ExtractionFolderOverwriteMode::AskBeforeOverwrite:
        return QCoreApplication::translate("ExtractionFolderOverwriteMode", "Ask before writing into");
    case ExtractionFolderOverwriteMode::WriteIntoExisting:
        return QCoreApplication::translate("ExtractionFolderOverwriteMode", "Write into existing");
    case ExtractionFolderOverwriteMode::SkipExisting:
        return QCoreApplication::translate("ExtractionFolderOverwriteMode", "Skip existing");
    default:
        Q_ASSERT(false); return QString();
    }
}

}
