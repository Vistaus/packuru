// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QString>
#include <QCoreApplication>

#include "compressionratiomode.h"


namespace Packuru::Core
{

QString toString(CompressionRatioMode value)
{
    switch(value)
    {
    case CompressionRatioMode::OriginalToPacked:
        return QCoreApplication::translate("CompressionRatioMode", "Original / packed");
    case CompressionRatioMode::PackedToOriginal:
        return QCoreApplication::translate("CompressionRatioMode", "Packed / original");
    default:
        Q_ASSERT(false); return QString();
    }
}


QString toSettingsValue(CompressionRatioMode value)
{
    switch(value)
    {
    case CompressionRatioMode::OriginalToPacked: return "original_to_packed";
    case CompressionRatioMode::PackedToOriginal: return "packed_to_original";
    default: Q_ASSERT(false); return "";
    }
}


void fromString(const QString& key, CompressionRatioMode& out)
{
    if (key == "original_to_packed")
        out =  CompressionRatioMode::OriginalToPacked;
    else if (key == "packed_to_original")
        out = CompressionRatioMode::PackedToOriginal;
    else
        out = CompressionRatioMode::___INVALID;
}


double computeSizeRatio(qulonglong originalSize, qulonglong packedSize, CompressionRatioMode mode)
{
    double ratio = 0;

    if (originalSize > 0 && packedSize > 0)
    {
        if (mode == CompressionRatioMode::PackedToOriginal)
            ratio = static_cast<double>(packedSize) / originalSize;
        else if (mode == CompressionRatioMode::OriginalToPacked)
            ratio = static_cast<double>(originalSize) / packedSize;
    }

    return ratio;
}

}
