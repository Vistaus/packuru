// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QFileInfoList>

#include "core/symbol_export.h"


class QString;

namespace Packuru::Core
{

// Returns empty string if not found
PACKURU_CORE_EXPORT QString findCommonParentPath(const std::vector<QFileInfo>& list);

}
