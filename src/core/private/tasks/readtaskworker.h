// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include "taskworker.h"
#include "core/private/plugin-api/backendarchiveproperties.h"
#include "archivecontentdata.h"


namespace Packuru::Core
{

class ReadTaskData;


class ReadTaskWorker : public TaskWorker
{
    Q_OBJECT
public:
    ReadTaskWorker(QObject* owner, const ReadTaskData& taskData, std::unique_ptr<AbstractBackendHandler> backendHandler);
    ~ReadTaskWorker() override;

    // Thread-safe
    ReadTaskData getData() const;

    // Thread-safe
    QString getArchiveName() const override;

    // Thread-safe
    ArchiveContentData getArchiveContentData() const;

    // Thread-safe
    BackendArchiveProperties getArchiveProperties() const;

    // Thread-safe
    QString getPassword() const override;

private:
    bool preStartInit() override;
    void startImpl() override;
    void onBackendFinished(TaskState concludedState) override;

    void updatePassword(const QString& password) override;

    struct Private;
    std::unique_ptr<Private> priv;
};

}
