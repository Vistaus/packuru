// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QDataStream>

#include "deletiontaskdata.h"


QDataStream& operator<<(QDataStream& out, const Packuru::Core::DeletionTaskData& data)
{
    return out << data.runInQueue
               << data.backendData;
}


QDataStream& operator>>(QDataStream& in, Packuru::Core::DeletionTaskData& data)
{
    return in >> data.runInQueue
              >> data.backendData;
}
