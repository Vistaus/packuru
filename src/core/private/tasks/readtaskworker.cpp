// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <shared_mutex>

#include "core/private/3rdparty/cs_libguarded/cs_shared_guarded.h"
#include "readtaskworker.h"
#include "readtaskdata.h"
#include "taskworkerprivate.h"
#include "core/private/plugin-api/abstractbackendhandler.h"
#include "archivefiletreebuilder.h"
#include "taskstate.h"
#include "globalsettingsmanager.h"
#include "private/globalsettings.h"
#include "core/private/compressionratiomode.h"


namespace Packuru::Core
{

struct ReadTaskWorker::Private
{
    Private(const ReadTaskData& data)
        : taskData(data) {}

    ArchiveFileTreeBuilder* treeBuilder;
    libguarded::shared_guarded<ReadTaskData> taskData;
    libguarded::shared_guarded<ArchiveContentData> archiveContentData;
    libguarded::shared_guarded<BackendArchiveProperties> archiveProperties;

};


ReadTaskWorker::ReadTaskWorker(QObject* owner, const ReadTaskData& taskData, std::unique_ptr<AbstractBackendHandler> backendHandler)
    : TaskWorker(owner,
                 taskData.backendData.archiveType != ArchiveType::___NOT_SUPPORTED,
                 std::move(backendHandler)),
      priv(new Private(taskData))

{
    const auto ratioMode = GlobalSettingsManager::instance()
            ->getValueAs<CompressionRatioMode>(GlobalSettings::Enum::AC_CompressionRatioMode);
    priv->treeBuilder = new ArchiveFileTreeBuilder(ratioMode, this);

    auto handler = TaskWorker::priv->backendHandler;

    if (handler)
        connect(handler, &AbstractBackendHandler::newItems,
                priv->treeBuilder, &ArchiveFileTreeBuilder::addItems);
}


ReadTaskWorker::~ReadTaskWorker()
{

}


ReadTaskData ReadTaskWorker::getData() const
{
    return *priv->taskData.lock_shared();
}


QString ReadTaskWorker::getArchiveName() const
{  
    return priv->taskData.lock_shared()->backendData.archiveInfo.fileName();
}


ArchiveContentData ReadTaskWorker::getArchiveContentData() const
{
    return *priv->archiveContentData.lock_shared();
}


BackendArchiveProperties ReadTaskWorker::getArchiveProperties() const
{
    return *priv->archiveProperties.lock_shared();
}


QString ReadTaskWorker::getPassword() const
{
    return priv->taskData.lock_shared()->backendData.password;
}


bool ReadTaskWorker::preStartInit()
{
    const auto data = priv->taskData.lock_shared();
    return TaskWorker::priv->confirmArchiveExistence(data->backendData.archiveInfo.absoluteFilePath());
}


void ReadTaskWorker::startImpl()
{
    const auto data = priv->taskData.lock_shared();
    auto handler = TaskWorker::priv->backendHandler;
    handler->readArchive(data->backendData);
}


void ReadTaskWorker::onBackendFinished(TaskState concludedState)
{
    Q_ASSERT(isStateFinished(concludedState));

    *priv->archiveContentData.lock() = priv->treeBuilder->getArchiveContentData();
    priv->treeBuilder->reset();
    auto handler = TaskWorker::priv->backendHandler;
    *priv->archiveProperties.lock() = handler->getArchiveProperties();

    TaskWorker::priv->changeState(concludedState);
}


void ReadTaskWorker::updatePassword(const QString& password)
{
    priv->taskData.lock()->backendData.password = password;
}

}
