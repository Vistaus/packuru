// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "task.h"


namespace Packuru::Core
{

class ArchivingTaskData;
class CreationNameErrorPromptCore;


class ArchivingTask : public Task
{
    Q_OBJECT
public:
    ArchivingTask(const ArchivingTaskData& taskData, const BackendHandlerFactory& backendFactory, QObject *parent = nullptr);
    ~ArchivingTask() override;

signals:
    // Task is the parent of promptCore and will delete it
   void archiveCreationNameError(CreationNameErrorPromptCore* promptCore);
   void openNewArchive(const QString& path);
   void openDestinationPath(const QString& path);

private:
    std::unique_ptr<TaskWorker> createWorker() override;

    struct Private;
    std::unique_ptr<Private> priv;
};

}
