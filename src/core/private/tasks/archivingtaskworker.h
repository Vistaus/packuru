// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "taskworker.h"


namespace Packuru::Core
{

class ArchivingTaskData;
class CreationNameErrorPromptCore;


class ArchivingTaskWorker : public TaskWorker
{
    Q_OBJECT
public:
    ArchivingTaskWorker(QObject* owner,
                        const ArchivingTaskData& taskData,
                        std::unique_ptr<AbstractBackendHandler> backendHandler);

    ~ArchivingTaskWorker() override;

    // Thread-safe
    ArchivingTaskData getData() const;

    // Thread-safe
    QString getArchiveName() const override;
    QString getPassword() const override;

signals:
    // Task is the parent of promptCore and will delete it
    void archiveCreationNameError(CreationNameErrorPromptCore* promptCore);
    void openNewArchive(const QString& path);
    void openDestinationPath(const QString& path);

private:
    bool preStartInit() override;
    void startImpl() override;
    void onBackendFinished(TaskState concludedState) override;
    void updatePassword(const QString& password) override;

    QString checkArchiveNameError(const ArchivingTaskData& data) const;
    // Thread-safe but used only in worker thread
    void changeArchiveName(const QString& newName);

    struct Private;
    std::unique_ptr<Private> priv;
};

}
