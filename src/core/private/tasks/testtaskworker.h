// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once


#include "taskworker.h"


namespace Packuru::Core
{

class TestTaskData;
class BackendTestHandler;


class TestTaskWorker : public TaskWorker
{
    Q_OBJECT
public:
    TestTaskWorker(QObject* owner, const TestTaskData& taskData, std::unique_ptr<AbstractBackendHandler> backendHandler);
    ~TestTaskWorker() override;

    // Thread-safe
    TestTaskData getData() const;

    // Thread-safe
    QString getArchiveName() const override;

    // Thread-safe
    QString getPassword() const override;


private:
    bool preStartInit() override;
    void startImpl() override;
    void updatePassword(const QString& password) override;

    struct Private;
    std::unique_ptr<Private> priv;
};

}
