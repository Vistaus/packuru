// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QCoreApplication>

#include "taskwarning.h"


namespace Packuru::Core
{

QString toString(TaskWarning value)
{
    switch (value)
    {
    case TaskWarning::BackendVersionDeprecated:
        return QCoreApplication::translate("TaskWarning", "Backend version deprecated");
    case TaskWarning::BackendVersionNotTested:
        return QCoreApplication::translate("TaskWarning", "Backend version not tested");
    case TaskWarning::LowDiskSpace:
        return QCoreApplication::translate("TaskWarning", "Low disk space");
    case TaskWarning::Unknown:
        return QCoreApplication::translate("TaskWarning", "Unknown warning");
    default:
        Q_ASSERT(false); return QString();
    }
}

}
