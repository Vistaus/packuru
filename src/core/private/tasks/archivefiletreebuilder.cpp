// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QStringBuilder>
#include <QFileInfo>

#include "archivefiletreebuilder.h"
#include "archivedirnode.h"
#include "archivecontentdata.h"
#include "pathlookuptable.h"


namespace Packuru::Core
{

ArchiveFileTreeBuilder::ArchiveFileTreeBuilder(CompressionRatioMode ratioMode, QObject* parent)
    : QObject(parent),
      ratioMode(ratioMode)
{
    data.getLookupTable()->addDir("/",data.getRootNode());
}


ArchiveFileTreeBuilder::~ArchiveFileTreeBuilder()
{
}


void ArchiveFileTreeBuilder::addItems(std::vector<ArchiveItem>& items)
{
    QFileInfo info;

    for (auto& item : items)
    {
#ifdef QT_DEBUG
        Q_ASSERT(!item.name.isEmpty());
        Q_ASSERT(!(item.nodeType != NodeType::Folder && item.name.endsWith('/')));
#else
        if (item.name.isEmpty() || (item.nodeType != NodeType::Folder && item.name.endsWith('/')))
            continue;
#endif

        /*
         "file" becomes "/file"
         "dir/" becomes "/dir/"
         Unix absolute paths are supported:
         "/file" becomes "//file"
         "/" becomes "//"
        */

        QString itemAbsolutePath = QLatin1Char('/') % item.name;

        // It's easier to extract name from both dir and file paths when there's no '/' at the end
        if (itemAbsolutePath.endsWith(QLatin1Char('/')))
            itemAbsolutePath.chop(1);

        // Remove the path from item's name
        const int lastSeparatorPos = itemAbsolutePath.lastIndexOf(QLatin1Char('/'));
        Q_ASSERT(lastSeparatorPos >= 0); // should not happen with checks at the beginning
        item.name = itemAbsolutePath.right(itemAbsolutePath.size() - lastSeparatorPos - 1);

        if (item.nodeType == NodeType::Folder)
        {
            /* Dir count is incremented in the createPath(const QString&) function for each part
               of the path because for some types of archives backends output only info about
               files and directories have to be created implicitly from the file path. */

            // Put the separator back, it is required for the getDirNode() function
            itemAbsolutePath.push_back(QLatin1Char('/'));

            auto dirNode = getDirNode(itemAbsolutePath);
            dirNode->setItem(item);
        }
        else
        {
            ++data.fileCount_;
            data.totalOriginalSize_ += item.originalSize;
            data.totalPackedSize_ += item.packedSize;

            // Absolute parent path including final '/'
            const QString absParentPath = itemAbsolutePath.left(lastSeparatorPos + 1);
            auto dirNode = getDirNode(absParentPath);
            auto*& fileNode = fileNodes[itemAbsolutePath];

            if (fileNode)
            {
                fileNode->setItem(item);
            }
            else
            {
                fileNode = dirNode->addFile(item);
                info.setFile(itemAbsolutePath);
                if (item.nodeType == NodeType::File)
                    fileNode->getItem().extension = info.completeSuffix().toLower();
            }

            fileNode->computeCompressionRatio(ratioMode);
        }
    }

    items.clear();
}


ArchiveDirNode*ArchiveFileTreeBuilder::getDirNode(const QString& path)
{
    Q_ASSERT(path.endsWith(QLatin1Char('/')));

    auto dirNode = data.getLookupTable()->getDir(path);

    if (!dirNode)
        dirNode = createPath(path);

    return dirNode;
}


ArchiveDirNode* ArchiveFileTreeBuilder::createPath(const QString& path)
{
    Q_ASSERT(path.endsWith(QLatin1Char('/')));

    std::vector<QString> pathParts;

    int nameBegin = 0;
    int separatorPos = 0;

    while (true)
    {
        separatorPos = path.indexOf(QLatin1Char('/'), nameBegin);

        if (separatorPos >= 0)
        {
            pathParts.push_back(path.mid(nameBegin, separatorPos - nameBegin));
            nameBegin = separatorPos + 1;
        }
        else
            break;
    }

    ArchiveDirNode* parent = data.getRootNode();
    bool remainingPathsDontExist = false;

    QString tempPath;
    tempPath.reserve(path.size() + 1);

    for (const QString& part : pathParts)
    {
        tempPath += part + QLatin1String("/");

        ArchiveDirNode* tempDirNode;

        if (remainingPathsDontExist)
            tempDirNode = nullptr;
        else
            tempDirNode = data.getLookupTable()->getDir(tempPath);

        if (!tempDirNode)
        {
            parent = parent->addDir(part);
            data.getLookupTable()->addDir(tempPath, parent);
            ++data.dirCount_;
            remainingPathsDontExist = true;
        }
        else
            parent = tempDirNode;
    }

    return parent;
}


void ArchiveFileTreeBuilder::reset()
{
    fileNodes.clear();
}

}
