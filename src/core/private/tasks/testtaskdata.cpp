// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QDataStream>

#include "testtaskdata.h"


QDataStream& operator<<(QDataStream& out, const Packuru::Core::TestTaskData& data)
{
    return out << data.runInQueue
               << data.backendData;
}


QDataStream& operator>>(QDataStream& in, Packuru::Core::TestTaskData& data)
{
    return in >> data.runInQueue
              >> data.backendData;
}
