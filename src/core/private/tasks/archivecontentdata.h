// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QtGlobal>


namespace Packuru::Core
{

class ArchiveDirNode;
class PathLookupTable;


class ArchiveContentData
{
    friend class ArchiveFileTreeBuilder;

public:
    ArchiveContentData();
    ~ArchiveContentData();

    ArchiveDirNode* getRootNode() const { return rootNode.get(); }
    PathLookupTable* getLookupTable() const { return pathLookupTable.get(); }

    qulonglong totalOriginalSize() const { return totalOriginalSize_; }
    qulonglong totalPackedSize() const { return totalPackedSize_; }
    int dirCount() const { return dirCount_; }
    int fileCount() const { return fileCount_; }

    bool containsData() const;

private:
    std::shared_ptr<ArchiveDirNode> rootNode;
    std::shared_ptr<PathLookupTable> pathLookupTable;

    qulonglong totalOriginalSize_ = 0;
    qulonglong totalPackedSize_ = 0;
    int fileCount_ = 0;
    int dirCount_ = 0;
};

}
