// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "core/private/plugin-api/backendtestdata.h"
#include "core/symbol_export.h"


namespace Packuru::Core
{

struct TestTaskData
{
    bool runInQueue = false;
    BackendTestData backendData;
};

}


PACKURU_CORE_EXPORT QDataStream& operator<<(QDataStream& out, const Packuru::Core::TestTaskData& data);
PACKURU_CORE_EXPORT QDataStream& operator>>(QDataStream& in, Packuru::Core::TestTaskData& data);
