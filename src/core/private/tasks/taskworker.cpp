// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QThread>

#include "taskworker.h"
#include "taskworkerprivate.h"
#include "taskstate.h"
#include "core/private/plugin-api/backendhandlerstate.h"
#include "core/private/plugin-api/abstractbackendhandler.h"
#include "core/private/plugin-api/backendhandlererror.h"
#include "passwordpromptcore.h"
#include "taskerror.h"
#include "core/private/promptcore/passwordpromptcoreaux.h"


namespace Packuru::Core
{

TaskWorker::TaskWorker(QObject* promptOwner,
                       bool archiveTypeSupported,
                       std::unique_ptr<AbstractBackendHandler> backendHandler)
    : priv(new TaskWorkerPrivate(archiveTypeSupported, this))
{
    priv->promptOwner = promptOwner;
    priv->publ = this;

    if (backendHandler)
    {
        backendHandler->setParent(this);
        priv->backendHandler = backendHandler.release();

        connect(priv->backendHandler, &AbstractBackendHandler::progressChanged,
                this, &TaskWorker::progressChanged);

        connect(priv->backendHandler, &AbstractBackendHandler::stateChanged,
                this, &TaskWorker::onBackendStateChanged);
    }
}


TaskWorker::~TaskWorker()
{
}


void TaskWorker::start()
{
    Q_ASSERT(thread() == QThread::currentThread());
    Q_ASSERT(thread() == priv->thread());

    if (priv->getState() != TaskState::ReadyToRun)
        return;

    if (!preStartInit())
    {
        Q_ASSERT(priv->getState() == TaskState::ReadyToRun);
        return priv->changeState(TaskState::Errors);
    }

    if (!priv->backendHandler)
    {
        if (!priv->archiveTypeSupported)
            priv->errors.insert(TaskError::ArchiveTypeNotSupported);
        else
            priv->errors.insert(TaskError::NoPluginForArchiveType);
        return priv->changeState(TaskState::Errors);
    }

    priv->changeState(TaskState::InProgress);
    startImpl();
}


void TaskWorker::stop()
{
    Q_ASSERT(thread() == QThread::currentThread());
    Q_ASSERT(thread() == priv->thread());

    if (isStateBusy(priv->getState()))
        stopImpl();
}


void TaskWorker::setPassword(const QString& password)
{
    Q_ASSERT(thread() == QThread::currentThread()); // Backend handler must not be accessed from other threads
    Q_ASSERT(thread() == priv->thread());

    updatePassword(password);

    if (priv->getState() == TaskState::WaitingForPassword && priv->backendHandler)
        priv->backendHandler->enterPassword(password);
}


bool TaskWorker::preStartInit()
{
    return true;
}


void TaskWorker::stopImpl()
{
    if (priv->backendHandler && priv->backendHandler->isBusy())
        priv->backendHandler->stop();
    else
        priv->changeState(TaskState::Aborted);
}


void TaskWorker::onBackendStateChanged(BackendHandlerState state)
{
    switch (state)
    {
    case BackendHandlerState::Success:
    case BackendHandlerState::Warnings:
    case BackendHandlerState::Errors:
    case BackendHandlerState::Aborted:
    {
        const TaskState concludedState = priv->processBackendResults();
        return onBackendFinished(concludedState);
    }
    case BackendHandlerState::InProgress:
        priv->changeState(TaskState::InProgress);
        break;
    case BackendHandlerState::WaitingForPassword:
    {
        priv->changeState(TaskState::WaitingForPassword);

        PasswordPromptCoreAux::Init init;
        init.archiveName= getArchiveName();

        PasswordPromptCoreAux::Backdoor backdoor;

        auto promptCore = new PasswordPromptCore(init, backdoor);
        Q_ASSERT(backdoor.emitter);

        promptCore->moveToThread(priv->promptOwner->thread());
        promptCore->setParent(priv->promptOwner);

        connect(backdoor.emitter, &PasswordPromptCoreAux::Emitter::passwordEntered,
                this, &TaskWorker::setPassword);

        connect(backdoor.emitter, &PasswordPromptCoreAux::Emitter::abortRequested,
                this, &TaskWorker::stop);

        connect(this, &TaskWorker::stateChanged,
                promptCore, &QObject::deleteLater);

        emit passwordNeeded(promptCore);
        break;
    }
    case BackendHandlerState::Paused:
        priv->changeState(TaskState::Paused);
        break;
    case BackendHandlerState::ReadyToRun:
        break;
    case BackendHandlerState::___INVALID:
    default:
        Q_ASSERT(false);
    }
}


void TaskWorker::onBackendFinished(TaskState concludedState)
{
    priv->changeState(concludedState);
}

}
