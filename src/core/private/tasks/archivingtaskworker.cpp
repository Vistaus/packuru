// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <shared_mutex>

#include <QDirIterator>
#include <QRegularExpression>

#include "core/private/3rdparty/cs_libguarded/cs_shared_guarded.h"
#include "archivingtaskworker.h"
#include "archivingtaskdata.h"
#include "taskworkerprivate.h"
#include "core/private/plugin-api/abstractbackendhandler.h"
#include "taskstate.h"
#include "creationnameerrorpromptcore.h"
#include "taskerror.h"
#include "core/private/promptcore/creationnameerrorpromptcoreaux.h"


namespace Packuru::Core
{

struct ArchivingTaskWorker::Private
{
    libguarded::shared_guarded<ArchivingTaskData> taskData;
};


ArchivingTaskWorker::ArchivingTaskWorker(QObject* owner,
                                         const ArchivingTaskData& taskData,
                                         std::unique_ptr<AbstractBackendHandler> backendHandler)
    : TaskWorker(owner,
                 taskData.backendData.archiveType != ArchiveType::___NOT_SUPPORTED,
                 std::move(backendHandler)),
      priv(new Private{std::move(taskData)})
{
    auto data = priv->taskData.lock();

    if (!data->backendData.destinationPath.endsWith('/'))
        data->backendData.destinationPath += QLatin1Char('/');
}


ArchivingTaskWorker::~ArchivingTaskWorker()
{

}


ArchivingTaskData ArchivingTaskWorker::getData() const
{
    return *priv->taskData.lock_shared();
}


QString ArchivingTaskWorker::getArchiveName() const
{
    return priv->taskData.lock_shared()->backendData.archiveName;
}


QString ArchivingTaskWorker::getPassword() const
{
    return priv->taskData.lock_shared()->backendData.password;
}


bool ArchivingTaskWorker::preStartInit()
{
    const auto data = priv->taskData.lock_shared();

    if (data->backendData.createNewArchive)
        return true;

    Q_ASSERT(data->backendData.destinationPath.endsWith('/'));

    const QString path = data->backendData.destinationPath + data->backendData.archiveName;
    return TaskWorker::priv->confirmArchiveExistence(path);
}


void ArchivingTaskWorker::startImpl()
{
    const auto data = priv->taskData.lock_shared();

    if (data->backendData.createNewArchive)
    {
        // Task state is changed to Running in Task::start() but it might be changed to WaitingForInput
        // when creating a new archive and the file name already exists
        TaskWorker::priv->changeState(TaskState::InProgress);

        const QString error = checkArchiveNameError(*data);
        if (!error.isEmpty() )
        {
            TaskWorker::priv->changeState(TaskState::WaitingForDecision);

            CreationNameErrorPromptCoreAux::Init init;
            init.archiveName = data->backendData.archiveName;
            init.errorInfo = error;

            CreationNameErrorPromptCoreAux::Backdoor backdoor;

            auto promptCore = new CreationNameErrorPromptCore(init, backdoor);
            promptCore->moveToThread(TaskWorker::priv->promptOwner->thread());
            promptCore->setParent(TaskWorker::priv->promptOwner);

            connect(backdoor.emitter, &CreationNameErrorPromptCoreAux::Emitter::changeArchiveNameRequested,
                    this, &ArchivingTaskWorker::changeArchiveName);

            connect(backdoor.emitter, &CreationNameErrorPromptCoreAux::Emitter::retryRequested,
                    this, &ArchivingTaskWorker::startImpl);

            connect(backdoor.emitter, &CreationNameErrorPromptCoreAux::Emitter::abortRequested,
                    this, &TaskWorker::stop);

            connect(this, &TaskWorker::stateChanged,
                    promptCore, &QObject::deleteLater);

            emit archiveCreationNameError(promptCore);
            return;
        }
    }

    if (!TaskWorker::priv->tryWriteToFolder(data->backendData.destinationPath))
        return TaskWorker::priv->changeState(TaskState::Errors);

    TaskWorker::priv->backendHandler->addToArchive(data->backendData);
}


void ArchivingTaskWorker::onBackendFinished(TaskState concludedState)
{
    Q_ASSERT(isStateFinished(concludedState));

    if (isStateCompleted(concludedState))
    {
        auto data = priv->taskData.lock_shared();

        if (data->openArchiveOnCompletion)
        {
            auto handler = TaskWorker::priv->backendHandler;

            QString createdArchiveName;
            bool splitArchive = data->backendData.partSize > 0;

            if (splitArchive)
            {
                createdArchiveName = handler->createMultiPartArchiveName(data->backendData.archiveName,
                                                                         data->backendData.archiveType);
            }
            else
            {
                createdArchiveName = handler->createSinglePartArchiveName(data->backendData.archiveName,
                                                                          data->backendData.archiveType);
            }

            Q_ASSERT(createdArchiveName.size() > 0);

            const QString path = data->backendData.destinationPath;
            Q_ASSERT(path.endsWith('/'));

            emit openNewArchive(path + createdArchiveName);
        }

        if ( data->openDestinationOnCompletion)
            emit openDestinationPath(data->backendData.destinationPath);
    }

    TaskWorker::priv->changeState(concludedState);
}


void ArchivingTaskWorker::updatePassword(const QString& password)
{
    priv->taskData.lock()->backendData.password = password;
}


QString ArchivingTaskWorker::checkArchiveNameError(const ArchivingTaskData& data) const
{
    Q_ASSERT(data.backendData.destinationPath.endsWith('/'));

    QString result;
    QString path = data.backendData.destinationPath;
    const AbstractBackendHandler* handler = TaskWorker::priv->backendHandler;
    const bool splitArchive = data.backendData.partSize > 0;

    if (!splitArchive)
    {
        const QString name = handler->createSinglePartArchiveName(data.backendData.archiveName, data.backendData.archiveType);
        path += name;
        QFileInfo info(path);

        if (info.exists())
            result = Packuru::Core::ArchivingTaskWorker::tr("File '%1' already exists.").arg(info.fileName());
    }
    else
    {
        QDirIterator dirIt(data.backendData.destinationPath,
                           QDir::Files | QDir::NoDotAndDotDot | QDir::Hidden | QDir::CaseSensitive);
        const QString regExPattern = handler->createMultiPartArchiveRegexNamePattern(data.backendData.archiveName,
                                                                                     data.backendData.archiveType);

        const QRegularExpression rx(regExPattern);
        std::vector<QString> matched;

        while (dirIt.hasNext())
        {
            const QString name = dirIt.next();
            const QRegularExpressionMatch match = rx.match(name);
            if (match.hasMatch())
                matched.push_back(name);
        }

        if (matched.size() > 0)
        {
            result = Packuru::Core::ArchivingTaskWorker::tr("Following files or directories already exist:\n");
            for (const auto& file : qAsConst(matched))
                result += file + QLatin1Char('\n');
            result.chop(1);
        }
    }

    return result;
}


void ArchivingTaskWorker::changeArchiveName(const QString& newName)
{
    priv->taskData.lock()->backendData.archiveName = newName;
}

}
