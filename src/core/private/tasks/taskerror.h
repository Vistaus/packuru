// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QString>
#include <QMetaType>

#include "core/symbol_export.h"


namespace Packuru::Core
{

enum class TaskError
{
    AccessDenied,
    // Archive to be processed could not be found
    ArchiveNotFound,
    ArchivePartNotFound,
    // Type is not supported by the program
    ArchiveTypeNotSupported,
    ArchivingMethodNotSupported,
    BackendCrash,
    // When backend executable could not be found; See also BackendSubmoduleMissing error
    BackendExecutableNotFound,
    // When backend requires additional modules (libraries or executables) to perform a task
    BackendSubmoduleNotFound,
    // When archive contains a link could overwrite existing data (only 7-Zip)
    DangerousLink,
    // Includes: unrecognized data format, corrupted data, truncated archive
    DataError,
    DiskFull,
    // When adding files to archive and some of them could not be found (removed?)
    FileNotFound,
    InvalidBackendCommandLine,
    InvalidPasswordOrDataError,
    /* When an archive contains file and folder with the same path, it can cause an
       error during extraction on Unix systems, unless backend supports auto-renaming
       existing or extracted items, even when they have different type. */
    ItemTypeMismatch,
    /* Sometimes backends report success even if nothing was extracted, e.g. when format is not
       fully supported and error reporting is flawed. This behavior is confusing and may lead
       a user to delete the 'extracted' archive. */
    NoFilesExtracted,
    // Type is supported by the program but no currently loaded plugin supports it
    NoPluginForArchiveType,
    NotEnoughMemory,
    // When handling temporary archive during write operations
    TemporaryArchiveError,
    TemporaryFolderError,
    TemporaryFolderNotEmpty,
    TemporaryFolderNotFound,
    Unknown,
    WriteError
};

PACKURU_CORE_EXPORT QString toString(TaskError value);

}

Q_DECLARE_METATYPE(Packuru::Core::TaskError);
