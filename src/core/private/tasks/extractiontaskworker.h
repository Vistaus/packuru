// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once


#include "taskworker.h"


namespace Packuru::Core
{

class ExtractionTaskData;
class TempFileMover;
class ExtractionFileOverwritePromptCore;
class ExtractionFolderOverwritePromptCore;
class ExtractionTypeMismatchPromptCore;


class ExtractionTaskWorker : public TaskWorker
{
    Q_OBJECT
public:
    ExtractionTaskWorker(QObject* owner, const ExtractionTaskData& taskData, std::unique_ptr<AbstractBackendHandler> backendHandler);
    ~ExtractionTaskWorker() override;

    // Thread-safe
    ExtractionTaskData getData() const;

    // Thread-safe
    QString getArchiveName() const override;
    QString getPassword() const override;

signals:
    void fileExists(ExtractionFileOverwritePromptCore* promptCore);
    void folderExists(ExtractionFolderOverwritePromptCore* promptCore);
    void itemTypeMismatch(ExtractionTypeMismatchPromptCore* promptCore);
    void openDestinationPath(const QString& path);

private:
    bool preStartInit() override;
    void startImpl() override;
    void onBackendFinished(TaskState concludedState) override;
    void updatePassword(const QString& password) override;

    struct Private;
    std::unique_ptr<Private> priv;
};

}
