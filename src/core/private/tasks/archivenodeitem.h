// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QString>

#include "core/private/plugin-api/archiveitem.h"


namespace Packuru::Core
{

enum class CompressionRatioMode;


struct ArchiveNodeItem : public ArchiveItem
{
    ArchiveNodeItem() = default;

    ArchiveNodeItem(ArchiveItem&& other)
        : ArchiveItem(std::forward<ArchiveItem>(other))
    {
    }

    void computeRatio(CompressionRatioMode ratioMode);

    float ratio = 0;
    QString extension;
};

}
