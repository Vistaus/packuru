// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "core/private/plugin-api/backendextractiondata.h"
#include "core/private/extractiontopfoldermode.h"
#include "core/private/extractionfileoverwritemode.h"
#include "core/private/extractionfolderoverwritemode.h"
#include "core/symbol_export.h"


namespace Packuru::Core
{

struct ExtractionTaskData
{
    QString destinationPath;
    ExtractionTopFolderMode topFolderMode = ExtractionTopFolderMode::___INVALID;
    ExtractionFileOverwriteMode fileOverwriteMode = ExtractionFileOverwriteMode::AskBeforeOverwrite;
    ExtractionFolderOverwriteMode folderOverwriteMode = ExtractionFolderOverwriteMode::AskBeforeOverwrite;
    bool openDestinationOnCompletion = false;
    bool runInQueue = false;
    BackendExtractionData backendData;
};

}


PACKURU_CORE_EXPORT QDataStream& operator<<(QDataStream& out, const Packuru::Core::ExtractionTaskData& data);
PACKURU_CORE_EXPORT QDataStream& operator>>(QDataStream& in, Packuru::Core::ExtractionTaskData& data);
