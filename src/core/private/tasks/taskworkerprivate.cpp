// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <unordered_set>

#include <QDir>
#include <QTemporaryFile>
#include <QThread>
#include <QStorageInfo>

#include "taskworkerprivate.h"
#include "core/private/plugin-api/backendhandlererror.h"
#include "core/private/plugin-api/backendhandlerwarning.h"
#include "core/private/plugin-api/abstractbackendhandler.h"
#include "taskerror.h"
#include "taskworker.h"
#include "taskstate.h"
#include "taskwarning.h"


namespace Packuru::Core
{

TaskWorkerPrivate::TaskWorkerPrivate(bool archiveTypeSupported, QObject *parent)
    : QObject(parent),
      archiveTypeSupported(archiveTypeSupported)
{

}


TaskWorkerPrivate::~TaskWorkerPrivate()
{
}


bool TaskWorkerPrivate::confirmArchiveExistence(const QString& archiveFilePath)
{
    QFileInfo info(archiveFilePath);

    if (!info.exists())
    {
        const auto error = TaskError::ArchiveNotFound;
        errors.insert(error);
        log.append(toString(error) + QLatin1String(": ") + archiveFilePath);
        return false;
    }

    return true;
}


bool TaskWorkerPrivate::tryWriteToFolder(const QString& path)
{
    Q_ASSERT(thread() == QThread::currentThread());
    Q_ASSERT(path.endsWith('/'));

    QDir dir(path);

    bool ready = dir.mkpath(path);

    if (ready)
    {
        QTemporaryFile f(path + QLatin1String("tmp"));
        ready = f.open();
    }

    if (!ready)
    {
        auto error = TaskError::AccessDenied;

        while (!dir.exists() && dir.cdUp())
        {
        }

        if (dir.exists())
        {
            const QStorageInfo info(dir.path());

            if (info.bytesAvailable() == 0)
                error = TaskError::DiskFull;
        }

        errors.insert(error);
        log.append(toString(error) + QLatin1String(": ") + path + QLatin1Char('\n'));
    }

    return ready;
}


TaskError TaskWorkerPrivate::convertBackendError(BackendHandlerError e) const
{
    switch (e)
    {
    case BackendHandlerError::AccessDenied: return TaskError::AccessDenied;
    case BackendHandlerError::ArchivePartNotFound: return TaskError::ArchivePartNotFound;
    case BackendHandlerError::ArchivingMethodNotSupported: return TaskError::ArchivingMethodNotSupported;
    case BackendHandlerError::Crash: return TaskError::BackendCrash;
    case BackendHandlerError::DangerousLink: return TaskError::DangerousLink;
    case BackendHandlerError::DataError: return TaskError::DataError;
    case BackendHandlerError::DiskFull: return TaskError::DiskFull;
    case BackendHandlerError::ExecutableNotFound: return TaskError::BackendExecutableNotFound;
    case BackendHandlerError::FileNotFound: return TaskError::FileNotFound;
    case BackendHandlerError::InvalidCommandLine: return TaskError::InvalidBackendCommandLine;
    case BackendHandlerError::InvalidPasswordOrDataError: return TaskError::InvalidPasswordOrDataError;
    case BackendHandlerError::ItemTypeMismatch: return TaskError::ItemTypeMismatch;
    case BackendHandlerError::NotEnoughMemory: return TaskError::NotEnoughMemory;
    case BackendHandlerError::SubmoduleNotFound: return TaskError::BackendSubmoduleNotFound;
    case BackendHandlerError::TemporaryArchiveError: return TaskError::TemporaryArchiveError;
    case BackendHandlerError::TemporaryFolderNotEmpty: return TaskError::TemporaryFolderNotEmpty;
    case BackendHandlerError::TemporaryFolderNotFound: return TaskError::TemporaryFolderNotFound;
    case BackendHandlerError::WriteError: return TaskError::WriteError;
    default: Q_ASSERT(false); return TaskError::Unknown;
    }
}


TaskWarning TaskWorkerPrivate::convertBackendWarning(BackendHandlerWarning w) const
{
    switch (w)
    {
    case BackendHandlerWarning::LowDiskSpace: return TaskWarning::LowDiskSpace;
    case BackendHandlerWarning::VersionDeprecated: return TaskWarning::BackendVersionDeprecated;
    case BackendHandlerWarning::VersionNotTested: return TaskWarning::BackendVersionNotTested;
    default: Q_ASSERT(false); return TaskWarning::Unknown;
    }
}


void TaskWorkerPrivate::changeState(TaskState newState)
{
    if (newState != taskState)
    {
        taskState = newState;

        if (isStateFinished(taskState))
        {
            if (errors.size() > 0)
                emit publ->errors(errors);

            if (warnings.size() > 0)
                emit publ->warnings(warnings);

            if (!log.isEmpty())
                emit publ->log(log);
        }

        emit publ->stateChanged(taskState);

        if (isStateFinished(taskState))
            emit publ->finished();
    }
}


TaskState TaskWorkerPrivate::processBackendResults()
{
    if (isStateFinished(concludedState))
        return concludedState;

    const auto backendState = backendHandler->getState();
    Q_ASSERT(isStateFinished(backendState));

    if (backendHandler->hasWarnings())
    {
        Q_ASSERT(backendState != BackendHandlerState::Success);

        const std::unordered_set<BackendHandlerWarning> bckWarnings = backendHandler->getWarnings();

        for (const auto w : bckWarnings)
        {
            const TaskWarning taskWarning = convertBackendWarning(w);
            warnings.insert(taskWarning);
        }
    }

    if (backendHandler->hasErrors())
    {
        Q_ASSERT(backendState != BackendHandlerState::Success);
        Q_ASSERT(backendState != BackendHandlerState::Warnings);

        const std::unordered_set<BackendHandlerError> bckErrors = backendHandler->getErrors();

        for (const auto e : bckErrors)
        {
            const TaskError taskError = convertBackendError(e);
            errors.insert(taskError);
        }

        /* Invalid passwords are cleaned up so when the task is reset it will
               ask for a password again. */
        if (errors.find(TaskError::InvalidPasswordOrDataError) != errors.end())
            publ->updatePassword("");
    }

    switch (backendState)
    {
    case BackendHandlerState::Success:
        concludedState = TaskState::Success; break;
    case BackendHandlerState::Warnings:
    {
        concludedState = TaskState::Warnings;
        // Backend handler might not set any warning
        if (warnings.empty())
            warnings.insert(TaskWarning::Unknown);
        break;
    }
    case BackendHandlerState::Errors:
    {
        concludedState = TaskState::Errors;
        // Backend handler might not recognize any error
        if (errors.empty())
            errors.insert(TaskError::Unknown);
        break;
    }
    case BackendHandlerState::Aborted:
        concludedState = TaskState::Aborted; break;

    default:
        Q_ASSERT(false);
    }

    log.append(backendHandler->clearLog());

    return concludedState;
}

}
