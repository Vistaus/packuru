// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QDataStream>

#include "extractiontaskdata.h"


QDataStream& operator<<(QDataStream& out, const Packuru::Core::ExtractionTaskData& data)
{
    return out << data.destinationPath
               << data.topFolderMode
               << data.fileOverwriteMode
               << data.folderOverwriteMode
               << data.openDestinationOnCompletion
               << data.runInQueue
               << data.backendData;
}


QDataStream& operator>>(QDataStream& in, Packuru::Core::ExtractionTaskData& data)
{
    return in >> data.destinationPath
              >> data.topFolderMode
              >> data.fileOverwriteMode
              >> data.folderOverwriteMode
              >> data.openDestinationOnCompletion
              >> data.runInQueue
              >> data.backendData;
}
