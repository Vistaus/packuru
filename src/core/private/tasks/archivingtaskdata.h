// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QString>

#include "core/private/plugin-api/backendarchivingdata.h"
#include "core/symbol_export.h"


namespace Packuru::Core
{

struct ArchivingTaskData
{
    bool openArchiveOnCompletion = false;
    bool openDestinationOnCompletion = false;
    bool runInQueue = false;
    /* Recognize plugins by id.
       Even if plugin configuration has changed during sending data from Browser to Queue
       the same plugin must be used because backend data contains private data specific
       for particular plugin. */
    uint pluginId = 0;
    BackendArchivingData backendData;
};

}


PACKURU_CORE_EXPORT QDataStream& operator<<(QDataStream& out, const Packuru::Core::ArchivingTaskData& data);
PACKURU_CORE_EXPORT QDataStream& operator>>(QDataStream& in, Packuru::Core::ArchivingTaskData& data);
