// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "archivingtask.h"
#include "archivingtaskworker.h"
#include "taskprivate.h"
#include "archivingtaskdata.h"
#include "creationnameerrorpromptcore.h"
#include "core/private/plugin-api/abstractbackendhandler.h"


namespace Packuru::Core
{

struct ArchivingTask::Private
{
    void connectToSignals(ArchivingTask* task, ArchivingTaskWorker* worker) const;
};


ArchivingTask::ArchivingTask(const ArchivingTaskData& taskData,
                             const BackendHandlerFactory& backendFactory,
                             QObject* parent)
    : Task(taskData.backendData.createNewArchive ? TaskType::Create : TaskType::Add,
           std::make_unique<ArchivingTaskWorker>(this, taskData, backendFactory(taskData.backendData.archiveType)),
           backendFactory,
           parent),
      priv(new Private)
{
    auto worker = qobject_cast<ArchivingTaskWorker*>(Task::priv->worker.get());
    Q_ASSERT(worker);
    priv->connectToSignals(this, worker);
}


ArchivingTask::~ArchivingTask()
{

}


std::unique_ptr<TaskWorker> ArchivingTask::createWorker()
{
    auto worker = qobject_cast<ArchivingTaskWorker*>(Task::priv->worker.get());
    Q_ASSERT(worker);
    const ArchivingTaskData data = worker->getData();
    auto newWorker = std::make_unique<ArchivingTaskWorker>(this, data, Task::priv->backendFactory(data.backendData.archiveType));
    priv->connectToSignals(this, newWorker.get());
    return newWorker;
}


void ArchivingTask::Private::connectToSignals(ArchivingTask* task, ArchivingTaskWorker* worker) const
{
    QObject::connect(worker, &ArchivingTaskWorker::archiveCreationNameError,
                     task, &ArchivingTask::archiveCreationNameError);

    QObject::connect(worker, &ArchivingTaskWorker::openNewArchive,
                     task, &ArchivingTask::openNewArchive);

    QObject::connect(worker, &ArchivingTaskWorker::openDestinationPath,
                     task, &ArchivingTask::openDestinationPath);
}

}
