// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>
#include <unordered_set>

#include <QObject>

#include "taskstate.h"


namespace Packuru::Core
{

class TaskWorker;
class AbstractBackendHandler;
enum class TaskState;
enum class TaskError;
enum class TaskWarning;
enum class BackendHandlerError;
enum class BackendHandlerWarning;
enum class BackendHandlerState;

class TaskWorkerPrivate : public QObject
{
    Q_OBJECT
public:
    explicit TaskWorkerPrivate(bool archiveTypeSupported, QObject *parent = nullptr);
    ~TaskWorkerPrivate();

    bool confirmArchiveExistence(const QString& archiveFilePath);
    bool tryWriteToFolder(const QString& path);

    TaskError convertBackendError(BackendHandlerError e) const;
    TaskWarning convertBackendWarning(BackendHandlerWarning w) const;

    void changeState(TaskState newState);
    TaskState getState() const { return taskState; }

    TaskState getConcludedState() const { return concludedState; }
    // Returns concluded state, call only once after backend handler finishes
    TaskState processBackendResults();

    QObject* promptOwner = nullptr;
    TaskWorker* publ = nullptr;
    std::unordered_set<TaskError> errors;
    std::unordered_set<TaskWarning> warnings;
    QString log;
    AbstractBackendHandler* backendHandler = nullptr;
    const bool archiveTypeSupported;

private:
    TaskState taskState = TaskState::ReadyToRun;
    /* Describes the state concluded after processing finished backend handler's results
      (errors, warnings, state, abort request). It acts as a hint of what a final task state
      should be. However different tasks may perform additional actions after backend handler
      finishes, e.g. extraction task moves data from temporary folder to final destination and
      additional errors may occur which influences the final task state. */
    TaskState concludedState = TaskState::___INVALID;
};

}
