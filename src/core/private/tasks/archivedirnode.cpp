// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "archivedirnode.h"


namespace Packuru::Core
{

ArchiveDirNode::ArchiveDirNode(ArchiveDirNode* parent, int row, const QString& name)
    : ArchiveFileNode(parent, row, name)
{
    _item.nodeType = NodeType::Folder;
}


ArchiveDirNode::ArchiveDirNode()
    : ArchiveDirNode(nullptr, 0, QString())
{

}


ArchiveDirNode::~ArchiveDirNode()
{
    deleteChildren();
}


ArchiveFileNode* ArchiveDirNode::getChild(int row) const
{
    Q_ASSERT(row >= 0 && row < static_cast<int>(children.size()));
    return children[static_cast<std::size_t>(row)];
}


void ArchiveDirNode::deleteChildren()
{
    for (auto node : children)
        delete node;
}


ArchiveDirNode* ArchiveDirNode::addDir(const QString& name)
{
    ArchiveDirNode* node = new ArchiveDirNode(this, static_cast<int>(children.size()), name);
    children.push_back(node);
    return node;
}


ArchiveFileNode* ArchiveDirNode::addFile(ArchiveItem& item)
{
    ArchiveFileNode* node = createFileNode(this, static_cast<int>(children.size()), item);
    children.push_back(node);
    return node;
}


void ArchiveDirNode::updateChildrenRows()
{
    for (std::size_t i = 0; i < children.size(); ++i)
        children[i]->setRow(static_cast<int>(i));
}

}
