// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QCoreApplication>

#include "taskstate.h"


namespace Packuru::Core
{

QString toString(TaskState state)
{
    switch (state)
    {
    case TaskState::ReadyToRun:
        return QCoreApplication::translate("TaskState", "Ready to run");
    case TaskState::StartingUp:
        return QCoreApplication::translate("TaskState", "Starting up");
    case TaskState::InProgress:
        return QCoreApplication::translate("TaskState", "In progress");
    case TaskState::WaitingForPassword:
        return QCoreApplication::translate("TaskState", "Waiting for password");
    case TaskState::WaitingForDecision:
        return QCoreApplication::translate("TaskState", "Waiting for decision");
    case TaskState::Paused:
        return QCoreApplication::translate("TaskState", "Paused");
    case TaskState::Success:
        return QCoreApplication::translate("TaskState", "Success");
    case TaskState::Warnings:
        return QCoreApplication::translate("TaskState", "Finished with warnings");
    case TaskState::Errors:
        return QCoreApplication::translate("TaskState", "Finished with errors");
    case TaskState::Aborted:
        return QCoreApplication::translate("TaskState", "Aborted");
    default:
        Q_ASSERT(false); return QString();
    }
}


bool isStateBusy(TaskState state)
{
    return (state > TaskState::___BUSY_STATES_BEGIN
            && state < TaskState::___BUSY_STATES_END);
}


bool isStateFinished(TaskState state)
{
    return (state > TaskState::___FINISHED_STATES_BEGIN
            && state < TaskState::___FINISHED_STATES_END);
}


bool isStateCompleted(TaskState state)
{
    return state == TaskState::Success
            || state == TaskState::Warnings;
}


bool isStateValid(TaskState state)
{
    return state == TaskState::ReadyToRun
            || isStateBusy(state)
            || isStateFinished(state);
}

}
