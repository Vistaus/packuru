// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <vector>

#include "archivefilenode.h"


namespace Packuru::Core
{

class ArchiveDirNode : public ArchiveFileNode
{
    friend class ArchiveFileTreeBuilder;
public:
    ArchiveDirNode(); // Creates root node
    ~ArchiveDirNode() override;

    ArchiveFileNode* getChild(int row) const;
    std::vector<ArchiveFileNode*>& getChildren() { return children; }
    int getChildrenCount() const { return static_cast<int>(children.size()); }
    void deleteChildren();
    void updateChildrenRows();

private:
    ArchiveDirNode(ArchiveDirNode* parent, int row, const QString& name);

    ArchiveDirNode* addDir(const QString& name);
    ArchiveFileNode* addFile(ArchiveItem& _item);

    std::vector<ArchiveFileNode*> children;
    bool unixRootDir_;
};

}
