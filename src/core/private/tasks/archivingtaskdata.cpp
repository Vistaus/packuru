// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QDataStream>

#include "archivingtaskdata.h"


QDataStream& operator<<(QDataStream& out, const Packuru::Core::ArchivingTaskData& data)
{
    return out << data.openArchiveOnCompletion
               << data.openDestinationOnCompletion
               << data.runInQueue
               << data.pluginId
               << data.backendData;
}


QDataStream& operator>>(QDataStream& in, Packuru::Core::ArchivingTaskData& data)
{
    return in >> data.openArchiveOnCompletion
              >> data.openDestinationOnCompletion
              >> data.runInQueue
              >> data.pluginId
              >> data.backendData;
}
