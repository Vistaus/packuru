// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "task.h"
#include "backendhandlerfactory.h"


namespace Packuru::Core
{

class DeletionTaskData;


class DeletionTask : public Task
{
    Q_OBJECT
public:
    DeletionTask(const DeletionTaskData& taskData, const BackendHandlerFactory& backendFactory, QObject *parent = nullptr);
    ~DeletionTask() override;

private:
    std::unique_ptr<TaskWorker> createWorker() override;
};

}
