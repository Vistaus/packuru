// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <functional>
#include <memory>


namespace Packuru::Core
{

class AbstractBackendHandler;
enum class ArchiveType;

using BackendHandlerFactory = std::function<std::unique_ptr<AbstractBackendHandler>(ArchiveType)>;

}
