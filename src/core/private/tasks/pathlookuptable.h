// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <unordered_map>
#include <vector>

#include "utils/private/qttypehasher.h"


namespace Packuru::Core
{

class ArchiveDirNode;

class PathLookupTable
{
public:
    using DirContainer = std::unordered_map<QString, ArchiveDirNode*, Packuru::Utils::QtTypeHasher<QString>>;

    void addDir(const QString& path, ArchiveDirNode* dirNode)
    {
        dirs[path] = dirNode;
    }

    ArchiveDirNode* getDir(const QString& path) const
    {
        auto it = dirs.find(path);

        if (it == dirs.end())
            return nullptr;
        else
            return it->second;
    }

    const DirContainer& getDirs() const { return dirs; }

    void clear() { dirs.clear(); }

    std::vector<QString> keys() const
    {
        std::vector<QString> result;
        for (const auto& it : dirs)
            result.push_back(it.first);

        return result;
    }

private:
    DirContainer dirs;
};

}
