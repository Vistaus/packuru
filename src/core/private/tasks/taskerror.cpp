// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QCoreApplication>

#include "taskerror.h"


namespace Packuru::Core
{

QString toString(TaskError value)
{
    switch (value)
    {
    case TaskError::AccessDenied:
        return QCoreApplication::translate("TaskError", "Access denied");

    case TaskError::ArchiveNotFound:
        return QCoreApplication::translate("TaskError", "Archive not found");

    case TaskError::ArchivePartNotFound:
        return QCoreApplication::translate("TaskError", "Archive part not found");

    case TaskError::ArchiveTypeNotSupported:
        return QCoreApplication::translate("TaskError", "Archive type not supported");

    case TaskError::ArchivingMethodNotSupported:
        return QCoreApplication::translate("TaskError", "Archiving method not supported");

    case TaskError::BackendCrash:
        return QCoreApplication::translate("TaskError", "Backend crashed");

    case TaskError::BackendExecutableNotFound:
        return QCoreApplication::translate("TaskError", "Backend executable not found");

    case TaskError::BackendSubmoduleNotFound:
        return QCoreApplication::translate("TaskError", "Backend submodule not found");

    case TaskError::DangerousLink:
        return QCoreApplication::translate("TaskError", "Dangerous link");

    case TaskError::DataError:
        return QCoreApplication::translate("TaskError", "Data error");

    case TaskError::DiskFull:
        return QCoreApplication::translate("TaskError", "Disk full");

    case TaskError::FileNotFound:
        return QCoreApplication::translate("TaskError", "File not found");

    case TaskError::InvalidBackendCommandLine:
        return QCoreApplication::translate("TaskError", "Invalid backend command line");

    case TaskError::InvalidPasswordOrDataError:
        return QCoreApplication::translate("TaskError", "Invalid password or data error");

    case TaskError::ItemTypeMismatch:
        return QCoreApplication::translate("TaskError", "Item type mismatch");

    case TaskError::NoFilesExtracted:
        return QCoreApplication::translate("TaskError", "No files extracted");

    case TaskError::NoPluginForArchiveType:
        return QCoreApplication::translate("TaskError", "No plugin for this archive type");

    case TaskError::NotEnoughMemory:
        return QCoreApplication::translate("TaskError", "Not enough memory");

    case TaskError::TemporaryArchiveError:
        return QCoreApplication::translate("TaskError", "Temporary archive error");

    case TaskError::TemporaryFolderError:
        return QCoreApplication::translate("TaskError", "Temporary folder error");

    case TaskError::TemporaryFolderNotEmpty:
        return QCoreApplication::translate("TaskError", "Temporary folder not empty");

    case TaskError::TemporaryFolderNotFound:
        return QCoreApplication::translate("TaskError", "Temporary folder not found");

    case TaskError::Unknown:
        return QCoreApplication::translate("TaskError", "Unknown error");

    case TaskError::WriteError:
        return QCoreApplication::translate("TaskError", "Write error");

    default:
        Q_ASSERT(false); return QString();
    }
}

}
