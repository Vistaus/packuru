// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "readtask.h"
#include "tasktype.h"
#include "readtaskworker.h"
#include "taskprivate.h"
#include "readtaskdata.h"
#include "archivecontentdata.h"
#include "core/private/plugin-api/abstractbackendhandler.h"
#include "backendhandlerfactory.h"


namespace Packuru::Core
{

ReadTask::ReadTask(const ReadTaskData& taskData, const BackendHandlerFactory& backendFactory, QObject* parent)
    : Task(TaskType::Read,
           std::make_unique<ReadTaskWorker>(this, taskData, backendFactory(taskData.backendData.archiveType)),
           backendFactory,
           parent)
{
}


ReadTask::~ReadTask()
{

}


ReadTaskData ReadTask::getTaskData() const
{
    auto worker = qobject_cast<ReadTaskWorker*>(Task::priv->worker.get());
    return worker->getData();
}


ArchiveContentData ReadTask::getArchiveContentData() const
{
    auto worker = qobject_cast<ReadTaskWorker*>(Task::priv->worker.get());
    return worker->getArchiveContentData();
}


BackendArchiveProperties ReadTask::getArchiveProperties() const
{
    auto worker = qobject_cast<ReadTaskWorker*>(Task::priv->worker.get());
    return worker->getArchiveProperties();
}


std::unique_ptr<TaskWorker> ReadTask::createWorker()
{
    auto worker = qobject_cast<ReadTaskWorker*>(Task::priv->worker.get());
    Q_ASSERT(worker);
    const ReadTaskData data = worker->getData();
    auto newWorker = std::make_unique<ReadTaskWorker>(this, data, Task::priv->backendFactory(data.backendData.archiveType));
    return newWorker;
}

}
