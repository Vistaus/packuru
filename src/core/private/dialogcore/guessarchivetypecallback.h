// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <functional>


class QFileInfo;


namespace Packuru::Core
{

enum class ArchiveType;

using GuessArchiveTypeCallback = std::function<ArchiveType(const QFileInfo&)>;

}
