// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <unordered_set>
#include <vector>

#include <QAbstractTableModel>
#include <QFileInfo>
#include <QUrl>
#include <QMimeDatabase>

#include "utils/private/qttypehasher.h"

#include "guessarchivetypecallback.h"
#include "getbackendfortypecallback.h"
#include "core/private/plugin-api/archivetype.h"


namespace Packuru::Core
{

class InputArchivesModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    class ArchiveInfo
    {
        friend class InputArchivesModel;
    public:
        QFileInfo info;
        ArchiveType type = ArchiveType::___NOT_SUPPORTED;

    private:
        ArchiveInfo(const QFileInfo& info, ArchiveType type, const QMimeType& mediaType, const QString& backendName)
            : info(info), type(type), mediaType(mediaType), pluginName(backendName) {}

        QMimeType mediaType;
        QString pluginName;
    };

    explicit InputArchivesModel(const GuessArchiveTypeCallback& guessArchiveType,
                                const GetBackendForTypeCallback& getBackend,
                                QObject *parent = nullptr);

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    Qt::ItemFlags flags(const QModelIndex& index) const override;
    bool canDropMimeData(const QMimeData* data, Qt::DropAction action, int row, int column, const QModelIndex& parent) const override;
    bool dropMimeData(const QMimeData* data, Qt::DropAction action, int row, int column, const QModelIndex& parent) override;

    QString getArchiveBackend(int row) const;
    void addArchives(const std::vector<QFileInfo>& list);
    Q_INVOKABLE void addArchives(const QList<QUrl>& list);
    Q_INVOKABLE void removeArchives(const QModelIndexList& indexes);
    std::vector<ArchiveInfo> getArchives() const { return archives; }
    QString getCommonParentFolder() const;

signals:
    void modelEmpty(bool value) const;

private:
    bool removeRows(int row, int count, const QModelIndex& parent = QModelIndex()) override;

    std::vector<ArchiveInfo> archives;
    std::unordered_set<QString, Packuru::Utils::QtTypeHasher<QString>> fullPaths;
    GuessArchiveTypeCallback guessArchiveType;
    GetBackendForTypeCallback getBackendForType;
    QMimeDatabase mimeDB;
};

}
