// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <functional>

#include <QCoreApplication>

#include "qcs-core/controller.h"


namespace Packuru::Core
{

class DirectoryErrorCtrl : public qcs::core::Controller<QString>
{
    Q_DECLARE_TR_FUNCTIONS(DirectoryErrorCtrl)
public:
    DirectoryErrorCtrl();

    void update() override;

private:
    const QString pathErrorText;
};

}
