// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <functional>
#include <memory>
#include <vector>

#include <QFileInfo>
#include <QString>

#include "core/private/plugin-api/encryptionstate.h"


namespace Packuru::Core
{

struct ArchivingTaskData;
class ArchivingParameterHandlerFactory;

struct ArchivingDialogCoreInitAdd
{
    using OnAcceptedCallback = std::function<void(const ArchivingTaskData&)>;

    bool isValid() const;

    QFileInfo archiveInfo;
    std::unique_ptr<ArchivingParameterHandlerFactory> factory;
    EncryptionState archiveEncryption = EncryptionState::___INVALID;
    QString password;
    QString archiveInternalPath;
    std::vector<QFileInfo> filesToArchive;
    OnAcceptedCallback onAccepted;
};

}
