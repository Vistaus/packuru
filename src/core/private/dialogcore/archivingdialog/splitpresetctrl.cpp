// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "splitpresetctrl.h"
#include "core/private/sizeunits.h"
#include "globalsettingsmanager.h"
#include "private/globalsettings.h"
#include "archivingdialogcontrollertype.h"


using qcs::core::ControllerProperty;


namespace Packuru::Core
{

SplitPresetCtrl::SplitPresetCtrl(const std::function<bool()>& archiveSplittingSupport)
    : archiveSplittingSupport(archiveSplittingSupport),
      disabledSplitIndex(0),
      disabledSplitText(Packuru::Core::SplitPresetCtrl::tr("Disabled")),
      units(GlobalSettingsManager::instance()
            ->getValueAs<SizeUnits>(GlobalSettings::Enum::AC_SizeUnits)),
      customSplitIndex(1),
      customSplitText(Packuru::Core::SplitPresetCtrl::tr("Custom")),
      customSplitDefaultSize(units == SizeUnits::Binary ? 1024*1024*20 : 1000*1000*20),
      userPublicValue(disabledSplitText),
      userPrivateValue(0)
{  
    const auto lists = createLists({ {userPublicValue, userPrivateValue},
                                     {customSplitText, customSplitDefaultSize},
                                     {"CD 650", 681'984'000},
                                     {"CD 700", 737'280'000},
                                     {"DVD", 4'700'000'000},
                                     {"DVD DL", 8'500'000'000},
                                     {"Blu-ray", 25'000'000'000},
                                     {"Blu-ray DL", 50'000'000'000} });

    setValueLists(lists.first, lists.second);

    sizePresets = lists.second;
    setCurrentValueFromIndex(disabledSplitIndex);

    properties[qcs::core::ControllerProperty::LabelText]
            = Packuru::Core::SplitPresetCtrl::tr("Split");
}


void SplitPresetCtrl::onValueSyncedToWidget(const QVariant& value)
{
    Q_UNUSED(value)
    userPublicValue = getPropertyAs<QString>(ControllerProperty::PublicCurrentValue);
    userPrivateValue = getPropertyAs<qulonglong>(ControllerProperty::PrivateCurrentValue);
}


void SplitPresetCtrl::update()
{
    if (archiveSplittingSupport())
    {
        setEnabled(true);

        const auto splitSize = getControllerPropertyAs<int>(ArchivingDialogControllerType::Type::SplitSize,
                                                            ControllerProperty::PublicCurrentValue);

        const uint baseUnit = (units == SizeUnits::Binary ? 1024 : 1000);
        const auto currentPresetSizeBytes = getPropertyAs<qulonglong>(ControllerProperty::PrivateCurrentValue);
        const auto currentPresetSize = static_cast<int>(currentPresetSizeBytes / baseUnit / baseUnit);

        if (splitSize != currentPresetSize)
        {
            const auto splitSizeBytes = static_cast<qulonglong>(splitSize) * baseUnit * baseUnit;
            userPrivateValue = splitSizeBytes;

            if (splitSizeBytes == 0)
            {
                sizePresets[customSplitIndex] = customSplitDefaultSize;
                userPublicValue = disabledSplitText;
            }
            else
            {
                sizePresets[customSplitIndex] = splitSizeBytes;
                userPublicValue = customSplitText;
            }
            properties[ControllerProperty::PrivateValueList].setValue(sizePresets);
        }

        setCurrentValue(userPublicValue, userPrivateValue);
    }
    else
    {
        setCurrentValueFromIndex(disabledSplitIndex);
        setEnabled(false);
    }
}

}
