// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "opennewarchivectrl.h"


using qcs::core::ControllerProperty;


namespace Packuru::Core
{

OpenNewArchiveCtrl::OpenNewArchiveCtrl()
{
    setCurrentValue(false);
    properties[ControllerProperty::Text] = Packuru::Core::OpenNewArchiveCtrl::tr("Open archive");
}

}
