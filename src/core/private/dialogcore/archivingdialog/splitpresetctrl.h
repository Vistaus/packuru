// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <functional>

#include <QCoreApplication>

#include "qcs-core/controller.h"


namespace Packuru::Core
{

enum class SizeUnits;


class SplitPresetCtrl : public qcs::core::Controller<QString, qulonglong>
{
    Q_DECLARE_TR_FUNCTIONS(SplitPresetCtrl)
public:
    SplitPresetCtrl(const std::function<bool()>& archiveSplittingSupport);

    void onValueSyncedToWidget(const QVariant& value) override;
    void update() override;

private:
    const std::function<bool()> archiveSplittingSupport;
    QList<qulonglong> sizePresets;

    const int disabledSplitIndex;
    const QString disabledSplitText;
    const SizeUnits units;
    const int customSplitIndex;
    const QString customSplitText;
    const qulonglong customSplitDefaultSize;
    QString userPublicValue;
    qulonglong userPrivateValue;
};

}
