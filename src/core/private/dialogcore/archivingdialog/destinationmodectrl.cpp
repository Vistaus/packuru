// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "destinationmodectrl.h"
#include "archivingmode.h"
#include "archivingdialogcontrollertype.h"


using qcs::core::ControllerProperty;


namespace Packuru::Core
{

DestinationModeCtrl::DestinationModeCtrl()
    : userChoice(ArchivingDestinationMode::CustomFolder)
{
    setValueListsFromPrivate({ArchivingDestinationMode::CustomFolder,
                     ArchivingDestinationMode::ItemParentFolder});

    setCurrentValueFromPrivate(ArchivingDestinationMode::CustomFolder);

    properties[qcs::core::ControllerProperty::LabelText]
            = Packuru::Core::DestinationModeCtrl::tr("Create in");
}


void DestinationModeCtrl::update()
{
    const auto archivingMode = getControllerPropertyAs<ArchivingMode>
            (ArchivingDialogControllerType::Type::ArchivingMode,
             ControllerProperty::PrivateCurrentValue);

    switch (archivingMode) {
    case ArchivingMode::ArchivePerItem:
        setValueListsFromPrivate({ArchivingDestinationMode::CustomFolder,
                         ArchivingDestinationMode::ItemParentFolder});
        setCurrentValueFromPrivate(userChoice);
        setEnabled(true);
        break;
    case ArchivingMode::SingleArchive:
        setValueListsFromPrivate({ArchivingDestinationMode::CustomFolder});
        setCurrentValueFromPrivate(ArchivingDestinationMode::CustomFolder);
        setEnabled(false);
        break;
    default:
        Q_ASSERT(false);
    }
}


void DestinationModeCtrl::onValueSyncedToWidget(const QVariant &)
{
    userChoice = getPropertyAs<ArchivingDestinationMode>(ControllerProperty::PrivateCurrentValue);
}

}
