// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <unordered_set>
#include <vector>

#include <QAbstractTableModel>
#include <QFileInfo>
#include <QMimeType>
#include <QMimeDatabase>


class QItemSelection;


namespace Packuru::Core
{

class InputFilesModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit InputFilesModel(QObject *parent = nullptr);

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
    bool removeRows(int row, int count, const QModelIndex& parent = QModelIndex()) override;

    Qt::ItemFlags flags(const QModelIndex& index) const override;
    bool canDropMimeData(const QMimeData* data, Qt::DropAction action, int row, int column, const QModelIndex& parent) const override;
    bool dropMimeData(const QMimeData* data, Qt::DropAction action, int row, int column, const QModelIndex& parent) override;

    void addItems(const std::vector<QFileInfo>& newItems);
    void addItems(const QList<QUrl>& newItems);
    void removeItems(const QModelIndexList& indexes);

    std::vector<QFileInfo> getItems() const { return infoList; }
    QString getCommonParentFolder() const;

signals:
    void modelEmpty(bool value);

private:
    std::unordered_set<uint> filePaths;
    std::unordered_set<uint> dirPaths;
    std::vector<QFileInfo> infoList;

    const bool duplicatesAllowed;
    QMimeType folderType;
    QMimeDatabase mimeDB;
};

}
