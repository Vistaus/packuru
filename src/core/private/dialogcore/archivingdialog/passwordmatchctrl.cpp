// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "qcs-core/controllerproperty.h"

#include "passwordmatchctrl.h"
#include "archivingdialogcontrollertype.h"


using namespace qcs::core;


namespace Packuru::Core
{


PasswordMatchCtrl::PasswordMatchCtrl()
{

}


void PasswordMatchCtrl::update()
{
    const auto password = getControllerPropertyAs<QString>
            (ArchivingDialogControllerType::Type::Password,
             ControllerProperty::PublicCurrentValue);

    const auto passwordRepeat = getControllerPropertyAs<QString>
            (ArchivingDialogControllerType::Type::PasswordRepeat,
             ControllerProperty::PublicCurrentValue);

    QString message;
    auto match = PasswordMatch::___INVALID;

    if (password == passwordRepeat)
    {
        if (password.isEmpty())
            match = PasswordMatch::EmptyPasswords;
        else
            match = PasswordMatch::Match;
    }
    else if (!passwordRepeat.isEmpty() && password.startsWith(passwordRepeat))
    {
        message = Packuru::Core::PasswordMatchCtrl::tr("Partial password match");
        match = PasswordMatch::PartialMatch;
    }
    else
    {
        message = Packuru::Core::PasswordMatchCtrl::tr("Passwords do not match");
        match = PasswordMatch::NoMatch;
    }

    Q_ASSERT(match != PasswordMatch::___INVALID);

    setCurrentValue(message, match);
    setVisible(!message.isEmpty());
}

}
