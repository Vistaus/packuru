// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "archivenameerrorctrl.h"
#include "archivingmode.h"
#include "archivingdialogcontrollertype.h"


using qcs::core::ControllerProperty;


namespace Packuru::Core
{

ArchiveNameErrorCtrl::ArchiveNameErrorCtrl()
    :  errorText(Packuru::Core::ArchiveNameErrorCtrl::tr("Archive name cannot be empty"))
{

}


void ArchiveNameErrorCtrl::update()
{
    const auto archivingMode = getControllerPropertyAs<ArchivingMode>
            (ArchivingDialogControllerType::Type::ArchivingMode,
             ControllerProperty::PrivateCurrentValue);

    switch (archivingMode)
    {
    case ArchivingMode::ArchivePerItem:
    {
        setCurrentValue("");
        setVisible(false);
        break;
    }
    case ArchivingMode::SingleArchive:
    {
        const auto archiveBaseName = getControllerPropertyAs<QString>
                (ArchivingDialogControllerType::Type::ArchiveName,
                 ControllerProperty::PublicCurrentValue);

        QString error;
        if (archiveBaseName.isEmpty())
            error = errorText;

        setCurrentValue(error);
        setVisible(!error.isEmpty());

        break;
    }
    default:
        Q_ASSERT(false);
    }

}

}
