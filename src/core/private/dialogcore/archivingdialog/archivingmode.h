// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QMetaType>


namespace Packuru::Core
{

enum class ArchivingMode
{
    SingleArchive,
    ArchivePerItem
};

QString toString(ArchivingMode value);

}

Q_DECLARE_METATYPE(Packuru::Core::ArchivingMode);
