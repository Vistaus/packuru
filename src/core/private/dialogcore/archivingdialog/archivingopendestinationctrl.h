// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QCoreApplication>

#include "qcs-core/controller.h"


namespace Packuru::Core
{

class ArchivingOpenDestinationCtrl : public qcs::core::Controller<bool>
{
    Q_DECLARE_TR_FUNCTIONS(ArchivingOpenDestinationCtrl)
public:
    ArchivingOpenDestinationCtrl();
};


}
