// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QCoreApplication>

#include "qcs-core/controller.h"


namespace Packuru::Core
{

class ArchiveNameCtrl : public qcs::core::Controller<QString>
{
    Q_DECLARE_TR_FUNCTIONS(ArchiveNameCtrl)
public:
    ArchiveNameCtrl(const QString& initialName);

    void update() override;
    void onValueSyncedToWidget(const QVariant& value) override;

private:
    const QString automaticNameText;
    QString userSetValue;
};

}
