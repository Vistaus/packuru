// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <functional>

#include <QCoreApplication>
#include <QString>

#include "qcs-core/controller.h"


namespace Packuru::Core
{

enum class EncryptionState;
enum class EncryptionSupport;


class EncryptionStateCtrl : public qcs::core::Controller<QString>
{
    Q_DECLARE_TR_FUNCTIONS(EncryptionStateCtrl)

    using GetEncryptionSupportFunction = std::function<EncryptionSupport()>;
    using GetEncryptionMethodFunction = std::function<QString()>;
    using SetDialogErrorFunction = std::function<void(const QString&)>;

public:
    EncryptionStateCtrl(const GetEncryptionSupportFunction& getEncryptionSupport,
                        const GetEncryptionMethodFunction& getEncryptionMethod);

    void update() override;
    void onEncryptionMethodChanged(const QString& method);
    EncryptionState getEncryptionState() const { return currentEncryptionState; }

private:
    QString setEnabledLabel(const QString& encryptionMethod) const;

    GetEncryptionSupportFunction getEncryptionSupport;
    GetEncryptionMethodFunction getEncryptionMethod;
    EncryptionState currentEncryptionState;
};

}
