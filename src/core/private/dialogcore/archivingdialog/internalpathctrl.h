// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <functional>

#include <QCoreApplication>

#include "qcs-core/controller.h"


namespace Packuru::Core
{

class InternalPathCtrl : public qcs::core::Controller<QString>
{
    Q_DECLARE_TR_FUNCTIONS(InternalPathCtrl)
public:
    InternalPathCtrl(const QString& currentPath, const std::function<bool()>& customInternalPathSupport);

    void update() override;

private:
    void onValueSyncedToWidget(const QVariant& value) override;

    const std::function<bool()> customInternalPathSupport;
    const QLatin1String defaultPath;
    QString userPath;
};

}
