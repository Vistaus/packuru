// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QMetaType>


namespace Packuru::Core
{

enum class ArchivingDestinationMode
{
    CustomFolder,
    ItemParentFolder
};


QString toString(ArchivingDestinationMode value);

}

Q_DECLARE_METATYPE(Packuru::Core::ArchivingDestinationMode);
