// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "internalpathctrl.h"
#include "archivingdialogcontrollertype.h"
#include "core/private/plugin-api/archivingfilepaths.h"


using qcs::core::ControllerProperty;


namespace Packuru::Core
{

InternalPathCtrl::InternalPathCtrl(const QString& currentPath, const std::function<bool()>& customInternalPathSupport)
    : customInternalPathSupport(customInternalPathSupport),
      defaultPath(""),
      userPath(currentPath)
{
    /* Ensure that path suitable for backend is shown.
       Absolute archive paths inside archives have additional leading /
       e.g. /dir1/dir2/ in the archive is shown as //dir1/dir2/ in the browser.
       But backends will not understand paths from browser so leading /
       has to be removed. */
    if (userPath.startsWith(QLatin1Char('/')))
        userPath.remove(0, 1);

    setCurrentValue(userPath);

    properties[qcs::core::ControllerProperty::LabelText]
            = Packuru::Core::InternalPathCtrl::tr("Internal path");
}


void InternalPathCtrl::update()
{
    const auto filePaths = getControllerPropertyAs<ArchivingFilePaths>
            (ArchivingDialogControllerType::Type::FilePaths,
             ControllerProperty::PrivateCurrentValue);

    if (customInternalPathSupport()
            && (filePaths == ArchivingFilePaths::FullPaths || filePaths == ArchivingFilePaths::RelativePaths))
    {
        setEnabled(true);
        setCurrentValue(userPath);
    }
    else
    {
        setEnabled(false);
        setCurrentValue(defaultPath);
    }
}


void InternalPathCtrl::onValueSyncedToWidget(const QVariant& value)
{
    userPath = Utils::getValueAs<QString>(value);
}

}
