// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <functional>
#include <unordered_set>

#include <QString>
#include <QCoreApplication>

#include "qcs-core/controller.h"

#include "core/private/plugin-api/archivingfilepaths.h"


namespace Packuru::Core
{

class ArchivingFilePathsCtrl : public qcs::core::Controller<QString, ArchivingFilePaths>
{
    using GetFilePathsSupport = std::function<std::unordered_set<ArchivingFilePaths>()>;
    Q_DECLARE_TR_FUNCTIONS(ArchivingFilePathsCtrl)
public:
    ArchivingFilePathsCtrl(const GetFilePathsSupport& getFilePathsSupport);

    void update() override;

private:
    void onValueSyncedToWidget(const QVariant& value) override;

    GetFilePathsSupport getFilePathsSupport;

    ArchivingFilePaths userChoice;
};

}
