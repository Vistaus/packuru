// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QCoreApplication>

#include "destinationpathctrl.h"
#include "archivingdestinationmode.h"
#include "archivingdialogcontrollertype.h"


using qcs::core::ControllerProperty;


namespace Packuru::Core
{

DestinationPathCtrl::DestinationPathCtrl(const QString &initialPath)
    : automaticPathText(Packuru::Core::DestinationPathCtrl::tr("Automatic")),
      userSetValue(initialPath)
{
    properties[qcs::core::ControllerProperty::LabelText]
            = Packuru::Core::DestinationPathCtrl::tr("Destination path");
}


void DestinationPathCtrl::update()
{
    const auto destinationMode = getControllerPropertyAs<ArchivingDestinationMode>
            (ArchivingDialogControllerType::Type::DestinationMode,
             ControllerProperty::PrivateCurrentValue);

    switch (destinationMode)
    {
    case ArchivingDestinationMode::ItemParentFolder:
        setCurrentValue(automaticPathText);
        setEnabled(false);
        break;
    case ArchivingDestinationMode::CustomFolder:
        setCurrentValue(userSetValue);
        setEnabled(true);
        break;
    default:
        Q_ASSERT(false);
    }
}


void DestinationPathCtrl::onValueSyncedToWidget(const QVariant &value)
{
    userSetValue = Utils::getValueAs<QString>(value);
}

}
