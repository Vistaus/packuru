// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QString>
#include <QCoreApplication>

#include "archivingdestinationmode.h"


namespace Packuru::Core
{

QString toString(ArchivingDestinationMode value)
{
    switch (value)
    {
    case ArchivingDestinationMode::ItemParentFolder:
        return QCoreApplication::translate("ArchivingDestinationMode", "Item's parent folder");
    case ArchivingDestinationMode::CustomFolder:
        return QCoreApplication::translate("ArchivingDestinationMode", "Custom folder");
    default:
        Q_ASSERT(false); return QString();
    }

}

}
