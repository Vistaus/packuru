// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <functional>

#include <QCoreApplication>
#include <QString>

#include "qcs-core/controller.h"


namespace Packuru::Core
{

class ArchivingCompressionInfo;


class CompressionStateCtrl : public qcs::core::Controller<QString>
{
    Q_DECLARE_TR_FUNCTIONS(CompressionStateCtrl)
public:
    CompressionStateCtrl(std::function<ArchivingCompressionInfo()>&& getCompressionInfo);

    void update() override;

private:
    std::function<ArchivingCompressionInfo()> getCompressionInfo;
};

}
