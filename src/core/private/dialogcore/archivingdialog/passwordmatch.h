// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QMetaType>


namespace Packuru::Core
{

enum class PasswordMatch
{
    EmptyPasswords,
    NoMatch,
    Match,
    PartialMatch,

    ___INVALID
};

}

Q_DECLARE_METATYPE(Packuru::Core::PasswordMatch)
