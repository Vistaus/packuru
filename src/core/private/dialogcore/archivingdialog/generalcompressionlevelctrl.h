// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <functional>

#include <QCoreApplication>

#include "qcs-core/controller.h"


namespace Packuru::Core
{

class ArchivingCompressionInfo;


class GeneralCompressionLevelCtrl : public qcs::core::Controller<int>
{
    Q_DECLARE_TR_FUNCTIONS(GeneralCompressionLevelCtrl)

public:
    GeneralCompressionLevelCtrl(std::function<ArchivingCompressionInfo()>&& getCompressionInfo,
                                std::function<void(int)>&& setCompressionLevel);

    void update() override;
    void onValueSyncedToWidget(const QVariant& value) override;

    void onExternalCompressionInfoChange();

private:
    void updateCompressionInfo();

    std::function<ArchivingCompressionInfo()> getCompressionInfo;
    std::function<void(int)> setCompressionLevel;
};

}
