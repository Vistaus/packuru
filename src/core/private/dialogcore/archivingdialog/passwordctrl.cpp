// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "passwordctrl.h"


using qcs::core::ControllerProperty;


namespace Packuru::Core
{

PasswordCtrl::PasswordCtrl(const QString& label,
                           const QString& initPassword,
                           const GetEncryptionSupport& getEncryptionSupport)
    : getEncryptionSupport(getEncryptionSupport)
{
    setCurrentValue(initPassword);
    setVisible(initPassword.isEmpty());

    properties[qcs::core::ControllerProperty::LabelText] = label;
}


void PasswordCtrl::update()
{
    if (getEncryptionSupport() == EncryptionSupport::NoSupport)
    {
        if (temp.length() == 0)
        {
            temp = getPropertyAs<QString>(ControllerProperty::PublicCurrentValue);
            setCurrentValue("");
        }
        setEnabled(false);
    }
    else
    {
        if (temp.length() > 0)
        {
            setCurrentValue(temp);
            temp.clear();
        }
        setEnabled(true);
    }
}

}
