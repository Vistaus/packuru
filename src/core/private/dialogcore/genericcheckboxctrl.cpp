// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "genericcheckboxctrl.h"



namespace Packuru::Core
{

GenericCheckBoxCtrl::GenericCheckBoxCtrl(const QString& labelText, bool initValue)
{
    setCurrentValue(initValue);
    properties[qcs::core::ControllerProperty::LabelText] = labelText;
}

}
