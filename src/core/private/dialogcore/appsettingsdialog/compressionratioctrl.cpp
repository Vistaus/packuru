// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "compressionratioctrl.h"


namespace Packuru::Core
{

CompressionRatioCtrl::CompressionRatioCtrl(CompressionRatioMode ratioMode)
{
    setValueListsFromPrivate({CompressionRatioMode::OriginalToPacked, CompressionRatioMode::PackedToOriginal});
    setCurrentValueFromPrivate(ratioMode);

    properties[qcs::core::ControllerProperty::LabelText]
            = Packuru::Core::CompressionRatioCtrl::tr("Compression ratio display mode");
}

}
