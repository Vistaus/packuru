// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "sizeunitsctrl.h"


namespace Packuru::Core
{

SizeUnitsCtrl::SizeUnitsCtrl(SizeUnits units)
{
    setValueListsFromPrivate({SizeUnits::Decimal, SizeUnits::Binary});
    setCurrentValueFromPrivate(units);

    properties[qcs::core::ControllerProperty::LabelText]
            = Packuru::Core::SizeUnitsCtrl::tr("Size units");
}

}
