// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QCoreApplication>

#include "qcs-core/controller.h"

#include "private/sizeunits.h"


namespace Packuru::Core
{

class SizeUnitsCtrl : public qcs::core::Controller<QString, SizeUnits>
{
    Q_DECLARE_TR_FUNCTIONS(SizeUnitsCtrl)
public:
    SizeUnitsCtrl(SizeUnits units);
};

}
