// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <functional>

#include <QString>


namespace Packuru::Core
{

enum class ArchiveType;

using GetBackendForTypeCallback = std::function<QString(ArchiveType)>;

}
