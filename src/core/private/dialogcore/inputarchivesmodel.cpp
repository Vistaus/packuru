// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QMimeData>
#include <QCoreApplication>
#include <QIcon>

#include "inputarchivesmodel.h"


namespace Packuru::Core
{

namespace  {

enum class Column
{
    Name,
    Path,
    Type,
    Plugin,
    ___COUNT,
    ___INVALID
};


inline QString toString(Column value)
{
    switch (value)
    {
    case Column::Name: return Packuru::Core::InputArchivesModel::tr("Name");
    case Column::Path: return Packuru::Core::InputArchivesModel::tr("Path");
    case Column::Plugin: return Packuru::Core::InputArchivesModel::tr("Plugin");
    case Column::Type: return Packuru::Core::InputArchivesModel::tr("Type");
    default: Q_ASSERT(false); return QString();
    }
}

}


InputArchivesModel::InputArchivesModel(const GuessArchiveTypeCallback& guessArchiveType,
                                       const GetBackendForTypeCallback& getBackend,
                                       QObject *parent)
    : QAbstractTableModel(parent),
      guessArchiveType(guessArchiveType),
      getBackendForType(getBackend)
{
}


int InputArchivesModel::rowCount(const QModelIndex&) const
{
    return static_cast<int>(archives.size());
}


int InputArchivesModel::columnCount(const QModelIndex&) const
{
    return static_cast<int>(Column::___COUNT);
}


QVariant InputArchivesModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
        return QVariant{};

    const auto row = static_cast<std::size_t>(index.row());

    if (row < archives.size())
    {
        auto column = Column::___INVALID;
        const auto& archive = archives[row];

        if (index.column() >= 0  and index.column() < static_cast<int>(Column::___COUNT))
            column = static_cast<Column>(index.column());

        if (role == Qt::DisplayRole)
        {
            if (column == Column::Name)
                return archive.info.fileName();
            else if (column == Column::Path)
                return archive.info.absolutePath();
            else if (column == Column::Type)
                return toString(archive.type);
            else if (column == Column::Plugin)
                return archive.pluginName;
        }
        else if (role == Qt::DecorationRole)
        {
            switch (column)
            {

            case Column::Name:
            {
                auto icon = QIcon::fromTheme(archive.mediaType.iconName());

                if (icon.isNull())
                    icon = QIcon::fromTheme("application-octet-stream");

                return icon;
            }

            default:
                return QVariant();
            }
        }
    }

    return QVariant();
}


QVariant InputArchivesModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role!=Qt::DisplayRole or orientation == Qt::Vertical)
        return QVariant();

    auto data = Column::___INVALID;

    if (section >= 0  and section < static_cast<int>(Column::___COUNT))
        data = static_cast<Column>(section);

    if (data != Column::___INVALID)
        return toString(data);
    else
        return QVariant();
}


bool InputArchivesModel::removeRows(int row, int count, const QModelIndex& parent)
{
    Q_UNUSED(parent)
    Q_ASSERT(row >=0 && count >= 0);

    const auto stdRow = static_cast<std::size_t>(row);
    const auto stdCount = static_cast<std::size_t>(count);
    if (stdRow < archives.size() && stdRow + stdCount > stdRow and stdRow + stdCount <= archives.size())
    {
        const auto rangeBegin = archives.begin() + row;
        const auto rangeEnd = archives.begin() + row + count;

        beginRemoveRows(QModelIndex(), row, row + count - 1);

        for (auto it = rangeBegin; it != rangeEnd; ++it)
            fullPaths.erase(it->info.absoluteFilePath());

        archives.erase(rangeBegin, rangeEnd);

        endRemoveRows();

        if (archives.empty())
            emit modelEmpty(true);

        return true;
    }

    return false;
}


Qt::ItemFlags InputArchivesModel::flags(const QModelIndex& index) const
{
    return QAbstractItemModel::flags(index) | Qt::ItemIsDropEnabled;
}


bool InputArchivesModel::canDropMimeData(const QMimeData* data,
                                         Qt::DropAction action,
                                         int row, int column,
                                         const QModelIndex& parent) const
{
    Q_UNUSED(action)
    Q_UNUSED(row)
    Q_UNUSED(column)
    Q_UNUSED(parent)

    return data->hasUrls();
}


bool InputArchivesModel::dropMimeData(const QMimeData* data,
                                      Qt::DropAction action,
                                      int row,
                                      int column,
                                      const QModelIndex& parent)
{
    Q_UNUSED(action)
    Q_UNUSED(row)
    Q_UNUSED(column)
    Q_UNUSED(parent)

    if (data->hasUrls())
    {
        addArchives(data->urls());
        return true;
    }

    return false;
}


void InputArchivesModel::addArchives(const std::vector<QFileInfo>& list)
{
    QList<ArchiveInfo> newArchives;

    for (const auto& fileInfo : list)
    {
        if (fullPaths.find(fileInfo.absoluteFilePath()) == fullPaths.end())
        {
            if (fileInfo.isDir())
                continue;

            const ArchiveType type = guessArchiveType(fileInfo.absoluteFilePath());
            const auto mediaType = mimeDB.mimeTypeForFile(fileInfo.fileName(), QMimeDatabase::MatchExtension);
            const QString backendName = getBackendForType(type);

            newArchives.push_back(ArchiveInfo(fileInfo, type, mediaType, backendName));

            fullPaths.insert(fileInfo.absoluteFilePath());
        }
    }

    if (newArchives.empty())
        return;

    const int oldSize = static_cast<int>(archives.size());
    const int firstRow = oldSize;
    const int lastRow = oldSize + newArchives.size() - 1;

    beginInsertRows(QModelIndex{}, firstRow, lastRow);

    archives.insert(archives.end(), newArchives.cbegin(), newArchives.cend());

    endInsertRows();

    emit modelEmpty(archives.empty());
}


void InputArchivesModel::addArchives(const QList<QUrl>& list)
{
    std::vector<QFileInfo> infoList;
    infoList.reserve(list.size());
    for (const auto& url : qAsConst(list))
    {
        QFileInfo info(url.path());
        if (info.exists())
            infoList.push_back(info);
    }

    addArchives(infoList);
}


void InputArchivesModel::removeArchives(const QModelIndexList& indexes)
{
    auto sortedIndexes = indexes;
    // Remove rows starting from the end to avoid row shifting
    std::sort(sortedIndexes.begin(), sortedIndexes.end(),
              [](const auto& index1, const auto& index2) { return index1.row() > index2.row(); });

    for (const auto& i: qAsConst(sortedIndexes))
    {
        removeRows(i.row(), 1, QModelIndex());
    }
}


QString InputArchivesModel::getArchiveBackend(int row) const
{
    Q_ASSERT(row >= 0);
    const auto stdRow = static_cast<std::size_t>(row);
    if (stdRow < archives.size())
        return archives[stdRow].pluginName;
    else
        return QString{};
}


QString InputArchivesModel::getCommonParentFolder() const
{
    if (archives.empty())
        return QString();

    QString currentParent = archives.front().info.absolutePath();

    for (const auto& archive : archives)
    {
        QString archiveParent = archive.info.absolutePath();

        int shorterLength = archiveParent.length() < currentParent.length() ? archiveParent.length() : currentParent.length();

        QString newParent;

        for (int i = 0; i < shorterLength; ++i)
        {
            if (currentParent[i] == archiveParent[i])
                newParent += currentParent[i];
            else
                break;
        }

        currentParent = newParent;
    }

    return currentParent;
}

}
