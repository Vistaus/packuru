// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QUrl>

#include "extractiondestinationerrorctrl.h"
#include "extractiondestinationmode.h"
#include "extractiondialogcontrollertype.h"


using qcs::core::ControllerProperty;


namespace Packuru::Core
{

ExtractionDestinationErrorCtrl::ExtractionDestinationErrorCtrl()
{

}


void ExtractionDestinationErrorCtrl::update()
{
    const auto destinationMode = getControllerPropertyAs<ExtractionDestinationMode>
            (ExtractionDialogControllerType::Type::DestinationMode,
             ControllerProperty::PrivateCurrentValue);

    if (destinationMode != ExtractionDestinationMode::CustomFolder)
    {
        setVisible(false);
        return;
    }

    const auto path = getControllerPropertyAs<QString>
            (ExtractionDialogControllerType::Type::DestinationPath,
             ControllerProperty::PublicCurrentValue);

    QString error;
    if (path.isEmpty())
        error = Packuru::Core::ExtractionDestinationErrorCtrl::tr("Destination path cannot be empty");

    setCurrentValue(error);
    setVisible(!error.isEmpty());
}

}
