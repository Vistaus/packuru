// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <vector>
#include <functional>

#include <QFileInfo>
#include <QString>

#include "core/private/plugin-api/archivetype.h"
#include "core/private/plugin-api/encryptionstate.h"


namespace Packuru::Core
{

struct ExtractionTaskData;

struct ExtractionDialogCoreInitSingle
{
    using OnAcceptedCallback = std::function<void(const ExtractionTaskData&)>;

    bool isValid() const;

    QFileInfo archiveInfo;
    ArchiveType archiveType = ArchiveType::___NOT_SUPPORTED;
    EncryptionState archiveEncryption = EncryptionState::___INVALID;
    QString password;
    std::vector<QString> selectedFiles;
    OnAcceptedCallback onAccepted;
};

}
