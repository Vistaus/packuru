// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QMetaType>


namespace Packuru::Core
{

enum class ExtractionDestinationMode
{
    ArchiveParentFolder,
    CustomFolder,

    ___COUNT
};

QString toString(ExtractionDestinationMode value);

}

Q_DECLARE_METATYPE(Packuru::Core::ExtractionDestinationMode)
