// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "fileextractionmodectrl.h"


namespace Packuru::Core
{

FileExtractionModeCtrl::FileExtractionModeCtrl(bool areFilesSelected)
{
    setValueListsFromPrivate({FileExtractionMode::SelectedFilesOnly,
                     FileExtractionMode::AllFiles});

    if (areFilesSelected)
        setCurrentValueFromPrivate(FileExtractionMode::SelectedFilesOnly);
    else
    {
        setCurrentValueFromPrivate(FileExtractionMode::AllFiles);
        setEnabled(false);
    }

    properties[qcs::core::ControllerProperty::LabelText]
            = Packuru::Core::FileExtractionModeCtrl::tr("Extract files");
}

}
