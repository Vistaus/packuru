// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QCoreApplication>

#include "qcs-core/controller.h"

#include "core/private/fileextractionmode.h"


namespace Packuru::Core
{

class FileExtractionModeCtrl : public qcs::core::Controller<QString, FileExtractionMode>
{
    Q_DECLARE_TR_FUNCTIONS(FileExtractionModeCtrl)
public:
    FileExtractionModeCtrl(bool areFilesSelected);
};

}
