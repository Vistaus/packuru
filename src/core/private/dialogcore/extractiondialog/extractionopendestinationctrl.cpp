// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "extractionopendestinationctrl.h"


using qcs::core::ControllerProperty;


namespace Packuru::Core
{

ExtractionOpenDestinationCtrl::ExtractionOpenDestinationCtrl()
{
    setCurrentValue(false);
    properties[ControllerProperty::Text] =
            Packuru::Core::ExtractionOpenDestinationCtrl::tr("Open destination folder on completion");
}

}
