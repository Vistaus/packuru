// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "extractiondestinationpathctrl.h"
#include "extractiondestinationmode.h"
#include "extractiondialogcontrollertype.h"


using qcs::core::ControllerProperty;


namespace Packuru::Core
{

ExtractionDestinationPathCtrl::ExtractionDestinationPathCtrl(const QString& initialPath)
    : automaticPathText(Packuru::Core::ExtractionDestinationPathCtrl::tr("Automatic"))
{
    tempPath = initialPath;
    properties[qcs::core::ControllerProperty::LabelText]
            = Packuru::Core::ExtractionDestinationPathCtrl::tr("Destination path");
}


void ExtractionDestinationPathCtrl::update()
{   
    const auto destinationMode = getControllerPropertyAs<ExtractionDestinationMode>
            (ExtractionDialogControllerType::Type::DestinationMode,
             ControllerProperty::PrivateCurrentValue);

    if (destinationMode == ExtractionDestinationMode::ArchiveParentFolder)
    {
        setEnabled(false);
        setCurrentValue(automaticPathText);
    }
    else if (destinationMode == ExtractionDestinationMode::CustomFolder)
    {
        setEnabled(true);
        setCurrentValue(tempPath);
    }
}


void ExtractionDestinationPathCtrl::onValueSyncedToWidget(const QVariant& value)
{
    tempPath = Utils::getValueAs<QString>(value);
}

}
