// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QString>
#include <QCoreApplication>

#include "extractiondestinationmode.h"


namespace Packuru::Core
{

QString toString(ExtractionDestinationMode value)
{
    switch (value)
    {
    case ExtractionDestinationMode::ArchiveParentFolder:
        return QCoreApplication::translate("ExtractionDestinationMode", "Archive's parent folder");
    case ExtractionDestinationMode::CustomFolder:
        return QCoreApplication::translate("ExtractionDestinationMode", "Custom folder");
    default:
        Q_ASSERT(false); return QString();
    }
}

}
