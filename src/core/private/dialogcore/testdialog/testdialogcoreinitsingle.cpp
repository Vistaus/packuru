// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "testdialogcoreinitsingle.h"


namespace Packuru::Core
{

bool TestDialogCoreInitSingle::isValid() const
{
    return !archiveInfo.absoluteFilePath().isEmpty()
            && Packuru::Core::isValid(archiveType)
            && archiveEncryption != EncryptionState::___INVALID
            && !(archiveEncryption == EncryptionState::EntireArchive && password.isEmpty())
            && onAccepted;
}

}
