// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <vector>

#include <QString>
#include <QFileInfo>

#include "core/private/dialogcore/guessarchivetypecallback.h"
#include "core/private/dialogcore/getbackendfortypecallback.h"


namespace Packuru::Core
{

struct TestTaskData;

struct TestDialogCoreInitMulti
{
    using OnAcceptedCallback = std::function<void(const std::vector<TestTaskData>&)>;

    bool isValid() const;

    GuessArchiveTypeCallback guessArchiveType;
    GetBackendForTypeCallback getBackend;
    std::vector<QFileInfo> archives;
    OnAcceptedCallback onAccepted;
};

}
