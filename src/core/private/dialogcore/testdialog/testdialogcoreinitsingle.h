// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <vector>

#include <QString>
#include <QFileInfo>

#include "core/private/plugin-api/archivetype.h"
#include "core/private/plugin-api/encryptionstate.h"


namespace Packuru::Core
{

struct TestTaskData;

struct TestDialogCoreInitSingle
{
    using OnAcceptedCallback = std::function<void(const TestTaskData&)>;

    bool isValid() const;

    QFileInfo archiveInfo;
    ArchiveType archiveType = ArchiveType::___NOT_SUPPORTED;
    EncryptionState archiveEncryption = EncryptionState::___INVALID;
    QString password;
    OnAcceptedCallback onAccepted;

};

}
