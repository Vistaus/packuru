// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <vector>

#include "qcs-core/controller.h"
#include "qcs-core/controllerproperty.h"

#include "private/privatestrings.h"


namespace Packuru::Core
{

template <typename ControllerId>
class GenericDialogErrorsCtrl : public qcs::core::Controller<QString, bool>
{
public:
    GenericDialogErrorsCtrl(bool fileListEmpty,
                            const std::vector<ControllerId>& errorControllers);

    void setFileListEmpty(bool empty);

private:
    void update() override;

    QString join(const QStringList& list);

    bool fileListEmpty_;
    std::vector<ControllerId> errorControllers_;
};


template<typename ControllerId>
GenericDialogErrorsCtrl<ControllerId>::GenericDialogErrorsCtrl(bool fileListEmpty,
                                                               const std::vector<ControllerId>& errorControllers)
    : fileListEmpty_(fileListEmpty),
      errorControllers_(errorControllers)
{
    setCurrentValue("", false);
}


template<typename ControllerId>
void GenericDialogErrorsCtrl<ControllerId>::setFileListEmpty(bool empty)
{
    fileListEmpty_ = empty;
    update();
    externalValueInput();
}


template<typename ControllerId>
void GenericDialogErrorsCtrl<ControllerId>::update()
{
    QStringList errorList;

    if (fileListEmpty_)
        errorList << PrivateStrings::getString(PrivateStrings::UserString::DialogFileListEmpty);

    for (const auto id : errorControllers_)
        errorList << getControllerPropertyAs<QString>(id,
                                                      qcs::core::ControllerProperty::PublicCurrentValue);

    const QString errorsStr = join(errorList);

    properties[qcs::core::ControllerProperty::ToolTip] = errorsStr;
    properties[qcs::core::ControllerProperty::PrivateCurrentValue] = !errorsStr.isEmpty();
    setVisible(!errorsStr.isEmpty());
}


template<typename ControllerId>
QString GenericDialogErrorsCtrl<ControllerId>::join(const QStringList& list)
{
    QString result;

    for (const auto& s : list)
    {
        if (!s.isEmpty())
            result += s + QLatin1Char('\n');
    }

    result.chop(1);

    return result;
}

}
