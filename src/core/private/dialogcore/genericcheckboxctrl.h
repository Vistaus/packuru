// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "qcs-core/controller.h"
#include "qcs-core/controllerproperty.h"


namespace Packuru::Core
{

class GenericCheckBoxCtrl : public qcs::core::Controller<bool>
{
public:
    GenericCheckBoxCtrl(const QString& labelText, bool initValue);
};

}
