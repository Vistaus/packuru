// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "utils/qvariant_utils.h"

#include "coresettingsconverter.h"
#include "private/globalsettings.h"
#include "sizeunits.h"
#include "compressionratiomode.h"


namespace Packuru::Core
{

CoreSettingsConverter::CoreSettingsConverter()
{

}


std::unordered_set<int> CoreSettingsConverter::getSupportedKeys() const
{
    return
    {
        toInt(GlobalSettings::Enum::AC_CompressionRatioMode),
        toInt(GlobalSettings::Enum::AC_SizeUnits)
    };
}


QString CoreSettingsConverter::getKeyName(int key) const
{
    const auto keyId = static_cast<GlobalSettings::Enum>(key);
    switch (keyId)
    {
    case GlobalSettings::Enum::AC_CompressionRatioMode:
        return QLatin1String("ac_compression_ratio_mode");
    case GlobalSettings::Enum::AC_SizeUnits:
        return QLatin1String("ac_size_units");
    default:
        Q_ASSERT(false); return QLatin1String();
    }
}


QVariant CoreSettingsConverter::toSettingsType(int key, const QVariant& value) const
{
    QVariant result;

    const auto keyId = static_cast<GlobalSettings::Enum>(key);
    switch (keyId)
    {
    case GlobalSettings::Enum::AC_SizeUnits:
    {
        const auto v = Utils::getValueAs<SizeUnits>(value);
        result = toSettingsType(v);
        break;
    }
    case GlobalSettings::Enum::AC_CompressionRatioMode:
    {
        const auto v = Utils::getValueAs<CompressionRatioMode>(value);
        result = toSettingsType(v);
        break;
    }
    default:
        Q_ASSERT(false);
        break;
    }

    return result;
}


QVariant CoreSettingsConverter::fromSettingsType(int key, const QVariant& value) const
{
    const auto keyId = static_cast<GlobalSettings::Enum>(key);
    switch (keyId)
    {
    case GlobalSettings::Enum::AC_SizeUnits:
    {
        SizeUnits units;
        fromSettingsType(value.toString(), units);
        if (units == SizeUnits::___INVALID)
            units = SizeUnits::Decimal;
        return QVariant::fromValue(units);
    }
    case GlobalSettings::Enum::AC_CompressionRatioMode:
    {
        CompressionRatioMode ratioMode;
        fromSettingsType(value.toString(), ratioMode);
        if (ratioMode == CompressionRatioMode::___INVALID)
            ratioMode = CompressionRatioMode::PackedToOriginal;
        return QVariant::fromValue(ratioMode);
    }
    default:
        Q_ASSERT(false);
        return QVariant();
    }
}


QString CoreSettingsConverter::toSettingsType(SizeUnits value) const
{
    switch (value)
    {
    case SizeUnits::Binary: return "binary";
    case SizeUnits::Decimal: return "decimal";
    default: Q_ASSERT(false); return "";
    }
}


void CoreSettingsConverter::fromSettingsType(const QString& key, SizeUnits& out) const
{
    if (key == QLatin1String("binary"))
        out = SizeUnits::Binary;
    else if (key == QLatin1String("decimal"))
        out = SizeUnits::Decimal;
    else
        out = SizeUnits::___INVALID;
}


QString CoreSettingsConverter::toSettingsType(CompressionRatioMode value) const
{
    switch(value)
    {
    case CompressionRatioMode::OriginalToPacked: return "original_to_packed";
    case CompressionRatioMode::PackedToOriginal: return "packed_to_original";
    default: Q_ASSERT(false); return "";
    }
}


void CoreSettingsConverter::fromSettingsType(const QString& key, CompressionRatioMode& out) const
{
    if (key == "original_to_packed")
        out =  CompressionRatioMode::OriginalToPacked;
    else if (key == "packed_to_original")
        out = CompressionRatioMode::PackedToOriginal;
    else
        out = CompressionRatioMode::___INVALID;
}

}
