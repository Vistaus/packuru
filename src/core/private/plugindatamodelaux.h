// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <vector>


namespace Packuru::Core
{

struct PluginData;

namespace PluginDataModelAux
{

struct Init
{
    std::vector<PluginData> dataList;
};

}

}
