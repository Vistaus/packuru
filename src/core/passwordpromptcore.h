// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QObject>

#include "symbol_export.h"

/// @file

namespace Packuru::Core
{

namespace PasswordPromptCoreAux
{
struct Init;
struct Backdoor;
}


/**
 @brief Contains data and logic necessary for password dialog.

 Object of this class is emitted by StatusPageCore and TaskQueueModel objects
 and will be automatically destroyed after user action (enterPassword() or abortTask())
 has been received.

 @warning The object must not be destroyed manually.

 @headerfile "core/passwordpromptcore.h"
 */
class PACKURU_CORE_EXPORT PasswordPromptCore : public QObject
{
    Q_OBJECT
public:
    /// @copydoc ArchivingDialogCore::UserString
    enum class UserString
    {
        ___INVALID, // Keep at index 0 to catch implicit conversions of invalid values to 0 in QML

        Abort, ///< 'Abort' button.
        DialogTitle ///< Dialog title.
    };
    Q_ENUM(UserString)

    explicit PasswordPromptCore(const PasswordPromptCoreAux::Init& init,
                                PasswordPromptCoreAux::Backdoor& backdoor,
                                QObject* parent = nullptr);
    ~PasswordPromptCore();

    /// @copydoc ArchivingDialogCore::getString
    Q_INVOKABLE static QString getString(UserString value);

    /// @brief Returns archive name.
    Q_INVOKABLE QString getArchiveName() const;

    ///@{
    /// @name Actions
    /// @warning Calling Actions will also automatically destroy the object.

    /** @brief Enters password.
    * @warning The password must not be empty.
    */
    Q_INVOKABLE void enterPassword(const QString& value);

    /// @copydoc CreationNameErrorPromptCore::abort()
    Q_INVOKABLE void abort();

    ///@}

signals:
    /// @brief Required for QML.
    void deleted();

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}
