# Packuru

<!-- + App version will be added to the first line at run-time + -->

**Archive manager**

__version__

<br />

<table cellspacing="5">
    <tr>
        <td align="right">Homepage</td>
        <td><a href="https://gitlab.com/kyeastmood/packuru">gitlab.com/kyeastmood/packuru</a></td>
    </tr>
    <tr>
        <td align="right">Copyright</td>
        <td>The Packuru Developers</td>
    </tr>
    <tr>
        <td align="right">License</td>
        <td>BSD-2-Clause</td>
    </tr>
</table>

---

## 3rd-party code

<br />

### CsLibGuarded

<table cellspacing="5">
    <tr>
        <td align="right">Homepage</td>
        <td><a href="https://github.com/copperspice/cs_libguarded">github.com/copperspice/cs_libguarded</a></td>
    </tr>
    <tr>
        <td align="right">Copyright</td>
        <td>Ansel Sermersheim</td>
    </tr>
    <tr>
        <td align="right">License</td>
        <td>BSD-2-Clause</td>
    </tr>
</table>

<br />

### KDSingleApplication

<table cellspacing="5">
    <tr>
        <td align="right">Homepage</td>
        <td><a href="https://github.com/KDAB/KDSingleApplication">github.com/KDAB/KDSingleApplication</a></td>
    </tr>
    <tr>
        <td align="right">Copyright</td>
        <td>Klarälvdalens Datakonsult AB</td>
    </tr>
    <tr>
        <td align="right">License</td>
        <td>MIT</td>
    </tr>
</table>
