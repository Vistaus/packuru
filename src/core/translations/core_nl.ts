<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl">
<context>
    <name>ArchiveNameCtrl</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/archivenamectrl.cpp" line="17"/>
        <source>Automatic</source>
        <translation>Automatisch</translation>
    </message>
    <message>
        <location filename="../private/dialogcore/archivingdialog/archivenamectrl.cpp" line="21"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
</context>
<context>
    <name>ArchiveNameErrorCtrl</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/archivenameerrorctrl.cpp" line="17"/>
        <source>Archive name cannot be empty</source>
        <translation>Geef het archief een naam</translation>
    </message>
</context>
<context>
    <name>ArchiveTypeCtrl</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/archivetypectrl.cpp" line="21"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
</context>
<context>
    <name>ArchivingDestinationMode</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/archivingdestinationmode.cpp" line="19"/>
        <source>Item&apos;s parent folder</source>
        <translation>Bovenliggende map</translation>
    </message>
    <message>
        <location filename="../private/dialogcore/archivingdialog/archivingdestinationmode.cpp" line="21"/>
        <source>Custom folder</source>
        <translation>Aangepaste map</translation>
    </message>
</context>
<context>
    <name>ArchivingFilePaths</name>
    <message>
        <location filename="../private/plugin-api/archivingfilepaths.cpp" line="18"/>
        <source>Absolute paths</source>
        <translation>Directe paden</translation>
    </message>
    <message>
        <location filename="../private/plugin-api/archivingfilepaths.cpp" line="20"/>
        <source>Full paths</source>
        <translation>Volledige paden</translation>
    </message>
    <message>
        <location filename="../private/plugin-api/archivingfilepaths.cpp" line="22"/>
        <source>Relative paths</source>
        <translation>Relatieve paden</translation>
    </message>
</context>
<context>
    <name>ArchivingMode</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/archivingmode.cpp" line="19"/>
        <source>Separate archive for each item</source>
        <translation>Eén archief per item</translation>
    </message>
    <message>
        <location filename="../private/dialogcore/archivingdialog/archivingmode.cpp" line="21"/>
        <source>Single archive</source>
        <translation>Eén archief</translation>
    </message>
</context>
<context>
    <name>ArchivingOpenDestinationCtrl</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/archivingopendestinationctrl.cpp" line="18"/>
        <source>Open destination folder</source>
        <translation>Bestemmingsmap openen</translation>
    </message>
</context>
<context>
    <name>CommandLineHandlerBase</name>
    <message>
        <location filename="../private/commandlinehandlerbase.cpp" line="18"/>
        <source>urls</source>
        <translation>url&apos;s</translation>
    </message>
    <message>
        <location filename="../private/commandlinehandlerbase.cpp" line="19"/>
        <source>URLs to open</source>
        <translation>De te openen url&apos;s</translation>
    </message>
    <message>
        <location filename="../private/commandlinehandlerbase.cpp" line="20"/>
        <source>[urls...]</source>
        <translation>[urls…]</translation>
    </message>
</context>
<context>
    <name>CompressionRatioCtrl</name>
    <message>
        <location filename="../private/dialogcore/appsettingsdialog/compressionratioctrl.cpp" line="17"/>
        <source>Compression ratio display mode</source>
        <translation>Weergave van compressieverhouding</translation>
    </message>
</context>
<context>
    <name>CompressionRatioMode</name>
    <message>
        <location filename="../private/compressionratiomode.cpp" line="19"/>
        <source>Original / packed</source>
        <translation>Oorspronkelijk/Ingepakt</translation>
    </message>
    <message>
        <location filename="../private/compressionratiomode.cpp" line="21"/>
        <source>Packed / original</source>
        <translation>Ingepakt/Oorspronkelijk</translation>
    </message>
</context>
<context>
    <name>CompressionStateCtrl</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/compressionstatectrl.cpp" line="16"/>
        <source>State</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../private/dialogcore/archivingdialog/compressionstatectrl.cpp" line="27"/>
        <source>Disabled</source>
        <translation>Uitgeschakeld</translation>
    </message>
    <message>
        <location filename="../private/dialogcore/archivingdialog/compressionstatectrl.cpp" line="30"/>
        <source>Enabled</source>
        <translation>Ingeschakeld</translation>
    </message>
    <message numerus="yes">
        <location filename="../private/dialogcore/archivingdialog/compressionstatectrl.cpp" line="36"/>
        <source>level %n</source>
        <translation>
            <numerusform>niveau %n</numerusform>
            <numerusform>niveau %n</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>DestinationModeCtrl</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/destinationmodectrl.cpp" line="25"/>
        <source>Create in</source>
        <translation>Aanmaken in</translation>
    </message>
</context>
<context>
    <name>DestinationPathCtrl</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/destinationpathctrl.cpp" line="19"/>
        <source>Automatic</source>
        <translation>Automatisch</translation>
    </message>
    <message>
        <location filename="../private/dialogcore/archivingdialog/destinationpathctrl.cpp" line="23"/>
        <source>Destination path</source>
        <translation>Bestemming</translation>
    </message>
</context>
<context>
    <name>DirectoryErrorCtrl</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/directoryerrorctrl.cpp" line="19"/>
        <source>Destination path cannot be empty</source>
        <translation>Voer een bestemming in</translation>
    </message>
</context>
<context>
    <name>EncryptionModeCtrl</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/encryptionmodectrl.cpp" line="22"/>
        <source>Encrypt entire archive</source>
        <translation>Archief versleutelen</translation>
    </message>
    <message>
        <location filename="../private/dialogcore/archivingdialog/encryptionmodectrl.cpp" line="23"/>
        <source>Encrypt files content only</source>
        <translation>Alleen bestandsinhoud versleutelen</translation>
    </message>
</context>
<context>
    <name>EncryptionState</name>
    <message>
        <location filename="../private/plugin-api/encryptionstate.cpp" line="19"/>
        <source>Disabled</source>
        <translation>Uitgeschakeld</translation>
    </message>
    <message>
        <location filename="../private/plugin-api/encryptionstate.cpp" line="21"/>
        <source>Enabled (entire archive)</source>
        <translation>Ingeschakeld (gehele archief)</translation>
    </message>
    <message>
        <location filename="../private/plugin-api/encryptionstate.cpp" line="23"/>
        <source>Enabled (files content only)</source>
        <translation>Ingeschakeld (alleen bestandsinhoud)</translation>
    </message>
</context>
<context>
    <name>EncryptionStateCtrl</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/encryptionstatectrl.cpp" line="24"/>
        <source>State</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../private/dialogcore/archivingdialog/encryptionstatectrl.cpp" line="56"/>
        <source>Disabled</source>
        <translation>Uitgeschakeld</translation>
    </message>
    <message>
        <location filename="../private/dialogcore/archivingdialog/encryptionstatectrl.cpp" line="83"/>
        <source>Enabled</source>
        <translation>Ingeschakeld</translation>
    </message>
</context>
<context>
    <name>ExtractionDestinationErrorCtrl</name>
    <message>
        <location filename="../private/dialogcore/extractiondialog/extractiondestinationerrorctrl.cpp" line="42"/>
        <source>Destination path cannot be empty</source>
        <translation>Voer een bestemming in</translation>
    </message>
</context>
<context>
    <name>ExtractionDestinationMode</name>
    <message>
        <location filename="../private/dialogcore/extractiondialog/extractiondestinationmode.cpp" line="19"/>
        <source>Archive&apos;s parent folder</source>
        <translation>Bovenliggende map</translation>
    </message>
    <message>
        <location filename="../private/dialogcore/extractiondialog/extractiondestinationmode.cpp" line="21"/>
        <source>Custom folder</source>
        <translation>Aangepaste map</translation>
    </message>
</context>
<context>
    <name>ExtractionDestinationModeCtrl</name>
    <message>
        <location filename="../private/dialogcore/extractiondialog/extractiondestinationmodectrl.cpp" line="25"/>
        <source>Extract to</source>
        <translation>Uitpakken naar</translation>
    </message>
</context>
<context>
    <name>ExtractionDestinationPathCtrl</name>
    <message>
        <location filename="../private/dialogcore/extractiondialog/extractiondestinationpathctrl.cpp" line="17"/>
        <source>Automatic</source>
        <translation>Automatisch</translation>
    </message>
    <message>
        <location filename="../private/dialogcore/extractiondialog/extractiondestinationpathctrl.cpp" line="21"/>
        <source>Destination path</source>
        <translation>Bestemming</translation>
    </message>
</context>
<context>
    <name>ExtractionFileOverwriteCtrl</name>
    <message>
        <location filename="../private/dialogcore/extractiondialog/extractionfileoverwritectrl.cpp" line="21"/>
        <source>File overwriting</source>
        <translation>Bestanden overschrijven</translation>
    </message>
</context>
<context>
    <name>ExtractionFileOverwriteMode</name>
    <message>
        <location filename="../private/extractionfileoverwritemode.cpp" line="19"/>
        <source>Ask before overwrite</source>
        <translation>Om actie vragen</translation>
    </message>
    <message>
        <location filename="../private/extractionfileoverwritemode.cpp" line="21"/>
        <source>Overwrite existing</source>
        <translation>Bestaande bestanden overschrijven</translation>
    </message>
    <message>
        <location filename="../private/extractionfileoverwritemode.cpp" line="23"/>
        <source>Skip existing</source>
        <translation>Bestaande bestanden overslaan</translation>
    </message>
    <message>
        <location filename="../private/extractionfileoverwritemode.cpp" line="25"/>
        <source>Update existing</source>
        <translation>Bestaande bestanden bijwerken</translation>
    </message>
</context>
<context>
    <name>ExtractionFolderOverwriteCtrl</name>
    <message>
        <location filename="../private/dialogcore/extractiondialog/extractionfolderoverwritectrl.cpp" line="20"/>
        <source>Folder overwriting</source>
        <translation>Mappen overschrijven</translation>
    </message>
</context>
<context>
    <name>ExtractionFolderOverwriteMode</name>
    <message>
        <location filename="../private/extractionfolderoverwritemode.cpp" line="19"/>
        <source>Ask before writing into</source>
        <translation>Om actie vragen</translation>
    </message>
    <message>
        <location filename="../private/extractionfolderoverwritemode.cpp" line="21"/>
        <source>Write into existing</source>
        <translation>Naar bestaande map uitpakken</translation>
    </message>
    <message>
        <location filename="../private/extractionfolderoverwritemode.cpp" line="23"/>
        <source>Skip existing</source>
        <translation>Bestaande map overslaan</translation>
    </message>
</context>
<context>
    <name>ExtractionOpenDestinationCtrl</name>
    <message>
        <location filename="../private/dialogcore/extractiondialog/extractionopendestinationctrl.cpp" line="18"/>
        <source>Open destination folder on completion</source>
        <translation>Bestemmingsmap openen na afronden</translation>
    </message>
</context>
<context>
    <name>ExtractionTopFolderMode</name>
    <message>
        <location filename="../private/extractiontopfoldermode.cpp" line="19"/>
        <source>Create top folder</source>
        <translation>Hoofdmap aanmaken</translation>
    </message>
    <message>
        <location filename="../private/extractiontopfoldermode.cpp" line="21"/>
        <source>Create if absent in the archive</source>
        <translation>Aanmaken indien niet aanwezig</translation>
    </message>
    <message>
        <location filename="../private/extractiontopfoldermode.cpp" line="23"/>
        <source>Do not create top folder</source>
        <translation>Niet aanmaken</translation>
    </message>
</context>
<context>
    <name>FileExtractionMode</name>
    <message>
        <location filename="../private/fileextractionmode.cpp" line="20"/>
        <source>All</source>
        <translation>Alles</translation>
    </message>
    <message>
        <location filename="../private/fileextractionmode.cpp" line="22"/>
        <source>Selected only</source>
        <translation>Alleen selectie</translation>
    </message>
</context>
<context>
    <name>FileExtractionModeCtrl</name>
    <message>
        <location filename="../private/dialogcore/extractiondialog/fileextractionmodectrl.cpp" line="25"/>
        <source>Extract files</source>
        <translation>Bestanden uitpakken</translation>
    </message>
</context>
<context>
    <name>GeneralCompressionLevelCtrl</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/generalcompressionlevelctrl.cpp" line="28"/>
        <source>Level</source>
        <translation>Niveau</translation>
    </message>
</context>
<context>
    <name>InternalPathCtrl</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/internalpathctrl.cpp" line="32"/>
        <source>Internal path</source>
        <translation>Interne locatie</translation>
    </message>
</context>
<context>
    <name>OpenNewArchiveCtrl</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/opennewarchivectrl.cpp" line="17"/>
        <source>Open archive</source>
        <translation>Archief openen</translation>
    </message>
</context>
<context>
    <name>Packuru::Core::AbstractBackendHandler</name>
    <message>
        <location filename="../private/plugin-api/abstractbackendhandler.cpp" line="98"/>
        <source>Temporary folder not found: </source>
        <translation>De tijdelijke map is niet aangetroffen: </translation>
    </message>
    <message>
        <location filename="../private/plugin-api/abstractbackendhandler.cpp" line="105"/>
        <source>Temporary folder is not empty: </source>
        <translation>De tijdelijke map is niet leeg: </translation>
    </message>
</context>
<context>
    <name>Packuru::Core::AppClosingDialogCore</name>
    <message>
        <location filename="../appclosingdialogcore.cpp" line="29"/>
        <source>Confirm quit</source>
        <translation>Om bevestiging vragen bij afsluiten</translation>
    </message>
    <message>
        <location filename="../appclosingdialogcore.cpp" line="35"/>
        <source>Do you want to quit?</source>
        <translation>Weet je zeker dat je wilt afsluiten?</translation>
    </message>
    <message>
        <location filename="../appclosingdialogcore.cpp" line="37"/>
        <source>Quit</source>
        <translation>Afsluiten</translation>
    </message>
    <message numerus="yes">
        <location filename="../appclosingdialogcore.cpp" line="25"/>
        <source>There are %n busy tabs.</source>
        <translation>
            <numerusform>Er is %n actief tabblad.</numerusform>
            <numerusform>Er zijn %n actieve tabbladen.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../appclosingdialogcore.cpp" line="31"/>
        <source>%n opened dialog windows will be closed.</source>
        <translation>
            <numerusform>%n geopend dialoogvenster wordt gesloten.</numerusform>
            <numerusform>%n geopende dialoogvensters worden gesloten.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../appclosingdialogcore.cpp" line="33"/>
        <source>%n unfinished tasks will be aborted.</source>
        <translation>
            <numerusform>%n onafgeronde taak wordt afgebroken.</numerusform>
            <numerusform>%n onafgeronde taken worden afgebroken.</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>Packuru::Core::AppSettingsDialogCore</name>
    <message>
        <location filename="../appsettingsdialogcore.cpp" line="77"/>
        <source>General</source>
        <translation>Algemeen</translation>
    </message>
    <message>
        <location filename="../appsettingsdialogcore.cpp" line="80"/>
        <source>Plugins</source>
        <translation>Plug-ins</translation>
    </message>
    <message>
        <location filename="../appsettingsdialogcore.cpp" line="83"/>
        <source>Archive types</source>
        <translation>Archiefsoorten</translation>
    </message>
    <message>
        <location filename="../appsettingsdialogcore.cpp" line="92"/>
        <source>Backend homepage</source>
        <translation>Website van backend</translation>
    </message>
    <message>
        <location filename="../appsettingsdialogcore.cpp" line="96"/>
        <source>Please note that each plugin can interpret file names differently (e.g. because of the level of support for different text encodings).

It is therefore recommended to select the same plugin for read and write operations.

Moreover, after changing plugin configuration for a particular type, it is advised to reload all archives of this type and discard all extraction and deletion tasks for this type that reference selected file names.</source>
        <translation>Let op: elke plug-in kan bestandsnament anders interpreteren. Dit komt door het niveau van ondersteuning voor de verschillende tekensets.

Het is daarom aanbevolen om dezelfde plug-in voor zowel uitlezen als wegschrijven te gebruiken.

En na het aanpassen van de plug-in-instellingen, is het aanbevolen om alle archieven van dat type te herladen en alle taken van dat type te verwijderen.</translation>
    </message>
    <message>
        <location filename="../appsettingsdialogcore.cpp" line="105"/>
        <source>Description</source>
        <comment>Plugin description</comment>
        <translation>Beschrijving</translation>
    </message>
    <message>
        <location filename="../appsettingsdialogcore.cpp" line="108"/>
        <source>File path</source>
        <translation>Bestandslocatie</translation>
    </message>
    <message>
        <location filename="../appsettingsdialogcore.cpp" line="111"/>
        <source>Reads</source>
        <comment>Which types the plugin reads</comment>
        <translation>Leest uit:</translation>
    </message>
    <message>
        <location filename="../appsettingsdialogcore.cpp" line="114"/>
        <source>Writes</source>
        <comment>Which types the plugin writes</comment>
        <translation>Schrijft weg naar:</translation>
    </message>
    <message>
        <location filename="../appsettingsdialogcore.cpp" line="117"/>
        <source>Read warning</source>
        <translation>Leeswaarschuwing</translation>
    </message>
</context>
<context>
    <name>Packuru::Core::ArchiveTypeModel</name>
    <message>
        <location filename="../archivetypemodel.cpp" line="63"/>
        <source>Archive type</source>
        <translation>Archiefsoort</translation>
    </message>
    <message>
        <location filename="../archivetypemodel.cpp" line="64"/>
        <source>Read plugin</source>
        <translation>Uitleesplug-in</translation>
    </message>
    <message>
        <location filename="../archivetypemodel.cpp" line="65"/>
        <source>Write plugin</source>
        <translation>Wegschrijfplug-in</translation>
    </message>
</context>
<context>
    <name>Packuru::Core::ArchivingDialogCore</name>
    <message>
        <location filename="../archivingdialogcore.cpp" line="137"/>
        <source>Add files...</source>
        <comment>Button</comment>
        <translation>Bestanden toevoegen…</translation>
    </message>
    <message>
        <location filename="../archivingdialogcore.cpp" line="140"/>
        <source>Add folder</source>
        <translation>Map toevoegen</translation>
    </message>
    <message>
        <location filename="../archivingdialogcore.cpp" line="146"/>
        <source>Compression</source>
        <translation>Compressie</translation>
    </message>
    <message>
        <location filename="../archivingdialogcore.cpp" line="149"/>
        <source>Add files</source>
        <translation>Bestanden toevoegen</translation>
    </message>
    <message>
        <location filename="../archivingdialogcore.cpp" line="152"/>
        <source>Create new archive</source>
        <translation>Archief aanmaken</translation>
    </message>
    <message>
        <location filename="../archivingdialogcore.cpp" line="155"/>
        <source>Encryption</source>
        <translation>Versleuteling</translation>
    </message>
    <message>
        <location filename="../archivingdialogcore.cpp" line="158"/>
        <source>File paths</source>
        <translation>Bestandslocaties</translation>
    </message>
    <message>
        <location filename="../archivingdialogcore.cpp" line="161"/>
        <source>On completion</source>
        <translation>Na afronden</translation>
    </message>
    <message>
        <location filename="../archivingdialogcore.cpp" line="164"/>
        <source>Plugin settings</source>
        <translation>Plug-in-instellingen</translation>
    </message>
    <message>
        <location filename="../archivingdialogcore.cpp" line="167"/>
        <source>Remove</source>
        <translation>Verwijderen</translation>
    </message>
    <message>
        <location filename="../archivingdialogcore.cpp" line="170"/>
        <source>Advanced</source>
        <translation>Geavanceerd</translation>
    </message>
    <message>
        <location filename="../archivingdialogcore.cpp" line="173"/>
        <source>Archive</source>
        <translation>Archief</translation>
    </message>
    <message>
        <location filename="../archivingdialogcore.cpp" line="176"/>
        <source>Files</source>
        <translation>Bestanden</translation>
    </message>
    <message>
        <location filename="../archivingdialogcore.cpp" line="179"/>
        <source>Options</source>
        <translation>Opties</translation>
    </message>
    <message>
        <location filename="../archivingdialogcore.cpp" line="401"/>
        <source>archive</source>
        <translation>archief</translation>
    </message>
    <message>
        <location filename="../archivingdialogcore.cpp" line="520"/>
        <source>Repeat</source>
        <translation>Herhalen</translation>
    </message>
</context>
<context>
    <name>Packuru::Core::ArchivingTaskWorker</name>
    <message>
        <location filename="../private/tasks/archivingtaskworker.cpp" line="196"/>
        <source>File &apos;%1&apos; already exists.</source>
        <translation>‘%1’ bestaat al.</translation>
    </message>
    <message>
        <location filename="../private/tasks/archivingtaskworker.cpp" line="218"/>
        <source>Following files or directories already exist:
</source>
        <translation>De volgende mappen en/of bestanden bestaan al:
</translation>
    </message>
</context>
<context>
    <name>Packuru::Core::CommonStrings</name>
    <message>
        <location filename="../commonstrings.cpp" line="25"/>
        <source>Add...</source>
        <comment>Add items</comment>
        <translation>Toevoegen…</translation>
    </message>
    <message>
        <location filename="../commonstrings.cpp" line="28"/>
        <source>Properties</source>
        <translation>Eigenschappen</translation>
    </message>
    <message>
        <location filename="../commonstrings.cpp" line="31"/>
        <source>Back</source>
        <translation>Terug</translation>
    </message>
    <message>
        <location filename="../commonstrings.cpp" line="34"/>
        <source>Cancel</source>
        <translation>Annuleren</translation>
    </message>
    <message>
        <location filename="../commonstrings.cpp" line="37"/>
        <source>Close</source>
        <comment>e.g. a dialog</comment>
        <translation>Sluiten</translation>
    </message>
    <message>
        <location filename="../commonstrings.cpp" line="40"/>
        <source>Command line error</source>
        <translation>Opdrachtregelfout</translation>
    </message>
    <message>
        <location filename="../commonstrings.cpp" line="43"/>
        <source>Command line help</source>
        <translation>Opdrachtregelhulp</translation>
    </message>
    <message>
        <location filename="../commonstrings.cpp" line="46"/>
        <source>Comment</source>
        <translation>Opmerking</translation>
    </message>
    <message>
        <location filename="../commonstrings.cpp" line="49"/>
        <source>Error log</source>
        <translation>Logboek</translation>
    </message>
    <message>
        <location filename="../commonstrings.cpp" line="52"/>
        <source>Remove</source>
        <comment>Remove items</comment>
        <translation>Verwijderen</translation>
    </message>
</context>
<context>
    <name>Packuru::Core::CreationNameErrorPromptCore</name>
    <message>
        <location filename="../creationnameerrorpromptcore.cpp" line="53"/>
        <source>Cannot create archive</source>
        <translation>Aanmaken mislukt</translation>
    </message>
    <message>
        <location filename="../creationnameerrorpromptcore.cpp" line="55"/>
        <source>File name conflict</source>
        <translation>Bestandsnaamconflict</translation>
    </message>
    <message>
        <location filename="../creationnameerrorpromptcore.cpp" line="57"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location filename="../creationnameerrorpromptcore.cpp" line="61"/>
        <source>You can change archive name and try again.</source>
        <translation>Wijzig de archiefnaam en probeer het opnieuw.</translation>
    </message>
</context>
<context>
    <name>Packuru::Core::ExtractionDialogCore</name>
    <message>
        <location filename="../extractiondialogcore.cpp" line="114"/>
        <source>Extract archives</source>
        <translation>Archieven uitpakken</translation>
    </message>
    <message>
        <location filename="../extractiondialogcore.cpp" line="117"/>
        <source>Extract</source>
        <translation>Uitpakken</translation>
    </message>
    <message>
        <location filename="../extractiondialogcore.cpp" line="120"/>
        <source>Archives</source>
        <translation>Archieven</translation>
    </message>
    <message>
        <location filename="../extractiondialogcore.cpp" line="123"/>
        <source>Options</source>
        <translation>Opties</translation>
    </message>
</context>
<context>
    <name>Packuru::Core::ExtractionFileOverwritePromptCore</name>
    <message>
        <location filename="../extractionfileoverwritepromptcore.cpp" line="78"/>
        <source>File</source>
        <translation>Bestand</translation>
    </message>
    <message>
        <location filename="../extractionfileoverwritepromptcore.cpp" line="80"/>
        <source>File already exists</source>
        <translation>Bestand bestaat al</translation>
    </message>
    <message>
        <location filename="../extractionfileoverwritepromptcore.cpp" line="82"/>
        <source>Location</source>
        <translation>Locatie</translation>
    </message>
    <message>
        <location filename="../extractionfileoverwritepromptcore.cpp" line="84"/>
        <source>Modified</source>
        <translation>Bewerkt</translation>
    </message>
    <message>
        <location filename="../extractionfileoverwritepromptcore.cpp" line="86"/>
        <source>Overwrite</source>
        <translation>Overschrijven</translation>
    </message>
    <message>
        <location filename="../extractionfileoverwritepromptcore.cpp" line="90"/>
        <source>Size</source>
        <translation>Grootte</translation>
    </message>
</context>
<context>
    <name>Packuru::Core::ExtractionFolderOverwritePromptCore</name>
    <message>
        <location filename="../extractionfolderoverwritepromptcore.cpp" line="100"/>
        <source>Content</source>
        <translation>Inhoud</translation>
    </message>
    <message>
        <location filename="../extractionfolderoverwritepromptcore.cpp" line="104"/>
        <source>Folder</source>
        <translation>Map</translation>
    </message>
    <message>
        <location filename="../extractionfolderoverwritepromptcore.cpp" line="106"/>
        <source>Folder already exists</source>
        <translation>Map bestaat al</translation>
    </message>
    <message>
        <location filename="../extractionfolderoverwritepromptcore.cpp" line="108"/>
        <source>Modified</source>
        <translation>Bewerkt</translation>
    </message>
    <message>
        <location filename="../extractionfolderoverwritepromptcore.cpp" line="116"/>
        <source>Write into</source>
        <translation>Opslaan in</translation>
    </message>
    <message numerus="yes">
        <location filename="../extractionfolderoverwritepromptcore.cpp" line="137"/>
        <location filename="../extractionfolderoverwritepromptcore.cpp" line="145"/>
        <source>%n items</source>
        <translation>
            <numerusform>%n item</numerusform>
            <numerusform>%n items</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>Packuru::Core::ExtractionTypeMismatchPromptCore</name>
    <message>
        <location filename="../extractiontypemismatchpromptcore.cpp" line="93"/>
        <source>File</source>
        <translation>Bestand</translation>
    </message>
    <message>
        <location filename="../extractiontypemismatchpromptcore.cpp" line="95"/>
        <source>Folder</source>
        <translation>Map</translation>
    </message>
    <message>
        <location filename="../extractiontypemismatchpromptcore.cpp" line="97"/>
        <source>Items&apos; type mismatch</source>
        <translation>Itemtypes komen niet overeen</translation>
    </message>
    <message>
        <location filename="../extractiontypemismatchpromptcore.cpp" line="99"/>
        <source>Link</source>
        <translation>Link</translation>
    </message>
    <message>
        <location filename="../extractiontypemismatchpromptcore.cpp" line="101"/>
        <source>Location</source>
        <translation>Locatie</translation>
    </message>
</context>
<context>
    <name>Packuru::Core::InputArchivesModel</name>
    <message>
        <location filename="../private/dialogcore/inputarchivesmodel.cpp" line="32"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location filename="../private/dialogcore/inputarchivesmodel.cpp" line="33"/>
        <source>Path</source>
        <translation>Locatie</translation>
    </message>
    <message>
        <location filename="../private/dialogcore/inputarchivesmodel.cpp" line="34"/>
        <source>Plugin</source>
        <translation>Plug-in</translation>
    </message>
    <message>
        <location filename="../private/dialogcore/inputarchivesmodel.cpp" line="35"/>
        <source>Type</source>
        <translation>Type</translation>
    </message>
</context>
<context>
    <name>Packuru::Core::InputFilesModel</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/inputfilesmodel.cpp" line="88"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location filename="../private/dialogcore/archivingdialog/inputfilesmodel.cpp" line="90"/>
        <source>Path</source>
        <translation>Locatie</translation>
    </message>
</context>
<context>
    <name>Packuru::Core::PasswordPromptCore</name>
    <message>
        <location filename="../passwordpromptcore.cpp" line="52"/>
        <source>Password required</source>
        <translation>Wachtwoord vereist</translation>
    </message>
</context>
<context>
    <name>Packuru::Core::PrivateStrings</name>
    <message>
        <location filename="../private/privatestrings.cpp" line="19"/>
        <source>Abort</source>
        <translation>Afbreken</translation>
    </message>
    <message>
        <location filename="../private/privatestrings.cpp" line="22"/>
        <source>About %1</source>
        <translation>Over %1</translation>
    </message>
    <message>
        <location filename="../private/privatestrings.cpp" line="25"/>
        <source>Extract</source>
        <translation>Uitpakken</translation>
    </message>
    <message>
        <location filename="../private/privatestrings.cpp" line="28"/>
        <source>New</source>
        <translation>Nieuw</translation>
    </message>
    <message>
        <location filename="../private/privatestrings.cpp" line="31"/>
        <source>Test</source>
        <translation>Testen</translation>
    </message>
    <message>
        <location filename="../private/privatestrings.cpp" line="34"/>
        <source>Apply to all</source>
        <translation>Op alles toepassen</translation>
    </message>
    <message>
        <location filename="../private/privatestrings.cpp" line="37"/>
        <source>Destination</source>
        <translation>Bestemming</translation>
    </message>
    <message>
        <location filename="../private/privatestrings.cpp" line="40"/>
        <source>File list is empty</source>
        <translation>De bestandslijst is blanco</translation>
    </message>
    <message>
        <location filename="../private/privatestrings.cpp" line="43"/>
        <source>Filter...</source>
        <translation>Filteren…</translation>
    </message>
    <message>
        <location filename="../private/privatestrings.cpp" line="46"/>
        <source>Miscellaneous</source>
        <translation>Overig</translation>
    </message>
    <message>
        <location filename="../private/privatestrings.cpp" line="49"/>
        <source>Mode</source>
        <translation>Modus</translation>
    </message>
    <message>
        <location filename="../private/privatestrings.cpp" line="52"/>
        <source>Password</source>
        <translation>Wachtwoord</translation>
    </message>
    <message>
        <location filename="../private/privatestrings.cpp" line="55"/>
        <source>Quit</source>
        <translation>Afsluiten</translation>
    </message>
    <message>
        <location filename="../private/privatestrings.cpp" line="58"/>
        <source>Retry</source>
        <translation>Opnieuw proberen</translation>
    </message>
    <message>
        <location filename="../private/privatestrings.cpp" line="61"/>
        <source>Settings</source>
        <translation>Instellingen</translation>
    </message>
    <message>
        <location filename="../private/privatestrings.cpp" line="64"/>
        <source>Skip</source>
        <translation>Overslaan</translation>
    </message>
    <message>
        <location filename="../private/privatestrings.cpp" line="67"/>
        <source>Source</source>
        <translation>Bron</translation>
    </message>
</context>
<context>
    <name>Packuru::Core::TestDialogCore</name>
    <message>
        <location filename="../testdialogcore.cpp" line="95"/>
        <source>Test archives</source>
        <translation>Archieven testen</translation>
    </message>
    <message>
        <location filename="../testdialogcore.cpp" line="98"/>
        <source>Test</source>
        <translation>Testen</translation>
    </message>
    <message>
        <location filename="../testdialogcore.cpp" line="101"/>
        <source>Archives</source>
        <translation>Archieven</translation>
    </message>
    <message>
        <location filename="../testdialogcore.cpp" line="104"/>
        <source>Options</source>
        <translation>Opties</translation>
    </message>
</context>
<context>
    <name>PasswordMatchCtrl</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/passwordmatchctrl.cpp" line="46"/>
        <source>Partial password match</source>
        <translation>De wachtwoorden komen gedeeltelijk overeen</translation>
    </message>
    <message>
        <location filename="../private/dialogcore/archivingdialog/passwordmatchctrl.cpp" line="51"/>
        <source>Passwords do not match</source>
        <translation>De wachtwoorden komen niet overeen</translation>
    </message>
</context>
<context>
    <name>RunInQueueCtrl</name>
    <message>
        <location filename="../private/dialogcore/runinqueuectrl.cpp" line="27"/>
        <source>Run in queue</source>
        <translation>Toevoegen aan wachtrij</translation>
    </message>
</context>
<context>
    <name>SizeUnits</name>
    <message>
        <location filename="../private/sizeunits.cpp" line="17"/>
        <source>Binary</source>
        <translation>Binair</translation>
    </message>
    <message>
        <location filename="../private/sizeunits.cpp" line="18"/>
        <source>Decimal</source>
        <translation>Decimaal</translation>
    </message>
</context>
<context>
    <name>SizeUnitsCtrl</name>
    <message>
        <location filename="../private/dialogcore/appsettingsdialog/sizeunitsctrl.cpp" line="17"/>
        <source>Size units</source>
        <translation>Grootte-eenheden</translation>
    </message>
</context>
<context>
    <name>SizeUnitsExampleCtrl</name>
    <message>
        <location filename="../private/dialogcore/appsettingsdialog/sizeunitsexamplectrl.cpp" line="33"/>
        <source>bytes</source>
        <comment>1024</comment>
        <translation>bytes</translation>
    </message>
    <message>
        <location filename="../private/dialogcore/appsettingsdialog/sizeunitsexamplectrl.cpp" line="36"/>
        <source>bytes</source>
        <comment>1000</comment>
        <translation>bytes</translation>
    </message>
</context>
<context>
    <name>SplitPresetCtrl</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/splitpresetctrl.cpp" line="21"/>
        <source>Disabled</source>
        <translation>Uitgeschakeld</translation>
    </message>
    <message>
        <location filename="../private/dialogcore/archivingdialog/splitpresetctrl.cpp" line="25"/>
        <source>Custom</source>
        <translation>Aangepast</translation>
    </message>
    <message>
        <location filename="../private/dialogcore/archivingdialog/splitpresetctrl.cpp" line="45"/>
        <source>Split</source>
        <translation>Splitsen</translation>
    </message>
</context>
<context>
    <name>SplitSizeCtrl</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/splitsizectrl.cpp" line="28"/>
        <source>Part size</source>
        <translation>Grootte van onderdeel</translation>
    </message>
</context>
<context>
    <name>TaskError</name>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="27"/>
        <source>Archive type not supported</source>
        <translation>Dit type wordt niet ondersteund</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="30"/>
        <source>Archiving method not supported</source>
        <translation>Deze methode wordt niet ondersteund</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="48"/>
        <source>Disk full</source>
        <translation>Geen vrije schijfruimte</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="54"/>
        <source>Invalid backend command line</source>
        <translation>Ongeldige opdrachtregeloptie</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="60"/>
        <source>Item type mismatch</source>
        <translation>Item komt niet overeen</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="18"/>
        <source>Access denied</source>
        <translation>Toegang geweigerd</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="21"/>
        <source>Archive not found</source>
        <translation>Het archief is niet aangetroffen</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="24"/>
        <source>Archive part not found</source>
        <translation>Het archiefonderdeel is niet aangetroffen</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="33"/>
        <source>Backend crashed</source>
        <translation>Het backend is gecrasht</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="36"/>
        <source>Backend executable not found</source>
        <translation>Het backendbestand is niet aangetroffen</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="39"/>
        <source>Backend submodule not found</source>
        <translation>De backend-submodule is niet aangetroffen</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="42"/>
        <source>Dangerous link</source>
        <translation>Gevaarlijke koppeling</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="45"/>
        <source>Data error</source>
        <translation>Gegevensfout</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="51"/>
        <source>File not found</source>
        <translation>Het bestand is niet aangetroffen</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="57"/>
        <source>Invalid password or data error</source>
        <translation>Ongeldig wachtwoord of gegevensfout</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="63"/>
        <source>No files extracted</source>
        <translation>Er zijn geen bestanden uitgepakt</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="66"/>
        <source>No plugin for this archive type</source>
        <translation>Er is geen plug-in voor dit bestandstype</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="69"/>
        <source>Not enough memory</source>
        <translation>Er is onvoldoende geheugen beschikbaar</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="72"/>
        <source>Temporary archive error</source>
        <translation>Fout in tijdelijk archief</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="75"/>
        <source>Temporary folder error</source>
        <translation>Fout in tijdelijke map</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="78"/>
        <source>Temporary folder not empty</source>
        <translation>De tijdelijke map is niet leeg</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="81"/>
        <source>Temporary folder not found</source>
        <translation>De tijdelijke map is niet aangetroffen</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="84"/>
        <source>Unknown error</source>
        <translation>Onbekende foutmelding</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="87"/>
        <source>Write error</source>
        <translation>Wegschrijffout</translation>
    </message>
</context>
<context>
    <name>TaskState</name>
    <message>
        <location filename="../private/tasks/taskstate.cpp" line="18"/>
        <source>Ready to run</source>
        <translation>Klaar om uit te voeren</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskstate.cpp" line="20"/>
        <source>Starting up</source>
        <translation>Bezig met opstarten…</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskstate.cpp" line="22"/>
        <source>In progress</source>
        <translation>Bezig met uitvoeren…</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskstate.cpp" line="24"/>
        <source>Waiting for password</source>
        <translation>Bezig met wachten op wachtwoord…</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskstate.cpp" line="26"/>
        <source>Waiting for decision</source>
        <translation>Bezig met wachten op actie…</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskstate.cpp" line="28"/>
        <source>Paused</source>
        <translation>Onderbroken</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskstate.cpp" line="36"/>
        <source>Aborted</source>
        <translation>Afgebroken</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskstate.cpp" line="30"/>
        <source>Success</source>
        <translation>Afgerond</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskstate.cpp" line="32"/>
        <source>Finished with warnings</source>
        <translation>Afgerond met waarschuwingen</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskstate.cpp" line="34"/>
        <source>Finished with errors</source>
        <translation>Afgerond met foutmeldingen</translation>
    </message>
</context>
<context>
    <name>TaskWarning</name>
    <message>
        <location filename="../private/tasks/taskwarning.cpp" line="18"/>
        <source>Backend version deprecated</source>
        <translation>Verouderde backendversie</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskwarning.cpp" line="20"/>
        <source>Backend version not tested</source>
        <translation>Ongeteste backendversie</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskwarning.cpp" line="22"/>
        <source>Low disk space</source>
        <translation>Weinig vrije schijfruimte</translation>
    </message>
    <message>
        <location filename="../private/tasks/taskwarning.cpp" line="24"/>
        <source>Unknown warning</source>
        <translation>Onbekende waarschuwing</translation>
    </message>
</context>
<context>
    <name>TopFolderModeCtrl</name>
    <message>
        <location filename="../private/dialogcore/extractiondialog/topfoldermodectrl.cpp" line="21"/>
        <source>Top folder</source>
        <translation>Hoofdmap</translation>
    </message>
</context>
</TS>
