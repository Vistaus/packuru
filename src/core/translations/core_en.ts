<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>ArchiveNameCtrl</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/archivenamectrl.cpp" line="17"/>
        <source>Automatic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/dialogcore/archivingdialog/archivenamectrl.cpp" line="21"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ArchiveNameErrorCtrl</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/archivenameerrorctrl.cpp" line="17"/>
        <source>Archive name cannot be empty</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ArchiveTypeCtrl</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/archivetypectrl.cpp" line="21"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ArchivingDestinationMode</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/archivingdestinationmode.cpp" line="19"/>
        <source>Item&apos;s parent folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/dialogcore/archivingdialog/archivingdestinationmode.cpp" line="21"/>
        <source>Custom folder</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ArchivingFilePaths</name>
    <message>
        <location filename="../private/plugin-api/archivingfilepaths.cpp" line="18"/>
        <source>Absolute paths</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/plugin-api/archivingfilepaths.cpp" line="20"/>
        <source>Full paths</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/plugin-api/archivingfilepaths.cpp" line="22"/>
        <source>Relative paths</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ArchivingMode</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/archivingmode.cpp" line="19"/>
        <source>Separate archive for each item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/dialogcore/archivingdialog/archivingmode.cpp" line="21"/>
        <source>Single archive</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ArchivingOpenDestinationCtrl</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/archivingopendestinationctrl.cpp" line="18"/>
        <source>Open destination folder</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CommandLineHandlerBase</name>
    <message>
        <location filename="../private/commandlinehandlerbase.cpp" line="18"/>
        <source>urls</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/commandlinehandlerbase.cpp" line="19"/>
        <source>URLs to open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/commandlinehandlerbase.cpp" line="20"/>
        <source>[urls...]</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CompressionRatioCtrl</name>
    <message>
        <location filename="../private/dialogcore/appsettingsdialog/compressionratioctrl.cpp" line="17"/>
        <source>Compression ratio display mode</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CompressionRatioMode</name>
    <message>
        <location filename="../private/compressionratiomode.cpp" line="19"/>
        <source>Original / packed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/compressionratiomode.cpp" line="21"/>
        <source>Packed / original</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CompressionStateCtrl</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/compressionstatectrl.cpp" line="16"/>
        <source>State</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/dialogcore/archivingdialog/compressionstatectrl.cpp" line="27"/>
        <source>Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/dialogcore/archivingdialog/compressionstatectrl.cpp" line="30"/>
        <source>Enabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../private/dialogcore/archivingdialog/compressionstatectrl.cpp" line="36"/>
        <source>level %n</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
</context>
<context>
    <name>DestinationModeCtrl</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/destinationmodectrl.cpp" line="25"/>
        <source>Create in</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DestinationPathCtrl</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/destinationpathctrl.cpp" line="19"/>
        <source>Automatic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/dialogcore/archivingdialog/destinationpathctrl.cpp" line="23"/>
        <source>Destination path</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DirectoryErrorCtrl</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/directoryerrorctrl.cpp" line="19"/>
        <source>Destination path cannot be empty</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EncryptionModeCtrl</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/encryptionmodectrl.cpp" line="22"/>
        <source>Encrypt entire archive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/dialogcore/archivingdialog/encryptionmodectrl.cpp" line="23"/>
        <source>Encrypt files content only</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EncryptionState</name>
    <message>
        <location filename="../private/plugin-api/encryptionstate.cpp" line="19"/>
        <source>Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/plugin-api/encryptionstate.cpp" line="21"/>
        <source>Enabled (entire archive)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/plugin-api/encryptionstate.cpp" line="23"/>
        <source>Enabled (files content only)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EncryptionStateCtrl</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/encryptionstatectrl.cpp" line="24"/>
        <source>State</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/dialogcore/archivingdialog/encryptionstatectrl.cpp" line="56"/>
        <source>Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/dialogcore/archivingdialog/encryptionstatectrl.cpp" line="83"/>
        <source>Enabled</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExtractionDestinationErrorCtrl</name>
    <message>
        <location filename="../private/dialogcore/extractiondialog/extractiondestinationerrorctrl.cpp" line="42"/>
        <source>Destination path cannot be empty</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExtractionDestinationMode</name>
    <message>
        <location filename="../private/dialogcore/extractiondialog/extractiondestinationmode.cpp" line="19"/>
        <source>Archive&apos;s parent folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/dialogcore/extractiondialog/extractiondestinationmode.cpp" line="21"/>
        <source>Custom folder</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExtractionDestinationModeCtrl</name>
    <message>
        <location filename="../private/dialogcore/extractiondialog/extractiondestinationmodectrl.cpp" line="25"/>
        <source>Extract to</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExtractionDestinationPathCtrl</name>
    <message>
        <location filename="../private/dialogcore/extractiondialog/extractiondestinationpathctrl.cpp" line="17"/>
        <source>Automatic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/dialogcore/extractiondialog/extractiondestinationpathctrl.cpp" line="21"/>
        <source>Destination path</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExtractionFileOverwriteCtrl</name>
    <message>
        <location filename="../private/dialogcore/extractiondialog/extractionfileoverwritectrl.cpp" line="21"/>
        <source>File overwriting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExtractionFileOverwriteMode</name>
    <message>
        <location filename="../private/extractionfileoverwritemode.cpp" line="19"/>
        <source>Ask before overwrite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/extractionfileoverwritemode.cpp" line="21"/>
        <source>Overwrite existing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/extractionfileoverwritemode.cpp" line="23"/>
        <source>Skip existing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/extractionfileoverwritemode.cpp" line="25"/>
        <source>Update existing</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExtractionFolderOverwriteCtrl</name>
    <message>
        <location filename="../private/dialogcore/extractiondialog/extractionfolderoverwritectrl.cpp" line="20"/>
        <source>Folder overwriting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExtractionFolderOverwriteMode</name>
    <message>
        <location filename="../private/extractionfolderoverwritemode.cpp" line="19"/>
        <source>Ask before writing into</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/extractionfolderoverwritemode.cpp" line="21"/>
        <source>Write into existing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/extractionfolderoverwritemode.cpp" line="23"/>
        <source>Skip existing</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExtractionOpenDestinationCtrl</name>
    <message>
        <location filename="../private/dialogcore/extractiondialog/extractionopendestinationctrl.cpp" line="18"/>
        <source>Open destination folder on completion</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ExtractionTopFolderMode</name>
    <message>
        <location filename="../private/extractiontopfoldermode.cpp" line="19"/>
        <source>Create top folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/extractiontopfoldermode.cpp" line="21"/>
        <source>Create if absent in the archive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/extractiontopfoldermode.cpp" line="23"/>
        <source>Do not create top folder</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileExtractionMode</name>
    <message>
        <location filename="../private/fileextractionmode.cpp" line="20"/>
        <source>All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/fileextractionmode.cpp" line="22"/>
        <source>Selected only</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FileExtractionModeCtrl</name>
    <message>
        <location filename="../private/dialogcore/extractiondialog/fileextractionmodectrl.cpp" line="25"/>
        <source>Extract files</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GeneralCompressionLevelCtrl</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/generalcompressionlevelctrl.cpp" line="28"/>
        <source>Level</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InternalPathCtrl</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/internalpathctrl.cpp" line="32"/>
        <source>Internal path</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OpenNewArchiveCtrl</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/opennewarchivectrl.cpp" line="17"/>
        <source>Open archive</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Packuru::Core::AbstractBackendHandler</name>
    <message>
        <location filename="../private/plugin-api/abstractbackendhandler.cpp" line="98"/>
        <source>Temporary folder not found: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/plugin-api/abstractbackendhandler.cpp" line="105"/>
        <source>Temporary folder is not empty: </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Packuru::Core::AppClosingDialogCore</name>
    <message>
        <location filename="../appclosingdialogcore.cpp" line="29"/>
        <source>Confirm quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appclosingdialogcore.cpp" line="35"/>
        <source>Do you want to quit?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appclosingdialogcore.cpp" line="37"/>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../appclosingdialogcore.cpp" line="25"/>
        <source>There are %n busy tabs.</source>
        <translation>
            <numerusform>There is %n busy tab.</numerusform>
            <numerusform>There are %n busy tabs.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../appclosingdialogcore.cpp" line="31"/>
        <source>%n opened dialog windows will be closed.</source>
        <translation>
            <numerusform>%n opened dialog window will be closed.</numerusform>
            <numerusform>%n opened dialog windows will be closed.</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../appclosingdialogcore.cpp" line="33"/>
        <source>%n unfinished tasks will be aborted.</source>
        <translation>
            <numerusform>%n unfinished task will be aborted.</numerusform>
            <numerusform>%n unfinished tasks will be aborted.</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>Packuru::Core::AppSettingsDialogCore</name>
    <message>
        <location filename="../appsettingsdialogcore.cpp" line="77"/>
        <source>General</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettingsdialogcore.cpp" line="80"/>
        <source>Plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettingsdialogcore.cpp" line="83"/>
        <source>Archive types</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettingsdialogcore.cpp" line="92"/>
        <source>Backend homepage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettingsdialogcore.cpp" line="96"/>
        <source>Please note that each plugin can interpret file names differently (e.g. because of the level of support for different text encodings).

It is therefore recommended to select the same plugin for read and write operations.

Moreover, after changing plugin configuration for a particular type, it is advised to reload all archives of this type and discard all extraction and deletion tasks for this type that reference selected file names.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettingsdialogcore.cpp" line="105"/>
        <source>Description</source>
        <comment>Plugin description</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettingsdialogcore.cpp" line="108"/>
        <source>File path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettingsdialogcore.cpp" line="111"/>
        <source>Reads</source>
        <comment>Which types the plugin reads</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettingsdialogcore.cpp" line="114"/>
        <source>Writes</source>
        <comment>Which types the plugin writes</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../appsettingsdialogcore.cpp" line="117"/>
        <source>Read warning</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Packuru::Core::ArchiveTypeModel</name>
    <message>
        <location filename="../archivetypemodel.cpp" line="63"/>
        <source>Archive type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivetypemodel.cpp" line="64"/>
        <source>Read plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivetypemodel.cpp" line="65"/>
        <source>Write plugin</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Packuru::Core::ArchivingDialogCore</name>
    <message>
        <location filename="../archivingdialogcore.cpp" line="137"/>
        <source>Add files...</source>
        <comment>Button</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivingdialogcore.cpp" line="140"/>
        <source>Add folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivingdialogcore.cpp" line="146"/>
        <source>Compression</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivingdialogcore.cpp" line="149"/>
        <source>Add files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivingdialogcore.cpp" line="152"/>
        <source>Create new archive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivingdialogcore.cpp" line="155"/>
        <source>Encryption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivingdialogcore.cpp" line="158"/>
        <source>File paths</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivingdialogcore.cpp" line="161"/>
        <source>On completion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivingdialogcore.cpp" line="164"/>
        <source>Plugin settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivingdialogcore.cpp" line="167"/>
        <source>Remove</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivingdialogcore.cpp" line="170"/>
        <source>Advanced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivingdialogcore.cpp" line="173"/>
        <source>Archive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivingdialogcore.cpp" line="176"/>
        <source>Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivingdialogcore.cpp" line="179"/>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivingdialogcore.cpp" line="401"/>
        <source>archive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivingdialogcore.cpp" line="520"/>
        <source>Repeat</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Packuru::Core::ArchivingTaskWorker</name>
    <message>
        <location filename="../private/tasks/archivingtaskworker.cpp" line="196"/>
        <source>File &apos;%1&apos; already exists.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/archivingtaskworker.cpp" line="218"/>
        <source>Following files or directories already exist:
</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Packuru::Core::CommonStrings</name>
    <message>
        <location filename="../commonstrings.cpp" line="25"/>
        <source>Add...</source>
        <comment>Add items</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commonstrings.cpp" line="28"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commonstrings.cpp" line="31"/>
        <source>Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commonstrings.cpp" line="34"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commonstrings.cpp" line="37"/>
        <source>Close</source>
        <comment>e.g. a dialog</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commonstrings.cpp" line="40"/>
        <source>Command line error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commonstrings.cpp" line="43"/>
        <source>Command line help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commonstrings.cpp" line="46"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commonstrings.cpp" line="49"/>
        <source>Error log</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../commonstrings.cpp" line="52"/>
        <source>Remove</source>
        <comment>Remove items</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Packuru::Core::CreationNameErrorPromptCore</name>
    <message>
        <location filename="../creationnameerrorpromptcore.cpp" line="53"/>
        <source>Cannot create archive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../creationnameerrorpromptcore.cpp" line="55"/>
        <source>File name conflict</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../creationnameerrorpromptcore.cpp" line="57"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../creationnameerrorpromptcore.cpp" line="61"/>
        <source>You can change archive name and try again.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Packuru::Core::ExtractionDialogCore</name>
    <message>
        <location filename="../extractiondialogcore.cpp" line="114"/>
        <source>Extract archives</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../extractiondialogcore.cpp" line="117"/>
        <source>Extract</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../extractiondialogcore.cpp" line="120"/>
        <source>Archives</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../extractiondialogcore.cpp" line="123"/>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Packuru::Core::ExtractionFileOverwritePromptCore</name>
    <message>
        <location filename="../extractionfileoverwritepromptcore.cpp" line="78"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../extractionfileoverwritepromptcore.cpp" line="80"/>
        <source>File already exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../extractionfileoverwritepromptcore.cpp" line="82"/>
        <source>Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../extractionfileoverwritepromptcore.cpp" line="84"/>
        <source>Modified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../extractionfileoverwritepromptcore.cpp" line="86"/>
        <source>Overwrite</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../extractionfileoverwritepromptcore.cpp" line="90"/>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Packuru::Core::ExtractionFolderOverwritePromptCore</name>
    <message>
        <location filename="../extractionfolderoverwritepromptcore.cpp" line="100"/>
        <source>Content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../extractionfolderoverwritepromptcore.cpp" line="104"/>
        <source>Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../extractionfolderoverwritepromptcore.cpp" line="106"/>
        <source>Folder already exists</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../extractionfolderoverwritepromptcore.cpp" line="108"/>
        <source>Modified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../extractionfolderoverwritepromptcore.cpp" line="116"/>
        <source>Write into</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../extractionfolderoverwritepromptcore.cpp" line="137"/>
        <location filename="../extractionfolderoverwritepromptcore.cpp" line="145"/>
        <source>%n items</source>
        <translation>
            <numerusform>%n item</numerusform>
            <numerusform>%n items</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>Packuru::Core::ExtractionTypeMismatchPromptCore</name>
    <message>
        <location filename="../extractiontypemismatchpromptcore.cpp" line="93"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../extractiontypemismatchpromptcore.cpp" line="95"/>
        <source>Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../extractiontypemismatchpromptcore.cpp" line="97"/>
        <source>Items&apos; type mismatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../extractiontypemismatchpromptcore.cpp" line="99"/>
        <source>Link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../extractiontypemismatchpromptcore.cpp" line="101"/>
        <source>Location</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Packuru::Core::InputArchivesModel</name>
    <message>
        <location filename="../private/dialogcore/inputarchivesmodel.cpp" line="32"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/dialogcore/inputarchivesmodel.cpp" line="33"/>
        <source>Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/dialogcore/inputarchivesmodel.cpp" line="34"/>
        <source>Plugin</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/dialogcore/inputarchivesmodel.cpp" line="35"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Packuru::Core::InputFilesModel</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/inputfilesmodel.cpp" line="88"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/dialogcore/archivingdialog/inputfilesmodel.cpp" line="90"/>
        <source>Path</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Packuru::Core::PasswordPromptCore</name>
    <message>
        <location filename="../passwordpromptcore.cpp" line="52"/>
        <source>Password required</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Packuru::Core::PrivateStrings</name>
    <message>
        <location filename="../private/privatestrings.cpp" line="19"/>
        <source>Abort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/privatestrings.cpp" line="22"/>
        <source>About %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/privatestrings.cpp" line="25"/>
        <source>Extract</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/privatestrings.cpp" line="28"/>
        <source>New</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/privatestrings.cpp" line="31"/>
        <source>Test</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/privatestrings.cpp" line="34"/>
        <source>Apply to all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/privatestrings.cpp" line="37"/>
        <source>Destination</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/privatestrings.cpp" line="40"/>
        <source>File list is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/privatestrings.cpp" line="43"/>
        <source>Filter...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/privatestrings.cpp" line="46"/>
        <source>Miscellaneous</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/privatestrings.cpp" line="49"/>
        <source>Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/privatestrings.cpp" line="52"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/privatestrings.cpp" line="55"/>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/privatestrings.cpp" line="58"/>
        <source>Retry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/privatestrings.cpp" line="61"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/privatestrings.cpp" line="64"/>
        <source>Skip</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/privatestrings.cpp" line="67"/>
        <source>Source</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Packuru::Core::TestDialogCore</name>
    <message>
        <location filename="../testdialogcore.cpp" line="95"/>
        <source>Test archives</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../testdialogcore.cpp" line="98"/>
        <source>Test</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../testdialogcore.cpp" line="101"/>
        <source>Archives</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../testdialogcore.cpp" line="104"/>
        <source>Options</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PasswordMatchCtrl</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/passwordmatchctrl.cpp" line="46"/>
        <source>Partial password match</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/dialogcore/archivingdialog/passwordmatchctrl.cpp" line="51"/>
        <source>Passwords do not match</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RunInQueueCtrl</name>
    <message>
        <location filename="../private/dialogcore/runinqueuectrl.cpp" line="27"/>
        <source>Run in queue</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SizeUnits</name>
    <message>
        <location filename="../private/sizeunits.cpp" line="17"/>
        <source>Binary</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/sizeunits.cpp" line="18"/>
        <source>Decimal</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SizeUnitsCtrl</name>
    <message>
        <location filename="../private/dialogcore/appsettingsdialog/sizeunitsctrl.cpp" line="17"/>
        <source>Size units</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SizeUnitsExampleCtrl</name>
    <message>
        <location filename="../private/dialogcore/appsettingsdialog/sizeunitsexamplectrl.cpp" line="33"/>
        <source>bytes</source>
        <comment>1024</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/dialogcore/appsettingsdialog/sizeunitsexamplectrl.cpp" line="36"/>
        <source>bytes</source>
        <comment>1000</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SplitPresetCtrl</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/splitpresetctrl.cpp" line="21"/>
        <source>Disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/dialogcore/archivingdialog/splitpresetctrl.cpp" line="25"/>
        <source>Custom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/dialogcore/archivingdialog/splitpresetctrl.cpp" line="45"/>
        <source>Split</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SplitSizeCtrl</name>
    <message>
        <location filename="../private/dialogcore/archivingdialog/splitsizectrl.cpp" line="28"/>
        <source>Part size</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TaskError</name>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="27"/>
        <source>Archive type not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="30"/>
        <source>Archiving method not supported</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="48"/>
        <source>Disk full</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="54"/>
        <source>Invalid backend command line</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="60"/>
        <source>Item type mismatch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="18"/>
        <source>Access denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="21"/>
        <source>Archive not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="24"/>
        <source>Archive part not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="33"/>
        <source>Backend crashed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="36"/>
        <source>Backend executable not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="39"/>
        <source>Backend submodule not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="42"/>
        <source>Dangerous link</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="45"/>
        <source>Data error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="51"/>
        <source>File not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="57"/>
        <source>Invalid password or data error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="63"/>
        <source>No files extracted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="66"/>
        <source>No plugin for this archive type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="69"/>
        <source>Not enough memory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="72"/>
        <source>Temporary archive error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="75"/>
        <source>Temporary folder error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="78"/>
        <source>Temporary folder not empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="81"/>
        <source>Temporary folder not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="84"/>
        <source>Unknown error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskerror.cpp" line="87"/>
        <source>Write error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TaskState</name>
    <message>
        <location filename="../private/tasks/taskstate.cpp" line="18"/>
        <source>Ready to run</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskstate.cpp" line="20"/>
        <source>Starting up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskstate.cpp" line="22"/>
        <source>In progress</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskstate.cpp" line="24"/>
        <source>Waiting for password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskstate.cpp" line="26"/>
        <source>Waiting for decision</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskstate.cpp" line="28"/>
        <source>Paused</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskstate.cpp" line="36"/>
        <source>Aborted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskstate.cpp" line="30"/>
        <source>Success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskstate.cpp" line="32"/>
        <source>Finished with warnings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskstate.cpp" line="34"/>
        <source>Finished with errors</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TaskWarning</name>
    <message>
        <location filename="../private/tasks/taskwarning.cpp" line="18"/>
        <source>Backend version deprecated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskwarning.cpp" line="20"/>
        <source>Backend version not tested</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskwarning.cpp" line="22"/>
        <source>Low disk space</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/tasks/taskwarning.cpp" line="24"/>
        <source>Unknown warning</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TopFolderModeCtrl</name>
    <message>
        <location filename="../private/dialogcore/extractiondialog/topfoldermodectrl.cpp" line="21"/>
        <source>Top folder</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
