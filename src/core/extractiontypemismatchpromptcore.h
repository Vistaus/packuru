// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QObject>

#include "symbol_export.h"

/// @file

class QFileInfo;


namespace Packuru::Core
{

namespace ExtractionTypeMismatchPromptCoreAux
{
struct Init;
struct Backdoor;
}

/**
 @brief Contains data and logic necessary for Extraction Type Mismatch dialog.

 Object of this class is emitted by StatusPageCore and TaskQueueModel objects
 and will be automatically destroyed after user action (skip(), retry() or abort()) has been received.

 @warning The object must not be destroyed manually.

 @headerfile "core/extractiontypemismatchpromptcore.h"
 */
class PACKURU_CORE_EXPORT ExtractionTypeMismatchPromptCore : public QObject
{
    Q_OBJECT

public:
    /// @brief Describes the type of the item.
    enum class FileType
    {
        Dir, ///< Directory.
        File, ///< File.
        Link, ///< Link.

        ___INVALID
    };
    Q_ENUM(FileType)

    /// @copydoc ArchivingDialogCore::UserString
    enum class UserString
    {
        ___INVALID, // Keep at index 0 to catch implicit conversions of invalid values to 0 in QML

        Abort, ///< 'Abort' button.
        ApplyToAll, ///< 'Apply to all' checkbox.
        Destination, ///< 'Destination' header label.
        File, ///< 'File' label.
        Folder, ///< 'Folder' label.
        ItemsTypeMismatch, ///< 'Items' type mismatch' label.
        Link, ///< 'Link' label.
        Location, ///< 'Location' label.
        Retry, ///< 'Retry' button.
        Skip, ///< 'Skip' button.
        Source ///< 'Source' header label.
    };
    Q_ENUM(UserString)

    explicit ExtractionTypeMismatchPromptCore(const ExtractionTypeMismatchPromptCoreAux::Init& init,
                                              ExtractionTypeMismatchPromptCoreAux::Backdoor& backdoor,
                                              QObject* parent = nullptr);

    ~ExtractionTypeMismatchPromptCore() override;

    /// @copydoc ArchivingDialogCore::getString
    Q_INVOKABLE static QString getString(UserString value);

    /// @brief Returns source file type.
    Q_INVOKABLE FileType sourceFileType() const;

    /// @brief Returns destination file type.
    Q_INVOKABLE FileType destinationFileType() const;

    /// @brief Returns item file name.
    Q_INVOKABLE QString itemFileName() const;

    /// @brief Returns source item parent path.
    Q_INVOKABLE QString sourcePath() const;

    /// @brief Returns destination item parent path.
    Q_INVOKABLE QString destinationPath() const;

    /// @brief Returns source item file path.
    Q_INVOKABLE QString sourceFilePath() const;

    /// @brief Returns destination item file path.
    Q_INVOKABLE QString destinationFilePath() const;

    ///@{
    /// @name Actions
    /// @warning Calling Actions will also automatically destroy the object.

    /** @brief Skips current item.
     * @param applyToAll Specifies whether all subsequent type mismatches should be automatically skipped.
     */
    Q_INVOKABLE void skip(bool applyToAll);

    /** @brief Retries to move the item to the destination folder.
     */
    Q_INVOKABLE void retry();

    /// @copydoc CreationNameErrorPromptCore::abort()
    Q_INVOKABLE void abort();

    ///@}

signals:
    /// @brief Required for QML.
    void deleted();

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}
