// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../qcs-core/controllerengine.h"

#include "appsettingsdialogcore.h"
#include "appsettingscontrollertype.h"
#include "archivetypemodel.h"
#include "globalsettingsmanager.h"
#include "private/globalsettings.h"
#include "commonstrings.h"
#include "private/dialogcore/appsettingsdialog/sizeunitsctrl.h"
#include "private/dialogcore/appsettingsdialog/sizeunitsexamplectrl.h"
#include "private/dialogcore/appsettingsdialog/compressionratioctrl.h"
#include "private/dialogcore/appsettingsdialog/appsettingsdialogcoreaux.h"
#include "private/privatestrings.h"


using qcs::core::ControllerEngine;
using qcs::core::ControllerProperty;


namespace Packuru::Core
{

using Type = AppSettingsControllerType::Type;


struct AppSettingsDialogCore::Private
{
    ControllerEngine* engine;
    AppSettingsDialogCoreAux::InitData initData;
};


AppSettingsDialogCore::AppSettingsDialogCore(const AppSettingsDialogCoreAux::InitData& initData,
                                             QObject *parent)
    : QObject(parent),
      priv(new Private)
{
    priv->engine = new ControllerEngine(this);
    priv->initData = initData;

    const auto ratioMode = GlobalSettingsManager::instance()
            ->getValueAs<CompressionRatioMode>(GlobalSettings::Enum::AC_CompressionRatioMode);
    priv->engine->createTopController<CompressionRatioCtrl>(Type::CompressionRatioDisplayMode, ratioMode);

    const auto units = GlobalSettingsManager::instance()
            ->getValueAs<SizeUnits>(GlobalSettings::Enum::AC_SizeUnits);

    auto sizeUnitsCtrl = priv->engine->createTopController<SizeUnitsCtrl>(Type::SizeUnits, units);
    auto sizeUnitsExampleCtrl = priv->engine->createController<SizeUnitsExampleCtrl>(Type::SizeUnitsExample);

    sizeUnitsCtrl->addDependant(sizeUnitsExampleCtrl);

    priv->engine->updateAllControllers();
}


AppSettingsDialogCore::~AppSettingsDialogCore()
{
}


void AppSettingsDialogCore::destroyLater()
{
    deleteLater();
}


QString AppSettingsDialogCore::getString(UserString value)
{
    switch(value)
    {
    case UserString::CategoryGeneral:
        return Packuru::Core::AppSettingsDialogCore::tr("General");

    case UserString::CategoryPlugins:
        return Packuru::Core::AppSettingsDialogCore::tr("Plugins");

    case UserString::CategoryArchiveTypes:
        return Packuru::Core::AppSettingsDialogCore::tr("Archive types");

    case UserString::DialogTitle:
        return PrivateStrings::getString(PrivateStrings::UserString::Settings);

    case UserString::FilterPlaceholderText:
        return PrivateStrings::getString(PrivateStrings::UserString::FilterPlaceholderText);

    case UserString::PluginBackendHomepage:
        return Packuru::Core::AppSettingsDialogCore::tr("Backend homepage");

    case UserString::PluginConfigChangeWarning:
        return Packuru::Core::AppSettingsDialogCore::
                tr("Please note that each plugin can interpret file names differently "
                   "(e.g. because of the level of support for different text encodings).\n\n"
                   "It is therefore recommended to select the same plugin for read and write "
                   "operations.\n\n"
                   "Moreover, after changing plugin configuration for a particular type, "
                   "it is advised to reload all archives of this type and discard all extraction "
                   "and deletion tasks for this type that reference selected file names.");

    case UserString::PluginDescription:
        return Packuru::Core::AppSettingsDialogCore::tr("Description", "Plugin description");

    case UserString::PluginFilePath:
        return Packuru::Core::AppSettingsDialogCore::tr("File path");

    case UserString::PluginReads:
        return Packuru::Core::AppSettingsDialogCore::tr("Reads", "Which types the plugin reads");

    case UserString::PluginWrites:
        return Packuru::Core::AppSettingsDialogCore::tr("Writes", "Which types the plugin writes");

    case UserString::ReadWarning:
        return Packuru::Core::AppSettingsDialogCore::tr("Read warning");

    default:
        Q_ASSERT(false); return "";
    }
}


ControllerEngine* AppSettingsDialogCore::getControllerEngine()
{
    return priv->engine;
}


PluginDataModel* AppSettingsDialogCore::getPluginModel()
{
    return priv->initData.pluginModel;
}


ArchiveTypeModel* AppSettingsDialogCore::getArchiveTypeModel()
{
    return priv->initData.archiveTypeModelAccessor.model;
}


void AppSettingsDialogCore::accept()
{
    priv->initData.archiveTypeModelAccessor.saveModelChanges();

    const auto ratioMode = priv->engine
            ->getControllerPropertyAs<CompressionRatioMode>(Type::CompressionRatioDisplayMode,
                                                            ControllerProperty::PrivateCurrentValue);

    GlobalSettingsManager::instance()->setValue(GlobalSettings::Enum::AC_CompressionRatioMode, ratioMode);

    const auto sizeUnits = priv->engine
            ->getControllerPropertyAs<SizeUnits>(Type::SizeUnits,
                                                 ControllerProperty::PrivateCurrentValue);

    GlobalSettingsManager::instance()->setValue(GlobalSettings::Enum::AC_SizeUnits, sizeUnits);
}

}
