// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>
#include <functional>

#include <QObject>

#include "symbol_export.h"

/// @file

namespace Packuru::Core
{

namespace CreationNameErrorPromptCoreAux
{
struct Init;
struct Backdoor;
}

/**
 @brief Contains data and logic necessary for Creation Name Error dialog.

 Object of this class is emitted by TaskQueueModel object and will be automatically
 destroyed after user action (retry() or abort()) has been received.

 @warning The object must not be destroyed manually.

 @headerfile "core/creationnameerrorpromptcore.h"
 */
class PACKURU_CORE_EXPORT CreationNameErrorPromptCore : public QObject
{
    Q_OBJECT
public:
    /**
     @brief Identifies strings to display in the dialog.
     */
    enum class UserString
    {
        ___INVALID, // Keep at index 0 to catch implicit conversions of invalid values to 0 in QML

        Abort, ///< Abort button.
        CannotCreateArchive, ///< 'Cannot create archive' string.
        DialogTitle, ///< Dialog title.
        Name, ///< 'Name' string.
        Retry, ///< Retry button.
        YouCanChangeArchiveName ///< 'You can change archive name and try again.' string.
    };
    Q_ENUM(UserString)

    explicit CreationNameErrorPromptCore(const CreationNameErrorPromptCoreAux::Init& initData,
                                         CreationNameErrorPromptCoreAux::Backdoor& backdoorData,
                                         QObject *parent = nullptr);
    ~CreationNameErrorPromptCore();

    /// @brief Returns a translated string for specified ID.
    Q_INVOKABLE static QString getString(UserString value);

    /// @brief Returns archive name.
    Q_INVOKABLE QString archiveName() const;

    /// @brief Returns detailed information about the conflict.
    Q_INVOKABLE QString errorInfo() const;

    ///@{
    /// @name Actions
    /// @warning Calling Actions will also automatically destroy the object.

    /** @brief Retries to create the archive with a new name.
     */
    Q_INVOKABLE void retry(const QString &archiveName);

    /** @brief Aborts the task.
     */
    Q_INVOKABLE void abort();

    ///@}

signals:
    /// @brief Required for QML.
    void deleted();

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}
