// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QDateTime>
#include <QDir>

#include "../utils/boolflag.h"

#include "extractionfolderoverwritepromptcore.h"
#include "commonstrings.h"
#include "private/privatestrings.h"
#include "private/promptcore/extractionfolderoverwritepromptcoreaux.h"


namespace Packuru::Core
{

using ExtractionFolderOverwritePromptCoreAux::Init;
using ExtractionFolderOverwritePromptCoreAux::Backdoor;
using ExtractionFolderOverwritePromptCoreAux::Emitter;


struct ExtractionFolderOverwritePromptCore::Private
{
    Init initData;
    Utils::BoolFlag<false> finished;
    Emitter* emitter;
};


ExtractionFolderOverwritePromptCore::ExtractionFolderOverwritePromptCore(const Init& init,
                                                                         Backdoor& backdoor,
                                                                         QObject* parent)
    : QObject(parent),
      priv(new Private)
{
    priv->initData = init;

    priv->initData.source.setFilter(QDir::AllEntries | QDir::NoDotAndDotDot | QDir::Hidden);
    priv->initData.destination.setFilter(QDir::AllEntries | QDir::NoDotAndDotDot | QDir::Hidden);

    priv->emitter = new ExtractionFolderOverwritePromptCoreAux::Emitter(this);

    backdoor.emitter = priv->emitter;
}


ExtractionFolderOverwritePromptCore::~ExtractionFolderOverwritePromptCore()
{
    emit deleted();
}


void ExtractionFolderOverwritePromptCore::overwrite(bool applyToAll)
{
    if (!priv->finished.set())
        return;

    emit priv->emitter->writeIntoRequested(applyToAll);
}


void ExtractionFolderOverwritePromptCore::skip(bool applyToAll)
{
    if (!priv->finished.set())
        return;

    emit priv->emitter->skipRequested(applyToAll);
}


void ExtractionFolderOverwritePromptCore::retry()
{
    if (!priv->finished.set())
        return;

    emit priv->emitter->retryRequested();
}


void ExtractionFolderOverwritePromptCore::abort()
{
    if (!priv->finished.set())
        return;

    emit priv->emitter->abortRequested();
}


QString ExtractionFolderOverwritePromptCore::getString(ExtractionFolderOverwritePromptCore::UserString value)
{
    switch (value)
    {
    case UserString::Abort:
        return PrivateStrings::getString(PrivateStrings::UserString::Abort);
    case UserString::ApplyToAll:
        return PrivateStrings::getString(PrivateStrings::UserString::ApplyToAll);
    case UserString::Content:
        return Packuru::Core::ExtractionFolderOverwritePromptCore::tr("Content");
    case UserString::Destination:
        return PrivateStrings::getString(PrivateStrings::UserString::Destination);
    case UserString::Folder:
        return Packuru::Core::ExtractionFolderOverwritePromptCore::tr("Folder");
    case UserString::FolderAlreadyExists:
        return Packuru::Core::ExtractionFolderOverwritePromptCore::tr("Folder already exists");
    case UserString::Modified:
        return Packuru::Core::ExtractionFolderOverwritePromptCore::tr("Modified");
    case UserString::Retry:
        return PrivateStrings::getString(PrivateStrings::UserString::Retry);
    case UserString::Skip:
        return PrivateStrings::getString(PrivateStrings::UserString::Skip);
    case UserString::Source:
        return PrivateStrings::getString(PrivateStrings::UserString::Source);
    case UserString::WriteInto:
        return Packuru::Core::ExtractionFolderOverwritePromptCore::tr("Write into");
    default:
        Q_ASSERT(false); return "";
    }
}


QString ExtractionFolderOverwritePromptCore::sourcePath() const
{
    return priv->initData.source.absolutePath();
}


QString ExtractionFolderOverwritePromptCore::destinationPath() const
{
    return priv->initData.destination.absolutePath();
}


QString ExtractionFolderOverwritePromptCore::sourceContent() const
{
    return Packuru::Core::ExtractionFolderOverwritePromptCore::tr("%n items",
                                                                  "",
                                                                  static_cast<int>(priv->initData.source.count()));
}


QString ExtractionFolderOverwritePromptCore::destinationContent() const
{
    return Packuru::Core::ExtractionFolderOverwritePromptCore::tr("%n items",
                                                                  "",
                                                                  static_cast<int>(priv->initData.destination.count()));
}


QString ExtractionFolderOverwritePromptCore::sourceModifiedDate() const
{
    QFileInfo info(priv->initData.source.absolutePath());
    return info.lastModified().toString("yyyy/MM/dd HH:mm:ss");
}


QString ExtractionFolderOverwritePromptCore::destinationModifiedDate() const
{
    QFileInfo info(priv->initData.destination.absolutePath());
    return info.lastModified().toString("yyyy/MM/dd HH:mm:ss");
}

}
