// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QObject>

#include "symbol_export.h"

/// @file

class QFileInfo;


namespace Packuru::Core
{

namespace ExtractionFileOverwritePromptCoreAux
{
struct Init;
struct Backdoor;
}

/**
 @brief Contains data and logic necessary for Extraction File Overwrite dialog.

 Object of this class is emitted by StatusPageCore and TaskQueueModel objects
 and will be automatically destroyed after user action (overwrite(), skip(), retry() or abort())
 has been received.

 @warning The object must not be destroyed manually.

 @headerfile "core/extractionfileoverwritepromptcore.h"
 */
class PACKURU_CORE_EXPORT ExtractionFileOverwritePromptCore : public QObject
{
    Q_OBJECT

public:
    /// @brief Describes the type of the file.
    enum class FileType
    {
        File, ///< File.
        Link ///< Link.
    };
    Q_ENUM(FileType)

    /// @copydoc ArchivingDialogCore::UserString
    enum class UserString
    {
        ___INVALID, // Keep at index 0 to catch implicit conversions of invalid values to 0 in QML

        Abort, ///< 'Abort' button.
        ApplyToAll, ///< 'Apply to all' checkbox.
        Destination, ///< 'Destination' header label.
        File, ///< 'File' label.
        FileAlreadyExists, ///< 'File already exists' label.
        Location, ///< 'Location' label.
        Modified, ///< 'Modified' label.
        Overwrite, ///< 'Overwrite' button.
        Retry, ///< 'Retry' button.
        Size, ///< 'Size' label.
        Skip, ///< 'Skip' button.
        Source ///< 'Source' header label.
    };
    Q_ENUM(UserString)

    explicit ExtractionFileOverwritePromptCore(const ExtractionFileOverwritePromptCoreAux::Init& init,
                                               ExtractionFileOverwritePromptCoreAux::Backdoor& backdoor,
                                               QObject* parent = nullptr);

    ~ExtractionFileOverwritePromptCore() override;

    /// @copydoc ArchivingDialogCore::getString
    Q_INVOKABLE static QString getString(UserString value);

    /// @brief Returns file type.
    FileType getFileType() const;

    /// @brief Returns source file name.
    Q_INVOKABLE QString sourceName() const;

    /// @brief Returns destination file name.
    Q_INVOKABLE QString destinationName() const;

    /// @brief Returns source file path.
    Q_INVOKABLE QString sourceFilePath() const;

    /// @brief Returns destination file path.
    Q_INVOKABLE QString destinationFilePath() const;

    /// @brief Returns source file folder.
    Q_INVOKABLE QString sourceLocation() const;

    /// @brief Returns destination file folder.
    Q_INVOKABLE QString destinationLocation() const;

    /// @brief Returns source file modification time.
    Q_INVOKABLE QString sourceModifiedDate() const;

    /// @brief Returns destination file modification time.
    Q_INVOKABLE QString destinationModifiedDate() const;

    /// @brief Returns source file size.
    Q_INVOKABLE QString sourceSize() const;

    /// @brief Returns destination file size.
    Q_INVOKABLE QString destinationSize() const;

    ///@{
    /// @name Actions
    /// @warning Calling Actions will also automatically destroy the object.

    /**
     * @brief Overwrites destination file.
     * @param applyToAll Specifies whether all subsequent files should be automatically overwritten.
     */
    Q_INVOKABLE void overwrite(bool applyToAll);

    /**
     * @brief Skips the file.
     * @param applyToAll Specifies whether all subsequent files should be automatically skipped.
     */
    Q_INVOKABLE void skip(bool applyToAll);

    /**
     * @brief Retries to move the file to its destination.
     */
    Q_INVOKABLE void retry();

    /// @copydoc CreationNameErrorPromptCore::abort()
    Q_INVOKABLE void abort();

    ///@}

signals:
    /// @brief Required for QML.
    void deleted();

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}
