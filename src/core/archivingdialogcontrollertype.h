// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QObject>

#include "symbol_export.h"

/// @file

namespace Packuru::Core
{

/**
 @brief Identifies input widgets in Archiving Dialog.
 @headerfile "core/archivingdialogcontrollertype.h"
 */
class ArchivingDialogControllerType : public QObject
{
    Q_OBJECT
public:
    /// @copybrief Packuru::Core::ArchivingDialogControllerType
    enum class Type
    {
        AcceptButton, ///< Push button.
        ArchiveName, ///< Line edit.
        ArchiveNameError, ///< Label.
        ArchiveType, ///< Combo box.
        ArchivingMode, ///< Combo box.
        CompressionLevel, ///< Slider.
        CompressionState, ///< Label.
        DestinationMode, ///< Combo box.
        DestinationPath, ///< Line edit.
        DialogErrors, ///< Label.
        EncryptionState, ///< Label.
        DirectoryError, ///< Label.
        EncryptionMode, ///< Combo box.
        FilePaths, ///< Combo box.
        InternalPath, ///< Line edit.
        OpenDestinationPath, ///< Check box.
        OpenNewArchive, ///< Check box.
        Password, ///< Line edit.
        PasswordRepeat, ///< Line edit.
        PasswordMatch, ///< Label
        RunInQueue, ///< Combo box.
        SplitPreset, ///< Combo box.
        SplitSize, ///< Spin box.
    };
    Q_ENUM(Type)
};

PACKURU_CORE_EXPORT uint qHash(ArchivingDialogControllerType::Type value, uint seed = 0);

}
