// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QString>

#include "symbol_export.h"

/// @file

class QString;

namespace Packuru::Core
{

/** @brief Returns HTML link for a given address and label.
 *
 * It is useful for labels with clickable links.
 * Declared in core/htmllink.h header.
 */
PACKURU_CORE_EXPORT QString htmlLink(const QString& address, const QString& label = QString());

}
