// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../utils/private/enumhash.h"

#include "extractiondialogcontrollertype.h"


namespace Packuru::Core
{

uint qHash(ExtractionDialogControllerType::Type value, uint seed)
{
    return Utils::enumHash(value, seed);
}

}
