// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QDateTime>
#include <QFileInfo>

#include "../utils/boolflag.h"

#include "extractionfileoverwritepromptcore.h"
#include "globalsettingsmanager.h"
#include "private/globalsettings.h"
#include "commonstrings.h"
#include "private/digitalsize.h"
#include "private/privatestrings.h"
#include "private/promptcore/extractionfileoverwritepromptcoreaux.h"



namespace Packuru::Core
{

using ExtractionFileOverwritePromptCoreAux::Init;
using ExtractionFileOverwritePromptCoreAux::Backdoor;
using ExtractionFileOverwritePromptCoreAux::Emitter;


struct ExtractionFileOverwritePromptCore::Private
{
    Init initData;
    Utils::BoolFlag<false> finished;
    ExtractionFileOverwritePromptCore::FileType fileType;
    SizeUnits units;
    Emitter* emitter;
};



ExtractionFileOverwritePromptCore::ExtractionFileOverwritePromptCore(const Init& init,
                                                                     Backdoor& backdoor,
                                                                     QObject* parent)
    : QObject(parent),
      priv(new Private)
{
    priv->initData = init;

    const bool files = init.source.isFile() && init.destination.isFile();
    const bool links = init.source.isSymLink() && init.destination.isSymLink();


    Q_ASSERT(files || links);

    priv->fileType = files ? FileType::File : FileType::Link;
    priv->units = GlobalSettingsManager::instance()->getValueAs<SizeUnits>(GlobalSettings::Enum::AC_SizeUnits);
    priv->emitter = new ExtractionFileOverwritePromptCoreAux::Emitter(this);

    backdoor.emitter = priv->emitter;
}


ExtractionFileOverwritePromptCore::~ExtractionFileOverwritePromptCore()
{
    emit deleted();
}


QString ExtractionFileOverwritePromptCore::getString(UserString value)
{
    switch (value)
    {
    case UserString::Abort:
        return PrivateStrings::getString(PrivateStrings::UserString::Abort);
    case UserString::ApplyToAll:
        return PrivateStrings::getString(PrivateStrings::UserString::ApplyToAll);
    case UserString::Destination:
        return PrivateStrings::getString(PrivateStrings::UserString::Destination);
    case UserString::File:
        return Packuru::Core::ExtractionFileOverwritePromptCore::tr("File");
    case UserString::FileAlreadyExists:
        return Packuru::Core::ExtractionFileOverwritePromptCore::tr("File already exists");
    case UserString::Location:
        return Packuru::Core::ExtractionFileOverwritePromptCore::tr("Location");
    case UserString::Modified:
        return Packuru::Core::ExtractionFileOverwritePromptCore::tr("Modified");
    case UserString::Overwrite:
        return Packuru::Core::ExtractionFileOverwritePromptCore::tr("Overwrite");
    case UserString::Retry:
        return PrivateStrings::getString(PrivateStrings::UserString::Retry);
    case UserString::Size:
        return Packuru::Core::ExtractionFileOverwritePromptCore::tr("Size");
    case UserString::Skip:
        return PrivateStrings::getString(PrivateStrings::UserString::Skip);
    case UserString::Source:
        return PrivateStrings::getString(PrivateStrings::UserString::Source);
    default:
        Q_ASSERT(false); return "";
    }
}


void ExtractionFileOverwritePromptCore::overwrite(bool applyToAll)
{
    if (!priv->finished.set())
        return;

    emit priv->emitter->overwriteRequested(applyToAll);
}


void ExtractionFileOverwritePromptCore::skip(bool applyToAll)
{
    if (!priv->finished.set())
        return;

    emit priv->emitter->skipRequested(applyToAll);
}


void ExtractionFileOverwritePromptCore::retry()
{
    if (!priv->finished.set())
        return;

    emit priv->emitter->retryRequested();
}


void ExtractionFileOverwritePromptCore::abort()
{
    if (!priv->finished.set())
        return;

    emit priv->emitter->abortRequested();
}


ExtractionFileOverwritePromptCore::FileType ExtractionFileOverwritePromptCore::getFileType() const
{
    return priv->fileType;
}


QString ExtractionFileOverwritePromptCore::sourceName() const
{
    return priv->initData.source.fileName();
}


QString ExtractionFileOverwritePromptCore::destinationName() const
{
    return priv->initData.destination.fileName();
}


QString ExtractionFileOverwritePromptCore::sourceFilePath() const
{
    return priv->initData.source.absoluteFilePath();
}


QString ExtractionFileOverwritePromptCore::destinationFilePath() const
{
    return priv->initData.destination.absoluteFilePath();
}


QString ExtractionFileOverwritePromptCore::sourceLocation() const
{
    return priv->initData.source.absolutePath();
}


QString ExtractionFileOverwritePromptCore::destinationLocation() const
{
    return priv->initData.destination.absolutePath();
}


QString ExtractionFileOverwritePromptCore::sourceModifiedDate() const
{
    return priv->initData.source.lastModified().toString("yyyy/MM/dd HH:mm:ss");
}


QString ExtractionFileOverwritePromptCore::destinationModifiedDate() const
{
    return priv->initData.destination.lastModified().toString("yyyy/MM/dd HH:mm:ss");
}


QString ExtractionFileOverwritePromptCore::sourceSize() const
{
    return DigitalSize::toString(static_cast<qulonglong>(priv->initData.source.size()), priv->units);
}


QString ExtractionFileOverwritePromptCore::destinationSize() const
{
    return DigitalSize::toString(static_cast<qulonglong>(priv->initData.destination.size()), priv->units);
}

}
