// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QObject>

#include "symbol_export.h"

/// @file

namespace Packuru::Core
{

/** @brief Contains strings that might not be available in any core object.
 */
class PACKURU_CORE_EXPORT CommonStrings : public QObject
{
    Q_OBJECT
public:
    enum class UserString
    {
        ___INVALID,

        Add, ///< "Add files to list" button.
        ArchivePropertiesDialogTitle, ///< Archive Properties dialog title.
        Back, ///< Back button.
        Cancel, ///< Cancel button.
        Close, ///< Close button.
        CommandLineErrorDialogTitle, ///< Command Line Error dialog title.
        CommandLineHelpDialogTitle, ///< Command Line Helpdialog title.
        Comment, ///< For Archive Properties dialog Comment tab.
        ErrorLogDialogTitle, ///< Error Log dialog title.
        Remove, ///< "Remove files from list" button.
    };
    Q_ENUM(UserString)

    explicit CommonStrings(QObject *parent = nullptr);

    /**
     @brief Returns a translated string for specified ID.
     @param value ID of the string.
     @param count Required for strings that contain numbers.
     */
    Q_INVOKABLE static QString getString(UserString value, int count = 0);
};

}
