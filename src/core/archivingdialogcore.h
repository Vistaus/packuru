// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QObject>
#include <QModelIndexList>
#include <QUrl>

#include "symbol_export.h"

/// @file

class QAbstractItemModel;


namespace qcs::core
{
class ControllerEngine;
class ControllerIdRowMapper;
}


namespace Packuru::Core
{

struct ArchivingDialogCoreInitAdd;
struct ArchivingDialogCoreInitCreate;

/**
 @brief Contains data and logic necessary for Archiving Dialog.

 Object of this class is provided by ArchiveBrowserCore and QueueCore objects.

 @headerfile "core/archivingdialogcore.h"
 */
class PACKURU_CORE_EXPORT ArchivingDialogCore : public QObject
{
    Q_OBJECT
public:
    /** @brief Describes the mode of the dialog

     The mode is set at construction time and does not change.
     */
    enum class DialogMode
    {
        AddToArchive, ///< 'Add to archive' mode (in Browser)
        CreateArchive ///< 'Create archive' mode (in Queue)
    };
    Q_ENUM(DialogMode)

    /**
     @brief Identifies strings to display in the dialog.
     */
    enum class UserString
    {
        ___INVALID, // Keep at index 0 to catch implicit conversions of invalid values to 0 in QML

        AddFiles, ///< 'Add files' button.
        AddFolder, ///< 'Add folders' button.
        Cancel, ///< 'Cancel' button.
        Compression, ///< 'Compression' header label.
        DialogTitleAdd, ///< Dialog title in 'Add to archive' mode.
        DialogTitleCreate, ///< Dialog title in 'Create archive' mode.
        Encryption, ///< 'Encryption' header label.
        FilePaths, ///< 'File paths' header label.
        Miscellaneous, ///< 'Miscellaneous' header label.
        OnCompletion, ///< 'On completion' header label.
        PluginSettings, ///< 'Plugin settings' header label.
        RemoveItems, ///< 'Remove items' button.
        TabAdvanced, ///< 'Advanced' tab title.
        TabArchive, ///< 'Archive' tab title.
        TabFiles, ///< 'Files' tab title.
        TabOptions, ///< 'Options' tab title.
    };
    Q_ENUM(UserString)

    // 'Add to archive' constructor
    explicit ArchivingDialogCore(ArchivingDialogCoreInitAdd initData, QObject *parent = nullptr);

    // 'Create archive' constructor
    explicit ArchivingDialogCore(ArchivingDialogCoreInitCreate initData, QObject *parent = nullptr);

    ~ArchivingDialogCore() override;

    /// @brief Allows destruction from QML
    Q_INVOKABLE void destroyLater();

    /// @brief Returns a translated string for specified ID.
    Q_INVOKABLE static QString getString(UserString value);

    /** @brief Returns the input files model.
     @note Object ownership is not transferred to the caller.
     */
    Q_INVOKABLE QAbstractItemModel* getFileModel() const;

    /** @brief Returns the mode of the dialog.

     This function can be useful for fine-tuning the appareance of the dialog depending
     on its mode.
     */
    Q_INVOKABLE DialogMode getMode() const;

    /**
     @brief Returns pointer to ControllerEngine controlling most input widgets in the dialog.

     The widgets must be mapped in WidgetMapper using ArchivingDialogControllerType::Type enum as the ID.

     @note Object ownership is not transferred to the caller.
     @sa qcs::qqc::WidgetMapper, qcs::qtw::WidgetMapper
     */
    Q_INVOKABLE qcs::core::ControllerEngine* getControllerEngine() const;

    /**
     @brief Returns ControllerIdRowMapper that contains logic for current plugin's input widgets.

     Pass ControllerIdRowMapper object to %PageStack so the widgets can be generated.

     When archive type changes in the dialog the ControllerIdRowMapper object also changes.
     The change is signaled by pluginRowMapperChanged() signal.

     @note Object ownership is not transferred to the caller.
     @sa PageStack, qcs::qtw::PageStack
     */
    Q_INVOKABLE qcs::core::ControllerIdRowMapper* getPluginRowMapper() const;

    /// @brief Adds input items to input files model.
    Q_INVOKABLE void addItems(const QList<QUrl>& newItems);

    /// @brief Removes items corresponding to specified indexes from input files model.
    Q_INVOKABLE void removeItems(const QModelIndexList& indexes);

    /**
     @brief Accepts the dialog.

     After the dialog has been accepted the archives are processed internally by the application.
     @warning The dialog must not contain any errors to be accepted.
     @note The object must be destroyed manually after it has been accepted.
     */
    Q_INVOKABLE void accept();

signals:
    /** @brief Signals that ControllerIdRowMapper has changed.

     This happens when archive type changes.
     */
    void pluginRowMapperChanged();

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}
