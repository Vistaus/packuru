// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../core/private/plugin-api/backendhandlererror.h"

#include "compressionhandlerpbzip2.h"
#include "compressioninitdata.h"
#include "decompressioninitdata.h"
#include "tocliarg.h"
#include "compressionhandlertype.h"


using namespace Packuru::Core;


namespace
{
const QString execName("pbzip2");
}


namespace Packuru::Plugins::GnuTar::Core
{

CompressionHandlerPBzip2::CompressionHandlerPBzip2(QObject* parent)
    : CompressionHandler(CompressionHandlerType::Pbzip2, parent)
{

}


QString CompressionHandlerPBzip2::executableName()
{
    return execName;
}


void CompressionHandlerPBzip2::compress(const CompressionInitData& data)
{
    QStringList args;
    args << QLatin1String("-z") // Compress
         << QLatin1String("-c") // from stdin to stdout
         << toCLIargCompressionLevel(data.compressionLevel, handlerType)
         << toCLIargThreadCount(data.requestedThreadCount, handlerType);

    process->setStandardOutputFile(data.archiveInfo.absoluteFilePath());
    process->start(execName, args);
}


void CompressionHandlerPBzip2::decompress(const DecompressionInitData& data)
{
    QStringList args;
    args << QLatin1String("-dc")
         << toCLIargThreadCount(data.requestedThreadCount, handlerType)
         << data.archiveInfo.absoluteFilePath();

    process->start(execName, args);
}


void CompressionHandlerPBzip2::processErrorLineHandler(const std::vector<QStringRef>& lines)
{
    for (const auto& line : lines)
    {
        if (line.startsWith(QLatin1String("pbzip2: *ERROR: Data integrity (CRC) error in data!"))
                || line.startsWith(QLatin1String("pbzip2: *ERROR: File ends unexpectedly!")))
            emit error(BackendHandlerError::DataError);
        else if (line == QLatin1String("pbzip2: *ERROR: system call failed with errno=[28: No space left on device]!"))
            emit error(BackendHandlerError::DiskFull);
    }
}

}
