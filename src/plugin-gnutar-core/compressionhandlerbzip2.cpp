// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../core/private/plugin-api/backendhandlererror.h"

#include "compressionhandlerbzip2.h"
#include "compressioninitdata.h"
#include "decompressioninitdata.h"
#include "tocliarg.h"
#include "compressionhandlertype.h"


using namespace Packuru::Core;


namespace
{
const QString execName("bzip2");
}


namespace Packuru::Plugins::GnuTar::Core
{

CompressionHandlerBzip2::CompressionHandlerBzip2(QObject* parent)
    : CompressionHandler(CompressionHandlerType::Bzip2 ,parent)
{

}


QString CompressionHandlerBzip2::executableName()
{
    return execName;
}


void CompressionHandlerBzip2::compress(const CompressionInitData& data)
{
    QStringList args;
    args << QLatin1String("-z") // Compress
         << QLatin1String("-c") // from stdin to stdout
         << toCLIargCompressionLevel(data.compressionLevel, handlerType);

    process->setStandardOutputFile(data.archiveInfo.absoluteFilePath());
    process->start(execName, args);
}


void CompressionHandlerBzip2::decompress(const DecompressionInitData& data)
{
    QStringList args;
    args << QLatin1String("-dc") << data.archiveInfo.absoluteFilePath();

    process->start(execName, args);
}


void CompressionHandlerBzip2::processErrorLineHandler(const std::vector<QStringRef>& lines)
{
    for (const auto& line : lines)
    {
        if (line == QLatin1String("bzip2: Data integrity error when decompressing.")
                || line == QLatin1String("bzip2: Compressed file ends unexpectedly;"))
            emit error(BackendHandlerError::DataError);
        else if (line == QLatin1String("bzip2: No space left on device"))
            emit error(BackendHandlerError::DiskFull);
    }
}

}
