// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <unordered_map>

#include <QVariant>


namespace Packuru::Plugins::GnuTar::Core
{

enum class PrivateExtractionParameter;

using PrivateExtractionParameterMap = std::unordered_map<PrivateExtractionParameter, QVariant>;

}

Q_DECLARE_METATYPE(Packuru::Plugins::GnuTar::Core::PrivateExtractionParameterMap);

