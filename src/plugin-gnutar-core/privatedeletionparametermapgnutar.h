// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <unordered_map>

#include <QVariant>


namespace Packuru::Plugins::GnuTar::Core
{

enum class PrivateDeletionParameter;

using PrivateDeletionParameterMap = std::unordered_map<PrivateDeletionParameter, QVariant>;

}

Q_DECLARE_METATYPE(Packuru::Plugins::GnuTar::Core::PrivateDeletionParameterMap);

