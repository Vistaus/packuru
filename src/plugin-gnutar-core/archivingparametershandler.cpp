// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QVariant>

#include "../qcs-core/controllerengine.h"
#include "../qcs-core/controlleridrowmapper.h"

#include "../core/private/plugin-api/archivingcompressioninfo.h"
#include "../core/private/plugin-api/archivingfilepaths.h"
#include "../core/private/plugin-api/archivetype.h"

#include "archivingparametershandler.h"
#include "privatearchivingparameter.h"
#include "privatearchivingparametermapgnutar.h"
#include "sparsefilesctrl.h"
#include "tarformatctrl.h"
#include "getcompressioninfofortype.h"
#include "threadcountctrl.h"
#include "createcompressionhandler.h"
#include "compressionhandlertype.h"


using namespace Packuru::Core;


namespace Packuru::Plugins::GnuTar::Core
{

ArchivingParametersHandler::ArchivingParametersHandler(const ArchivingParameterHandlerInitData& initData,
                                                       QObject* parent)
    : ArchivingParameterHandlerBase(initData, parent)
{
    compressionInfo = getCompressionInfoForType(initData.archiveType);

    if (initData.creatingNewArchive)
    {
        auto formatCtrl = controllerEngine
                ->createTopController<TarFormatCtrl>(PrivateArchivingParameter::TarFormat);

        controllerRowMapper
                ->addController(Packuru::Plugins::GnuTar::Core::ArchivingParametersHandler
                                ::tr("Tar format"),
                                PrivateArchivingParameter::TarFormat);

        // Sparse files are not supported in v7 and ustar formats; Also sparse files detection
        // must be disabled when adding files to existing archive as we don't know its exact format.
        auto sparseFilesCtrl = controllerEngine
                ->createController<SparseFilesCtrl>(PrivateArchivingParameter::SparseFileDetection);

        controllerRowMapper
                ->addController(Packuru::Plugins::GnuTar::Core::ArchivingParametersHandler
                                ::tr("Detect sparse files"),
                                PrivateArchivingParameter::SparseFileDetection);

        formatCtrl->addDependant(sparseFilesCtrl);

        if (initData.archiveType != ArchiveType::tar)
        {
            controllerEngine->createTopController<ThreadCountCtrl>(PrivateArchivingParameter::ThreadCount,
                                                                   initData.archiveType);

            controllerRowMapper
                    ->addController(Packuru::Plugins::GnuTar::Core::ArchivingParametersHandler
                                    ::tr("Thread count"),
                                    PrivateArchivingParameter::ThreadCount);
        }
    }



    controllerEngine->updateAllControllers();
}


ArchivingParametersHandler::~ArchivingParametersHandler()
{

}


ArchivingCompressionInfo ArchivingParametersHandler::getCompressionInfo() const
{
    return compressionInfo;
}


bool ArchivingParametersHandler::archiveSplittingSupport() const
{
    if (initData.archiveType == ArchiveType::tar_lzip)
    {
        const CompressionHandlerExecInfo info = getCompressionHandlerExecInfo(CompressionHandlerType::Plzip);

        // lzip supports splitting but is single-threaded
        // plzip does not support splitting but is multi-threaded
        // lzip automatically invokes plzip if available
        // solution: if plzip is installed splitting is disabled but in exchange thread count can be set
        return !info.available;
    }

    return false;
}


void ArchivingParametersHandler::setCompressionLevel(int value)
{
    compressionInfo.setCurrent(value);
}


QVariant ArchivingParametersHandler::getPrivateArchivingData() const
{
    auto map = controllerEngine->getAvailableCurrentValues<PrivateArchivingParameter>();
    map[PrivateArchivingParameter::CompressionLevel] = compressionInfo.getCurrent();

    QVariant result;
    result.setValue(map);

    return result;
}


std::unordered_set<ArchivingFilePaths> ArchivingParametersHandler::getAdditionalSupportedFilePaths() const
{
    return {ArchivingFilePaths::AbsolutePaths, ArchivingFilePaths::FullPaths};
}

}
