// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QMetaType>
#include <QString>


namespace Packuru::Plugins::GnuTar::Core
{

enum class CompressionHandlerType
{
    Brotli,
    Bzip2,
    Compress,
    Gzip,
    Lrzip,
    Lz4,
    Lzip,
    Lzop,
    Pbzip2,
    Pigz,
    Pixz,
    Plzip,
    Xz,
    Zstd,

    ___INVALID
};

CompressionHandlerType fromExecName(const QString& name);

}

Q_DECLARE_METATYPE(Packuru::Plugins::GnuTar::Core::CompressionHandlerType);
