// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QStorageInfo>

#include "compressionhandler.h"


namespace Packuru::Plugins::GnuTar::Core
{

class CompressionHandlerLz4: public CompressionHandler
{
    Q_OBJECT
public:
    CompressionHandlerLz4(QObject* parent = nullptr);

    static QString executableName();

    void compress(const CompressionInitData& data) override;
    void decompress(const DecompressionInitData& data) override;

private:
    void processErrorLineHandler(const std::vector<QStringRef>& lines) override;

    QStorageInfo storageInfo;
};

}
