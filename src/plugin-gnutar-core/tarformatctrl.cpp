// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "tarformatctrl.h"


using namespace qcs::core;
using Packuru::Plugins::GnuTar::Core::TarFormat;


namespace Packuru::Plugins::GnuTar::Core
{

using Core::TarFormat;


TarFormatCtrl::TarFormatCtrl()
{
    setControllerType(ControllerType::Type::ComboBox);

    auto pair = createLists(
    { {"gnu", TarFormat::Gnu},
      {"old gnu", TarFormat::OldGnu},
      {"pax", TarFormat::Pax},
      {"ustar", TarFormat::UStar},
      {"v7", TarFormat::V7} });

    setValueLists(pair.first, pair.second);

    setCurrentValueFromPrivate(TarFormat::Pax);
}

}
