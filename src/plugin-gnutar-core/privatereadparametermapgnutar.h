// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <unordered_map>

#include <QVariant>


namespace Packuru::Plugins::GnuTar::Core
{

enum class PrivateReadParameter;

using PrivateReadParameterMap = std::unordered_map<PrivateReadParameter, QVariant>;

}

Q_DECLARE_METATYPE(Packuru::Plugins::GnuTar::Core::PrivateReadParameterMap);

