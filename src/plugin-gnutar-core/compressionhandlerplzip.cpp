// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../core/private/plugin-api/backendhandlererror.h"
#include "../core/private/plugin-api/multipartarchivehelper.h"
#include "../core/private/plugin-api/archivetype.h"

#include "compressionhandlerplzip.h"
#include "compressioninitdata.h"
#include "decompressioninitdata.h"
#include "tocliarg.h"
#include "compressionhandlertype.h"


using namespace Packuru::Core;


namespace
{
const QString execName("plzip");
}


namespace Packuru::Plugins::GnuTar::Core
{

CompressionHandlerPlzip::CompressionHandlerPlzip(QObject* parent)
    : CompressionHandler(CompressionHandlerType::Plzip, parent)
{

}


QString CompressionHandlerPlzip::executableName()
{
    return execName;
}


void CompressionHandlerPlzip::compress(const CompressionInitData& data)
{
    QStringList args;
    args << toCLIargCompressionLevel(data.compressionLevel, handlerType);

    QString archivePath = data.archiveInfo.absoluteFilePath();
    if (data.partSize > 0)
    {
        args << toCLIargPartSize(data.partSize, handlerType);

        archivePath = data.archiveInfo.absoluteFilePath();
        Q_ASSERT(archivePath.endsWith(QLatin1String(".tar.lz")));
        archivePath.chop(2);
        archivePath.append(QLatin1String("part"));
    }

    args << toCLIargThreadCount(data.requestedThreadCount, handlerType)
         << QLatin1String("--output=") + archivePath;

    process->start(execName, args);
}


void CompressionHandlerPlzip::decompress(const DecompressionInitData& data)
{
    QStringList args;

    args << QLatin1String("-dc")
         << toCLIargThreadCount(data.requestedThreadCount, handlerType);

    if (isMultiPartArchiveName(data.archiveInfo.absoluteFilePath(), ArchiveType::tar_lzip))
    {
        const auto partPaths = getArchiveExistingPartPaths(data.archiveInfo.absoluteFilePath(), ArchiveType::tar_lzip);

        for (const QString& path : partPaths)
            args << path;
    }
    else
        args << data.archiveInfo.absoluteFilePath();

    process->start(execName, args);
}


void CompressionHandlerPlzip::processErrorLineHandler(const std::vector<QStringRef>& lines)
{
    for (const auto& line : lines)
    {
        if (line.contains(": Decoder error at pos ")
                || line.contains("Member size mismatch")
                || line.contains("Data size mismatch")
                || line.contains(": File ends unexpectedly at pos ")
                || line.endsWith(QLatin1String(": File ends unexpectedly at member header.")))
            emit error(BackendHandlerError::DataError);
        else if (line.endsWith(QLatin1String("No space left on device")))
            emit error(BackendHandlerError::DiskFull);
    }
}

}
