// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../core/private/plugin-api/archivetype.h"
#include "../core/private/plugin-api/backendhandlererror.h"

#include "compressionhandlerpixz.h"
#include "compressioninitdata.h"
#include "decompressioninitdata.h"
#include "tocliarg.h"
#include "compressionhandlertype.h"


using namespace Packuru::Core;


namespace
{
const QString execName("pixz");
}


namespace Packuru::Plugins::GnuTar::Core
{

CompressionHandlerPixz::CompressionHandlerPixz(QObject* parent)
    : CompressionHandler(CompressionHandlerType::Pixz, parent)
{

}


QString CompressionHandlerPixz::executableName()
{
    return execName;
}


void CompressionHandlerPixz::compress(const CompressionInitData& data)
{
    Q_ASSERT(data.archiveType == ArchiveType::tar_xz);

    QStringList args;
    args << toCLIargCompressionLevel(data.compressionLevel, handlerType)
         << QLatin1String("-t") // use non-tar mode, https://github.com/vasi/pixz/issues/65
         << toCLIargThreadCount(data.requestedThreadCount, handlerType);

    process->setStandardOutputFile(data.archiveInfo.absoluteFilePath());
    process->start(execName, args);
}


void CompressionHandlerPixz::decompress(const DecompressionInitData& data)
{
    Q_ASSERT(data.archiveType == ArchiveType::tar_xz);

    QStringList args;

    args << QLatin1String("-d")
         << toCLIargThreadCount(data.requestedThreadCount, handlerType);

    process->setStandardInputFile(data.archiveInfo.absoluteFilePath());
    process->start(execName, args);
}


void CompressionHandlerPixz::processErrorLineHandler(const std::vector<QStringRef>& lines)
{
    for (const auto& line : lines)
    {
        if (line == QLatin1String("Error decoding file index block header")
                || line == QLatin1String("Empty input")
                || line == QLatin1String("Error decoding stream footer"))
            emit error(BackendHandlerError::DataError);
//        This message is too ambigous to handle it
//        else if (line.endsWith(QLatin1String("Error writing block data")))
//            emit error(BackendHandlerError::DiskFull);
    }
}

}
