// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QStringList>


namespace Packuru::Plugins::GnuTar::Core
{

enum class TarFormat;
enum class CompressionHandlerType;

QString toCLIArgSparseFiles(bool value);
QString toCLIargTarFormat(TarFormat format);
QString toCLIargPartSize(qulonglong size, CompressionHandlerType compressionHandler);
QStringList toCLIargCompressionLevel(int level, CompressionHandlerType compressionHandler);
QString toCLIargThreadCount(int level, CompressionHandlerType compressionHandler);

}
