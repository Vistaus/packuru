// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../core/private/datastream/unordered_map_datastream.h"

#include "registerstreamoperators.h"
#include "privatearchivingparametermapgnutar.h"
#include "privatedeletionparametermapgnutar.h"
#include "privateextractionparametermapgnutar.h"
#include "privatereadparametermapgnutar.h"
#include "privatetestparametermapgnutar.h"
#include "tarformat.h"
#include "compressionhandlertype.h"


using Packuru::Core::operator<<;
using Packuru::Core::operator>>;

using Packuru::Plugins::GnuTar::Core::TarFormat;


namespace Packuru::Plugins::GnuTar::Core
{

void registerStreamOperators()
{
    qRegisterMetaTypeStreamOperators<PrivateArchivingParameterMap>("PrivateArchivingParameterMapGnuTar");
    qRegisterMetaTypeStreamOperators<PrivateDeletionParameterMap>("PrivateDeletionParameterMapGnuTar");
    qRegisterMetaTypeStreamOperators<PrivateExtractionParameterMap>("PrivateExtractionParameterMapGnuTar");
    qRegisterMetaTypeStreamOperators<PrivateReadParameterMap>("PrivateReadParameterMapGnuTar");
    qRegisterMetaTypeStreamOperators<PrivateTestParameterMap>("PrivateTestParameterMapGnuTar");
    qRegisterMetaTypeStreamOperators<CompressionHandlerType>("CompressionHandlerType");
    qRegisterMetaTypeStreamOperators<TarFormat>("TarFormat");
}

}
