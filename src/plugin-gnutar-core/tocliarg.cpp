// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "tocliarg.h"
#include "tarformat.h"
#include "compressionhandlertype.h"


namespace Packuru::Plugins::GnuTar::Core
{

QString toCLIArgSparseFiles(bool value)
{
    if (value)
        return QLatin1String("-S");
    else
        return QString();
}


QString toCLIargTarFormat(TarFormat format)
{
    switch (format)
    {
    case TarFormat::Gnu: return "gnu";
    case TarFormat::OldGnu: return "oldgnu";
    case TarFormat::Pax: return "pax";
    case TarFormat::UStar: return "ustar";
    case TarFormat::V7: return  "v7";
    default: Q_ASSERT(false); return "";
    }
}


QString toCLIargPartSize(qulonglong size, CompressionHandlerType compressionHandler)
{
    switch (compressionHandler)
    {
    case CompressionHandlerType::Brotli:
    case CompressionHandlerType::Bzip2:
    case CompressionHandlerType::Compress:
    case CompressionHandlerType::Gzip:
    case CompressionHandlerType::Lrzip:
    case CompressionHandlerType::Lz4:
    case CompressionHandlerType::Lzop:
    case CompressionHandlerType::Pbzip2:
    case CompressionHandlerType::Pigz:
    case CompressionHandlerType::Pixz:
    case CompressionHandlerType::Plzip: // It doesn't seem o support splitting
    case CompressionHandlerType::Xz:
    case CompressionHandlerType::Zstd:
        return "";
    case CompressionHandlerType::Lzip:
        return QLatin1String("--volume-size=") + QString::number(size);

    default: Q_ASSERT(false); return "";
    }
}


QStringList toCLIargCompressionLevel(int level, CompressionHandlerType compressionHandler)
{
    switch (compressionHandler)
    {
    case CompressionHandlerType::Brotli:
        return {QLatin1String("--quality=") + QString::number(level)};
    case CompressionHandlerType::Bzip2:
    case CompressionHandlerType::Gzip:
    case CompressionHandlerType::Lz4:
    case CompressionHandlerType::Lzip:
    case CompressionHandlerType::Lzop:
    case CompressionHandlerType::Pbzip2:
    case CompressionHandlerType::Pigz:
    case CompressionHandlerType::Pixz:
    case CompressionHandlerType::Plzip:
    case CompressionHandlerType::Xz:
        return {QLatin1String("-") + QString::number(level)};
    case CompressionHandlerType::Compress:
        return {};
    case CompressionHandlerType::Lrzip:
        return {QLatin1String("-L ") + QString::number(level)};
    case CompressionHandlerType::Zstd:
    {
        QStringList list;
        if (level > 19)
           list << QLatin1String("--ultra");
        return list << QLatin1String("-") + QString::number(level);

    }
    default: Q_ASSERT(false); return QStringList();
    }
}

QString toCLIargThreadCount(int count, CompressionHandlerType compressionHandler)
{
    switch (compressionHandler)
    {
    case CompressionHandlerType::Brotli:
    case CompressionHandlerType::Bzip2:
    case CompressionHandlerType::Compress:
    case CompressionHandlerType::Gzip:
    case CompressionHandlerType::Lz4:
    case CompressionHandlerType::Lzip:
    case CompressionHandlerType::Lzop:
        return QString();
    case CompressionHandlerType::Lrzip:
    case CompressionHandlerType::Pbzip2:
    case CompressionHandlerType::Pigz:
    case CompressionHandlerType::Pixz:
        return QLatin1String("-p") + QString::number(count);
    case CompressionHandlerType::Plzip:
        return QLatin1String("--threads=") + QString::number(count);
    case CompressionHandlerType::Xz:
    case CompressionHandlerType::Zstd:
        return QLatin1String("-T") + QString::number(count);
    default:
        Q_ASSERT(false);
        return QString();
    }
}


}
