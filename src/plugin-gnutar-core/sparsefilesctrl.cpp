// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../utils/qvariant_utils.h"

#include "sparsefilesctrl.h"
#include "tarformat.h"
#include "privatearchivingparameter.h"


using namespace qcs::core;


namespace Packuru::Plugins::GnuTar::Core
{

SparseFilesCtrl::SparseFilesCtrl()
    : userValue(false)
{
    setControllerType(ControllerType::Type::CheckBox);
    setCurrentValue(userValue);
}


void SparseFilesCtrl::update()
{
    const auto tarFormat = getControllerPropertyAs<TarFormat>(PrivateArchivingParameter::TarFormat,
                                                              ControllerProperty::PrivateCurrentValue);

    switch (tarFormat)
    {
    case TarFormat::Gnu:
    case TarFormat::OldGnu:
    case TarFormat::Pax:
        setCurrentValue(userValue);
        setEnabled(true);
        break;
    case TarFormat::V7:
    case TarFormat::UStar:
        setCurrentValue(false);
        setEnabled(false);
        break;
    default:
        Q_ASSERT(false);
    }
}


void SparseFilesCtrl::onValueSyncedToWidget(const QVariant& value)
{
    userValue = Utils::getValueAs<bool>(value);
}

}
