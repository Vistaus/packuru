// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "../core/private/plugin-api/archivingparameterhandlerbase.h"
#include "../core/private/plugin-api/archivingcompressioninfo.h"


namespace Packuru::Plugins::GnuTar::Core
{

class ArchivingParametersHandler : public Packuru::Core::ArchivingParameterHandlerBase
{
    Q_OBJECT
public:
    ArchivingParametersHandler(const Packuru::Core::ArchivingParameterHandlerInitData& initData,
                                     QObject *parent = nullptr);
    ~ArchivingParametersHandler() override;

    Packuru::Core::ArchivingCompressionInfo getCompressionInfo() const override;
    bool archiveSplittingSupport() const override;
    void setCompressionLevel(int value) override;
    QVariant getPrivateArchivingData() const override;

protected:
    std::unordered_set<Packuru::Core::ArchivingFilePaths> getAdditionalSupportedFilePaths() const override;

    Packuru::Core::ArchivingCompressionInfo compressionInfo;
};

}
