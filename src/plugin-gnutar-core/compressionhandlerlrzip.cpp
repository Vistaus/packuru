// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../core/private/plugin-api/backendhandlererror.h"

#include "compressionhandlerlrzip.h"
#include "compressioninitdata.h"
#include "decompressioninitdata.h"
#include "tocliarg.h"
#include "compressionhandlertype.h"


using namespace Packuru::Core;


namespace
{
const QString execName("lrzip");
}


namespace Packuru::Plugins::GnuTar::Core
{

CompressionHandlerLrzip::CompressionHandlerLrzip(QObject* parent)
    : CompressionHandler(CompressionHandlerType::Lrzip, parent)
{

}


QString CompressionHandlerLrzip::executableName()
{
    return execName;
}


void CompressionHandlerLrzip::compress(const CompressionInitData& data)
{
    // Encryption is not supported when reading from stdin

    QStringList args;
    args << toCLIargCompressionLevel(data.compressionLevel, handlerType)
         << toCLIargThreadCount(data.requestedThreadCount, handlerType)
         << QLatin1String("-o") << data.archiveInfo.absoluteFilePath();

    /* As of version version 0.631 lrzip does not report disk full when compressing and
     * the process returns 0 (SUCCESS) when redirecting standard output to a file.
     * It also continues compression despite the disk being full.
     *
     * The workaround is to use "-o" switch so lrzip writes the file directly.
     *
     * Bug report: https://github.com/ckolivas/lrzip/issues/172 (FIXED)
     */

    //process->setStandardOutputFile(data.archiveInfo.absoluteFilePath());

    process->start(execName, args);
}


void CompressionHandlerLrzip::decompress(const DecompressionInitData& data)
{
    QStringList args;
    args << QLatin1String("-d")
         << toCLIargThreadCount(data.requestedThreadCount, handlerType);

    // Encryption is not supported when reading from stdin
    process->setStandardInputFile(data.archiveInfo.absoluteFilePath());
    process->start(execName, args);
}


void CompressionHandlerLrzip::processErrorLineHandler(const std::vector<QStringRef>& lines)
{
    for (const auto& line : lines)
    {
        if (line.startsWith(QLatin1String("Failed to read_stream "))
                || line.startsWith(QLatin1String("Failed to decompress buffer"))
                || line == QLatin1String("Reached end of file on STDIN prematurely on v05 magic read")
                || line == QLatin1String("MD5 CHECK FAILED.")
                || line == QLatin1String("zpipe error: archive corrupted")) // zpaq
            emit error(BackendHandlerError::DataError);
        else if (line == QLatin1String("Unable to work from STDIN while reading password")
                 || line == QLatin1String("Cannot decompress encrypted file from STDIN"))
            emit error((BackendHandlerError::ArchivingMethodNotSupported));
        else if (line == QLatin1String("No space left on device"))
            emit error(BackendHandlerError::DiskFull);
    }
}

}
