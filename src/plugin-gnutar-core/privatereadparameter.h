// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QMetaType>


namespace Packuru::Plugins::GnuTar::Core
{

enum class PrivateReadParameter
{
    CompressionHandlerType, // CompressionHandlerType
};

}

Q_DECLARE_METATYPE(Packuru::Plugins::GnuTar::Core::PrivateReadParameter);
