// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QThread>

#include "core/private/plugin-api/archivetype.h"

#include "compressioninitdata.h"


namespace Packuru::Plugins::GnuTar::Core
{

CompressionInitData::CompressionInitData()
    : archiveType(Packuru::Core::ArchiveType::___NOT_SUPPORTED),
      partSize(0),
      compressionLevel(1),
      requestedThreadCount(QThread::idealThreadCount())
{

}

}
