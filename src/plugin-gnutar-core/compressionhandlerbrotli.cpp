// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../core/private/plugin-api/backendhandlererror.h"

#include "compressionhandlerbrotli.h"
#include "compressioninitdata.h"
#include "decompressioninitdata.h"
#include "tocliarg.h"
#include "compressionhandlertype.h"

using namespace Packuru::Core;


namespace
{
const QString execName("brotli");
}


namespace Packuru::Plugins::GnuTar::Core
{

CompressionHandlerBrotli::CompressionHandlerBrotli(QObject* parent)
    : CompressionHandler( CompressionHandlerType::Brotli, parent)
{

}


QString CompressionHandlerBrotli::executableName()
{
    return execName;
}


void CompressionHandlerBrotli::compress(const CompressionInitData& data)
{
    QStringList args;
    args << QLatin1String("-c") // from stdin to stdout
          << toCLIargCompressionLevel(data.compressionLevel, handlerType);

    process->setStandardOutputFile(data.archiveInfo.absoluteFilePath());
    process->start(execName, args);
}


void CompressionHandlerBrotli::decompress(const DecompressionInitData& data)
{
    QStringList args;
    args << QLatin1String("-dc") << data.archiveInfo.absoluteFilePath();

    process->start(execName, args);
}


void CompressionHandlerBrotli::processErrorLineHandler(const std::vector<QStringRef>& lines)
{
    for (const auto& line : lines)
    {
        if (line.startsWith(QLatin1String("corrupt input")))
            emit error(BackendHandlerError::DataError);
        else if (line.endsWith(QLatin1String("No space left on device")))
            emit error(BackendHandlerError::DiskFull);
    }
}

}
