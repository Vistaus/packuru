// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../core/private/plugin-api/archivetype.h"
#include "../core/private/plugin-api/archivingcompressioninfo.h"

#include "getcompressioninfofortype.h"


using namespace Packuru::Core;


namespace Packuru::Plugins::GnuTar::Core
{

ArchivingCompressionInfo getCompressionInfoForType(Packuru::Core::ArchiveType archiveType)
{
    // Compressors often support compression with the fastest level 0; this level is currently not
    // supported when creating archives as it means 'No compression' in the archiving dialog.
    switch (archiveType)
    {
    case ArchiveType::tar:
        return {0, 0 ,0};
    case ArchiveType::tar_brotli:
        return {1, 11, 5};
    case ArchiveType::tar_bzip2:
        return {1, 9, 9};
    case ArchiveType::tar_compress:
        return {1, 1, 1};
    case ArchiveType::tar_gzip:
    case ArchiveType::tar_lrzip:
        return  {1, 9, 7};
    case ArchiveType::tar_lz4:
        return {1, 12, 7};
    case ArchiveType::tar_lzip:
    case ArchiveType::tar_lzma:
    case ArchiveType::tar_xz:
        return {1, 9, 6};
    case ArchiveType::tar_lzop:
        return  {1, 9, 3};
    case ArchiveType::tar_zstd:
        return  {1, 22 ,3};
    default:
        Q_ASSERT(false); return ArchivingCompressionInfo();
    }
}

}
