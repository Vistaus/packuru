// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <vector>

#include <QObject>


namespace Packuru::Core
{
enum class ArchiveType;
}


namespace Packuru::Plugins::GnuTar::Core
{

class CompressionHandler;
enum class CompressionHandlerType;


enum class CompressionHandlerPickMode
{
    AllSupported,
    // Only handlers with executable available
    OnlyAvailable,
    // If there is only one handler in the list it will be selected even if its executable is not available.
    // In that case an "Executable not found" error will be reported during handler creation.
    SelectBest
};


struct CompressionHandlerCreationResult
{
    explicit operator bool() const { return handler; }

    CompressionHandler* handler = nullptr;
    QString errorInfo;
};


struct CompressionHandlerExecInfo
{
    explicit operator bool() const { return available; }

    CompressionHandlerType handlerType;
    QString execName;
    bool available = false;
};


// Can return an empty set
std::vector<CompressionHandlerExecInfo> getCompressionHandlers(Packuru::Core::ArchiveType archiveType,
                                                               CompressionHandlerPickMode pickMode);

CompressionHandlerExecInfo getCompressionHandlerExecInfo(CompressionHandlerType handlerType);

CompressionHandlerCreationResult createCompressionHandler(CompressionHandlerType handlerType, QObject* parent);

}
