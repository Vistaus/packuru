// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QString>
#include <QFileInfo>


namespace Packuru::Core
{
enum class ArchiveType;
}


namespace Packuru::Plugins::GnuTar::Core
{
    struct CompressionInitData
    {
        CompressionInitData();

        QFileInfo archiveInfo;
        Packuru::Core::ArchiveType archiveType;
        qulonglong partSize;
        int compressionLevel;
        int requestedThreadCount;
        QString password;
    };
}
