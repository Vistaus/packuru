// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

/// @file

namespace Packuru::Utils
{

/** @brief Stores a pointer to a variable and resets it in the destructor to a predefined value.

 When it goes out of scope it resets another variable to a predefined value.
 It is useful mostly in functions with multiple exit paths where it's difficult
 to ensure that some state is always set on exit. The advantage is a short syntax,
 however the end value must be stored in template parameter.

 @par Example

 @code
 {
    bool inProgess = false;

    {
        // Store a reference to inProgress, initialize it to true and reset to false when out of scope
        const LocalHolder<bool, false> lh(&inProgress, true);
    }

 }
 @endcode

 @headerfile "utils/localholder.h"
 @sa ScopeGuard
 */
template <typename T, T endValue = T()>
class LocalHolder
{
public:
    /**
     @brief Constructs %LocalHolder and sets a pointer to a variable.
     @param variable Pointer to variable.
     */
    explicit LocalHolder(T* variable)
        : var(variable)
    {

    }

    /**
     @brief Constructs %LocalHolder, sets a pointer to a variable and sets the variable value.
     @param variable Pointer to variable.
     @param initValue The value that the variable is initialised with.
     */
    explicit LocalHolder(T* variable, const T& initValue)
        : var(variable)
    {
        *var = initValue;
    }

    /**
      @brief Destroys %LocalHolder and resets the variable to a predefined value.
      */
    ~LocalHolder()
    {
        if (var)
            *var = endValue;
    }

    /**
     @brief Assigns a new value to the variable.
     @param value The value to be assigned.
     @return Reference to itself.
     */
    LocalHolder& operator=(const T& value)
    {
        Q_ASSERT(var);
        *var = value;
        return *this;
    }

private:
    T* var;
};

}
