// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <initializer_list>

/// @file

namespace Packuru::Utils
{

/** @brief Permits only a one-way change of its internal boolean value.

 The default value is provided as a template parameter and it will be used to initialise the
 internal value unless other value is provided in the constructor. Once the internal value is different
 from default any subsequent assignements are ignored until %BoolFlag is explicitly reset.

 @par Example

 @code
 BoolFlag<false> flag; // Initialize with default value
 flag = true;
 flag = false; // Ignored, the value remains true
 flag.reset(); // Resets to false
 flag = {false, true, false}; // Sets value to true
 @endcode

 @headerfile "utils/boolflag.h"
 */

template <bool(defaultValue)>
class BoolFlag
{

public:
    /**
     @brief Allows initialization with a different value than default.
     @param value By default it is assigned the class' template parameter value.
     */
    explicit BoolFlag(bool value = defaultValue) noexcept
        : value_(value) {}

    /**
     @brief Initializes with the value of another BoolFlag.
     */
    BoolFlag(const BoolFlag<true>& other) noexcept
        : BoolFlag(other.value)
    {

    }

    BoolFlag(const BoolFlag<false>& other) noexcept
        : BoolFlag(other.value)
    {

    }

    /**
     @brief Initializes with the value of another %BoolFlag and resets the other.
     @param other It will be reset to its default value.
     */
    BoolFlag(BoolFlag<true>&& other) noexcept
        : value_(other.value_)
    {
        other.reset();
    }

    BoolFlag(BoolFlag<false>&& other) noexcept
        : value_(other.value_)
    {
        other.reset();
    }

    /**
     @brief If the current value is different from default, assigns the value of another %BoolFlag.
     @return Reference to itself.
     */
    BoolFlag<defaultValue>& operator=(const BoolFlag<true>& other) noexcept
    {
        if (&other != this && value_ == defaultValue)
        {
            value_ = other.value_;
        }
        return *this;
    }

    BoolFlag<defaultValue>& operator=(const BoolFlag<false>& other) noexcept
    {
        if (&other != this && value_ == defaultValue)
        {
            value_ = other.value_;
        }
        return *this;
    }

    /**
     @brief If the current value is different from default, assigns the value of another %BoolFlag
     and resets the other.
     @param other It will be reset to its default value.
     @return Reference to itself.
     */
    BoolFlag<defaultValue>& operator=(BoolFlag<true>&& other) noexcept
    {
        if (&other != this && value_ == defaultValue)
        {
            value_ = other.value_;
            other.reset();
        }
        return *this;
    }

    BoolFlag<defaultValue>& operator=(BoolFlag<false>&& other) noexcept
    {
        if (&other != this && value_ == defaultValue)
        {
            value_ = other.value_;
            other.reset();
        }
        return *this;
    }

    /** @brief If the current value is different from default, assigns a new value.
     @return Reference to itself.
     */
    BoolFlag<defaultValue>& operator=(bool newValue) noexcept
    {
        if (value_ == defaultValue)
            value_ = newValue;
        return *this;
    }

    /** @brief If the current value is different from default, assigns the first value from
     the list that is different from the current value.
     @param newValues A list of bool values.
     @return Reference to itself.
     */
    BoolFlag<defaultValue>& operator=(std::initializer_list<bool> newValues) noexcept
    {
        if (value_ == defaultValue)
        {
            for (bool v : newValues)
                if (v != value_)
                {
                    value_ = v;
                    break;
                }
        }
        return *this;
    }

    /// @brief Implicit conversion to bool.
    operator bool() const noexcept { return value_; }

    /// @brief Resets to default value.
    void reset() noexcept { value_ = defaultValue; }

    /**
     @brief Changes the value if it's different from default and returns true.
     @return Returns true if the value has been changed, otherwise returns false.
     */
    bool set() noexcept
    {
        if (value_ == defaultValue)
        {
            value_ = !value_;
            return true;
        }

        return false;
    }

    /// @brief Returns current value.
    bool value() const noexcept { return value_; }

private:
    bool value_;
};

}
