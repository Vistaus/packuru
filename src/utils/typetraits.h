// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <type_traits>


namespace Packuru::Utils
{

template <typename  T, typename ... U>
struct AreSame
{
    using first_type = T;

    static constexpr bool value =
            std::is_same<T, typename AreSame<U...>::first_type >::value &&
    AreSame<U...>::value;
};


template <typename T,typename R>
struct AreSame<T, R>
{
    static constexpr bool value = std::is_same<T, R>::value;
};


template <typename T>
struct AreSame<T>
{
    static constexpr bool value = true;
};


template<typename T, typename ... U>
using AreSameV = typename AreSame<T, U...>::value;


// Check base of multiple types
template <typename  T, typename R, typename ... U>
struct IsBaseOf
{
    using base_type = T;

    static constexpr bool value = std::is_base_of<T, R>::value && IsBaseOf<T, U...>::value;
};


template <typename T, typename R>
struct IsBaseOf<T, R>
{
    static constexpr bool value = std::is_base_of<T, R>::value;
};

template<typename T, typename R, typename ... U>
using IsBaseOfV = typename IsBaseOf<T, R, U...>::value;

}
