// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <unordered_set>

#include <QMetaType>
#include <QString>

#include "qttypehasher.h"


namespace Packuru::Utils
{

using QStringUnorderedSet = std::unordered_set<QString, Packuru::Utils::QtTypeHasher<QString>>;

}

Q_DECLARE_METATYPE(Packuru::Utils::QStringUnorderedSet);
