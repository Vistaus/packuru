// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <type_traits>


namespace Packuru::Utils
{

// Safe casting between two enums with the same underlying type
template <typename OutputEnum, typename InputEnum>
std::enable_if_t<
std::is_same_v< std::underlying_type_t<OutputEnum>, std::underlying_type_t<InputEnum> >,
OutputEnum>
enumCast(InputEnum enumerator)
{
    return static_cast<OutputEnum>(enumerator);
}

}
