// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <unordered_map>

#include <QVariant>

#include "unordered_map_utils.h"
#include "../qvariant_utils.h"


namespace Packuru::Utils
{

template <typename KeyType>
using QVariantUnorderedMap = std::unordered_map<KeyType, QVariant>;

/*
 Returns as type T the value of QVariant stored in a map under the given key.

 The behavior is undefined unless the map contains element with the specific key
 and the element contains a value of type T.
 */
template <typename ReturnType, typename KeyType>
ReturnType getValueAs(const QVariantUnorderedMap<KeyType>& map, const KeyType& key)
{
    return Packuru::Utils::getValueAs<ReturnType>(Packuru::Utils::getValue(map, key));
}

template <typename ReturnType, typename KeyType>
ReturnType tryValueAs(const QVariantUnorderedMap<KeyType>& map, const KeyType& key, ReturnType defaultValue)
{
    const QVariant temp = Packuru::Utils::tryValue(map, key);

    if (Packuru::Utils::containsType<ReturnType>(temp))
        return Packuru::Utils::getValueAs<ReturnType>(temp);
    else
        return defaultValue;
}

}
