// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QVariant>

/// @file

namespace Packuru::Utils
{

/**
 @brief Checks whether a QVariant object contains a value of type T.
 @param variant Object to check.
 @return True if the variant contains type T, otherwise false.

 @par Example

 @code
 QVariant var(true);
 containsType<bool>(var); // Returns true
 @endcode
 */
template <typename T>
bool containsType(const QVariant& variant)
{
    return variant.userType() == qMetaTypeId<T>();
}


/**
 @brief Returns the value of QVariant as type T.
 @param variant Object to access.
 @return Object's value as type T.

 @warning
 The behavior is undefined unless the object contains a value of type T;
 use containsType() first.

 @par Example

 @code
 QVariant var(true);
 getValueAs<bool>(var); // OK
 getValueAs<int>(var); // Undefined behavior
 @endcode

 @sa containsType()
 */
template <typename T>
T getValueAs(const QVariant& variant)
{
    Q_ASSERT(containsType<T>(variant));

    return variant.value<T>();
}

}
