// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <memory>

#include "../utils/qvariant_utils.h"

#include "../core/private/plugin-api/backendarchiveproperty.h"
#include "../core/private/plugin-api/archivetype.h"

#include "../plugin-7zip-core/archiving/archivingparametershandler.h"
#include "../plugin-7zip-core/registerstreamoperators.h"
#include "../plugin-7zip-core/backendhandler.h"

#include "archivehandler7zip.h"


using namespace Packuru::Core;


namespace Packuru::Plugins::_7Zip
{

namespace
{

bool canEdit(ArchiveType archiveType, const BackendArchiveProperties& properties)
{
    bool multipart = false;
    auto it = properties.find(BackendArchiveProperty::Multipart);
    if (it != properties.end())
        multipart = Utils::getValueAs<bool>(it->second);

    int partCount = 1;
    it = properties.find(BackendArchiveProperty::PartCount);
    if (it != properties.end())
        partCount = Utils::getValueAs<int>(it->second);

    Q_ASSERT(partCount >  0);

    if (multipart || partCount > 1)
        return false;

    if (archiveType == ArchiveType::_7z || archiveType == ArchiveType::zip)
        return true;

    return false;
}

}


ArchiveHandler7Zip::ArchiveHandler7Zip()
{
    Core::registerStreamOperators();
}


std::unordered_set<ArchiveType> ArchiveHandler7Zip::supportedTypesForReading() const
{
    return {
        ArchiveType::_7z,
                ArchiveType::appimage,
                ArchiveType::ar,
                ArchiveType::arj,
                // ArchiveType::bzip, // No files listed
                ArchiveType::cab,
                ArchiveType::chm,
                ArchiveType::cpio,
                ArchiveType::cramfs,
                // Only data.tar is listed for deb archives and info is printed in non-standard way for many archives
                // which currently makes it unusable (same for rpms)
                // ArchiveType::deb,
                ArchiveType::dmg,
                ArchiveType::ext,
                ArchiveType::fat,
                ArchiveType::gzip,
                ArchiveType::iso9660,
                ArchiveType::lha,
                ArchiveType::mbr,
                ArchiveType::mdx,
                ArchiveType::msi,
                ArchiveType::nrg,
                ArchiveType::ntfs,
                ArchiveType::qcow,
                /* RAR support in 7-Zip requires non-free code from UnRAR and some distros
                 e.g. Debian removed this code from 7-Zip. It is therefore better to
                 drop RAR support in this plugin and instead rely on The Unarchiver
                 or unrar-nonfree for RAR archives.
                */
                // ArchiveType::rar,
                // Only data.tar is listed for rpm archives and info is printed in non-standard way for many archives
                // which currently makes it unusable (same for debs)
                // ArchiveType::rpm,
                ArchiveType::squashfs,
                ArchiveType::tar,
                ArchiveType::udf,
                ArchiveType::vdi,
                ArchiveType::vhd,
                ArchiveType::vmdk,
                ArchiveType::wim,
                ArchiveType::xar,
                // ArchiveType::xz, // No files listed
                ArchiveType::zip
    };
}


std::unordered_set<ArchiveType> ArchiveHandler7Zip::supportedTypesForWriting() const
{
    return { ArchiveType::_7z,
                ArchiveType::zip };
}


bool ArchiveHandler7Zip::canAddFiles(ArchiveType archiveType, const BackendArchiveProperties& properties) const
{
    return canEdit(archiveType, properties);
}


bool ArchiveHandler7Zip::canDeleteFiles(ArchiveType archiveType, const BackendArchiveProperties& properties) const
{
    return canEdit(archiveType, properties);
}


std::unique_ptr<AbstractBackendHandler> ArchiveHandler7Zip::createBackendHandler() const
{
    return std::make_unique<Core::BackendHandler>();
}


std::unique_ptr<ArchivingParameterHandlerBase>
ArchiveHandler7Zip::createArchivingParameterHandler(const ArchivingParameterHandlerInitData& initData) const
{
    return std::make_unique<Core::ArchivingParametersHandler>(initData);
}

}
