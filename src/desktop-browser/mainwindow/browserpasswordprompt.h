// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QWidget>


namespace Ui {
class BrowserPasswordPrompt;
}

namespace Packuru::Core
{
class PasswordPromptCore;
}

class BrowserPasswordPrompt : public QWidget
{
    Q_OBJECT
public:
    explicit BrowserPasswordPrompt(Packuru::Core::PasswordPromptCore* promptCore, QWidget *parent = nullptr);
    ~BrowserPasswordPrompt() override;

private:
    bool eventFilter(QObject *watched, QEvent *event) override;

    Ui::BrowserPasswordPrompt *ui;
};
