// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QWidget>


namespace Packuru::Core::Browser
{
class ArchiveBrowserCore;
}

class ArchiveBrowserWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ArchiveBrowserWidget(Packuru::Core::Browser::ArchiveBrowserCore* browserCore, QWidget *parent = nullptr);
    ~ArchiveBrowserWidget() override;

    QString getTitle() const;
    void onActivated();
    void onDeactivated();
    Packuru::Core::Browser::ArchiveBrowserCore* getCore();
    void previewRequested();
    void reloadRequested();
    void openFilterWidget();

signals:
    void titleChanged(const QString& title);
    void availableActionsChanged();
    void needsActivation(ArchiveBrowserWidget*);
    void statusIconChanged(ArchiveBrowserWidget*, const QIcon&);

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

