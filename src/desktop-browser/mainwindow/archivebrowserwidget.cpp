// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QDesktopServices>
#include <QUrl>
#include <QIcon>

#include "../core-browser/archivebrowsercore.h"

#include "archivebrowserwidget.h"
#include "ui_archivebrowserwidget.h"
#include "navigationwidget.h"
#include "archivebrowserstatuswidget.h"


using Packuru::Core::Browser::ArchiveBrowserCore;


struct ArchiveBrowserWidget::Private
{
    ~Private();

    void onTaskStateChanged(ArchiveBrowserCore::BrowserTaskState state);
    void setCurrentPage(QWidget* widget);

    ArchiveBrowserWidget* publ;
    Ui::ArchiveBrowserWidget *ui;
    ArchiveBrowserCore* browserCore;
    NavigationWidget* navigationWidget;
    ArchiveBrowserStatusWidget* statusWidget;
    bool showTaskSuccessSummary = true;
    bool isActiveBrowser = false;
};


ArchiveBrowserWidget::ArchiveBrowserWidget(ArchiveBrowserCore* browserCore,
                                           QWidget *parent)
    : QWidget(parent),
      priv(new Private)
{
    priv->publ = this;
    priv->ui = new Ui::ArchiveBrowserWidget;
    priv->ui->setupUi(this);
    priv->browserCore = browserCore;
    priv->navigationWidget = new NavigationWidget(browserCore->getNavigator());

    priv->statusWidget = new ArchiveBrowserStatusWidget(browserCore->getStatusPageCore());

    connect(priv->statusWidget, &ArchiveBrowserStatusWidget::focusProxyChanged,
            this, [publ = this] ()
    {
        if (publ->priv->isActiveBrowser)
            publ->setFocus(); // Set focused widget based on a new focus proxy path
    });

    priv->ui->stackedWidget->addWidget(priv->navigationWidget);
    priv->ui->stackedWidget->addWidget(priv->statusWidget);

    priv->setCurrentPage(priv->navigationWidget);

    connect(browserCore, &ArchiveBrowserCore::titleChanged,
            this, &ArchiveBrowserWidget::titleChanged);

    connect(browserCore, &ArchiveBrowserCore::uiStateChanged,
            this, [publ = this] (ArchiveBrowserCore::UIState state)
    {
        auto priv = publ->priv.get();
        switch (state)
        {
        case ArchiveBrowserCore::UIState::Navigation:
            priv->setCurrentPage(priv->navigationWidget);
            return;
        case ArchiveBrowserCore::UIState::StatusPage:
            priv->setCurrentPage(priv->statusWidget);
            return;
        default:
            Q_ASSERT(false);
        }
    });

    connect(browserCore, &ArchiveBrowserCore::needsActivation,
            this, [publ = this] ()
    {
        emit publ->needsActivation(publ);
    });

    connect(browserCore, &ArchiveBrowserCore::taskStateChanged,
            this, [publ = this] (auto state)
    {
        publ->priv->onTaskStateChanged(state);
    });

    connect(browserCore, &ArchiveBrowserCore::filesReadyForPreview,
            this, [] (const std::vector<QUrl>& urls)
    {
        for (const auto& url : urls)
            QDesktopServices::openUrl(url);
    });

    connect(browserCore, &ArchiveBrowserCore::openExtractionDestinationFolder,
            this, [] (const QUrl& url) { QDesktopServices::openUrl(url); });

    connect(browserCore, &ArchiveBrowserCore::availableActionsChanged,
            this, &ArchiveBrowserWidget::availableActionsChanged);

    connect(browserCore, &ArchiveBrowserCore::taskBusyChanged,
            this, [publ = this] (bool value)
    { publ->priv->navigationWidget->setEnabled(!value); });
}


ArchiveBrowserWidget::~ArchiveBrowserWidget()
{
    priv->browserCore->deleteLater();
}


QString ArchiveBrowserWidget::getTitle() const
{
    return priv->browserCore->getTitle();
}


void ArchiveBrowserWidget::onActivated()
{
    priv->isActiveBrowser = true;
    setFocus();
}


void ArchiveBrowserWidget::onDeactivated()
{
    priv->isActiveBrowser = false;
}


ArchiveBrowserCore* ArchiveBrowserWidget::getCore()
{
    return priv->browserCore;
}


void ArchiveBrowserWidget::previewRequested()
{
    priv->browserCore->previewSelectedFiles();
}


void ArchiveBrowserWidget::reloadRequested()
{
    priv->browserCore->reloadArchive();
}


void ArchiveBrowserWidget::openFilterWidget()
{
    priv->navigationWidget->openFilterWidget();
}


ArchiveBrowserWidget::Private::~Private()
{
    delete ui;
}


void ArchiveBrowserWidget::Private::onTaskStateChanged(ArchiveBrowserCore::BrowserTaskState state)
{  
    QIcon icon;

    switch (state)
    {
    case ArchiveBrowserCore::BrowserTaskState::Aborted:
        icon = QIcon::fromTheme("error");
        break;
    case ArchiveBrowserCore::BrowserTaskState::Errors:
        icon = QIcon::fromTheme("error");
        break;
    case ArchiveBrowserCore::BrowserTaskState::Paused:
        icon = QIcon::fromTheme("media-playback-pause");
        break;
    case ArchiveBrowserCore::BrowserTaskState::Warnings:
        icon = QIcon::fromTheme("messagebox_warning");
        break;
    case ArchiveBrowserCore::BrowserTaskState::ReadyToRun:
        break;
    case ArchiveBrowserCore::BrowserTaskState::InProgress:
        icon = QIcon::fromTheme("clock");
        break;
    case ArchiveBrowserCore::BrowserTaskState::Success:
        icon = QIcon::fromTheme("dialog-ok");
        break;
    case ArchiveBrowserCore::BrowserTaskState::WaitingForInput:
        icon = QIcon::fromTheme("question");
        break;
    case ArchiveBrowserCore::BrowserTaskState::WaitingForPassword:
        icon = QIcon::fromTheme("document-encrypted");
        break;
    default:
        break;
    }

    statusWidget->updateIcon(icon);
    emit publ->statusIconChanged(publ, icon);
}


void ArchiveBrowserWidget::Private::setCurrentPage(QWidget* widget)
{
    ui->stackedWidget->setCurrentWidget(widget);
    publ->setFocusProxy(widget);

    if (isActiveBrowser)
        publ->setFocus();
}
