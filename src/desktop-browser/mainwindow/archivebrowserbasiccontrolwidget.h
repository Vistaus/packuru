// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QWidget>


namespace qcs::qtw
{
class WidgetMapper;
}

namespace Packuru::Core::Browser
{
class BasicControl;
}

class QPushButton;
class QLabel;


namespace Ui {
class ArchiveBrowserBasicControlWidget;
}

class ArchiveBrowserBasicControlWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ArchiveBrowserBasicControlWidget(Packuru::Core::Browser::BasicControl* basicControl,
                                              QWidget *parent = nullptr);
    ~ArchiveBrowserBasicControlWidget() override;

private:
    bool eventFilter(QObject *watched, QEvent *event) override;

    Ui::ArchiveBrowserBasicControlWidget *ui;
    qcs::qtw::WidgetMapper* widgetMapper;
    Packuru::Core::Browser::BasicControl* basicControl;
};

