// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QIcon>
#include <QMessageBox>
#include <QPushButton>
#include <QTabBar>
#include <QMouseEvent>
#include <QTimer>

#include "../utils/makeqpointer.h"

#include "../core-browser/archivebrowsercore.h"
#include "../core-browser/tabclosingdialogcore.h"

#include "archivebrowsertabwidget.h"
#include "archivebrowserwidget.h"


using namespace Packuru::Core::Browser;
using Packuru::Utils::makeQPointer;


ArchiveBrowserTabWidget::ArchiveBrowserTabWidget(QWidget* parent)
    : QTabWidget(parent),
      tabResizeTimer(new QTimer(this))
{
    setTabBarAutoHide(true);
    setTabsClosable(true);
    setMovable(true);
    setFocusPolicy(Qt::NoFocus);
    setElideMode(Qt::ElideRight);

    tabBar()->installEventFilter(this);

    connect(this, &ArchiveBrowserTabWidget::tabCloseRequested,
            this, &ArchiveBrowserTabWidget::onTabCloseRequested);

    connect(this, &ArchiveBrowserTabWidget::currentChanged,
            this, &ArchiveBrowserTabWidget::onCurrentTabChanged);

    tabResizeTimer->setSingleShot(true);
    tabResizeTimer->setInterval(100);
    connect(tabResizeTimer, &QTimer::timeout,
            this, &ArchiveBrowserTabWidget::updateTabWidth);
}


bool ArchiveBrowserTabWidget::eventFilter(QObject* watched, QEvent* event)
{
    if (watched == tabBar())
    {
        if (auto mouseEvent = dynamic_cast<QMouseEvent*>(event))
        {
            if (mouseEvent->type() == QEvent::MouseButtonRelease
                    && mouseEvent->button() == Qt::MiddleButton)
            {
                const int index = tabBar()->tabAt(mouseEvent->pos());
                if (index >= 0)
                {
                    onTabCloseRequested(index);
                    return true;
                }
            }
        }
    }

    return false;
}


ArchiveBrowserWidget* ArchiveBrowserTabWidget::getCurrentBrowser() const
{
    auto browser = qobject_cast<ArchiveBrowserWidget*>(currentWidget());
    return browser;
}


void ArchiveBrowserTabWidget::createBrowser(Packuru::Core::Browser::ArchiveBrowserCore* browserCore)
{
    auto browser (new ArchiveBrowserWidget(browserCore, this));

    connect(browser, &ArchiveBrowserWidget::needsActivation,
            this, &ArchiveBrowserTabWidget::activateBrowser);

    connect(browser, &ArchiveBrowserWidget::statusIconChanged,
            this, &ArchiveBrowserTabWidget::setStatusIcon);

    connect(browser, &ArchiveBrowserWidget::titleChanged,
            this, [tabWidget = this, browser] (const QString& title)
    { tabWidget->updateTitle(browser, title); });

    const int index = addTab(browser, browser->getTitle());
    setTabToolTip(index, browser->getTitle());
    setCurrentIndex(index);

    // Update immediately for the first tab so it will have maximum width even if
    // tab's text is shorter (this sets initial style sheet and must be done with
    // at least 1 tab).
    if (count() == 1)
        updateTabWidth();
    else
        tabResizeTimer->start();
}


void ArchiveBrowserTabWidget::previewRequested()
{
    if (auto browser = getCurrentBrowser())
        browser->previewRequested();
}


void ArchiveBrowserTabWidget::closeCurrentTab()
{
    const auto index = currentIndex();

    if (index >= 0)
        onTabCloseRequested(index);
}


void ArchiveBrowserTabWidget::resizeEvent(QResizeEvent* event)
{
    QTabWidget::resizeEvent(event);

    tabResizeTimer->start();
}


void ArchiveBrowserTabWidget::activateBrowser(ArchiveBrowserWidget *browser)
{
    const int index = indexOf(browser);
    Q_ASSERT(index != -1);
    setCurrentIndex(index);
}


void ArchiveBrowserTabWidget::onTabCloseRequested(int index)
{
    auto browser = qobject_cast<ArchiveBrowserWidget*>(widget(index));
    Q_ASSERT(browser);

    auto closeFunction = [tabWidget = makeQPointer(this), browser = makeQPointer(browser), index = index] ()
    {
        if (tabWidget)
            tabWidget->removeTab(index);
        if (browser)
            browser->deleteLater();

        tabWidget->tabResizeTimer->start();
    };

    if (browser->getCore()->isTaskBusy())
    {
        auto msgBox = new QMessageBox (this);

        msgBox->setWindowModality(Qt::WindowModal);
        msgBox->setWindowTitle(TabClosingDialogCore::getString(TabClosingDialogCore::UserString::DialogTitle));
        msgBox->setText(TabClosingDialogCore::getString(TabClosingDialogCore::UserString::ThisTabIsBusy));
        msgBox->setInformativeText(TabClosingDialogCore::getString(TabClosingDialogCore::UserString::DoYouWantToCloseIt));

        auto closeButton = new QPushButton(msgBox);
        closeButton->setText(TabClosingDialogCore::getString(TabClosingDialogCore::UserString::Close));
        msgBox->addButton(closeButton, QMessageBox::AcceptRole);

        auto cancelButton = new QPushButton(msgBox);
        cancelButton->setText(TabClosingDialogCore::getString(TabClosingDialogCore::UserString::Cancel));
        msgBox->addButton(cancelButton, QMessageBox::RejectRole);

        msgBox->setDefaultButton(cancelButton);

        msgBox->open();

        connect(msgBox, &QDialog::finished,
                [closeButton = closeButton, closeFunction = closeFunction, msgBox = msgBox] ()
        {
            if (msgBox->clickedButton() == closeButton)
                closeFunction();
            msgBox->deleteLater();
        });
    }
    else
    {
        closeFunction();
    }
}


void ArchiveBrowserTabWidget::setStatusIcon(ArchiveBrowserWidget* browser, const QIcon& icon)
{
    const int index = indexOf(browser);
    if (index >= 0)
        setTabIcon(index, icon);
}


void ArchiveBrowserTabWidget::onCurrentTabChanged()
{
    if (currentBrowser)
    {
        currentBrowser->onDeactivated();

        disconnect(currentBrowser, &ArchiveBrowserWidget::availableActionsChanged,
                   this, &ArchiveBrowserTabWidget::currentBrowserAvailableActionsChanged);

        disconnect(currentBrowser, &ArchiveBrowserWidget::titleChanged,
                   this, &ArchiveBrowserTabWidget::currentBrowserTitleChanged);
    }

    currentBrowser = getCurrentBrowser();

    if (currentBrowser)
    {
        currentBrowser->onActivated();

        connect(currentBrowser, &ArchiveBrowserWidget::availableActionsChanged,
                this, &ArchiveBrowserTabWidget::currentBrowserAvailableActionsChanged);

        connect(currentBrowser, &ArchiveBrowserWidget::titleChanged,
                this, &ArchiveBrowserTabWidget::currentBrowserTitleChanged);
    }
}


void ArchiveBrowserTabWidget::updateTitle(ArchiveBrowserWidget* browser, const QString& title)
{
    const int index = indexOf(browser);
    setTabText(index, title);
}


void ArchiveBrowserTabWidget::updateTabWidth()
{
    const int tabCount = tabBar()->count();

    if (tabCount == 0)
        return;

    const int scrollButtonsMargin = 50;
    const int availableWidth = width() - scrollButtonsMargin;
    const int minTabWidth = 150;
    const int maxTabWith = 300;

    int width = availableWidth / tabCount;

    if (width < minTabWidth)
        width = minTabWidth;
    else if (width > maxTabWith)
        width = maxTabWith;

    setStyleSheet("QTabBar::tab { min-width:" + QString::number(width)
                  + "px; max-width:" + QString::number(width) + "px; }");
}
