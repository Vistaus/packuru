// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QPushButton>

#include "../core/passwordpromptcore.h"
#include "../core/extractionfileoverwritepromptcore.h"
#include "../core/extractionfolderoverwritepromptcore.h"
#include "../core/extractiontypemismatchpromptcore.h"

#include "../core-browser/statuspagecore.h"

#include "../desktop-core/extractionfolderoverwriteprompt.h"
#include "../desktop-core/extractionfileoverwriteprompt.h"
#include "../desktop-core/extractiontypemismatchprompt.h"

#include "archivebrowserstatuswidget.h"
#include "ui_archivebrowserstatuswidget.h"
#include "archivebrowserbasiccontrolwidget.h"
#include "browserpasswordprompt.h"


using Packuru::Core::Browser::StatusPageCore;


struct ArchiveBrowserStatusWidget::Private
{
    Private(ArchiveBrowserStatusWidget* publ, StatusPageCore* core);
    ~Private();

    void showControlWidget(QWidget* widget);
    void updateTaskProgress(int value);

    ArchiveBrowserStatusWidget* publ;
    Ui::ArchiveBrowserStatusWidget *ui;
    ArchiveBrowserBasicControlWidget* basicControlWidget = nullptr;
    StatusPageCore* core = nullptr;
    QStringList errors;
};


ArchiveBrowserStatusWidget::ArchiveBrowserStatusWidget(StatusPageCore* core, QWidget *parent)
    : QWidget(parent),
      priv(new Private(this, core))
{
}


ArchiveBrowserStatusWidget::~ArchiveBrowserStatusWidget()
{
}


void ArchiveBrowserStatusWidget::updateIcon(const QIcon& icon)
{
    priv->ui->iconContainer->setPixmap(icon.pixmap(48, 48));
}


ArchiveBrowserStatusWidget::Private::Private(ArchiveBrowserStatusWidget* publ,
                                             StatusPageCore* core)
    : publ(publ),
      core(core)
{
    ui = new Ui::ArchiveBrowserStatusWidget;
    ui->setupUi(publ);

    auto layout = new QVBoxLayout;
    layout->setContentsMargins(0, 0, 0, 0);
    basicControlWidget = new ArchiveBrowserBasicControlWidget(core->getBasicControl());

    layout->addWidget(basicControlWidget);
    ui->controlWidget->setLayout(layout);

    ui->taskDescription->setText(core->getTaskDescription());

    QObject::connect(core, &StatusPageCore::taskDescriptionChanged,
                     ui->taskDescription, &QLabel::setText);

    QObject::connect(core, &StatusPageCore::taskStateChanged,
                     ui->taskState, &QLabel::setText);

    QObject::connect(core, &StatusPageCore::progressBarVisibleChanged,
                     ui->progressBar, &QWidget::setVisible);

    QObject::connect(core, &StatusPageCore::taskProgressChanged,
                     publ, [publ = publ] (auto value)
    {
        publ->priv->updateTaskProgress(value);
    });

    QObject::connect(core, &StatusPageCore::showBasicControl,
                     publ, [publ = publ] ()
    {
        auto priv = publ->priv.get();
        priv->showControlWidget(priv->basicControlWidget);
    });

    QObject::connect(core, &StatusPageCore::showPasswordPrompt,
                     publ, [publ = publ] (auto promptCore)
    {
        auto prompt = new BrowserPasswordPrompt(promptCore);
        publ->priv->showControlWidget(prompt);
    });

    QObject::connect(core, &StatusPageCore::showFileExistsPrompt,
                     publ, [publ = publ] (auto promptCore)
    {
        auto prompt = new ExtractionFileOverwritePrompt(promptCore, true);
        publ->priv->showControlWidget(prompt);
    });

    QObject::connect(core, &StatusPageCore::showFolderExistsPrompt,
                     publ, [publ = publ] (auto promptCore)
    {
        auto prompt = new ExtractionFolderOverwritePrompt(promptCore, true);
        publ->priv->showControlWidget(prompt);
    });

    QObject::connect(core, &StatusPageCore::showItemTypeMismatchPrompt,
                     publ, [publ = publ] (auto promptCore)
    {
        auto prompt = new ExtractionTypeMismatchPrompt(promptCore, true);
        publ->priv->showControlWidget(prompt);
    });
}


ArchiveBrowserStatusWidget::Private::~Private()
{
    delete ui;
}


void ArchiveBrowserStatusWidget::Private::showControlWidget(QWidget* widget)
{
    // QStackedWidget should not be used because its size is determined by all widgets, not only by the current widget
    // When the prompt is finished it destroys itself and is removed from the layout
    auto layout = ui->controlWidget->layout();
    bool widgetFound = false;

    for (int i = 0; i < layout->count(); ++i)
    {
        auto w = layout->itemAt(i)->widget();
        if (w == widget)
        {
            w->show();
            widgetFound = true;
        }
        else
            w->hide();
    }

    if (!widgetFound)
    {
        layout->addWidget(widget);
        widget->show();
    }

    publ->setFocusProxy(widget);
    emit publ->focusProxyChanged();
}


void ArchiveBrowserStatusWidget::Private::updateTaskProgress(int value)
{
    if (value < 0)
    {
        ui->progressBar->setRange(0, 0);
        ui->progressBar->reset();
        return;
    }

    auto bar = ui->progressBar;
    bar->setMaximum(100);
    bar->setValue(value);
}
