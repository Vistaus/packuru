// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QCloseEvent>
#include <QFileDialog>
#include <QMessageBox>
#include <QPushButton>
#include <QSettings>
#include <QMimeData>
#include <QAction>

#include "../core/appclosingdialogcore.h"
#include "../core/commonstrings.h"
#include "../core/archivingdialogcore.h"
#include "../core/extractiondialogcore.h"
#include "../core/testdialogcore.h"
#include "../core/aboutdialogcore.h"

#include "../core-browser/deletiondialogcore.h"
#include "../core-browser/mainwindowcore.h"
#include "../core-browser/queuemessenger.h"

#include "../desktop-core/showcommandlinedialog.h"
#include "../desktop-core/aboutdialog.h"
#include "../desktop-core/appclosingdialog.h"
#include "../desktop-core/extractiondialog.h"
#include "../desktop-core/testdialog.h"
#include "../desktop-core/archivingdialog.h"

#include "browsermainwindow.h"
#include "ui_browsermainwindow.h"
#include "archivebrowsertabwidget.h"
#include "deletiondialog.h"
#include "archivebrowserwidget.h"
#include "queuemessengerdialog.h"
#include "archivepropertiesdialog.h"
#include "browsersettingsdialog.h"




using namespace Packuru::Core::Browser;
using namespace Packuru::Core;


struct BrowserMainWindow::Private
{
    ~Private();

    void onBrowserCoreCreated(ArchiveBrowserCore* browserCore);
    void updateWindowTitle();
    void setUpArchiveActions();
    void disableArchiveActions();
    void onActionAddTriggered();
    void onActionExtractTriggered();
    void onActionDeleteTriggered();
    void onActionTestTriggered();
    void onActionPropertiesTriggered();

    Ui::BrowserMainWindow *ui;
    BrowserMainWindow* publ = nullptr;
    ArchiveBrowserTabWidget* tabWidget;
    MainWindowCore* core;
};


BrowserMainWindow::BrowserMainWindow(MainWindowCore* mainWindowCore, QWidget *parent)
    : QMainWindow(parent),
      priv(new Private)
{
    priv->ui = new Ui::BrowserMainWindow;
    auto ui = priv->ui;
    ui->setupUi(this);

    priv->publ = this;
    priv->tabWidget = new ArchiveBrowserTabWidget(this);
    priv->core = mainWindowCore;

    setWindowTitle(mainWindowCore->getString(MainWindowCore::UserString::WindowTitle));
    setAcceptDrops(true);

    centralWidget()->layout()->addWidget(priv->tabWidget);

    priv->disableArchiveActions();

    ui->actionAdd_to_archive->setText(ArchiveBrowserCore::getString(ArchiveBrowserCore::UserString::ActionAdd));
    ui->actionDelete->setText(ArchiveBrowserCore::getString(ArchiveBrowserCore::UserString::ActionDelete));
    ui->actionExtract->setText(ArchiveBrowserCore::getString(ArchiveBrowserCore::UserString::ActionExtract));
    ui->actionFilter->setText(ArchiveBrowserCore::getString(ArchiveBrowserCore::UserString::ActionFilter));
    ui->actionPreview->setText(ArchiveBrowserCore::getString(ArchiveBrowserCore::UserString::ActionPreview));
    ui->actionProperties->setText(ArchiveBrowserCore::getString(ArchiveBrowserCore::UserString::ActionProperties));
    ui->actionReload->setText(ArchiveBrowserCore::getString(ArchiveBrowserCore::UserString::ActionReload));
    ui->actionTest->setText(ArchiveBrowserCore::getString(ArchiveBrowserCore::UserString::ActionTest));

    ui->actionAbout->setText(priv->core->getString(MainWindowCore::UserString::ActionAbout));
    ui->actionClose_archive->setText(priv->core->getString(MainWindowCore::UserString::ActionClose));
    ui->actionNew->setText(priv->core->getString(MainWindowCore::UserString::ActionNew));
    ui->actionOpen->setText(priv->core->getString(MainWindowCore::UserString::ActionOpen));
    ui->actionQuit->setText(priv->core->getString(MainWindowCore::UserString::ActionQuit));
    ui->actionSettings->setText(priv->core->getString(MainWindowCore::UserString::ActionSettings));

    connect(priv->tabWidget, &ArchiveBrowserTabWidget::currentBrowserAvailableActionsChanged,
           this,  [publ = this] ()
    {
        publ->priv->setUpArchiveActions();
    });

    connect(priv->tabWidget, &ArchiveBrowserTabWidget::currentBrowserTitleChanged,
            this, [publ = this] () { publ->priv->updateWindowTitle(); });

    connect(priv->tabWidget, &ArchiveBrowserTabWidget::currentChanged,
            this, [publ = this] ()
    {
        publ->priv->updateWindowTitle();
        publ->priv->setUpArchiveActions();
    });

    connect(ui->actionPreview, &QAction::triggered,
            priv->tabWidget, &ArchiveBrowserTabWidget::previewRequested);

    connect(priv->core, &MainWindowCore::newMessage,
            this, [publ = this] ()
    {
        publ->activateWindow();
        publ->raise();
    });

    connect(priv->core, &MainWindowCore::commandLineError,
            this, [publ = this] (const QString& info)
    {
        showCommandLineDialog(publ,
                              CommonStrings::getString(CommonStrings::UserString::CommandLineErrorDialogTitle),
                              false,
                              info);
    });

    connect(priv->core, &MainWindowCore::commandLineHelpRequested,
            this, [publ = this] (const QString& info)
    {
        showCommandLineDialog(publ,
                              CommonStrings::getString(CommonStrings::UserString::CommandLineHelpDialogTitle),
                              false,
                              info);
    });

    connect(priv->core, &MainWindowCore::newBrowserCoreCreated,
            this, [publ = this] (auto browserCore)
    {
        publ->priv->onBrowserCoreCreated(browserCore);
    });

    auto messenger = priv->core->getQueueMessenger();
    connect(messenger, &QueueMessenger::activated,
            this, [publ = this, messenger = messenger] ()
    {
        QueueMessengerDialog dialog(messenger, publ);
        dialog.exec();
    });


    connect(ui->actionNew, &QAction::triggered,
            priv->core, &MainWindowCore::createArchive);

    connect(ui->actionOpen, &QAction::triggered,
            this, [priv = priv.get(), win = this] ()
    {
        const auto list = QFileDialog::getOpenFileNames(win);
        if (!list.empty())
            priv->core->openArchives(list);
    });

    connect(ui->actionAdd_to_archive, &QAction::triggered,
            this, [priv = priv.get()] () { priv->onActionAddTriggered(); });

    connect(ui->actionExtract, &QAction::triggered,
            this, [priv = priv.get()] () { priv->onActionExtractTriggered(); });

    connect(ui->actionDelete, &QAction::triggered,
            this, [priv = priv.get()] () { priv->onActionDeleteTriggered(); });

    connect(ui->actionReload, &QAction::triggered,
            this, [priv = priv.get()] ()
    {
        auto browser = priv->tabWidget->getCurrentBrowser();
        Q_ASSERT(browser);
        browser->reloadRequested();
    });

    connect(ui->actionTest, &QAction::triggered,
            this, [priv = priv.get()] () { priv->onActionTestTriggered(); });

    connect(ui->actionProperties, &QAction::triggered,
            this, [priv = priv.get()] () { priv->onActionPropertiesTriggered(); });

    connect(ui->actionClose_archive, &QAction::triggered,
            this, [priv = priv.get()] () {priv->tabWidget->closeCurrentTab(); });

    connect(ui->actionFilter, &QAction::triggered,
            this, [publ = this] ()
    {
        const auto browser = publ->priv->tabWidget->getCurrentBrowser();
        browser->openFilterWidget();
    });

    connect(ui->actionSettings, &QAction::triggered,
            this, [core = priv->core, win = this] ()
    {
        auto appSettingsCore = core->createAppSettingsDialogCore();
        auto browserSettingsCore = core->createBrowserSettingsDialogCore();

        BrowserSettingsDialog d(appSettingsCore, browserSettingsCore, win);
        d.exec();
    });


    connect(ui->actionAbout, &QAction::triggered,
            this, [publ = this] ()
    {
        auto d = new AboutDialog(publ);
        connect(d, &QDialog::rejected, &QObject::deleteLater);
        d->open();
    });

    connect(ui->actionQuit, &QAction::triggered,
            this, &BrowserMainWindow::close);

    QSettings s;
    const QSize size = s.value("browser_main_window_size").toSize();
    if (size.isValid())
        resize(size);
}

BrowserMainWindow::~BrowserMainWindow()
{
    QSettings s;
    s.setValue("browser_main_window_size", size());
}


void BrowserMainWindow::closeEvent(QCloseEvent *event)
{
    const int count = priv->core->getBusyBrowserCount();
    if (count == 0)
    {
        event->accept();
        emit finished();
    }
    else
    {
        AppClosingDialog d(this);
        d.setText(AppClosingDialogCore::getString(AppClosingDialogCore::UserString::BrowserBusyBrowsersInfo, count));
        d.exec();

        if (d.quitRequested())
        {
            event->accept();
            emit finished();
        }
        else
        {
            event->ignore();
        }
    }
}


void BrowserMainWindow::dragEnterEvent(QDragEnterEvent* event)
{
    if (event->mimeData()->hasUrls())
        event->acceptProposedAction();
}


void BrowserMainWindow::dropEvent(QDropEvent* event)
{
    if (event->mimeData()->hasUrls())
    {
        priv->core->openArchives(event->mimeData()->urls());
        event->acceptProposedAction();
    }
}


void BrowserMainWindow::Private::onActionAddTriggered()
{
    auto browserCore = tabWidget->getCurrentBrowser()->getCore();
    auto dialogCore = browserCore->createArchivingDialogCore();
    ArchivingDialog dialog(dialogCore, publ);
    dialog.setWindowTitle(dialogCore->getString(ArchivingDialogCore::UserString::DialogTitleAdd)
                          + QLatin1String(" - ") + browserCore->getArchiveName());
    dialog.exec();
}


void BrowserMainWindow::Private::onActionExtractTriggered()
{
    auto browserCore = tabWidget->getCurrentBrowser()->getCore();
    auto dialogCore = browserCore->createExtractionDialogCore();
    ExtractionDialog dialog(dialogCore, publ);
    dialog.setWindowTitle(dialogCore->getString(ExtractionDialogCore::UserString::DialogTitleSingle)
                          + QLatin1String(" - ") + browserCore->getArchiveName());
    dialog.exec();
}


void BrowserMainWindow::Private::onActionDeleteTriggered()
{
    auto browserCore = tabWidget->getCurrentBrowser()->getCore();
    auto dialogCore = browserCore->createDeletionDialogCore();
    DeletionDialog dialog(dialogCore, publ);
    dialog.setWindowTitle(dialogCore->getString(DeletionDialogCore::UserString::DialogTitle)
                          + QLatin1String(" - ") + browserCore->getArchiveName());
    dialog.exec();
}


void BrowserMainWindow::Private::onActionTestTriggered()
{
    auto browserCore = tabWidget->getCurrentBrowser()->getCore();
    auto dialogCore = browserCore->createTestDialogCore();
    TestDialog dialog(dialogCore, publ);
    dialog.setWindowTitle(dialogCore->getString(TestDialogCore::UserString::DialogTitleSingle)
                          + QLatin1String(" - ") + browserCore->getArchiveName());
    dialog.exec();
}


void BrowserMainWindow::Private::onActionPropertiesTriggered()
{
    auto browserCore = tabWidget->getCurrentBrowser()->getCore();
    ArchivePropertiesDialog dialog(browserCore->getPropertiesModel(),
                                   browserCore->getArchiveComment(),
                                   publ);
    dialog.setWindowTitle(CommonStrings::getString(CommonStrings::UserString::ArchivePropertiesDialogTitle) + QLatin1String(" - ")
                          + browserCore->getArchiveName());
    dialog.exec();
}


void BrowserMainWindow::Private::disableArchiveActions()
{
    ui->actionAdd_to_archive->setEnabled(false);
    ui->actionExtract->setEnabled(false);
    ui->actionPreview->setEnabled(false);
    ui->actionDelete->setEnabled(false);
    ui->actionReload->setEnabled(false);
    ui->actionTest->setEnabled(false);
    ui->actionClose_archive->setEnabled(false);
    ui->actionProperties->setEnabled(false);
    ui->actionFilter->setEnabled(false);
}


BrowserMainWindow::Private::~Private()
{
    delete ui;
}


void BrowserMainWindow::Private::onBrowserCoreCreated(ArchiveBrowserCore* browserCore)
{
    tabWidget->createBrowser(browserCore);
}


void BrowserMainWindow::Private::updateWindowTitle()
{
    const auto browser = tabWidget->getCurrentBrowser();

    const auto name = core->getString(MainWindowCore::UserString::WindowTitle);
    if (!browser)
        publ->setWindowTitle(name);
    else
        publ->setWindowTitle(browser->getCore()->getTitle() +  " - " + name);
}


void BrowserMainWindow::Private::setUpArchiveActions()
{
    const auto browser = tabWidget->getCurrentBrowser();

    if (!browser)
        return disableArchiveActions();

    const auto browserCore = browser->getCore();

    ui->actionAdd_to_archive->setEnabled(browserCore->isAddAvailable());
    ui->actionExtract->setEnabled(browserCore->isExtractAvailable());
    ui->actionPreview->setEnabled(browserCore->isPreviewAvailable());
    ui->actionDelete->setEnabled(browserCore->isDeleteAvailable());
    ui->actionReload->setEnabled(browserCore->isReloadAvailable());
    ui->actionTest->setEnabled(browserCore->isExtractAvailable());
    ui->actionClose_archive->setEnabled(tabWidget->count() > 0);
    ui->actionProperties->setEnabled(browserCore->archivePropertiesAvailable());
    ui->actionFilter->setEnabled(browserCore->isFilterAvailable());
}

