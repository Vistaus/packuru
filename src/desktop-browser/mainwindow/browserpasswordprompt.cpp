// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QPushButton>
#include <QIcon>
#include <QFocusEvent>
#include <QAction>

#include "../utils/makeqpointer.h"

#include "../core/passwordpromptcore.h"

#include "../desktop-core/passwordedithandler.h"

#include "browserpasswordprompt.h"
#include "ui_browserpasswordprompt.h"


using Packuru::Utils::makeQPointer;
using namespace Packuru::Core;


BrowserPasswordPrompt::BrowserPasswordPrompt(PasswordPromptCore* promptCore, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::BrowserPasswordPrompt)
{
    ui->setupUi(this);

    ui->promptTitle->setText(promptCore->getString(PasswordPromptCore::UserString::DialogTitle));
    ui->abortButton->setText(promptCore->getString(PasswordPromptCore::UserString::Abort));

    ui->acceptButton->setIcon(QIcon::fromTheme("dialog-ok"));

    const auto enterPassword = [publ = this, promptCore = makeQPointer(promptCore)] ()
    {
        const auto password = publ->ui->passwordEdit->text();
        if (password.isEmpty())
            return;

        publ->setEnabled(false);
        if (promptCore)
            promptCore->enterPassword(password);
    };

    connect(ui->acceptButton, &QPushButton::clicked,
            this, enterPassword);

    ui->abortButton->setIcon(QIcon::fromTheme("dialog-cancel"));

    const auto abortPrompt = [publ = this, promptCore = makeQPointer(promptCore)] ()
    {
        publ->setEnabled(false);
        if (promptCore)
            promptCore->abort();
    };

    connect(ui->abortButton, &QPushButton::clicked,
            this, abortPrompt);

    connect(ui->passwordEdit, &QLineEdit::returnPressed,
            this, enterPassword);

    ui->acceptButton->setEnabled(false);

    connect(ui->passwordEdit, &QLineEdit::textChanged,
            this, [publ = this] (const QString& text)
    { publ->ui->acceptButton->setEnabled(!text.isEmpty()); });

    setFocusProxy(ui->passwordEdit);

    ui->acceptButton->installEventFilter(this);
    ui->abortButton->installEventFilter(this);
    ui->passwordEdit->installEventFilter(this);

    new PasswordEditHandler(ui->passwordEdit, PasswordEditHandler::WidgetRole::Main, this);

    connect(promptCore, &QObject::destroyed,
            this, &QObject::deleteLater);

    auto abortAction = new QAction(this);
    abortAction->setShortcut(Qt::Key_Escape);
    connect(abortAction, &QAction::triggered,
            this, abortPrompt);
    addAction(abortAction);
}


BrowserPasswordPrompt::~BrowserPasswordPrompt()
{
    delete ui;
}


bool BrowserPasswordPrompt::eventFilter(QObject *watched, QEvent *event)
{
    if (event->type() == QEvent::FocusIn)
    {
        const auto focusEvent = static_cast<QFocusEvent*>(event);
        Q_ASSERT(event);

        const auto reason = focusEvent->reason();

        if  (reason == Qt::MouseFocusReason
             || reason == Qt::TabFocusReason
             || reason == Qt::BacktabFocusReason)
        {
            auto widget = qobject_cast<QWidget*>(watched);
            Q_ASSERT(widget);
            setFocusProxy(widget);
        }
    }

    return false;
}
