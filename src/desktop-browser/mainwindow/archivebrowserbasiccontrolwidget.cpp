// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QFocusEvent>
#include <QAction>

#include "qcs-qtwidgets/widgetmapper.h"

#include "core-browser/basiccontrolcontrollertype.h"
#include "core-browser/basiccontrol.h"

#include "desktop-core/errorlogwidget.h"

#include "archivebrowserbasiccontrolwidget.h"
#include "ui_archivebrowserbasiccontrolwidget.h"


using namespace qcs::qtw;
using namespace Packuru::Core::Browser;


ArchiveBrowserBasicControlWidget::ArchiveBrowserBasicControlWidget(BasicControl* basicControl,
                                                                   QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ArchiveBrowserBasicControlWidget),
    widgetMapper(new WidgetMapper(this)),
    basicControl(basicControl)
{
    ui->setupUi(this);

    using Type = BasicControlControllerType::Type;
    widgetMapper->addWidgets(Type::AbortButton, ui->abortButton);
    widgetMapper->addWidgets(Type::BrowserButton, ui->browseButton);
    widgetMapper->addWidgets(Type::ErrorsWarningsLabel, ui->errorInfoLabel);
    widgetMapper->addWidgets(Type::RetryButton, ui->retryButton);
    widgetMapper->addWidgets(Type::ViewLogButton, ui->viewLogButton);
    widgetMapper->setEngine(basicControl->getControllerEngine());

    connect(ui->retryButton, &QPushButton::clicked, basicControl, &BasicControl::retry);
    connect(ui->abortButton, &QPushButton::clicked, basicControl, &BasicControl::abort);
    connect(ui->browseButton, &QPushButton::clicked, basicControl, &BasicControl::browse);
    connect(ui->viewLogButton, &QPushButton::clicked,
            this, [widget = this] ()
    {
        ErrorLogWidget w(widget);
        w.setText(widget->basicControl->getLog());
        w.exec();
    });

    auto abortAction = new QAction(this);
    abortAction->setShortcut(Qt::Key_Escape);
    connect(abortAction, &QAction::triggered,
            this, [widget = this] ()
    {
        QPushButton* button = widget->ui->abortButton;

        if (button->isVisible() && button->isEnabled())
            return widget->basicControl->abort();

        button = widget->ui->browseButton;

        if (button->isVisible() && button->isEnabled())
            return widget->basicControl->browse();
    });
    addAction(abortAction);

    ui->retryButton->installEventFilter(this);
    ui->abortButton->installEventFilter(this);
    ui->browseButton->installEventFilter(this);
    ui->viewLogButton->installEventFilter(this);

    connect(basicControl, &BasicControl::defaultButtonChanged,
            this, [widget = this, ui = ui] (BasicControl::DefaultButton buttonId)
    {
        QWidget* button = nullptr;
        switch (buttonId)
        {
        case BasicControl::DefaultButton::Abort:
            button = ui->abortButton; break;
        case BasicControl::DefaultButton::Retry:
            button = ui->retryButton; break;
        case BasicControl::DefaultButton::BrowseArchive:
            button = ui->browseButton; break;
        case BasicControl::DefaultButton::ViewLog:
            button = ui->viewLogButton; break;
        default: Q_ASSERT(false); break;
        }

        widget->setFocusProxy(button);
    });
}


ArchiveBrowserBasicControlWidget::~ArchiveBrowserBasicControlWidget()
{
    delete ui;
}


bool ArchiveBrowserBasicControlWidget::eventFilter(QObject *watched, QEvent *event)
{
    if (event->type() == QEvent::FocusIn)
    {
        const auto focusEvent = static_cast<QFocusEvent*>(event);
        Q_ASSERT(event);

        const auto reason = focusEvent->reason();

        if (reason == Qt::MouseFocusReason
                || reason == Qt::TabFocusReason
                || reason == Qt::BacktabFocusReason)
        {
            auto widget = qobject_cast<QWidget*>(watched);
            Q_ASSERT(widget);
            setFocusProxy(widget);
        }
    }

    return false;
}
