// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QLabel>
#include <QApplication>

#include "../utils/qvariant_utils.h"

#include "../core/htmllink.h"

#include "propertiesitemdelegate.h"


using namespace Packuru::Core;


PropertiesItemDelegate::PropertiesItemDelegate(QObject *parent)
    : QStyledItemDelegate(parent)
{

}


void PropertiesItemDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    const auto* const model = qobject_cast<const QAbstractItemModel*>(index.model());
    const QVariant value = model->data(index);

    if (!model || !Packuru::Utils::containsType<QUrl>(value))
        return QStyledItemDelegate::paint(painter, option, index);

    QApplication::style()->drawControl(QStyle::CE_ItemViewItem, &option, painter);
}


QWidget* PropertiesItemDelegate::createEditor(QWidget* parent,
                                              const QStyleOptionViewItem& option,
                                              const QModelIndex& index) const
{
    Q_UNUSED(option)
    auto editor = new QLabel(parent);
    editor->setOpenExternalLinks(true);

    const auto* const model = qobject_cast<const QAbstractItemModel*>(index.model());
    Q_ASSERT(model);

    const QVariant value = model->data(index);
    Q_ASSERT(Packuru::Utils::containsType<QUrl>(value));

    editor->setText(htmlLink(value.toString(), value.toString()));

    return editor;
}


void PropertiesItemDelegate::updateEditorGeometry(QWidget* editor, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    Q_UNUSED(index)
    editor->setGeometry(option.rect);
}
