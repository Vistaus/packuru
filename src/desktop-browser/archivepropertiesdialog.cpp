// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QSettings>
#include <QPlainTextEdit>
#include <QTableView>
#include <QHeaderView>

#include "../utils/qvariant_utils.h"

#include "../core/commonstrings.h"

#include "archivepropertiesdialog.h"
#include "ui_archivepropertiesdialog.h"
#include "propertiesitemdelegate.h"


using namespace Packuru::Core;

ArchivePropertiesDialog::ArchivePropertiesDialog(QAbstractTableModel* model,
                                                 const QString& archiveComment,
                                                 QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ArchivePropertiesDialog)
{
    ui->setupUi(this);

    auto tableView = new QTableView(this);
    tableView->verticalHeader()->setDefaultAlignment(Qt::AlignRight | Qt::AlignVCenter);
    tableView->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    tableView->horizontalHeader()->hide();
    tableView->setModel(model);
    tableView->horizontalHeader()->setStretchLastSection(true);
    tableView->setItemDelegate(new PropertiesItemDelegate(this));

    // Show urls in QLabel so they look like links and are clickable
    for (int r = 0; r < model->rowCount(); ++r)
    {
        const auto index = model->index(r, 0);
        const auto value = model->data(index);
        if (Packuru::Utils::containsType<QUrl>(value))
            tableView->openPersistentEditor(index);
    }

    ui->tabWidget->addTab(tableView, CommonStrings::getString(CommonStrings::UserString::ArchivePropertiesDialogTitle));

    if (!archiveComment.isEmpty())
    {
        auto edit = new QPlainTextEdit(this);
        edit->setReadOnly(true);
        edit->setLineWrapMode(QPlainTextEdit::NoWrap);
        edit->setPlainText(archiveComment);
        edit->setFont(QFont("monospace"));
        ui->tabWidget->addTab(edit, CommonStrings::getString(CommonStrings::UserString::Comment));
        setTabOrder(ui->buttonBox, ui->tabWidget);
        setTabOrder(ui->tabWidget, tableView);
    }
    else
    {
        ui->tabWidget->setFocusPolicy(Qt::NoFocus);
        setTabOrder(ui->buttonBox, tableView);
    }

    QSettings s;
    const QSize size = s.value("archive_properties_dialog_size").toSize();
    if (size.isValid())
        resize(size);
}


ArchivePropertiesDialog::~ArchivePropertiesDialog()
{
    QSettings s;
    s.setValue("archive_properties_dialog_size", size());
    delete ui;
}
