// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QDialog>


namespace Ui {
class DeletionDialog;
}

namespace Packuru::Core::Browser
{
class DeletionDialogCore;
}

namespace qcs::qtw
{
class WidgetMapper;
}


class DeletionDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DeletionDialog(Packuru::Core::Browser::DeletionDialogCore* core, QWidget *parent = nullptr);
    ~DeletionDialog();

private:
    Ui::DeletionDialog *ui;
    Packuru::Core::Browser::DeletionDialogCore* core;
    qcs::qtw::WidgetMapper* widgetMapper;
};
