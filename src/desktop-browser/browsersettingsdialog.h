// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "../desktop-core/settingsdialog.h"


namespace Packuru::Core::Browser
{
class SettingsDialogCore;
}

namespace Packuru::Core
{
class AppSettingsDialogCore;
}


class BrowserSettingsDialog : public SettingsDialog
{
    Q_OBJECT
public:
    explicit BrowserSettingsDialog(Packuru::Core::AppSettingsDialogCore* appSettingsCore,
                                   Packuru::Core::Browser::SettingsDialogCore* browserSettingsCore,
                                   QWidget *parent = nullptr);
    ~BrowserSettingsDialog() override;
};
