// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QWidget>


namespace Ui {
class BrowserSettingsPage;
}


namespace qcs::core
{
class ControllerEngine;
}

namespace qcs::qtw
{
class WidgetMapper;
}


class BrowserSettingsPage : public QWidget
{
    Q_OBJECT
public:
    explicit BrowserSettingsPage(qcs::core::ControllerEngine* engine,
                                 const QString& pageTitle,
                                 QWidget *parent = nullptr);
    ~BrowserSettingsPage() override;

    void accept();

private:
    Ui::BrowserSettingsPage *ui;
    qcs::core::ControllerEngine* engine;
    qcs::qtw::WidgetMapper* mapper;
};
