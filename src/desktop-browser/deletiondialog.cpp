// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QSettings>

#include "../qcs-qtwidgets/widgetmapper.h"

#include "../core/commonstrings.h"

#include "../core-browser/deletiondialogcore.h"
#include "../core-browser/deletiondialogcontrollertype.h"

#include "../desktop-core/passwordedithandler.h"

#include "deletiondialog.h"
#include "ui_deletiondialog.h"


using namespace Packuru::Core;
using namespace Packuru::Core::Browser;


DeletionDialog::DeletionDialog(DeletionDialogCore* core, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DeletionDialog),
    core(core),
    widgetMapper(new qcs::qtw::WidgetMapper(this))
{
    ui->setupUi(this);

    core->setParent(this);

    widgetMapper->addLabelAndWidget(DeletionDialogControllerType::Type::Password,
                                    ui->passwordLabel,
                                    ui->passwordEdit);

    widgetMapper->addWidgets(DeletionDialogControllerType::Type::RunInQueue, ui->runInQueueCheck);

    widgetMapper->setEngine(core->getControllerEngine());

    connect(this, &DeletionDialog::accepted,
            core, &DeletionDialogCore::accept);

    if (widgetMapper->isSetVisible(DeletionDialogControllerType::Type::Password))
    {
        ui->passwordEdit->setFocus();
        new PasswordEditHandler(ui->passwordEdit, PasswordEditHandler::WidgetRole::Main, this);
    }
    else
        ui->runInQueueCheck->setFocus();

    ui->listView->setModel(core->getFileModel());
    const int count = core->getFileModel()->rowCount();
    ui->text->setText(core->getString(DeletionDialogCore::UserString::DialogQuestion, count));

    QSettings s;
    const QSize size = s.value("deletion_dialog_size").toSize();
    if (size.isValid())
        resize(size);
}


DeletionDialog::~DeletionDialog()
{
    QSettings s;
    s.setValue("deletion_dialog_size", size());
    delete ui;
}
