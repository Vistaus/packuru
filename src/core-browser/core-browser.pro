# SPDX-FileCopyrightText: The Packuru Developers
#
# SPDX-License-Identifier: 0BSD

include(../common-src.pri)
include(../common-core.pri)

MODULE_BUILD_NAME_SUFFIX = $$CORE_BROWSER_BUILD_NAME_SUFFIX
MODULE_BUILD_NAME = $$CORE_BROWSER_BUILD_NAME

include(../embed-qm-files.pri)

QT += network

TARGET = $$qtLibraryTarget($$MODULE_BUILD_NAME)

DEFINES += $${PROJECT_MACRO_NAME}_CORE_BROWSER_LIBRARY

LIBS += -L$$PROJECT_TARGET_LIB_DIR -l$$QCS_CORE_BUILD_NAME -l$${CORE_BUILD_NAME}

public_headers.files = $$files(*.h)
public_headers.path = $${PROJECT_API_TARGET_DIR}/$${MODULE_BUILD_NAME_SUFFIX}

TRANSLATIONS += \
    translations/$${MODULE_BUILD_NAME_SUFFIX}_en.ts \

HEADERS +=\
    archivebrowsercore.h \
    archivenavigator.h \
    basiccontrol.h \
    basiccontrolcontrollertype.h \
    deletiondialogcontrollertype.h \
    deletiondialogcore.h \
    init.h \
    mainwindowcore.h \
    private/basiccontrolaux.h \
    private/commandlinehandler.h \
    private/incomingmessageinterpreter.h \
    private/mainwindowcoreaux.h \
    private/queuemessengeraux.h \
    private/statuspagecoreaux.h \
    queuemessenger.h \
    settingscontrollertype.h \
    settingsdialogcore.h \
    statuspagecore.h \
    symbol_export.h \
    tabclosingdialogcore.h \
    private/archivebrowsercoreinitdata.h \
    private/archivebrowsercorenotifier.h \
    private/archivecontentfiltermodel.h \
    private/archivedialogacceptedcallback.h \
    private/archivedialogacceptedresult.h \
    private/archivenavigatorbackdoordata.h \
    private/archivenavigatorinitdata.h \
    private/deletiondialogcoreinitdata.h \
    private/itemfilteringmode.h \
    private/pluginconfigchangenotifier.h \
    private/previewrequestdata.h \
    private/commandlinereaddata.h \
    private/archivecontentmodel.h \
    private/associativemodel.h \
    private/reloadrequestdata.h \
    private/settingsconverter.h \
    private/viewfilterfilesctrl.h \
    private/viewfilterfoldersctrl.h \
    private/viewfilterscope.h \
    private/viewfilterscopectrl.h \
    private/viewfiltertextctrl.h \
    viewfiltercontrollertype.h

SOURCES += \
    archivebrowsercore.cpp \
    archivenavigator.cpp \
    basiccontrol.cpp \
    deletiondialogcontrollertype.cpp \
    deletiondialogcore.cpp \
    init.cpp \
    mainwindowcore.cpp \
    private/commandlinehandler.cpp \
    private/incomingmessageinterpreter.cpp \
    queuemessenger.cpp \
    settingsdialogcore.cpp \
    statuspagecore.cpp \
    tabclosingdialogcore.cpp \
    private/archivebrowsercorenotifier.cpp \
    private/archivecontentfiltermodel.cpp \
    private/archivenavigatorbackdoordata.cpp \
    private/commandlinereaddata.cpp \
    private/deletiondialogcoreinitdata.cpp \
    private/itemfilteringmode.cpp \
    private/pluginconfigchangenotifier.cpp \
    private/archivecontentmodel.cpp \
    private/associativemodel.cpp \
    private/settingsconverter.cpp \
    private/viewfilterfilesctrl.cpp \
    private/viewfilterfoldersctrl.cpp \
    private/viewfilterscopectrl.cpp \
    private/viewfiltertextctrl.cpp \
    viewfiltercontrollertype.cpp
