// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QItemSelectionModel>

#include "../utils/makeqpointer.h"

#include "../qcs-core/controllerengine.h"

#include "../core/commonstrings.h"

#include "archivenavigator.h"
#include "viewfiltercontrollertype.h"

#include "private/archivenavigatorinitdata.h"
#include "private/archivenavigatorbackdoordata.h"
#include "private/archivecontentmodel.h"
#include "private/archivecontentfiltermodel.h"
#include "private/viewfilterscopectrl.h"
#include "private/viewfilterfilesctrl.h"
#include "private/viewfilterfoldersctrl.h"
#include "private/viewfiltertextctrl.h"


using qcs::core::ControllerEngine;


namespace Packuru::Core::Browser
{

struct ArchiveNavigator::Private
{
    Private(ArchiveNavigator* publ, ArchiveNavigatorInitData& initData);

    bool changeDir(const QPersistentModelIndex& newRootSourceIndex);
    QString getCurrentPath() const { return currentPath; }
    QModelIndex getCurrentPathSourceIndex() const { return currentPathSourceIndex; }
    void onSelectionChanged(const QItemSelection &selected, const QItemSelection &deselected);
    int getSelectedFileCount() const { return selectedFiles; }
    int getSelectedFolderCount() const { return selectedFolders; }
    void onFilterParameterChanged();

    ArchiveNavigator* publ = nullptr;
    ArchiveNavigatorInitData initData;
    ArchiveContentFilterModel* filterModel = nullptr;
    // Selection model stores proxy indexes
    QItemSelectionModel* selectionModel = nullptr;
    ControllerEngine* filterCtrlEngine = nullptr;
    ArchiveNavigator::PreviousPathIndexSelection previousPathIndexSelection = ArchiveNavigator
            ::PreviousPathIndexSelection::NoSelection;

private:
    void setCurrentPathIndex(const QModelIndex& sourceIndex, const QModelIndex& proxyIndex);

    QString currentPath;
    QPersistentModelIndex currentPathSourceIndex;
    int selectedFiles = 0;
    int selectedFolders = 0;
};


ArchiveNavigator::ArchiveNavigator(ArchiveNavigatorInitData& initData,
                                   ArchiveNavigatorBackdoorData& backdoorData,
                                   QObject *parent)
    : QObject(parent),
      priv(new Private(this, initData))
{
    backdoorData.getSelectedItems = [publ = Utils::makeQPointer(this)] ()
    {
        std::vector<QModelIndex> items;

        if (!publ)
            return items;

        // Selection model stores proxy indexes but caller needs source indexes.
        const auto proxyIndexes = publ->priv->selectionModel->selectedIndexes();

        for (const auto& index : qAsConst(proxyIndexes))
        {
            if (index.column() == 0)
                items.push_back(publ->priv->filterModel->mapToSource(index));
        }

        return items;
    };

    backdoorData.hasSelectedFiles = [publ = Utils::makeQPointer(this)] ()
    {
        if (!publ)
            return false;
        return static_cast<bool>(publ->priv->getSelectedFileCount());
    };
}


ArchiveNavigator::~ArchiveNavigator()
{

}


QAbstractItemModel* ArchiveNavigator::getModel()
{
    return priv->filterModel;
}


QItemSelectionModel* ArchiveNavigator::getSelectionModel()
{
    return priv->selectionModel;
}


ControllerEngine* ArchiveNavigator::getFilterControllerEngine()
{
    if (!priv->filterCtrlEngine)
    {
        priv->filterCtrlEngine = new qcs::core::ControllerEngine(this);

        auto publ = Utils::makeQPointer(this);

        auto setFilterMode = [publ = publ] (bool recursive)
        {
            if (!publ)
                return;

            auto priv = publ->priv.get();
            priv->filterModel->setRecursiveFilteringEnabled(recursive);
            priv->onFilterParameterChanged();
        };

        priv->filterCtrlEngine->createTopController<ViewFilterScopeCtrl>(
                    ViewFilterControllerType::Type::FilterScope,
                    setFilterMode);

        auto setFileFiltering = [publ = publ] (ItemFilteringMode mode)
        {
            if (!publ)
                return;

            auto priv = publ->priv.get();
            priv->filterModel->setFileFiltering(mode);
            priv->onFilterParameterChanged();
        };

        priv->filterCtrlEngine->createTopController<ViewFilterFilesCtrl>
                (ViewFilterControllerType::Type::FilterFiles,
                 std::move(setFileFiltering));

        auto setFolderFiltering = [publ = publ] (ItemFilteringMode mode)
        {
            if (!publ)
                return;

            auto priv = publ->priv.get();
            priv->filterModel->setFolderFiltering(mode);
            priv->onFilterParameterChanged();
        };

        priv->filterCtrlEngine->createTopController<ViewFilterFoldersCtrl>
                (ViewFilterControllerType::Type::FilterFolders,
                 std::move(setFolderFiltering));

        auto setFilterText = [publ = publ] (const QString& text)
        {
            if (!publ)
                return;

            auto priv = publ->priv.get();
            priv->filterModel->setFilterEnabled(text.size() > 0);
            priv->filterModel->setFilterFixedString(text);
            priv->onFilterParameterChanged();
        };

        priv->filterCtrlEngine->createTopController<ViewFilterTextCtrl>
                (ViewFilterControllerType::Type::FilterText,
                 std::move(setFilterText));
    }

    return priv->filterCtrlEngine;
}


void ArchiveNavigator::openItem(const QModelIndex& index)
{
    Q_ASSERT(priv->filterModel->checkIndex(index,
                                           QAbstractItemModel::CheckIndexOption::IndexIsValid)
             || !index.isValid());

    const auto sourceIndex = priv->filterModel->mapToSource(index);

    if (priv->initData.archiveModel->isDir(sourceIndex))
        priv->changeDir(sourceIndex);
    else
    {
        if (priv->initData.fileClicked)
            priv->initData.fileClicked(sourceIndex);
    }
}


bool ArchiveNavigator::changeDir(const QString& absFilePath)
{
    if (absFilePath.isEmpty())
        return false;
    else if (absFilePath == QLatin1String("/"))
        return priv->changeDir(QModelIndex());

    const auto sourceIndex = priv->initData.archiveModel->getDirectoryIndex(absFilePath);

    /* Invalid index at this points means that the path does not exist in the source model,
       as root dir (/) is handled at the beginning of the function. But even if it's valid
       this path might have been filtered out in the filter model, in which case
       changeDir(const QModelIndex&) will return false. */
    if (sourceIndex.isValid())
        return priv->changeDir(sourceIndex);

    return false;
}


bool ArchiveNavigator::goUp()
{
    const auto currentRootSourceIndex = priv->getCurrentPathSourceIndex();

    if (!currentRootSourceIndex.isValid())
        return false;

    return priv->changeDir(currentRootSourceIndex.parent());
}


bool ArchiveNavigator::getCanGoUp() const
{
    return priv->getCurrentPathSourceIndex().isValid();
}


QString ArchiveNavigator::getCurrentPath() const
{
    return priv->getCurrentPath();
}


QModelIndex ArchiveNavigator::getCurrentPathIndex() const
{
    return priv->filterModel->mapFromSource(priv->getCurrentPathSourceIndex());
}


void ArchiveNavigator::setPreviousPathIndexSelection(PreviousPathIndexSelection value)
{
    Q_ASSERT(value != PreviousPathIndexSelection::___INVALID);

    if (value == priv->previousPathIndexSelection)
        return;

    priv->previousPathIndexSelection = value;
    emit previousPathIndexSelectionChanged(value);
}


ArchiveNavigator::PreviousPathIndexSelection ArchiveNavigator::getPreviousPathIndexSelection() const
{
    return priv->previousPathIndexSelection;
}


std::vector<int> ArchiveNavigator::getModelDefaultSections() const
{
    return priv->initData.archiveModel->getDefaultSections();
}


void ArchiveNavigator::sortModel(int column, Qt::SortOrder order)
{
    priv->filterModel->sort(column, order);
}


QString ArchiveNavigator::getString(UserString value) const
{
    switch (value)
    {
    case UserString::Close:
        return CommonStrings::getString(CommonStrings::UserString::Close);

    default:
        Q_ASSERT(false); return QString();
    }
}


ArchiveNavigator::Private::Private(ArchiveNavigator* publ, ArchiveNavigatorInitData& initData)
    : publ(publ),
      initData(std::move(initData)),
      filterModel(new ArchiveContentFilterModel(publ)),
      selectionModel(new QItemSelectionModel(filterModel, publ))
{
    filterModel->setSourceModel(initData.archiveModel);

    QObject::connect(filterModel, &QAbstractItemModel::modelAboutToBeReset,
                     publ, [priv = this] ()
    {
        // Clear it now so when the filter model starts rebuilding the tree
        // filter settings are already cleared.
        priv->filterModel->clearFilterSettings();
    });

    QObject::connect(filterModel, &QAbstractItemModel::modelReset,
                     publ, [priv = this] ()
    {
        // Selection model will reset itself, however it won't emit selectionChanged() signal.

        const QLatin1Char rootDir('/');
        if (priv->currentPath != rootDir)
        {
            priv->currentPath = rootDir;
            emit priv->publ->currentPathChanged(priv->currentPath);
            // Current path source index is a persistent index so it is automatically invalidated
            // on model reset.
            emit priv->publ->currentPathIndexChanged(QModelIndex());
        }
    });

    QObject::connect(selectionModel, &QItemSelectionModel::selectionChanged,
                     publ, [priv = this] (const QItemSelection &selected, const QItemSelection &deselected)
    { priv->onSelectionChanged(selected, deselected); });
}


bool ArchiveNavigator::Private::changeDir(const QPersistentModelIndex& newRootSourceIndex)
{
    auto const archiveModel = initData.archiveModel;

    Q_ASSERT(archiveModel);
    if (!archiveModel)
        return false;

    Q_ASSERT(archiveModel->checkIndex(newRootSourceIndex, QAbstractItemModel::CheckIndexOption::IndexIsValid)
             || !newRootSourceIndex.isValid());

    const QPersistentModelIndex currentRootSourceIndex = getCurrentPathSourceIndex();

    if (newRootSourceIndex == currentRootSourceIndex || !archiveModel->isDir(newRootSourceIndex))
        return false;

    // Will invalidate the filter when filterModel works in "Current dir" mode, but not in recursive mode
    filterModel->setFilterRootSourceIndex(newRootSourceIndex);

    /* New root proxy index must be mapped after setting filterModel's root index because setting
       the root index might invalidate the filter data and cause rebuilding of the tree.
       Additionally, changing root index or modyfing QItemSelectionModel causes signal emition
       which theoretically allows UI code to change the filter model in some way in the middle
       of this function and invalidate indexes.
       Therefore its best to store indexes as QPersistentModelIndex. */
    const QPersistentModelIndex newRootProxyIndex = filterModel->mapFromSource(newRootSourceIndex);

    /* A user can provide a path that has been filtered out in the filter model when it works in
       recursive mode, in which case we don't change dir.
       But when recursive mode is disabled we can make any path that's present in the source model
       available in the filter model by setting a new filter root index. */
    if (filterModel->isRecursiveFilteringEnabled())
    {
        // The dir has been filtered out
        if (newRootSourceIndex.isValid() && !newRootProxyIndex.isValid())
            return false;
    }

    // Will emit signals
    setCurrentPathIndex(newRootSourceIndex, newRootProxyIndex);

    // Modyfing QItemSelectionModel causes signal emition
    selectionModel->clear();

    // If going up
    if (newRootSourceIndex == currentRootSourceIndex.parent())
    {
        const QModelIndex proxyPreviousRoot = filterModel->mapFromSource(currentRootSourceIndex);

        // We're going up so proxyPreviousRoot can be invalid only if it has been filtered out
        if (proxyPreviousRoot.isValid())
        {
            if (previousPathIndexSelection != ArchiveNavigator::PreviousPathIndexSelection::NoSelection)
            {
                QItemSelectionModel::SelectionFlags flags;

                switch (previousPathIndexSelection)
                {
                case ArchiveNavigator::PreviousPathIndexSelection::SelectFirstIndex:
                    flags = QItemSelectionModel::Select; break;
                case ArchiveNavigator::PreviousPathIndexSelection::SelectRow:
                    flags = QItemSelectionModel::Select | QItemSelectionModel::Rows; break;
                default:
                    Q_ASSERT(false);
                }

                selectionModel->select(proxyPreviousRoot, flags);
            }

            selectionModel->setCurrentIndex(proxyPreviousRoot, QItemSelectionModel::NoUpdate);
        }
    }
    else
    {
        if (filterModel->rowCount(newRootProxyIndex) > 0)
        {
            const QModelIndex proxyFirstChild = filterModel->index(0, 0, newRootProxyIndex);
            selectionModel->setCurrentIndex(proxyFirstChild, QItemSelectionModel::NoUpdate);
        }
    }

    return true;
}


void ArchiveNavigator::Private::setCurrentPathIndex(const QModelIndex& sourceIndex,
                                                    const QModelIndex& proxyIndex)
{
    Q_ASSERT(sourceIndex == filterModel->mapToSource(proxyIndex));

    if (sourceIndex == currentPathSourceIndex)
        return;

    currentPathSourceIndex = sourceIndex;
    currentPath = initData.archiveModel->getAbsoluteFilePath(sourceIndex,
                                                             ArchiveContentModel::PathFormat::Browser);

    emit publ->currentPathIndexChanged(proxyIndex);
    emit publ->currentPathChanged(currentPath);
    emit publ->canGoUpChanged(currentPathSourceIndex.isValid());
}


void ArchiveNavigator::Private::onSelectionChanged(const QItemSelection& selected,
                                                   const QItemSelection& deselected)
{
    const auto* const archiveModel = initData.archiveModel;
    const auto prevSelectedFiles = selectedFiles;
    const auto prevSelectedFolders = selectedFolders;

    const auto proxySelectedIndexes = selected.indexes();
    for (const auto& proxyIndex : qAsConst(proxySelectedIndexes))
    {
        if (proxyIndex.column() == 0)
        {
            const auto sourceIndex = filterModel->mapToSource(proxyIndex);
            if (archiveModel->isDir(sourceIndex))
                ++selectedFolders;
            else
                ++selectedFiles;
        }
    }

    const auto proxyDeselectedIndexes = deselected.indexes();
    for (const auto& proxyIndex : qAsConst(proxyDeselectedIndexes))
    {
        if (proxyIndex.column() == 0)
        {
            const auto sourceIndex = filterModel->mapToSource(proxyIndex);
            if (archiveModel->isDir(sourceIndex))
                --selectedFolders;
            else
                --selectedFiles;
        }
    }

    if (selectedFiles != prevSelectedFiles || selectedFolders != prevSelectedFolders)
    {
        if (initData.selectionChanged)
            initData.selectionChanged(selectedFiles, selectedFolders);
    }
}


void ArchiveNavigator::Private::onFilterParameterChanged()
{
    const QModelIndex currentPathSourceIndex = getCurrentPathSourceIndex();

    /* Current path index might have been filtered out if filter model works in recursive mode,
       in which case we have to go up in the tree. */
    const auto newPathSourceIndex = filterModel->findClosestRelativeOfSource(currentPathSourceIndex);

    const auto newPathProxyIndex = filterModel->mapFromSource(newPathSourceIndex);

    // changeDir() returns false if new root equals current root
    if (changeDir(newPathSourceIndex))
        return;
    /* This covers the case when entire tree (filter's recursive mode) or branch (non recursive)
       was previously filtered out so the view was empty and current index was invalidated. So
       we try to set a new current index if there is none and if there are items in current dir. */
    else if (!selectionModel->currentIndex().isValid() && filterModel->rowCount(newPathProxyIndex) > 0)
    {
        const QModelIndex proxyFirstChild = filterModel->index(0, 0, newPathProxyIndex);
        selectionModel->setCurrentIndex(proxyFirstChild, QItemSelectionModel::NoUpdate);
    }
}

}
