// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QStringListModel>

#include "../qcs-core/controllerengine.h"

#include "../core/commonstrings.h"
#include "../core/private/plugin-api/encryptionstate.h"
#include "../core/private/tasks/deletiontaskdata.h"
#include "../core/private/dialogcore/runinqueuectrl.h"
#include "../core/private/dialogcore/genericpasswordctrl.h"
#include "../core/private/privatestrings.h"

#include "deletiondialogcore.h"
#include "deletiondialogcontrollertype.h"
#include "private/deletiondialogcoreinitdata.h"


using namespace Packuru::Core;
using namespace qcs::core;


namespace Packuru::Core::Browser
{

struct DeletionDialogCore::Private
{
    DeletionDialogCoreInitData initData_;
    ControllerEngine* engine = nullptr;
    bool accepted = false;
    QStringListModel* fileModel = nullptr;
};


DeletionDialogCore::DeletionDialogCore(DeletionDialogCoreInitData initData, QObject *parent)
    : QObject(parent),
      priv(new DeletionDialogCore::Private)
{
    priv->initData_ = std::move(initData);
    Q_ASSERT(priv->initData_.isValid());

    priv->engine = new ControllerEngine(this);

    const bool acceptsPassword
            = priv->initData_.archiveEncryption == EncryptionState::FilesContentOnly
            && priv->initData_.password.isEmpty();

    priv->engine->createTopController<GenericPasswordCtrl>(DeletionDialogControllerType::Type::Password,
                                                              priv->initData_.password,
                                                              acceptsPassword);
    priv->engine->createTopController<RunInQueueCtrl>
            (DeletionDialogControllerType::Type::RunInQueue, RunInQueueCtrl::Mode::Switchable);

    priv->engine->updateAllControllers();

    priv->fileModel = new QStringListModel(this);

    const auto fileCount = priv->initData_.filesToRemove.size();
    priv->fileModel->insertRows(0, static_cast<int>(fileCount));

    for (std::size_t i = 0; i < fileCount; ++i)
    {
        const auto index = priv->fileModel->index(static_cast<int>(i), 0);
        priv->fileModel->setData(index, priv->initData_.filesToRemove[i]);
    }
}


DeletionDialogCore::~DeletionDialogCore()
{
}


void DeletionDialogCore::destroyLater()
{
    deleteLater();
}


QString DeletionDialogCore::getString(DeletionDialogCore::UserString value, int count)
{
    switch (value)
    {
    case UserString::Cancel:
        return CommonStrings::getString(CommonStrings::UserString::Cancel);

    case UserString::DialogQuestion:
        return Packuru::Core::Browser::DeletionDialogCore::tr("Do you want to delete %n items?", "", count);

    case UserString::DialogTitle:
        return Packuru::Core::Browser::DeletionDialogCore::tr("Delete files");

    default:
        Q_ASSERT(false); return "";
    }
}


qcs::core::ControllerEngine* DeletionDialogCore::getControllerEngine()
{
    return priv->engine;
}


QAbstractItemModel* DeletionDialogCore::getFileModel()
{
    return priv->fileModel;
}


void DeletionDialogCore::accept()
{
    Q_ASSERT(!priv->accepted);
    priv->accepted = true;

    auto values = priv->engine->getAvailableCurrentValues<DeletionDialogControllerType::Type>();

    DeletionTaskData data;
    data.runInQueue = Utils::getValueAs<bool>(values[DeletionDialogControllerType::Type::RunInQueue]);
    data.backendData.password = Utils::getValueAs<QString>(values,
                                                           DeletionDialogControllerType::Type::Password);
    data.backendData.absoluteFilePaths = priv->initData_.filesToRemove;
    data.backendData.archiveType = priv->initData_.archiveType;
    data.backendData.archiveInfo.setFile(priv->initData_.archiveAbsFilePath);
    priv->initData_.onAccepted(data);
}


}
