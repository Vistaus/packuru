// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QObject>

/// @file

namespace Packuru::Core::Browser
{

/**
 @brief Identifies input widgets of Basic Control widget inside Status Page.
 @sa BasicControl, StatusPageCore
 @headerfile "core-browser/basiccontrolcontrollertype.h"
 */
class BasicControlControllerType : public QObject
{
    Q_OBJECT
public:
    /// @copybrief Packuru::Core::Browser::BasicControlControllerType
    enum class Type
    {
        AbortButton, ///< Button.
        BrowserButton, ///< Button.
        ErrorsWarningsLabel, ///< Label.
        RetryButton, ///< Button.
        ViewLogButton ///< Button.
    };
    Q_ENUM(Type)
};

}
