// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QObject>

/// @file

namespace qcs::core
{
class ControllerEngine;
}

namespace Packuru::Core::Browser
{

namespace BasicControlAux
{
struct Init;
struct Backdoor;
}

/**
 * @brief Provides basic control of the Status Page.
 *
 * %BasicControl features:
 * * Retry last task.
 * * Abort current task.
 * * Switch archive browser back to navigation mode.
 * * Contains task log.
 * * Contains task error information.
 * * Notifies which button is currently recommended to have focus.
 *
 * %BasicControl object is provided by StatusPageCore object.
 *
 * @headerfile "core-browser/basiccontrol.h"
 */
class BasicControl : public QObject
{
    Q_OBJECT
    Q_PROPERTY(DefaultButton defaultButton READ getDefaultButton NOTIFY defaultButtonChanged)

public:
    /**
     * @brief Identifies the recommended focused button.
     */
    enum class DefaultButton
    {
        ___INVALID,

        Abort, ///< 'Abort' button.
        BrowseArchive, ///< 'Browse archive' button.
        Retry, ///< 'Retry' button.
        ViewLog, ///< 'View log' button.
    };
    Q_ENUM(DefaultButton)

    BasicControl(BasicControlAux::Init& init,
                 BasicControlAux::Backdoor& backdoor,
                 QObject* parent = nullptr);
    ~BasicControl();

    /** @brief Returns pointer to ControllerEngine controlling widgets in Basic Control widget.
     *
     * The widgets must be mapped in WidgetMapper using BasicControlControllerType::Type enum as the ID.
     *
     * @note Object ownership is not transferred to the caller.
     * @sa qcs::qqc::WidgetMapper, qcs::qtw::WidgetMapper
     */
    Q_INVOKABLE qcs::core::ControllerEngine* getControllerEngine();

    /**
     * @brief Returns recommended focused button id.
     * @sa DefaultButton, defaultButtonChanged()
     */
    Q_INVOKABLE DefaultButton getDefaultButton() const;

    /// @brief Returns task log.
    Q_INVOKABLE QString getLog() const;

    /// @brief Attempts to re-run last task.
    Q_INVOKABLE void retry();

    /// @brief Aborts current task.
    Q_INVOKABLE void abort();

    /// @brief Switches ArchiveBrowserCore to navigation mode.
    Q_INVOKABLE void browse();

signals:
    /// @brief Signals when the recommended focused button has changed.
    void defaultButtonChanged(DefaultButton button);

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}
