// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QObject>
#include <QModelIndex>

#include "symbol_export.h"

/// @file

class QAbstractItemModel;
class QItemSelectionModel;

namespace qcs::core
{
class ControllerEngine;
}


namespace Packuru::Core::Browser
{

struct ArchiveNavigatorInitData;
struct ArchiveNavigatorBackdoorData;


/**
 * @brief Handles folder navigation, single file opening, selection and filtering of items.
 *
 * %ArchiveNavigator object is @link ArchiveBrowserCore::getNavigator() provided @endlink
 * by ArchiveBrowserCore object.
 * @headerfile "core-browser/archivenavigator.h"
 */
class PACKURU_CORE_BROWSER_EXPORT ArchiveNavigator : public QObject
{
    Q_OBJECT
    /** @brief Holds item model.
     * @sa getModel()
      */
    Q_PROPERTY(QAbstractItemModel* model READ getModel CONSTANT)

    /** @brief Holds item selection model.
     * @sa getSelectionModel()
      */
    Q_PROPERTY(QItemSelectionModel* selectionModel READ getSelectionModel CONSTANT)

    /** @brief Holds whether navigation up in the tree is possible.
     * @sa getCanGoUp(), canGoUpChanged(), goUp()
      */
    Q_PROPERTY(bool canGoUp READ getCanGoUp NOTIFY canGoUpChanged)

    /** @brief Holds current path.
     * @sa getCurrentPath(), currentPathChanged()
      */
    Q_PROPERTY(QString currentPath READ getCurrentPath NOTIFY currentPathChanged)

    /** @brief Holds QModelIndex of current path.
     * @sa getCurrentPathIndex(), currentPathIndexChanged()
      */
    Q_PROPERTY(QModelIndex currentPathIndex READ getCurrentPathIndex NOTIFY currentPathIndexChanged)

    /** @brief Holds previous path item selection mode.
     *
     * @sa PreviousPathIndexSelection, getPreviousPathIndexSelection(), setPreviousPathIndexSelection(),
     * previousPathIndexSelectionChanged()
      */
    Q_PROPERTY(PreviousPathIndexSelection previousPathIndexSelection READ getPreviousPathIndexSelection WRITE setPreviousPathIndexSelection NOTIFY previousPathIndexSelectionChanged)

public:
    /** @brief Describes previous path item selection mode.
     *
     * When going up in the tree, previous path first index or entire row can be selected automatically.
     * Single index selection is useful for list views and entire row selection is useful for
     * table/tree views.
     *
     * The default is NoSelection.
     */
    enum class PreviousPathIndexSelection
    {
        ___INVALID, // Keep at index 0 to catch implicit conversions of invalid values to 0 in QML

        NoSelection, ///< No selection.
        SelectFirstIndex, ///< Select first index.
        SelectRow ///< Select row.
    };
    Q_ENUM(PreviousPathIndexSelection)

    /// @copydoc ArchivingDialogCore::UserString
    enum class UserString
    {
        ___INVALID, // Keep at index 0 to catch implicit conversions of invalid values to 0 in QML

        Close, ///< [Filter] 'Close' button.
    };
    Q_ENUM(UserString)

    ArchiveNavigator(ArchiveNavigatorInitData& initData,
                     ArchiveNavigatorBackdoorData& backdoorData,
                     QObject *parent);
    ~ArchiveNavigator();

    /** @brief Returns item model.
     * @note Object ownership is not transferred.
     * @sa model
     */
    Q_INVOKABLE QAbstractItemModel* getModel();

    /** @brief Returns item selection model.
     * @note Object ownership is not transferred.
     * @sa selectionModel
     */
    Q_INVOKABLE QItemSelectionModel* getSelectionModel();

    /** @brief Returns item filter's ControllerEngine.
     *
     * The engine controls item filter's input widgets.
     * The widgets must be mapped in WidgetMapper using ViewFilterControllerType:Type enum as the ID.
     * @note Object ownership is not transferred to the caller.
     * @sa qcs::qqc::WidgetMapper, qcs::qtw::WidgetMapper
     */
    Q_INVOKABLE qcs::core::ControllerEngine* getFilterControllerEngine();

    /**
     * @brief Opens item corresponding to specified index.
     *
     * It the item is a file it is extracted for preview; if the item is a folder it is
     * navigated into which subsequently updates currentPath, currentPathIndex and canGoUp.
     *
     * @warning Passing outdated indexes is not supported; however passing invalid indexes
     * (QModelIndex::isValid() == false) is permitted.
     */
    Q_INVOKABLE void openItem(const QModelIndex& index);

    /**
     * @brief Attems to change directory to the one specified.
     *
     * If the attempt is successfull properties currentPath, currentPathIndex and canGoUp are updated.
     * @return Returns true on success, otherwise returns false.
     */
    Q_INVOKABLE bool changeDir(const QString& absFilePath);

    /**
     * @brief Attems to go up in the item tree.
     *
     * If the attempt is successfull properties currentPath, currentPathIndex and canGoUp are updated.
     * @return Returns true on success, otherwise returns false.
     * @sa getCanGoUp(), canGoUpChanged(), canGoUp
     */
    Q_INVOKABLE bool goUp();

    /**
     * @brief Returns whether going up in the item tree is possible.
     @ @sa canGoUpChanged(), goUp(), canGoUp
     */
    Q_INVOKABLE bool getCanGoUp() const;

    /**
     * @brief Returns current path.
     * @sa currentPathChanged(), currentPath
     */
    Q_INVOKABLE QString getCurrentPath() const;

    /**
     * @brief Returns current path index.
     * @sa currentPathIndexChanged(), currentPathIndex
     */
    Q_INVOKABLE QModelIndex getCurrentPathIndex() const;

    /**
     * @brief Sets PreviousPathIndexSelection mode.
     */
    Q_INVOKABLE void setPreviousPathIndexSelection(PreviousPathIndexSelection value);

    /**
     * @brief Returns PreviousPathIndexSelection mode.
     */
    Q_INVOKABLE PreviousPathIndexSelection getPreviousPathIndexSelection() const;

    /**
     * @brief Returns a list of item model default section/colummn numbers.
     *
     * The default are name, size, modification date.
     *
     * This allows to show only the most important sections in the UI even if the user has not
     * change settings yet.
     */
    std::vector<int> getModelDefaultSections() const;

    /** @brief Sorts item model.
     *
     * @note This function is a workaround for QML; in C++ item model can be sorted directly
     * by calling QAbstractItemModel::sort().
     *
     * In QML, invoking navigator.model.sort() will not work correctly and will result in
     * folders mixed with files etc. This is because model classes used in ArchiveNavigator
     * are not part of frontend API (QAbstractItemModel pointers are used) and therefore
     * are not registered in QML.
     */
    Q_INVOKABLE void sortModel(int column, Qt::SortOrder order);

    /// @copydoc ArchivingDialogCore::getString
    Q_INVOKABLE QString getString(UserString value) const;

signals:
    /**
     * @brief Signals that current path has changed.
     * @sa getCurrentPath(), currentPath
     */
    void currentPathChanged(const QString& path);

    /**
     * @brief Signals that current path index has changed.
     * @sa getCurrentPathIndex(), currentPathIndex
     */
    void currentPathIndexChanged(const QModelIndex& proxyIndex);

    /**
     * @brief Signals that
     * @param value
     */
    void canGoUpChanged(bool value);

    /**
     * @brief Signals that PreviousPathIndexSelection mode has changed.
     * @sa setPreviousPathIndexSelection(), getPreviousPathIndexSelection(), previousPathIndexSelection
     */
    void previousPathIndexSelectionChanged(PreviousPathIndexSelection value);

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}
