// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QObject>

#include "symbol_export.h"

/// @file

namespace Packuru::Core::Browser
{

/**
 * @brief Contains all data necessary for Browser Tab Closing dialog.
 *
 * This class is manually created. It only contains data and does not close the tab itself.
 *
 * @headerfile "core-browser/tabclosingdialogcore.h"
 */
class PACKURU_CORE_BROWSER_EXPORT TabClosingDialogCore : public QObject
{
    Q_OBJECT
public:
    /// @copydoc ArchivingDialogCore::UserString
    enum class UserString
    {
        ___INVALID, // Keep at index 0 to catch implicit conversions of invalid values to 0 in QML

        Cancel, ///< 'Cancel' button.
        Close, ///< 'Close' button.
        DialogTitle, ///< Dialog title.
        DoYouWantToCloseIt, ///< 'Do you want to close it?' question.
        ThisTabIsBusy ///< 'This tab is busy.' label.
    };
    Q_ENUM(UserString)

    explicit TabClosingDialogCore(QObject *parent = nullptr);

    /// @copydoc ArchivingDialogCore::getString
    Q_INVOKABLE static QString getString(UserString value);
};

}
