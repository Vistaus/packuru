// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QObject>

#include "symbol_export.h"

/// @file

namespace Packuru::Core::Browser
{

/**
 @brief Identifies input widgets in Deletion Dialog.
 @headerfile "core-browser/deletiondialogcontrollertype.h"
 */
class DeletionDialogControllerType : public QObject
{
    Q_OBJECT
public:
    /// @copybrief Packuru::Core::DeletionDialogControllerType
    enum class Type
    {
        Password, ///< Line edit.
        RunInQueue, ///< Check box.
    };
    Q_ENUM(Type)
};

PACKURU_CORE_BROWSER_EXPORT uint qHash(DeletionDialogControllerType::Type value, uint seed = 0);

}
