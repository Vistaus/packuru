// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "qcs-core/controller.h"
#include "qcs-core/controllerengine.h"

#include "basiccontrol.h"
#include "private/basiccontrolaux.h"
#include "basiccontrolcontrollertype.h"


namespace
{

class ButtonController: public qcs::core::ControllerBase
{
public:
    ButtonController(const QString& text);

    void setButtonVisible(bool value);
    void setButtonEnabled(bool value);
};


class LabelController: public qcs::core::Controller<QString>
{
public:
    LabelController();

    void setText(const QString& text);
};

}


namespace Packuru::Core::Browser
{

struct BasicControl::Private
{
    void setUpControllers();
    void setButtonState(BasicControlControllerType::Type button,
                        BasicControlAux::ButtonVisible visible,
                        BasicControlAux::ButtonEnabled enabled);
    void setDefaultButton(BasicControlControllerType::Type button);
    void setErrorsAndWarnings(const QString& text);
    void setLog(const QString& text);

    BasicControl* publ = nullptr;
    BasicControlAux::Init init;
    QString log;
    qcs::core::ControllerEngine* engine = nullptr;
    BasicControlControllerType::Type defaultButton;
};


BasicControl::BasicControl(BasicControlAux::Init& init,
                           BasicControlAux::Backdoor& backdoor,
                           QObject* parent)
    : QObject(parent),
      priv(new Private)
{
    priv->publ = this;
    priv->init = std::move(init);
    priv->engine = new qcs::core::ControllerEngine(this);
    priv->setUpControllers();

    backdoor.setButtonState = [priv = priv.get()]
            (BasicControlControllerType::Type button,
            BasicControlAux::ButtonVisible visible,
            BasicControlAux::ButtonEnabled enabled)
    { priv->setButtonState(button, visible, enabled); };

    backdoor.setDefaultButton= [priv = priv.get()] (BasicControlControllerType::Type button)
    { priv->setDefaultButton(button); };

    backdoor.setErrorsAndWarnings = [priv = priv.get()] (const QString& text)
    { priv->setErrorsAndWarnings(text); };

    backdoor.setLog = [priv = priv.get()] (const QString& text)
    { priv->setLog(text); };
}


BasicControl::~BasicControl()
{

}


qcs::core::ControllerEngine* BasicControl::getControllerEngine()
{
    return priv->engine;
}


BasicControl::DefaultButton BasicControl::getDefaultButton() const
{
    switch (priv->defaultButton)
    {
    case BasicControlControllerType::Type::AbortButton: return DefaultButton::Abort;
    case BasicControlControllerType::Type::BrowserButton: return DefaultButton::BrowseArchive;
    case BasicControlControllerType::Type::RetryButton: return DefaultButton::Retry;
    case BasicControlControllerType::Type::ViewLogButton: return DefaultButton::ViewLog;
    default: Q_ASSERT(false); return DefaultButton::___INVALID;
    }
}


QString BasicControl::getLog() const
{
    return priv->log;
}


void BasicControl::retry()
{
    priv->init.retryRequested();
}


void BasicControl::abort()
{
    priv->init.abortRequested();
}


void BasicControl::browse()
{
    priv->init.browseRequested();
}


void BasicControl::Private::setUpControllers()
{
    using Type = BasicControlControllerType::Type;

    engine->createTopController<ButtonController>(Type::AbortButton,
                                                  Packuru::Core::Browser::BasicControl::tr("Abort"));

    engine->createTopController<ButtonController>(Type::BrowserButton,
                                                  Packuru::Core::Browser::BasicControl::tr("Browse archive"));

    engine->createTopController<ButtonController>(Type::RetryButton,
                                                  Packuru::Core::Browser::BasicControl::tr("Retry"));

    engine->createTopController<ButtonController>(Type::ViewLogButton,
                                                  Packuru::Core::Browser::BasicControl::tr("View log"));

    engine->createTopController<LabelController>(Type::ErrorsWarningsLabel);

    engine->updateAllControllers();
}


void BasicControl::Private::setButtonState(BasicControlControllerType::Type button,
                                           BasicControlAux::ButtonVisible visible,
                                           BasicControlAux::ButtonEnabled enabled)
{
    auto ctrl = dynamic_cast<ButtonController*>(engine->getController(button));
    Q_ASSERT(ctrl);

    using BasicControlAux::ButtonVisible;
    using BasicControlAux::ButtonEnabled;

    ctrl->setButtonVisible(visible == ButtonVisible::True);
    ctrl->setButtonEnabled(enabled == ButtonEnabled::True);
}


void BasicControl::Private::setDefaultButton(BasicControlControllerType::Type button)
{
    if (button != defaultButton)
    {
        defaultButton = button;
        emit publ->defaultButtonChanged(publ->getDefaultButton());
    }
}


void BasicControl::Private::setErrorsAndWarnings(const QString& text)
{
    auto ctrl = dynamic_cast<LabelController*>(engine->getController(BasicControlControllerType::Type::ErrorsWarningsLabel));
    Q_ASSERT(ctrl);

    ctrl->setText(text);
}


void BasicControl::Private::setLog(const QString& text)
{
    log = text;
}

}


namespace
{

ButtonController::ButtonController(const QString& text)
{
    properties[qcs::core::ControllerProperty::Text] = text;
    setVisible(true);
    setEnabled(true);
}


void ButtonController::setButtonVisible(bool value)
{
    setVisible(value);
    externalValueInput();
}


void ButtonController::setButtonEnabled(bool value)
{
    setEnabled(value);
    externalValueInput();
}

}


LabelController::LabelController()
{
    setVisible(false);
}


void LabelController::setText(const QString& text)
{
    setCurrentValue(text);
    setVisible(!text.isEmpty());
    externalValueInput();
}
