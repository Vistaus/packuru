// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QObject>

#include "symbol_export.h"

/// @file

namespace Packuru::Core::Browser
{

/**
 @brief Identifies input widgets in View Filter dialog.
 @headerfile "core-browser/viewfiltercontrollertype.h"
 */
class ViewFilterControllerType : public QObject
{
    Q_OBJECT
public:
    /// @copydoc ArchivingDialogCore::UserString
    enum class Type
    {
        FilterFiles, ///< 'Files' label.
        FilterFolders, ///< 'Folders' label.
        FilterScope, ///< 'Scope' label.
        FilterText ///< Filter's placeholder text.
    };
    Q_ENUM(Type)
};

PACKURU_CORE_BROWSER_EXPORT uint qHash(ViewFilterControllerType::Type value, uint seed = 0);

}
