// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QCoreApplication>
#include <QFileInfo>
#include <QStandardPaths>
#include <QDir>

#include "../utils/makeqpointer.h"
#include "../utils/private/qttypehasher.h"

#include "../core/appinfo.h"
#include "../core/commonstrings.h"
#include "../core/plugindatamodel.h"
#include "../core/private/archivetypedetector.h"
#include "../core/private/archiveservices.h"
#include "../core/private/tasks/readtaskdata.h"
#include "../core/private/tasks/extractiontaskdata.h"
#include "../core/private/tasks/testtask.h"
#include "../core/private/tasks/testtaskdata.h"
#include "../core/private/tasks/deletiontask.h"
#include "../core/private/tasks/extractiontask.h"
#include "../core/private/tasks/archivingtaskdata.h"
#include "../core/private/tasks/archivingtask.h"
#include "../core/private/createmessage.h"
#include "../core/private/messagetype.h"
#include "../core/private/plugin-api/archivingparameterhandlerfactory.h"
#include "../core/private/plugindata.h"
#include "../core/private/tasks/deletiontaskdata.h"
#include "../core/private/pluginmanager.h"
#include "../core/private/dialogcore/appsettingsdialog/appsettingsdialogcoreaux.h"
#include "../core/private/privatestrings.h"
#include "../core/private/plugindatamodelaux.h"

#include "mainwindowcore.h"
#include "private/mainwindowcoreaux.h"
#include "archivebrowsercore.h"
#include "queuemessenger.h"
#include "settingsdialogcore.h"
#include "private/previewrequestdata.h"
#include "private/reloadrequestdata.h"
#include "private/archivebrowsercoreinitdata.h"
#include "private/archivebrowsercorenotifier.h"
#include "private/incomingmessageinterpreter.h"
#include "private/commandlinereaddata.h"
#include "private/pluginconfigchangenotifier.h"
#include "private/queuemessengeraux.h"


using Packuru::Utils::makeQPointer;


namespace Packuru::Core::Browser
{

using namespace MainWindowCoreAux;


struct MainWindowCore::Private
{
    Private();

    ReadTask* createReadTask(const ReloadRequestData& reqData);

    template <typename TaskData>
    static ArchiveDialogAcceptedResult archiveDialogAcceptedCommon(QPointer<MainWindowCore> publ,
                                                                   MessageType messageType,
                                                                   const TaskData& taskData,
                                                                   QPointer<ArchiveBrowserCoreNotifier> notifier,
                                                                   QPointer<ArchiveServices> archiveServices);

    void openArchive(const QFileInfo& info);
    void openArchives(const CommandLineReadData& data);

    MainWindowCore* publ = nullptr;
    ArchiveTypeDetector typeDetector;
    ArchiveServices* archiveServices = nullptr;
    std::unordered_map<QString, ArchiveBrowserCore*, Utils::QtTypeHasher<QString>> browsers;
    QString cacheLocation;
    QueueMessenger* queueMessenger = nullptr;
    QueueMessengerAux::Backdoor messengerBackdoor;
    PluginManager pm;
    IncomingMessageInterpreter* messageInterpreter = nullptr;
    PluginConfigChangeNotifier* pluginChangeNotifier = nullptr;
};


MainWindowCore::MainWindowCore(MainWindowCoreAux::Init& init,
                               MainWindowCoreAux::Backdoor& backdoor,
                               QObject *parent)
    : QObject(parent),
      priv(new MainWindowCore::Private)
{
    priv->publ = this;
    priv->archiveServices = new ArchiveServices(priv->pm.getAllPlugins(), this);

    QueueMessengerAux::Init messengerInit;
    messengerInit.queueExecName = init.queueExecName;
    messengerInit.queueServerName = init.queueServerName;

    priv->queueMessenger = new QueueMessenger(messengerInit, priv->messengerBackdoor, this);
    Q_ASSERT(priv->messengerBackdoor.requestArchivingDialog);
    Q_ASSERT(priv->messengerBackdoor.sendMessageToQueue);

    priv->messageInterpreter = new IncomingMessageInterpreter(this);
    
    connect(priv->messageInterpreter, &IncomingMessageInterpreter::newMessage,
            this, &MainWindowCore::newMessage);
    
    connect(priv->messageInterpreter, &IncomingMessageInterpreter::commandLineError,
            this, &MainWindowCore::commandLineError);
    
    connect(priv->messageInterpreter, &IncomingMessageInterpreter::commandLineHelpRequested,
            this, &MainWindowCore::commandLineHelpRequested);
    
    connect(priv->messageInterpreter, &IncomingMessageInterpreter::readArchives,
            this, [publ = this] (const auto& data)
    { publ->priv->openArchives(data); });

    backdoor.accessor = new Accessor(this);

    connect(backdoor.accessor, &Accessor::processMessage,
            priv->messageInterpreter, [interpreter = priv->messageInterpreter] (const QByteArray& message)
    { interpreter->interpret(message); });
}


MainWindowCore::~MainWindowCore()
{
    if (!priv->cacheLocation.isEmpty())
    {
        QDir dir(priv->cacheLocation);

        if (dir.exists())
            dir.removeRecursively();
    }
}


QString MainWindowCore::getString(UserString value)
{
    switch (value)
    {
    case UserString::ActionAbout:
        return PrivateStrings::getString(PrivateStrings::UserString::ActionAbout);

    case UserString::ActionClose:
        return CommonStrings::getString(CommonStrings::UserString::Close);

    case UserString::ActionNew:
        return PrivateStrings::getString(PrivateStrings::UserString::ActionNew);

    case UserString::ActionOpen:
        return Packuru::Core::Browser::MainWindowCore::tr("Open");

    case UserString::ActionQuit:
        return PrivateStrings::getString(PrivateStrings::UserString::Quit);

    case UserString::ActionSettings:
        return PrivateStrings::getString(PrivateStrings::UserString::Settings);

    case UserString::WindowTitle:
        return Packuru::Core::Browser::MainWindowCore::tr("%1 Browser").arg(AppInfo::getAppName());
    default:
        Q_ASSERT(false); return "";
    }
}


void MainWindowCore::createArchive()
{
    priv->messengerBackdoor.requestArchivingDialog();
}


void MainWindowCore::openArchives(const QStringList& absoluteFilePaths)
{
    for (const auto& path : qAsConst(absoluteFilePaths))
    {
        QFileInfo info(path);
        priv->openArchive(info);
    }
}


void MainWindowCore::openArchives(const QList<QUrl>& urls)
{
    for (const auto& url : qAsConst(urls))
        priv->openArchive(url.path());
}


void MainWindowCore::openArchives(const QList<QFileInfo>& files)
{
    for (const auto& file : qAsConst(files))
        priv->openArchive(file);
}


int MainWindowCore::getBusyBrowserCount() const
{
    int count = 0;
    
    for (const auto& it : priv->browsers)
    {
        auto browser = it.second;
        if (browser->isTaskBusy())
            ++count;
    }
    
    return count;
}


AppSettingsDialogCore* MainWindowCore::createAppSettingsDialogCore()
{
    PluginDataModelAux::Init init;
    init.dataList = priv->pm.getAllPlugins();
    auto pluginModel = new PluginDataModel(init, this);
    const ArchiveTypeModelAccessor accessor = priv->archiveServices->getArchiveTypeModelAccessor();
    auto settings = new AppSettingsDialogCore({pluginModel, accessor}, this);
    return settings;
}


SettingsDialogCore* MainWindowCore::createBrowserSettingsDialogCore()
{
    return new SettingsDialogCore(this);
}


QueueMessenger*MainWindowCore::getQueueMessenger()
{
    return priv->queueMessenger;
}


MainWindowCore::Private::Private()
    : pm(QCoreApplication::applicationDirPath() + "/" + PROJECT_PLUGIN_DIR_NAME)
{

}


ReadTask* MainWindowCore::Private::createReadTask(const ReloadRequestData& reqData)
{
    ReadTaskData data;
    data.backendData.archiveInfo = reqData.archiveInfo;
    data.backendData.archiveType = typeDetector.detectType(reqData.archiveInfo.absoluteFilePath());
    data.backendData.password = reqData.password;
    return archiveServices->createTask(data);
}


template<typename TaskData>
ArchiveDialogAcceptedResult
MainWindowCore::Private::archiveDialogAcceptedCommon(QPointer<MainWindowCore> publ,
                                                     MessageType messageType,
                                                     const TaskData& taskData,
                                                     QPointer<ArchiveBrowserCoreNotifier> notifier,
                                                     QPointer<ArchiveServices> archiveServices)
{
    ArchiveDialogAcceptedResult result;

    if (!publ)
        return result;

    result.messageProcessed = true;

    if (taskData.runInQueue)
    {
        result.willRunInQueue = true;
        result.updatedPassword = taskData.backendData.password;

        const QByteArray message = createMessage(messageType, taskData);
        const auto runInBrowserFallback = [notifier = notifier,
                archiveServices = archiveServices,
                taskData = taskData] ()
        {
            if (!notifier || !archiveServices)
                return;

            Task* const task = archiveServices->createTask(taskData);
            notifier->runInBrowserRequested(task);
        };

        publ->priv->messengerBackdoor.sendMessageToQueue(message, runInBrowserFallback);
    }
    else
        result.task = publ->priv->archiveServices->createTask(taskData);

    return result;
}


void MainWindowCore::Private::openArchive(const QFileInfo &info)
{
    if (info.isDir())
        return;
    
    ArchiveBrowserCore* browserCore = nullptr;
    
    auto it = browsers.find(info.absoluteFilePath());
    if (it != browsers.end())
        browserCore = it->second;
    
    if (!browserCore)
    {
        ArchiveBrowserCoreInitData initData;

        initData.archiveInfo = info;

        if (cacheLocation.isEmpty())
        {
            cacheLocation = QStandardPaths::standardLocations(QStandardPaths::CacheLocation)[0];
            Q_ASSERT(!cacheLocation.isEmpty());

            cacheLocation += QLatin1Char('/') + QString::number(QCoreApplication::applicationPid());
        }

        initData.tempDir = cacheLocation + info.absoluteFilePath();

        initData.notifier = new ArchiveBrowserCoreNotifier(publ);

        if (!pluginChangeNotifier)
        {
            pluginChangeNotifier = new PluginConfigChangeNotifier(publ);
            QObject::connect(archiveServices, &ArchiveServices::pluginConfigChanged,
                             pluginChangeNotifier, &PluginConfigChangeNotifier::pluginConfigChanged);
        }
        
        initData.pluginChangeNotifier = pluginChangeNotifier;

        initData.getSupportedWriteTasks = [archiveServices = makeQPointer(archiveServices)]
                (ArchiveType archiveType, const BackendArchiveProperties& properties)
        {
            if (!archiveServices)
                return std::unordered_set<TaskType>();
            return archiveServices->getSupportedWriteTasksForArchive(archiveType, properties);
        };
        
        initData.destroyed = [publ = makeQPointer(publ)] (const QFileInfo& info)
        {
            if (!publ)
                return;

            publ->priv->browsers.erase(info.absoluteFilePath());
        };

        initData.createReadTask = [publ = makeQPointer(publ)] (const ReloadRequestData& data)
        {
            if (!publ)
                return static_cast<ReadTask*>(nullptr);

            return publ->priv->createReadTask(data);
        };

        initData.createPreviewTask = [publ = makeQPointer(publ)] (const PreviewRequestData& previewData)
        {
            if (!publ || previewData.files.empty())
                return static_cast<ExtractionTask*>(nullptr);

            BackendExtractionData backendData;
            backendData.archiveInfo =  previewData.archiveInfo;
            backendData.archiveType = previewData.archiveType;
            backendData.filesToExtract = previewData.files;
            backendData.extractionForPreview = true;
            backendData.password = previewData.password;

            ExtractionTaskData taskData;
            taskData.topFolderMode = ExtractionTopFolderMode::DoNotCreateFolder;
            taskData.destinationPath = previewData.destinationPath;
            taskData.fileOverwriteMode = ExtractionFileOverwriteMode::OverwriteExisting;
            taskData.folderOverwriteMode = ExtractionFolderOverwriteMode::WriteIntoExisting;
            taskData.backendData = backendData;

            return publ->priv->archiveServices->createTask(taskData);
        };

        initData.createArchivingParameterHandlerFactory =
                [archiveServices = makeQPointer(archiveServices)] (ArchiveType type)
        {
            std::unique_ptr<ArchivingParameterHandlerFactory> ptr;
            if (!archiveServices)
                return ptr;

            return archiveServices->createArchivingParameterHandlerFactory(type);
        };

        initData.onArchivingDialogAccepted = [publ = makeQPointer(publ),
                notifier = makeQPointer(initData.notifier),
                archiveServices = makeQPointer(archiveServices)] (const ArchivingTaskData& taskData)
        {
            return MainWindowCore::Private::archiveDialogAcceptedCommon(publ,
                                                                        MessageType::ArchivingTaskData,
                                                                        taskData,
                                                                        notifier,
                                                                        archiveServices);
        };

        initData.onDeletionDialogAccepted = [publ = makeQPointer(publ),
                notifier = makeQPointer(initData.notifier),
                archiveServices = makeQPointer(archiveServices)] (const DeletionTaskData& taskData)
        {
            return MainWindowCore::Private::archiveDialogAcceptedCommon(publ,
                                                                        MessageType::DeletionTaskData,
                                                                        taskData,
                                                                        notifier,
                                                                        archiveServices);
        };

        initData.onExtractionDialogAccepted = [publ = makeQPointer(publ),
                notifier = makeQPointer(initData.notifier),
                archiveServices = makeQPointer(archiveServices)] (const ExtractionTaskData& taskData)
        {
            return MainWindowCore::Private::archiveDialogAcceptedCommon(publ,
                                                                        MessageType::ExtractionTaskData,
                                                                        taskData,
                                                                        notifier,
                                                                        archiveServices);
        };

        initData.onTestDialogAccepted = [publ = makeQPointer(publ),
                notifier = makeQPointer(initData.notifier),
                archiveServices = makeQPointer(archiveServices)] (const TestTaskData& taskData)
        {
            return MainWindowCore::Private::archiveDialogAcceptedCommon(publ,
                                                                        MessageType::TestTaskData,
                                                                        taskData,
                                                                        notifier,
                                                                        archiveServices);
        };

        browserCore = new ArchiveBrowserCore(initData, publ);
        browsers[info.absoluteFilePath()] = browserCore;
        emit publ->newBrowserCoreCreated(browserCore);
    }
    else
    {
        if (!browserCore->isTaskBusy())
            browserCore->reloadArchive();
    }
}


void MainWindowCore::Private::openArchives(const CommandLineReadData& data)
{
    for (const auto& item : data.inputItems)
    {
        QFileInfo info(item);
        openArchive(info);
    }
}

}
