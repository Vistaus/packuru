// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QLocalSocket>
#include <QTimer>
#include <QProcess>
#include <QCoreApplication>

#include "core/private/privatestrings.h"

#include "queuemessenger.h"
#include "private/queuemessengeraux.h"


namespace Packuru::Core::Browser
{

using namespace QueueMessengerAux;


struct QueueMessenger::Private
{
    Private(Init& init)
        : init_(std::move(init)),
          queueExecutable(QCoreApplication::applicationDirPath() + QLatin1Char('/') + init_.queueExecName)
    {}

    void setStatus(const QString& text);
    QString getStatus() const {return error; }
    void setActive(bool value);
    bool isActive() const { return active; }
    void sendMessageToQueueImpl();
    bool sendMessageToServer(QLocalSocket *socket, const QByteArray& message);
    bool startQueue(const QStringList& parameters);
    void reset();
    void sendMessageToQueue(const QByteArray& msg, std::function<void()> runInBrowserFunc);
    void requestArchivingDialog();

    const Init init_;
    const QString queueExecutable;
    QueueMessenger* publ = nullptr;
    QByteArray message;
    std::function<void()> runInBrowserFunction;
    bool queueStarted = false;
    QTimer* timer = nullptr;

private:
    QString error;
    bool active = false;
};


QueueMessenger::QueueMessenger(QueueMessengerAux::Init& init, QueueMessengerAux::Backdoor& backdoor, QObject *parent)
    : QObject(parent),
      priv(new Private(init))
{
    priv->publ = this;

    priv->timer = new QTimer(this);
    priv->timer->setSingleShot(true);
    connect(priv->timer, &QTimer::timeout,
            this, [publ = this] ()
    { publ->priv->sendMessageToQueueImpl(); });

    backdoor.requestArchivingDialog = [priv = priv.get()] () { priv->requestArchivingDialog(); };

    backdoor.sendMessageToQueue = [priv = priv.get()]
            (const QByteArray& msg, std::function<void()> runInBrowserFunc)
    { priv->sendMessageToQueue(msg, runInBrowserFunc); };
}


QueueMessenger::~QueueMessenger()
{

}


QString QueueMessenger::getString(UserString value)
{
    switch (value)
    {
    case UserString::Abort:
        return PrivateStrings::getString(PrivateStrings::UserString::Abort);

    case UserString::DialogTitle:
        return Packuru::Core::Browser::QueueMessenger::tr("Messaging queue");

    case UserString::RunInBrowser:
        return Packuru::Core::Browser::QueueMessenger::tr("Run in browser");

    default:
        Q_ASSERT(false); return "";
    }
}


bool QueueMessenger::isActive() const
{
    return priv->isActive();
}


QString QueueMessenger::getStatus() const
{
    return priv->getStatus();
}


bool QueueMessenger::getCanRunInBrowser() const
{
    return static_cast<bool>(priv->runInBrowserFunction);
}


void QueueMessenger::abort()
{
    if (!priv->isActive())
        return;

    priv->setStatus(Packuru::Core::Browser::QueueMessenger::tr("Aborted."));
    priv->timer->stop();
    emit priv->setActive(false);
}


void QueueMessenger::runInBrowser()
{
    if (!priv->isActive() || !priv->runInBrowserFunction)
        return;

    priv->setStatus(Packuru::Core::Browser::QueueMessenger::tr("Running in Browser."));
    priv->timer->stop();
    priv->runInBrowserFunction();
    emit priv->setActive(false);
}


void QueueMessenger::Private::setStatus(const QString& text)
{
    if (text != error)
    {
        error = text;
        emit publ->statusChanged(error);
    }
}


void QueueMessenger::Private::setActive(bool value)
{
    if (value != active)
    {
        active = value;
        emit publ->activeChanged(active);
        if (active)
            emit publ->activated();
        else
            emit publ->deactivated();
    }
}


void QueueMessenger::Private::sendMessageToQueueImpl()
{
    QLocalSocket socket;
    socket.connectToServer(init_.queueServerName);

    if (socket.isValid())
    {
        setStatus(Packuru::Core::Browser::QueueMessenger::tr("Sending message to Queue..."));

        const bool result = sendMessageToServer(&socket, message);
        socket.disconnectFromServer();

        if (result)
        {
            setStatus(Packuru::Core::Browser::QueueMessenger::tr("Message sent."));
            setActive(false);
        }
        else
        {
            setStatus(socket.errorString());
        }
    }
    else
    {
        if (queueStarted)
        {
            setStatus(socket.errorString());
        }
        else
        {
            startQueue({});
        }

        timer->start(1000);
    }
}


bool QueueMessenger::Private::sendMessageToServer(QLocalSocket* socket, const QByteArray& message)
{
    if (!socket->isValid())
        return false;

    socket->write(message);

    bool result = true;

    while (socket->bytesToWrite() > 0 && result)
        result = socket->waitForBytesWritten(-1);

    return result;
}


bool QueueMessenger::Private::startQueue(const QStringList& parameters)
{
    setStatus(Packuru::Core::Browser::QueueMessenger::tr("Starting up Queue..."));

    if (QProcess::startDetached(queueExecutable, parameters))
    {
        setStatus(Packuru::Core::Browser::QueueMessenger::tr("Queue started."));
        queueStarted = true;
        return true;
    }
    else
    {
        const QString status = Packuru::Core::Browser::QueueMessenger::tr("Cannot start")
                + QLatin1Char(' ') + queueExecutable + QLatin1Char('.');
        setStatus(status);
        return false;
    }
}


void QueueMessenger::Private::reset()
{
    queueStarted = false;
    setStatus("");
    runInBrowserFunction = {};
    emit publ->canRunInBrowserChanged(static_cast<bool>(runInBrowserFunction));
}


void QueueMessenger::Private::sendMessageToQueue(const QByteArray& msg,
                                                 std::function<void ()> runInBrowserFunc)
{
    Q_ASSERT(!isActive());
    Q_ASSERT(runInBrowserFunc);

    if (isActive())
        return;

    reset();

    message = msg;
    runInBrowserFunction = runInBrowserFunc;

    QTimer::singleShot(0, [priv = this] () { priv->sendMessageToQueueImpl(); });

    setActive(true);
}


void QueueMessenger::Private::requestArchivingDialog()
{
    Q_ASSERT(!isActive());

    if (isActive())
        return;

    reset();

    QTimer::singleShot(0, [priv = this] ()
    {
        if (priv->startQueue({"-c", "-d"}))
            priv->setActive(false);
    });

    setActive(true);
}


}
