<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>ItemFilteringMode</name>
    <message>
        <location filename="../private/itemfilteringmode.cpp" line="17"/>
        <source>Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/itemfilteringmode.cpp" line="18"/>
        <source>Show all</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/itemfilteringmode.cpp" line="19"/>
        <source>Hide all</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Packuru::Core::Browser::ArchiveBrowserCore</name>
    <message>
        <location filename="../archivebrowsercore.cpp" line="657"/>
        <source>Adding files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="659"/>
        <location filename="../archivebrowsercore.cpp" line="663"/>
        <source>Extracting</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="661"/>
        <source>Deleting files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="665"/>
        <source>Testing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="667"/>
        <source>Reading</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1036"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1044"/>
        <source>Created</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1047"/>
        <source>Modified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1039"/>
        <source>Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="447"/>
        <source>Add</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="449"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="453"/>
        <source>Filter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="455"/>
        <source>Preview</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="457"/>
        <source>Properties</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="459"/>
        <source>Reload</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1059"/>
        <source>Original size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1071"/>
        <source>Archive size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1078"/>
        <source>Compression ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1097"/>
        <source>Files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1098"/>
        <source>Folders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1108"/>
        <source>Archiving methods</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1124"/>
        <source>Recovery data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1147"/>
        <source>Snapshots</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1391"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1392"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1116"/>
        <source>Solid</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1087"/>
        <source>Multipart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1094"/>
        <source>Parts</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1131"/>
        <source>Encryption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../archivebrowsercore.cpp" line="1139"/>
        <source>Locked</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Packuru::Core::Browser::ArchiveContentModel</name>
    <message numerus="yes">
        <location filename="../private/archivecontentmodel.cpp" line="268"/>
        <source>%n items</source>
        <translation>
            <numerusform>%n item</numerusform>
            <numerusform>%n items</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="610"/>
        <source>Accessed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="611"/>
        <source>Attributes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="612"/>
        <source>Block</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="613"/>
        <source>Comment</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="614"/>
        <source>CRC</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="615"/>
        <source>Created</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="616"/>
        <source>Encrypted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="617"/>
        <source>Extension</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="618"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="619"/>
        <source>Host OS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="620"/>
        <source>Method</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="621"/>
        <source>Modified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="622"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="623"/>
        <source>Node type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="624"/>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="625"/>
        <source>Packed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="626"/>
        <source>Ratio</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/archivecontentmodel.cpp" line="627"/>
        <source>User</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Packuru::Core::Browser::BasicControl</name>
    <message>
        <location filename="../basiccontrol.cpp" line="140"/>
        <source>Abort</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../basiccontrol.cpp" line="143"/>
        <source>Browse archive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../basiccontrol.cpp" line="146"/>
        <source>Retry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../basiccontrol.cpp" line="149"/>
        <source>View log</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Packuru::Core::Browser::CommandLineHandler</name>
    <message>
        <location filename="../private/commandlinehandler.cpp" line="77"/>
        <source>Open archives</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Packuru::Core::Browser::DeletionDialogCore</name>
    <message>
        <location filename="../deletiondialogcore.cpp" line="93"/>
        <source>Delete files</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../deletiondialogcore.cpp" line="90"/>
        <source>Do you want to delete %n items?</source>
        <translation>
            <numerusform>Do you want to delete %n item?</numerusform>
            <numerusform>Do you want to delete %n items?</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>Packuru::Core::Browser::IncomingMessageInterpreter</name>
    <message>
        <location filename="../private/incomingmessageinterpreter.cpp" line="64"/>
        <source>Unknown message type</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Packuru::Core::Browser::MainWindowCore</name>
    <message>
        <location filename="../mainwindowcore.cpp" line="155"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwindowcore.cpp" line="164"/>
        <source>%1 Browser</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Packuru::Core::Browser::QueueMessenger</name>
    <message>
        <location filename="../queuemessenger.cpp" line="88"/>
        <source>Messaging queue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../queuemessenger.cpp" line="91"/>
        <source>Run in browser</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../queuemessenger.cpp" line="122"/>
        <source>Aborted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../queuemessenger.cpp" line="133"/>
        <source>Running in Browser.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../queuemessenger.cpp" line="171"/>
        <source>Sending message to Queue...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../queuemessenger.cpp" line="178"/>
        <source>Message sent.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../queuemessenger.cpp" line="220"/>
        <source>Starting up Queue...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../queuemessenger.cpp" line="224"/>
        <source>Queue started.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../queuemessenger.cpp" line="230"/>
        <source>Cannot start</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Packuru::Core::Browser::SettingsDialogCore</name>
    <message>
        <location filename="../settingsdialogcore.cpp" line="39"/>
        <source>Force single instance</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialogcore.cpp" line="46"/>
        <source>Show summary on task completion</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../settingsdialogcore.cpp" line="71"/>
        <source>Browser</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Packuru::Core::Browser::TabClosingDialogCore</name>
    <message>
        <location filename="../tabclosingdialogcore.cpp" line="29"/>
        <source>Confirm close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tabclosingdialogcore.cpp" line="32"/>
        <source>Do you want to close it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tabclosingdialogcore.cpp" line="35"/>
        <source>This tab is busy.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ViewFilterFilesCtrl</name>
    <message>
        <location filename="../private/viewfilterfilesctrl.cpp" line="25"/>
        <source>Files</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ViewFilterFoldersCtrl</name>
    <message>
        <location filename="../private/viewfilterfoldersctrl.cpp" line="24"/>
        <source>Folders</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ViewFilterScopeCtrl</name>
    <message>
        <location filename="../private/viewfilterscopectrl.cpp" line="17"/>
        <source>Current folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/viewfilterscopectrl.cpp" line="19"/>
        <source>Entire archive</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../private/viewfilterscopectrl.cpp" line="27"/>
        <source>Scope</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
