// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QObject>

#include "symbol_export.h"

/// @file

namespace Packuru::Core
{
class PasswordPromptCore;
class ExtractionFolderOverwritePromptCore;
class ExtractionFileOverwritePromptCore;
class ExtractionTypeMismatchPromptCore;
}


namespace Packuru::Core::Browser
{

class BasicControl;

namespace StatusPageCoreAux
{
struct Init;
struct Backdoor;
}

/**
 * @brief Contains logic and data for Status Page.
 *
 * %StatusPageCore provides information about currently executed task and signals which
 * task prompt/widget should be displayed in the control area of Status Page.
 *
 * %StatusPageCore object is provided by ArchiveBrowserCore object.
 *
 * @headerfile "core-browser/statuspagecore.h"
 */
class PACKURU_CORE_BROWSER_EXPORT StatusPageCore : public QObject
{
    Q_OBJECT
    /** @brief Holds task description.
     * @sa getTaskDescription(), taskDescriptionChanged()
     */
    Q_PROPERTY(QString taskDescription READ getTaskDescription NOTIFY taskDescriptionChanged)

    /** @brief Holds task state.
     * @sa getTaskState(), taskStateChanged()
     */
    Q_PROPERTY(QString taskState READ getTaskState NOTIFY taskStateChanged)

    /** @brief Holds task progress.
     * @sa getTaskProgress(), taskProgressChanged()
     */
    Q_PROPERTY(int taskProgress READ getTaskProgress NOTIFY taskProgressChanged)


    /** @brief Holds whether progress bar should be visible.
     * @sa getProgressBarVisible(), taskStateChanged()
     */
    Q_PROPERTY(bool progressBarVisible READ getProgressBarVisible NOTIFY progressBarVisibleChanged)

    /** @brief Holds BasicControl pointer.
     * @sa showBasicControl()
     */
    Q_PROPERTY(Packuru::Core::Browser::BasicControl* basicControl READ getBasicControl CONSTANT)

public:
    explicit StatusPageCore(StatusPageCoreAux::Init& init,
                            StatusPageCoreAux::Backdoor& backdoor,
                            QObject *parent = nullptr);
    ~StatusPageCore() override;

    /**
     * @brief Returns task description.
     * sa taskDescription, taskDescriptionChanged()
     */
    Q_INVOKABLE QString getTaskDescription() const;

    /**
     * @brief Returns task state.
     * @sa taskState, taskStateChanged()
     */
    Q_INVOKABLE QString getTaskState() const;

    /**
     * @brief Returns task progress.
     * @sa taskProgress, taskProgressChanged()
     */
    Q_INVOKABLE int getTaskProgress() const;

    /**
     * @brief Returns whether progress bar should be visible.
     * @sa progressBarVisible, progressBarVisibleChanged()
     */
    Q_INVOKABLE bool getProgressBarVisible() const;

    /**
     * @brief Returns BasicControl pointer.
     *
     * BasicControl allows to e.g. abort or retry last task.
     * @note Object ownership is not transferred to the caller.
     * @sa basicControl, showBasicControl()
     */
    Q_INVOKABLE BasicControl* getBasicControl();

signals:
    /**
     * @brief Signals task description change.
     * @sa taskDescription, getTaskDescription()
     */
    void taskDescriptionChanged(const QString& value);

    /**
     * @brief Signals task state change.
     * @sa taskState, getTaskState()
     */
    void taskStateChanged(const QString& value);

    /**
     * @brief Signals task progress change.
     * @sa taskProgress, getTaskProgress()
     */
    void taskProgressChanged(int value);

    /**
     * @brief Signals whether progress bar should be visible or not.
     * @sa progressBarVisible, getProgressBarVisible()
     */
    void progressBarVisibleChanged(bool value);

    /**
     * @brief Signals when Basic Control widget should be displayed in the control area.
     * @sa basicControl
     */
    void showBasicControl();

    /** @brief Signals when File Overwrite dialog should be displayed in the control area.
     *  @warning Object ownership is not transferred. The object must not be destroyed manually.
     */
    void showFileExistsPrompt(Packuru::Core::ExtractionFileOverwritePromptCore* promptCore);

    /** @brief Signals when Folder Overwrite dialog should be displayed in the control area.
     *  @warning Object ownership is not transferred. The object must not be destroyed manually.
     */
    void showFolderExistsPrompt(Packuru::Core::ExtractionFolderOverwritePromptCore* promptCore);

    /** @brief Signals when Type Mismatch dialog should be displayed in the control area.
     *  @warning Object ownership is not transferred. The object must not be destroyed manually.
     */
    void showItemTypeMismatchPrompt(Packuru::Core::ExtractionTypeMismatchPromptCore* promptCore);

    /** @brief Signals when password prompt should be displayed in the control area.
     *  @warning Object ownership is not transferred. The object must not be destroyed manually.
     */
    void showPasswordPrompt(Packuru::Core::PasswordPromptCore* promptCore);

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}
