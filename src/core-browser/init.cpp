// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QCoreApplication>
#include <QLoggingCategory>

#include "../core/globalsettingsmanager.h"
#include "../core/private/globalsettings.h"
#include "../core/private/loadtranslations.h"
#include "../core/private/coresettingsconverter.h"
#include "../core/private/globalsettingsmanageraux.h"
#include "../core/private/3rdparty/KDSingleApplication/kdsingleapplication.h"

#include "init.h"
#include "mainwindowcore.h"
#include "private/mainwindowcoreaux.h"
#include "private/commandlinehandler.h"
#include "private/settingsconverter.h"


namespace Packuru::Core::Browser
{

struct Init::Private
{
    Private();

    KDSingleApplication singleApplication;
    CommandLineHandler clHandler;
    std::unique_ptr<MainWindowCore> mainWindowCore;
    MainWindowCoreAux::Backdoor mainWindowCoreBackdoor;
    QString queueExecName;
    QString queueServerName;
    QByteArray serializedArguments;
};


Init::Init(const QString& queueExecName, const QString& queueServerName)
    : priv(new Private)
{
    priv->queueExecName = queueExecName;
    priv->queueServerName = queueServerName;

    loadTranslations();

    GlobalSettingsManagerAux::Init initData;
    initData.converters.push_back(std::make_unique<Packuru::Core::CoreSettingsConverter>());
    initData.converters.push_back(std::make_unique<Packuru::Core::Browser::SettingsConverter>());

    GlobalSettingsManager::createSingleInstance(initData, this);

    QLoggingCategory::setFilterRules("qt.core.qabstractitemmodel.checkindex=false");
}


Init::~Init()
{

}


bool Init::messagePrimaryInstance() const
{
    const auto forceSingleInstance = GlobalSettingsManager::instance()
            ->getValueAs<bool>(GlobalSettings::Enum::BC_ForceSingleInstance);

    if (forceSingleInstance && !priv->singleApplication.isPrimaryInstance())
    {
        priv->singleApplication.sendMessage(priv->serializedArguments);
        return true;
    }

    return false;
}


MainWindowCore* Init::getMainWindowCore()
{
    if (!priv->mainWindowCore)
    {
        MainWindowCoreAux::Init init;
        init.queueExecName = priv->queueExecName;
        init.queueServerName = priv->queueServerName;

        priv->mainWindowCore.reset(new MainWindowCore(init, priv->mainWindowCoreBackdoor));
        Q_ASSERT(priv->mainWindowCoreBackdoor.accessor);

        using Packuru::Core::Browser::MainWindowCoreAux::Accessor;

        connect(&priv->singleApplication, &KDSingleApplication::messageReceived,
                priv->mainWindowCoreBackdoor.accessor, &Accessor::processMessage);
    }

    return priv->mainWindowCore.get();
}


void Init::processArguments()
{
    auto accessor = priv->mainWindowCoreBackdoor.accessor;

    if (accessor)
        emit accessor->processMessage(priv->serializedArguments);
}


Init::Private::Private()
    : serializedArguments(clHandler.createMessageFromArguments())
{

}

}
