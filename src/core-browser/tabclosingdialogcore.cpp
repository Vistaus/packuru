// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "tabclosingdialogcore.h"
#include "core/commonstrings.h"


namespace Packuru::Core::Browser
{

TabClosingDialogCore::TabClosingDialogCore(QObject *parent) : QObject(parent)
{

}


QString TabClosingDialogCore::getString(UserString value)
{
    switch (value)
    {
    case UserString::Cancel:
        return CommonStrings::getString(CommonStrings::UserString::Cancel);

    case UserString::Close:
        return CommonStrings::getString(CommonStrings::UserString::Close);

    case UserString::DialogTitle:
        return Packuru::Core::Browser::TabClosingDialogCore::tr("Confirm close");

    case UserString::DoYouWantToCloseIt:
        return  Packuru::Core::Browser::TabClosingDialogCore::tr("Do you want to close it?");

    case UserString::ThisTabIsBusy:
        return Packuru::Core::Browser::TabClosingDialogCore::tr("This tab is busy.");

    default:
        Q_ASSERT(false); return "";
    }
}

}
