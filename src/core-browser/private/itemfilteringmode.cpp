// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QCoreApplication>

#include "itemfilteringmode.h"


namespace Packuru::Core::Browser
{

QString toString(ItemFilteringMode value)
{
    switch (value)
    {
    case ItemFilteringMode::Filter: return QCoreApplication::translate("ItemFilteringMode", "Filter");
    case ItemFilteringMode::ShowAll: return QCoreApplication::translate("ItemFilteringMode", "Show all");
    case ItemFilteringMode::HideAll: return QCoreApplication::translate("ItemFilteringMode", "Hide all");
    default: Q_ASSERT(false); return QString();
    }
}

}
