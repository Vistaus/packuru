// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <functional>

#include <QString>


namespace Packuru::Core::Browser
{

class BasicControl;

namespace StatusPageCoreAux
{

struct Init
{
    BasicControl* basicControl = nullptr;
};


struct Backdoor
{
    std::function<void (const QString& value)> setTaskDescription;
    std::function<void (const QString& value)> setTaskState;
    std::function<void (int value)> setTaskProgress;
    std::function<void (bool value)> setProgressBarVisible;
};

}

}
