// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "archivecontentfiltermodel.h"
#include "archivecontentmodel.h"
#include "itemfilteringmode.h"


namespace Packuru::Core::Browser
{

ArchiveContentFilterModel::ArchiveContentFilterModel(QObject* parent)
    : QSortFilterProxyModel(parent),
      fileFiltering(ItemFilteringMode::Filter),
      folderFiltering(ItemFilteringMode::Filter)
{
    setFilterCaseSensitivity(Qt::CaseInsensitive);
}


void ArchiveContentFilterModel::setSourceModel(QAbstractItemModel *sourceModel)
{
    auto model = qobject_cast<ArchiveContentModel*>(sourceModel);
    if (model)
        archiveModel = model;

    QSortFilterProxyModel::setSourceModel(sourceModel);
}


QModelIndex ArchiveContentFilterModel::findClosestRelativeOfSource(const QModelIndex& sourceIndex) const
{
    Q_ASSERT(sourceModel()->checkIndex(sourceIndex, QAbstractItemModel::CheckIndexOption::IndexIsValid)
             || !sourceIndex.isValid());

    QModelIndex source = sourceIndex;
    QModelIndex proxy;

    /* Try to find a source index that can be mapped to a valid proxy index, starting
       from source index and going up in the hierarchy to check parents. */
    while (source.isValid() && !proxy.isValid())
    {
        proxy = mapFromSource(source);

        if (!proxy.isValid())
            source = source.parent();
    }

    return mapToSource(proxy);
}


void ArchiveContentFilterModel::setFilterRootSourceIndex(const QModelIndex& sourceIndex)
{
    Q_ASSERT(sourceModel()->checkIndex(sourceIndex, QAbstractItemModel::CheckIndexOption::IndexIsValid)
             || !sourceIndex.isValid());

    whitelistedSourceParent = sourceIndex;
    whitelistedSourceIndexes.clear();

    if (sourceIndex.isValid())
    {
        auto index = sourceIndex;
        while (true)
        {
            whitelistedSourceIndexes.insert(index);
            index = index.parent();

            if (!index.isValid())
                break;
        }
    }

    if (filterEnabled && !isRecursiveFilteringEnabled())
        invalidateFilter();
}


void ArchiveContentFilterModel::setFilterEnabled(bool value)
{
    filterEnabled = value;
}


void ArchiveContentFilterModel::setFileFiltering(ItemFilteringMode mode)
{
    fileFiltering = mode;

    if (filterEnabled)
        invalidateFilter();
}


void ArchiveContentFilterModel::setFolderFiltering(ItemFilteringMode mode)
{
    folderFiltering = mode;

    if (filterEnabled)
        invalidateFilter();
}


void ArchiveContentFilterModel::clearFilterSettings()
{
    filterEnabled = false;
    whitelistedSourceParent = QModelIndex();
    whitelistedSourceIndexes.clear();
}


bool ArchiveContentFilterModel::lessThan(const QModelIndex& source_left, const QModelIndex& source_right) const
{
    Q_ASSERT(archiveModel);
    return archiveModel->lessThan(source_left, source_right, sortOrder());
}


bool ArchiveContentFilterModel::filterAcceptsRow(int source_row, const QModelIndex& source_parent) const
{
    if (!filterEnabled)
        return true;

    const QModelIndex sourceIndex = sourceModel()->index(source_row, 0, source_parent);

    if (source_parent == whitelistedSourceParent || isRecursiveFilteringEnabled())
    {
        Q_ASSERT(archiveModel);

        const auto isDir = archiveModel->isDir(sourceIndex);

        if (isDir)
        {
            switch (folderFiltering)
            {
            case ItemFilteringMode::ShowAll: return true;
            case ItemFilteringMode::HideAll: return false;
            default: break;
            }
        }
        else
        {
            switch (fileFiltering)
            {
            case ItemFilteringMode::ShowAll: return true;
            case ItemFilteringMode::HideAll: return false;
            default: break;
            }
        }

        return sourceModel()->data(sourceIndex).toString().contains(filterRegExp());
    }

    return whitelistedSourceIndexes.find(sourceIndex) != whitelistedSourceIndexes.end();
}

}

