// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <functional>

#include <QString>
#include <QTimer>

#include "qcs-core/controller.h"


namespace Packuru::Core::Browser
{

class ViewFilterTextCtrl : public qcs::core::Controller<QString>
{
public:
    using SetFilterTextFunction = std::function<void(const QString&)>;

    ViewFilterTextCtrl(SetFilterTextFunction&& setFilterText);

private:
    void onValueSyncedToWidget(const QVariant& value) override;

    SetFilterTextFunction setFilterText_;
    QTimer changeTextTimer;
};

}
