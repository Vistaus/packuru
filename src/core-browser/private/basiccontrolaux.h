// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <functional>

#include <QString>

#include "core-browser/basiccontrolcontrollertype.h"


namespace Packuru::Core::Browser::BasicControlAux
{

enum class ButtonVisible
{
    True,
    False
};


enum class ButtonEnabled
{
    True,
    False
};


struct Init
{
    std::function<void()> retryRequested;
    std::function<void()> abortRequested;
    std::function<void()> browseRequested;
};


struct Backdoor
{
    std::function<void (BasicControlControllerType::Type, ButtonVisible, ButtonEnabled)> setButtonState;
    std::function<void (BasicControlControllerType::Type)> setDefaultButton;
    std::function<void (const QString&)> setErrorsAndWarnings;
    std::function<void (const QString&)> setLog;
};

}
