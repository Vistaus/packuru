// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <functional>


class QModelIndex;


namespace Packuru::Core::Browser
{

class ArchiveContentModel;

struct ArchiveNavigatorInitData
{
    using FileClickedCallback = std::function<void(const QModelIndex& sourceIndex)>;
    using SelectionChangedCallback = std::function<void(int selectedFiles, int selectedFolders)>;

    ArchiveContentModel* archiveModel;
    FileClickedCallback fileClicked;
    SelectionChangedCallback selectionChanged;
};

}
