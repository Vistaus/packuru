// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QCommandLineOption>

#include "core/private/createmessage.h"
#include "core/private/messagetype.h"

#include "commandlinehandler.h"
#include "private/commandlinereaddata.h"


using namespace Packuru::Core;


namespace Packuru::Core::Browser
{

struct CommandLineHandler::Private
{
    Private();

    CommandLineReadData createCommandLineReadData();

    CommandLineHandler* publ;
    QCommandLineOption readOption;
};


CommandLineHandler::CommandLineHandler()
    : priv(new Private)
{
    priv->publ = this;
    parser.addHelpOption();
    parser.addOption(priv->readOption);
    parser.parse(QCoreApplication::instance()->arguments());
}


CommandLineHandler::~CommandLineHandler()
{

}


QByteArray CommandLineHandler::createMessageFromArguments() const
{
    QByteArray message;
    QString errorInfo = parser.errorText();

    if (errorInfo.length() > 0)
    {
        message = createMessage(MessageType::CommandLineError, errorInfo, true);
    }
    else if (parser.isSet("help"))
    {
        message = createMessage(MessageType::CommandLineHelp, parser.helpText(), true);
    }
    else if (parser.isSet(priv->readOption))
    {
        auto data = priv->createCommandLineReadData();
        message = createMessage(MessageType::CommandLineReadArchives, data, true);
    }
    else
    {
        message = createMessage(MessageType::CommandLineDefaultStartUp, QByteArray(), true);
    }

    return message;
}



CommandLineHandler::Private::Private()
    : readOption(QCommandLineOption({"o", "open"},
                                    Packuru::Core::Browser::CommandLineHandler::tr("Open archives")))
{

}


CommandLineReadData CommandLineHandler::Private::createCommandLineReadData()
{
    CommandLineReadData data;
    data.inputItems = publ->createUrlList();
    return data;
}

}
