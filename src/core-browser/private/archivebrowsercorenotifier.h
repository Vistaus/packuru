// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QObject>


namespace Packuru::Core
{
    class Task;
}

namespace Packuru::Core::Browser
{

class ArchiveBrowserCoreNotifier : public QObject
{
    Q_OBJECT
public:
    explicit ArchiveBrowserCoreNotifier(QObject *parent = nullptr);

signals:
    void runInBrowserRequested(Task* task);
};

}
