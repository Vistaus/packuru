// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QMetaType>


namespace Packuru::Core::Browser
{

enum class ViewFilterScope
{
    CurrentFolder,
    EntireArchive
};

}

Q_DECLARE_METATYPE(Packuru::Core::Browser::ViewFilterScope)
