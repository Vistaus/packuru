// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "viewfilterfoldersctrl.h"


using qcs::core::ControllerProperty;


namespace Packuru::Core::Browser
{

ViewFilterFoldersCtrl::ViewFilterFoldersCtrl(SetFilteringFunction&& setFiltering)
    : setFiltering_(setFiltering)
{
    setValueListsFromPrivate({ItemFilteringMode::Filter,
                              ItemFilteringMode::ShowAll,
                              ItemFilteringMode::HideAll});

    setCurrentValueFromPrivate(ItemFilteringMode::Filter);

    properties[qcs::core::ControllerProperty::LabelText]
            = Packuru::Core::Browser::ViewFilterFoldersCtrl::tr("Folders");
}


void ViewFilterFoldersCtrl::onValueSyncedToWidget(const QVariant& value)
{
    Q_UNUSED(value)
    const auto mode = getPropertyAs<ItemFilteringMode>(ControllerProperty::PrivateCurrentValue);
    setFiltering_(mode);
}

}


