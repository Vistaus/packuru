// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QDataStream>

#include "core/private/datastream/container_datastream.h"
#include "core/private/datastream/qfileinfo_datastream.h"

#include "commandlinereaddata.h"


using Packuru::Core::operator <<;
using Packuru::Core::operator >>;


QDataStream& operator<<(QDataStream& out, const Packuru::Core::Browser::CommandLineReadData& data)
{
    return out << data.inputItems;
}


QDataStream& operator>>(QDataStream& in, Packuru::Core::Browser::CommandLineReadData& data)
{
    return in >> data.inputItems;
}
