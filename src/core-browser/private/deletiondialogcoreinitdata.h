// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <vector>

#include <QString>

#include "core/private/plugin-api/archivetype.h"
#include "core/private/plugin-api/encryptionstate.h"


namespace Packuru::Core
{
struct DeletionTaskData;
}


namespace Packuru::Core::Browser
{

struct DeletionDialogCoreInitData
{
    using OnAcceptedCallback = std::function<void(const DeletionTaskData& data)>;

    bool isValid() const;

    QString archiveAbsFilePath;
    ArchiveType archiveType = ArchiveType::___NOT_SUPPORTED;
    std::vector<QString> filesToRemove;
    EncryptionState archiveEncryption = EncryptionState::___INVALID;
    QString password;
    OnAcceptedCallback onAccepted;
};

}
