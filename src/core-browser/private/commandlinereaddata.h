// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QList>
#include <QFileInfo>


namespace Packuru::Core::Browser
{

struct CommandLineReadData
{
    std::vector<QFileInfo> inputItems;
};

}


QDataStream& operator<<(QDataStream& out, const Packuru::Core::Browser::CommandLineReadData& data);
QDataStream& operator>>(QDataStream& in, Packuru::Core::Browser::CommandLineReadData& data);
