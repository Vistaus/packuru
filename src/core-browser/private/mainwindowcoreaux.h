// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QObject>


namespace Packuru::Core::Browser::MainWindowCoreAux
{

struct Init
{
    QString queueExecName;
    QString queueServerName;
};


class Accessor : public QObject
{
    Q_OBJECT
public:
    explicit Accessor(QObject* parent = nullptr)
        : QObject(parent) {}

signals:
    void processMessage(const QByteArray& message);
};


struct Backdoor
{
    Accessor* accessor = nullptr;
};

}
