// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <functional>

#include <QString>


namespace Packuru::Core::Browser::QueueMessengerAux
{

struct Init
{
    QString queueExecName;
    QString queueServerName;
};

struct Backdoor
{
    std::function<void (const QByteArray& message, std::function<void()> runInBrowserFunction)> sendMessageToQueue;
    std::function<void ()> requestArchivingDialog;
};

}
