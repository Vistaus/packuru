// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "archivebrowsercorenotifier.h"


namespace Packuru::Core::Browser
{

ArchiveBrowserCoreNotifier::ArchiveBrowserCoreNotifier(QObject *parent)
    : QObject(parent)
{

}

}
