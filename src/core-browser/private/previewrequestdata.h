// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <vector>

#include <QFileInfo>

#include "core/private/plugin-api/archivetype.h"


namespace Packuru::Core::Browser
{

struct PreviewRequestData
{
    QFileInfo archiveInfo;
    Packuru::Core::ArchiveType archiveType;
    std::vector<QString> files;
    QString destinationPath;
    QString password;
};

}
