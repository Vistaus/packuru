// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "deletiondialogcoreinitdata.h"


namespace Packuru::Core::Browser
{

bool DeletionDialogCoreInitData::isValid() const
{
    return !archiveAbsFilePath.isEmpty()
            && archiveType != ArchiveType::___NOT_SUPPORTED
            && !filesToRemove.empty()
            && archiveEncryption != EncryptionState::___INVALID
            && !(archiveEncryption == EncryptionState::EntireArchive && password.isEmpty())
            && onAccepted;
}

}
