// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QObject>
#include <QLocalSocket>


namespace Packuru::Core::Queue
{

class IncomingConnectionHandler : public QObject
{
    Q_OBJECT
public:
    explicit IncomingConnectionHandler(qintptr socketDescriptor,QObject *parent = nullptr);
    ~IncomingConnectionHandler();

    bool init();

signals:
    void sigNewMessageReceived(const QByteArray& data);
    void error(QLocalSocket::LocalSocketError socketError);
    void finished();

private slots:
    void slotSocketReadyRead();

private:
    QLocalSocket* socket;
    qintptr socketDescriptor;
    qint64 messageSize;
    bool isBusy;
};

}
