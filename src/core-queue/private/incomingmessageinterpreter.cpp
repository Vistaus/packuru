// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QDataStream>
#include <QDebug>

#include "core/private/messagetype.h"
#include "core/private/tasks/archivingtaskdata.h"
#include "core/private/tasks/extractiontaskdata.h"
#include "core/private/tasks/deletiontaskdata.h"
#include "core/private/tasks/testtaskdata.h"

#include "incomingmessageinterpreter.h"

#include "commandlinecreationdata.h"
#include "commandlineextractiondata.h"
#include "commandlinetestdata.h"


using Packuru::Core::MessageType;


namespace Packuru::Core::Queue
{

IncomingMessageInterpreter::IncomingMessageInterpreter(QObject* parent)
    : QObject(parent)
{

}


void IncomingMessageInterpreter::interpret(const QByteArray &message) const
{
    QDataStream stream(message);

    MessageType type;
    stream >> type;

    switch (type)
    {
    case MessageType::ArchivingTaskData:
    {
        Packuru::Core::ArchivingTaskData data;
        stream >> data;
        emit archivingTaskData(data);
        break;
    }
    case MessageType::ExtractionTaskData:
    {
        Packuru::Core::ExtractionTaskData data;
        stream >> data;
        emit extractionTaskData(data);
        break;
    }
    case MessageType::DeletionTaskData:
    {
        Packuru::Core::DeletionTaskData data;
        stream >> data;
        emit deletionTaskData(data);
        break;
    }
    case MessageType::TestTaskData:
    {
        Packuru::Core::TestTaskData data;
        stream >> data;
        emit testTaskData(data);
        break;
    }
    case MessageType::CommandLineCreateArchives:
    {
        CommandLineCreationData data;
        stream >> data;
        emit commandLineCreateArchives(data);
        break;
    }
    case MessageType::CommandLineDefaultStartUp:
    {
        emit defaultStartUp();
        break;
    }
    case MessageType::CommandLineError:
    {
        QString info;
        stream >> info;
        emit commandLineError(info);
        break;
    }
    case MessageType::CommandLineExtractArchives:
    {
        CommandLineExtractionData data;
        stream >> data;
        emit commandLineExtractArchives(data);
        break;
    }
    case MessageType::CommandLineHelp:
    {
        QString info;
        stream >> info;
        emit commandLineHelpRequested(info);
        break;
    }
    case MessageType::CommandLineTestArchives:
    {
        CommandLineTestData data;
        stream >> data;
        emit commandLineTestArchives(data);
        break;
    }
    default:
        Q_ASSERT(false);
        qWarning() << Packuru::Core::Queue::IncomingMessageInterpreter::tr("Unknown message type");
        return;
    }
}

}
