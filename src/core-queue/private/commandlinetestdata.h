// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <vector>

#include <QFileInfo>


namespace Packuru::Core::Queue
{

struct CommandLineTestData
{
    std::vector<QFileInfo> inputItems;
    bool openDialog;
};

}


QDataStream& operator<<(QDataStream& out, const Packuru::Core::Queue::CommandLineTestData& data);
QDataStream& operator>>(QDataStream& in, Packuru::Core::Queue::CommandLineTestData& data);
