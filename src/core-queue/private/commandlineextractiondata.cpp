// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QDataStream>

#include "core/private/datastream/container_datastream.h"
#include "core/private/datastream/qfileinfo_datastream.h"

#include "commandlineextractiondata.h"


using Packuru::Core::operator <<;
using Packuru::Core::operator >>;


QDataStream& operator<<(QDataStream& out, const Packuru::Core::Queue::CommandLineExtractionData& data)
{
    return out << data.archives
               << data.destinationPath
               << data.topFolderMode
               << data.openDialog;
}


QDataStream& operator>>(QDataStream& in, Packuru::Core::Queue::CommandLineExtractionData& data)
{
    return in >> data.archives
              >> data.destinationPath
              >> data.topFolderMode
              >> data.openDialog;
}
