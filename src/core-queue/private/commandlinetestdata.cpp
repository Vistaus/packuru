// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QDataStream>

#include "core/private/datastream/container_datastream.h"
#include "core/private/datastream/qfileinfo_datastream.h"

#include "commandlinetestdata.h"


using Packuru::Core::operator <<;
using Packuru::Core::operator >>;


QDataStream& operator<<(QDataStream& out, const Packuru::Core::Queue::CommandLineTestData& data)
{
    return out << data.inputItems
               << data.openDialog;
}


QDataStream& operator>>(QDataStream& in, Packuru::Core::Queue::CommandLineTestData& data)
{
    return in >> data.inputItems
              >> data.openDialog;
}
