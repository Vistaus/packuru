// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <functional>

#include <QString>
#include <QObject>


namespace Packuru::Core::Queue::QueueCoreAux
{

struct Init
{
    // This is the name of the server that receives messages from Browser.
    // It is separate from the Queue's instance server provided by SingleApplication.
    QString queueServerName;
};

class Accessor : public QObject
{
    Q_OBJECT
public:
    explicit Accessor(QObject* parent = nullptr)
        : QObject(parent) {}

signals:
    void processMessage(const QByteArray& message);
};

struct Backdoor
{
    Accessor* accessor = nullptr;
};

};

