// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <unordered_set>

#include <QObject>

#include "utils/private/qttypehasher.h"


namespace Packuru::Core
{
class Task;
}

namespace Packuru::Core::Queue
{

class TaskBatch : public QObject
{
    Q_OBJECT
public:
    explicit TaskBatch(QObject *parent = nullptr);

    void addTask(Packuru::Core::Task* task);

signals:
    void openDestinationPath(const QString& path);

private:
    void onTaskDestroyed();
    void onOpenDestinationRequested(const QString& path);

    std::unordered_set<Packuru::Core::Task*> tasks;
    std::unordered_set<QString, Packuru::Utils::QtTypeHasher<QString>> openedPaths;
};

}
