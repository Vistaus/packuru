// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QDataStream>

#include "core/private/datastream/container_datastream.h"
#include "core/private/datastream/qfileinfo_datastream.h"

#include "commandlinecreationdata.h"


using Packuru::Core::operator <<;
using Packuru::Core::operator >>;


QDataStream& operator<<(QDataStream& out, const Packuru::Core::Queue::CommandLineCreationData& data)
{
    return out << data.archiveType
               << data.inputItems
               << data.archivePerItem
               << data.destination
               << data.openDialog
               << data.appStartPath;
}


QDataStream& operator>>(QDataStream& in, Packuru::Core::Queue::CommandLineCreationData& data)
{
    return in >> data.archiveType
              >> data.inputItems
              >> data.archivePerItem
              >> data.destination
              >> data.openDialog
              >> data.appStartPath;
}
