// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "utils/qvariant_utils.h"

#include "core/private/globalsettings.h"

#include "settingsconverter.h"


using namespace Packuru::Core;


namespace Packuru::Core::Queue
{

SettingsConverter::SettingsConverter()
{

}


std::unordered_set<int> SettingsConverter::getSupportedKeys() const
{
    return {toInt(GlobalSettings::Enum::QC_AbortTasksRequiringPassword),
                toInt(GlobalSettings::Enum::QC_AutoCloseOnAllTasksCompletion),
                toInt(GlobalSettings::Enum::QC_AutoRemoveCompletedTasks),
                toInt(GlobalSettings::Enum::QC_MaxErrorCountToStop)};
}


QString SettingsConverter::getKeyName(int key) const
{
    const auto keyId = static_cast<GlobalSettings::Enum>(key);
    switch (keyId)
    {
    case GlobalSettings::Enum::QC_AbortTasksRequiringPassword:
        return QLatin1String("qc_abort_tasks_requiring_password");
    case GlobalSettings::Enum::QC_AutoCloseOnAllTasksCompletion:
        return QLatin1String("qc_auto_close_on_all_tasks_completion");
    case GlobalSettings::Enum::QC_AutoRemoveCompletedTasks:
        return QLatin1String("qc_auto_remove_completed_tasks");
    case GlobalSettings::Enum::QC_MaxErrorCountToStop:
        return QLatin1String("qc_max_error_count_to_stop");
    default:
        Q_ASSERT(false); return QLatin1String();
    }
}


QVariant SettingsConverter::toSettingsType(int key, const QVariant& value) const
{
    const auto keyId = static_cast<GlobalSettings::Enum>(key);
    switch (keyId)
    {
    case GlobalSettings::Enum::QC_AbortTasksRequiringPassword:
    case GlobalSettings::Enum::QC_AutoCloseOnAllTasksCompletion:
    case GlobalSettings::Enum::QC_AutoRemoveCompletedTasks:
        Q_ASSERT(Utils::containsType<bool>(value));
        return value;
    case GlobalSettings::Enum::QC_MaxErrorCountToStop:
        Q_ASSERT(Utils::containsType<int>(value));
        return value;
    default:
        Q_ASSERT(false);
        return QVariant();
    }
}


QVariant SettingsConverter::fromSettingsType(int key, const QVariant& value) const
{
    const auto keyId = static_cast<GlobalSettings::Enum>(key);
    switch (keyId)
    {
    case GlobalSettings::Enum::QC_AbortTasksRequiringPassword:
    case GlobalSettings::Enum::QC_AutoCloseOnAllTasksCompletion:
    case GlobalSettings::Enum::QC_AutoRemoveCompletedTasks:
        if (value.canConvert<bool>())
            return value.toBool();
        else
            return false;
    case GlobalSettings::Enum::QC_MaxErrorCountToStop:
        if (value.canConvert<int>())
            return value.toInt();
        else
            return 0;
    default:
        Q_ASSERT(false);
        return QVariant();
    }
}

}
