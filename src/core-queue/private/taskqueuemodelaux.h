// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <functional>

#include <QList>


namespace Packuru::Core
{

class Task;

namespace Queue::TaskQueueModelAux
{

struct Init
{
};


struct Backdoor
{
   std::function<void (QList<Packuru::Core::Task*>& tasks)> addTasks;
};

}

}
