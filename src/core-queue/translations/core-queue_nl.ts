<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl">
<context>
    <name>MaxErrorCountCtrl</name>
    <message>
        <location filename="../private/queuesettings/maxerrorcountctrl.cpp" line="16"/>
        <source>Maximum error count to stop queue (0 = never stop)</source>
        <translation>Maximum aantal foutmeldingen alvorens af te breken (0 = oneindig)</translation>
    </message>
</context>
<context>
    <name>Packuru::Core::Queue::CommandLineHandler</name>
    <message>
        <location filename="../private/commandlinehandler.cpp" line="108"/>
        <source>Create archives</source>
        <translation>Archieven samenstellen</translation>
    </message>
    <message>
        <location filename="../private/commandlinehandler.cpp" line="110"/>
        <source>Extract archives</source>
        <translation>Archieven uitpakken</translation>
    </message>
    <message>
        <location filename="../private/commandlinehandler.cpp" line="112"/>
        <source>Test archives</source>
        <translation>Archieven testen</translation>
    </message>
    <message>
        <location filename="../private/commandlinehandler.cpp" line="114"/>
        <source>Show dialog</source>
        <translation>Dialoogvenster tonen</translation>
    </message>
    <message>
        <location filename="../private/commandlinehandler.cpp" line="117"/>
        <source>Set destination.
If not set and a dialog is not shown, each archive is extracted to its parent folder.</source>
        <comment>path</comment>
        <translation>Stel de bestemming in.
Als dit niet is ingesteld en het dialoogvenster ontbreekt, dan wordt elk archief uitgepakt naar de bovenliggende map.</translation>
    </message>
    <message>
        <location filename="../private/commandlinehandler.cpp" line="122"/>
        <source>Set type of archive</source>
        <translation>Archieftype instellen</translation>
    </message>
    <message>
        <location filename="../private/commandlinehandler.cpp" line="123"/>
        <source>type</source>
        <translation>type</translation>
    </message>
    <message>
        <location filename="../private/commandlinehandler.cpp" line="125"/>
        <source>Creates single archive if set, otherwise archive per item will be created</source>
        <translation>Stel in om één groot archief samen te stellen in plaats van één archief per item</translation>
    </message>
    <message>
        <location filename="../private/commandlinehandler.cpp" line="129"/>
        <source>Create top folder. Valid values:
 yes, no, auto
Used when extracting archives</source>
        <translation>Maak de hoofdmap aan. Geldige waarden:
 ja, nee, auto
Wordt gebruikt bij het uitpakken van archieven.</translation>
    </message>
    <message>
        <location filename="../private/commandlinehandler.cpp" line="132"/>
        <source>value</source>
        <translation>waarde</translation>
    </message>
</context>
<context>
    <name>Packuru::Core::Queue::IncomingMessageInterpreter</name>
    <message>
        <location filename="../private/incomingmessageinterpreter.cpp" line="113"/>
        <source>Unknown message type</source>
        <translation>Onbekend bericht</translation>
    </message>
</context>
<context>
    <name>Packuru::Core::Queue::MainWindowCore</name>
    <message>
        <location filename="../mainwindowcore.cpp" line="89"/>
        <source>%1 Queue</source>
        <translation>%1-wachtrij</translation>
    </message>
</context>
<context>
    <name>Packuru::Core::Queue::QueueCore</name>
    <message>
        <location filename="../queuecore.cpp" line="143"/>
        <source>Error while starting queue server:</source>
        <translation>De wachtrijdienst kan niet  worden gestart:</translation>
    </message>
    <message>
        <location filename="../queuecore.cpp" line="252"/>
        <source>No plugins to create archives.</source>
        <translation>Er zijn geen plug-ins om archieven samen te stellen.</translation>
    </message>
    <message>
        <location filename="../queuecore.cpp" line="259"/>
        <source>No input urls specified.</source>
        <translation>Er zijn geen url&apos;s ingevoerd.</translation>
    </message>
    <message>
        <location filename="../queuecore.cpp" line="261"/>
        <source>Unsupported archive type specified.</source>
        <translation>Er is een niet-ondersteung type opgegeven.</translation>
    </message>
    <message>
        <location filename="../queuecore.cpp" line="274"/>
        <source>No plugin to create this type of archive.</source>
        <translation>Er is geen plug-ins om dit archieftype samen te stellen.</translation>
    </message>
    <message>
        <location filename="../queuecore.cpp" line="313"/>
        <location filename="../queuecore.cpp" line="325"/>
        <source>archive</source>
        <translation>archief</translation>
    </message>
    <message>
        <location filename="../queuecore.cpp" line="353"/>
        <source>No archives to extract.</source>
        <translation>Er zijn geen uit te pakken archieven.</translation>
    </message>
    <message>
        <location filename="../queuecore.cpp" line="392"/>
        <source>No archives to test.</source>
        <translation>Er zijn geen te testen archieven.</translation>
    </message>
</context>
<context>
    <name>Packuru::Core::Queue::SettingsDialogCore</name>
    <message>
        <location filename="../settingsdialogcore.cpp" line="39"/>
        <source>Abort tasks that require password</source>
        <translation>Taken voorzien van wachtwoord afbreken</translation>
    </message>
    <message>
        <location filename="../settingsdialogcore.cpp" line="46"/>
        <source>Quit if all tasks completed</source>
        <translation>Afsluiten na afronden van alle taken</translation>
    </message>
    <message>
        <location filename="../settingsdialogcore.cpp" line="54"/>
        <source>Automatically remove completed tasks</source>
        <translation>Afgeronde taken automatisch wissen</translation>
    </message>
    <message>
        <location filename="../settingsdialogcore.cpp" line="85"/>
        <source>Queue</source>
        <translation>Wachtrij</translation>
    </message>
</context>
<context>
    <name>Packuru::Core::Queue::TaskQueueModel</name>
    <message>
        <location filename="../taskqueuemodel.cpp" line="892"/>
        <source>Stop</source>
        <translation>Afbreken</translation>
    </message>
    <message>
        <location filename="../taskqueuemodel.cpp" line="897"/>
        <source>Add files</source>
        <translation>Bestanden toevoegen</translation>
    </message>
    <message>
        <location filename="../taskqueuemodel.cpp" line="899"/>
        <source>Create archive</source>
        <translation>Archief samenstellen</translation>
    </message>
    <message>
        <location filename="../taskqueuemodel.cpp" line="901"/>
        <source>Extract archive</source>
        <translation>Archief uitpakken</translation>
    </message>
    <message>
        <location filename="../taskqueuemodel.cpp" line="903"/>
        <source>Delete files</source>
        <translation>Bestanden verwijderen</translation>
    </message>
    <message>
        <location filename="../taskqueuemodel.cpp" line="905"/>
        <source>Test archive</source>
        <translation>Archief testen</translation>
    </message>
    <message>
        <location filename="../taskqueuemodel.cpp" line="253"/>
        <source>Task</source>
        <translation>Taak</translation>
    </message>
    <message>
        <location filename="../taskqueuemodel.cpp" line="255"/>
        <source>Archive</source>
        <translation>Archief</translation>
    </message>
    <message>
        <location filename="../taskqueuemodel.cpp" line="257"/>
        <source>State</source>
        <translation>Status</translation>
    </message>
    <message>
        <location filename="../taskqueuemodel.cpp" line="259"/>
        <source>Progress</source>
        <translation>Voortgang</translation>
    </message>
    <message>
        <location filename="../taskqueuemodel.cpp" line="261"/>
        <source>Error info</source>
        <translation>Foutinformatie</translation>
    </message>
    <message>
        <location filename="../taskqueuemodel.cpp" line="524"/>
        <source>Remove completed</source>
        <translation>Afgeronde taken wissen</translation>
    </message>
    <message>
        <location filename="../taskqueuemodel.cpp" line="527"/>
        <source>Remove task</source>
        <translation>Taak wissen</translation>
    </message>
    <message>
        <location filename="../taskqueuemodel.cpp" line="530"/>
        <source>Reset task</source>
        <translation>Taak herstellen</translation>
    </message>
    <message>
        <location filename="../taskqueuemodel.cpp" line="533"/>
        <source>Start queue</source>
        <translation>Wachtrij starten</translation>
    </message>
    <message>
        <location filename="../taskqueuemodel.cpp" line="536"/>
        <source>Stop here</source>
        <translation>Hier afbreken</translation>
    </message>
    <message>
        <location filename="../taskqueuemodel.cpp" line="539"/>
        <source>Stop queue</source>
        <translation>Wachtrij afbreken</translation>
    </message>
    <message>
        <location filename="../taskqueuemodel.cpp" line="871"/>
        <source>Stopped</source>
        <translation>Afgebroken</translation>
    </message>
    <message numerus="yes">
        <location filename="../taskqueuemodel.cpp" line="878"/>
        <source>%n left</source>
        <translation>
            <numerusform>Nog %n te gaan</numerusform>
            <numerusform>Nog %n te gaan</numerusform>
        </translation>
    </message>
</context>
</TS>
