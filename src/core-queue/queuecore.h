// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QObject>

#include "symbol_export.h"

/// @file

namespace Packuru::Core
{
class ArchivingDialogCore;
class ExtractionDialogCore;
class TestDialogCore;
class AppSettingsDialogCore;
}

namespace Packuru::Core::Queue
{

namespace QueueCoreAux
{
struct Init;
struct Backdoor;
}

class MainWindowCore;
class SettingsDialogCore;

/**
 * @brief Provides core %Queue objects for windows and dialogs.
 *
 * It provides core objects for archive dialogs, main window and settings dialog.
 *
 * %QueueCore object is provided by Init object.
 *
 * @headerfile "core-queue/queuecore.h"
 */
class PACKURU_CORE_QUEUE_EXPORT QueueCore : public QObject
{
    Q_OBJECT
public:
    QueueCore(QueueCoreAux::Init& init,
              QueueCoreAux::Backdoor& backdoor,
              QObject* parent = nullptr);
    ~QueueCore() override;

    /**
     * @brief Requests ArchivingDialogCore object.
     * @note The object will be provided in the archivingDialogCoreCreated() signal.
     */
    Q_INVOKABLE void createArchivingDialogCore();

    /**
     * @brief Requests ExtractionDialogCore object.
     * @note The object will be provided in the extractionDialogCoreCreated() signal.
     */
    Q_INVOKABLE void createExtractionDialogCore();

    /**
     * @brief Requests TestDialogCore object.
     * @note The object will be provided in the testDialogCoreCreated() signal.
     */
    Q_INVOKABLE void createTestDialogCore();

    /**
     * @brief Returns a pointer to AppSettingsDialogCore object that handles core application settings.
     * @note Object ownership is transferred to the caller.
     * @sa createQueueSettingsDialogCore()
     */
    Q_INVOKABLE Packuru::Core::AppSettingsDialogCore* createAppSettingsDialogCore();

    /**
     * @brief Returns a pointer to SettingsDialogCore object that handles core %Queue settings.
     * @note Object ownership is transferred to the caller.
     * @sa createAppSettingsDialogCore()
     */
    Q_INVOKABLE Packuru::Core::Queue::SettingsDialogCore* createQueueSettingsDialogCore();

    /**
     * @brief Returns a pointer to MainWindowCore object that handles core %Queue settings.
     * @note Object ownership is not transferred to the caller.
     */
    Q_INVOKABLE Packuru::Core::Queue::MainWindowCore* getMainWindowCore();

signals:
    /**
     * @brief Emitted when another %Queue instance has been started without any arguments.
     *
     * This signal allows to raise the main window or show it if hidden.
     */
    void defaultStartUp();

    /// @brief Emitted when the user requested help in command-line ("-h").
    void commandLineHelpRequested(const QString& info) const;

    /// @brief Emitted when %Queue has been invoked with invalid arguments.
    void commandLineError(const QString& info);

    /** @brief Emitted when new tasks have been added to queue.
     *
     * The tasks may be sent by %Browser instance or they may be created when archive task dialog core
     * object has been accepted.
     *
     * This signal allows to raise the main window or show it if hidden.
     */
    void newTasks();

    /** @brief Provides a pointer to ArchivingDialogCore object.
     *
     * This signal is emitted either when the object has been explicitly requested
     * by calling createArchivingDialogCore() or when another %Queue instance requested
     * the dialog in the command-line.
     *
     * @note Object ownership is transferred to the receiver.
     */
    void archivingDialogCoreCreated(Packuru::Core::ArchivingDialogCore* core) const;

    /** @brief Provides a pointer to ExtractionDialogCore object.
     *
     * This signal is emitted either when the object has been explicitly requested
     * by calling createExtractionDialogCore() or when another %Queue instance requested
     * the dialog in the command-line.
     *
     * @note Object ownership is transferred to the receiver.
     */
    void extractionDialogCoreCreated(Packuru::Core::ExtractionDialogCore* core) const;

    /** @brief Provides a pointer to TestDialogCore object.
     *
     * This signal is emitted either when the object has been explicitly requested
     * by calling createTestDialogCore() or when another %Queue instance requested
     * the dialog in the command-line.
     *
     * @note Object ownership is transferred to the receiver.
     */
    void testDialogCoreCreated(Packuru::Core::TestDialogCore* core) const;

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}
