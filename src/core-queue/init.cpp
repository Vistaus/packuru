// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QCoreApplication>

#include "../core/globalsettingsmanager.h"
#include "../core/private/loadtranslations.h"
#include "../core/private/coresettingsconverter.h"
#include "../core/private/globalsettingsmanageraux.h"
#include "../core/private/3rdparty/KDSingleApplication/kdsingleapplication.h"

#include "init.h"
#include "private/settingsconverter.h"
#include "private/commandlinehandler.h"
#include "queuecore.h"
#include "private/queuecoreaux.h"


namespace Packuru::Core::Queue
{

struct Init::Private
{
    Private();

    KDSingleApplication singleApplication;
    CommandLineHandler clHandler;
    QByteArray serializedArguments;
    std::unique_ptr<QueueCore> queueCore;
    QueueCoreAux::Backdoor queueCoreBackdoor;
    QString queueServerName;
};


Init::Init(const QString& queueServerName)
    : priv(new Private)
{
    priv->queueServerName = queueServerName;

    loadTranslations();

    GlobalSettingsManagerAux::Init initData;
    initData.converters.push_back(std::make_unique<Packuru::Core::CoreSettingsConverter>());
    initData.converters.push_back(std::make_unique<Packuru::Core::Queue::SettingsConverter>());

    GlobalSettingsManager::createSingleInstance(initData, this);
}


Init::~Init()
{

}


bool Init::messagePrimaryInstance() const
{
    if (!priv->singleApplication.isPrimaryInstance())
    {
        priv->singleApplication.sendMessage(priv->serializedArguments);
        return true;
    }

    return false;
}


QueueCore* Init::getQueueCore()
{
    if (!priv->queueCore)
    {
        QueueCoreAux::Init init;
        init.queueServerName = priv->queueServerName;

        priv->queueCore.reset(new QueueCore(init, priv->queueCoreBackdoor));
        Q_ASSERT(priv->queueCoreBackdoor.accessor);

        using Packuru::Core::Queue::QueueCoreAux::Accessor;

        connect(&priv->singleApplication, &KDSingleApplication::messageReceived,
                priv->queueCoreBackdoor.accessor, &Accessor::processMessage);
    }

    return priv->queueCore.get();
}


void Init::processArguments()
{
    auto accessor = priv->queueCoreBackdoor.accessor;

    if (accessor)
        emit accessor->processMessage(priv->serializedArguments);
}


Init::Private::Private()
    : serializedArguments(clHandler.createMessageFromArguments())
{

}

}
