// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QObject>

#include "symbol_export.h"

/// @file

class QCoreApplication;


namespace Packuru::Core::Queue
{

class QueueCore;

/**
 * @brief Initializes %Queue.
 *
 * This is the the top-level class in %Queue. It should be instantiated very early,
 * ideally just after QApplication.
 *
 * The class loads translations, instantiates GlobalSettingsManager, messages other instances
 * at start up, creates QueueCore and processes command-line arguments.
 *
 * @par Example
 *
 * @code
 * using Packuru::Core::AppInfo;
 * using Packuru::Core::Queue::Init;
 *
 * int main(int argc, char *argv[])
 * {
 *     QApplication app(argc, argv);
 *     app.setApplicationVersion(AppInfo::getAppVersion());
 *     app.setOrganizationName(AppInfo::getAppName());
 *     app.setApplicationName(AppInfo::getAppName() + " Desktop");
 *
 *     Init coreInit(QUEUE_EXEC_NAME);
 *
 *     if (coreInit.messagePrimaryInstance())
 *         return 0;
 *
 *     QueueDesktop queueDesktop(coreInit.getQueueCore());
 *
 *     QObject::connect(&queueDesktop, &QueueDesktop::quitApplication,
 *                      &app, &QApplication::quit);
 *
 *     coreInit.processArguments();
 *
 *     return app.exec();
 * }
 * @endcode
 *
 * @headerfile "core-queue/init.h"
 * @sa Packuru::Core::Browser::Init
 */
class PACKURU_CORE_QUEUE_EXPORT Init : public QObject
{
    Q_OBJECT
public:
    /**
     * @brief Constructs %Init object.
     * @param queueServerName Specifies %Queue server name.
     * %Queue can be messaged by %Browser when the user requests running a task in %Queue.
     * This server is separate from the %Queue's instance server provided by SingleApplication.
     * The name is frontend-specific.
     */
    explicit Init(const QString& queueServerName);
    ~Init();

    /**
     * @brief Attempts to send command-line arguments to primary instance.
     *
     * If this is a secondary application instance the function attempts to contact primary instance
     * and regardless of the result returns true (in this case this instance can be finished).
     * Otherwise it returns false.
     */
    bool messagePrimaryInstance() const;

    /**
     * @brief Returns pointer to QueueCore.
     *
     * The object is created on first invocation.
     * @note The object ownership is not transferred to the caller.
     */
    QueueCore* getQueueCore();

    /**
     * @brief Processes command-line arguments of this instance.
     * @note This function does nothing if QueueCore has not been created by calling
     * getQueueCore().
     */
    void processArguments();

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}
