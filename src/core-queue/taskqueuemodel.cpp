// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include <QIcon>
#include <QTimer>
#include <QUrl>

#include "utils/makeqpointer.h"

#include "core/passwordpromptcore.h"
#include "core/creationnameerrorpromptcore.h"
#include "core/extractionfileoverwritepromptcore.h"
#include "core/extractionfolderoverwritepromptcore.h"
#include "core/extractiontypemismatchpromptcore.h"
#include "core/globalsettingsmanager.h"
#include "core/private/globalsettings.h"
#include "core/commonstrings.h"
#include "core/private/tasks/task.h"
#include "core/private/tasks/taskstate.h"
#include "core/private/tasks/tasktype.h"
#include "core/private/tasks/archivingtask.h"
#include "core/private/tasks/extractiontask.h"

#include "taskqueuemodel.h"
#include "private/taskbatch.h"
#include "private/taskqueuemodelaux.h"


using namespace Packuru::Core;
using Packuru::Utils::makeQPointer;


namespace
{

enum class QueueState
{
    Running,
    Stopped
};


enum class TaskQueueHeaderData
{
    TaskInfo,
    ArchiveName,
    TaskState,
    TaskProgress,
    TaskError,

    ___COUNT
};


enum UserRole
{
    TaskInfo = Qt::UserRole,
    ArchiveName,
    TaskStateInfo,
    TaskStateId,
    TaskProgress,
    TaskError,
    TaskLogAvailable
};

}


namespace Packuru::Core::Queue
{

struct TaskQueueModel::Private
{
    Private(TaskQueueModel* publ);
    ~Private();

    void addTasks(QList<Task*>& tasks);
    QueueState getQueueState() const { return queueState; }
    void setQueueState(QueueState state);
    void onTaskStateChanged(Task* task);
    void onTaskProgressChanged(Task* task);
    int getTaskRow(const Task* task) const;
    bool processQueue(int fromPosition);
    void stopCurrentRunningTask();
    Task* indexToTask(const QModelIndex& index) const;
    void updateQueueDescription();
    QString createTaskInfo(const Task* task);
    void startDescriptionUpdateTimer();

    TaskQueueModel* publ;
    std::vector<Task*> queue;
    const int columnCount;
    QueueState queueState;
    int completedTasks;
    int readyTasks;
    Task* currentRunningTask;
    Task* stopAfterTask;
    int maxErrorCount = 0;
    int errorCount = 0;
    bool abortEncrypted = false;
    bool autoRemoveCompleted = false;
    QString queueDescription;
    QTimer* descriptionUpdateTimer;
};


TaskQueueModel::TaskQueueModel(const TaskQueueModelAux::Init& initData, TaskQueueModelAux::Backdoor& backdoorData, QObject *parent)
    : QAbstractTableModel(parent),
      priv(new Private(this))
{
    Q_UNUSED(initData)

    backdoorData.addTasks = [publ = makeQPointer(this)] (QList<Packuru::Core::Task*>& tasks)
    {
        if (!publ)
            return;
        publ->priv->addTasks(tasks);
    };
}


TaskQueueModel::~TaskQueueModel()
{
}


int TaskQueueModel::columnCount(const QModelIndex &) const
{
    return priv->columnCount;
}


int TaskQueueModel::rowCount(const QModelIndex &) const
{
    return static_cast<int>(priv->queue.size());
}


QVariant TaskQueueModel::data(const QModelIndex &index, int role) const
{
    Q_ASSERT(index.isValid());

    const auto stdRow = static_cast<std::size_t>(index.row());
    Q_ASSERT(index.column() >= 0 && index.column() < priv->columnCount
             && index.row() >= 0 && stdRow < priv->queue.size());

    const auto column = static_cast<TaskQueueHeaderData>(index.column());
    const Task* const task = priv->queue[stdRow];
    const auto getTaskErrors = [task = task] ()
    {
        const auto state = task->getState();
        if (state == TaskState::Errors || state == TaskState::Warnings)
            return QVariant::fromValue(task->getErrorAndWarningNames().join(QLatin1String(", ")));
        else
            return QVariant();
    };

    if (role == Qt::DisplayRole)
    {
        switch (column)
        {
        case TaskQueueHeaderData::TaskInfo:
            return priv->createTaskInfo(task);
        case TaskQueueHeaderData::ArchiveName:
            return task->getArchiveName();
        case TaskQueueHeaderData::TaskState:
            return toString(task->getState());
        case TaskQueueHeaderData::TaskProgress:
            return task->getProgress();
        case TaskQueueHeaderData::TaskError:
            return getTaskErrors();
        default:
            return QVariant{};
        }
    }
    else if (role == Qt::DecorationRole)
    {
        switch (column)
        {
        case TaskQueueHeaderData::TaskInfo:
        {
            switch (task->getState())
            {
            case TaskState::Aborted:
            case TaskState::Errors:
                return QIcon::fromTheme("error");
            case TaskState::ReadyToRun:
                return QIcon::fromTheme("clock");
            case TaskState::StartingUp:
            case TaskState::InProgress:
                return QIcon::fromTheme("media-playback-start");
            case TaskState::Paused:
                return QIcon::fromTheme("");
            case TaskState::Warnings:
                return QIcon::fromTheme("messagebox_warning");
            case TaskState::Success:
                return QIcon::fromTheme("dialog-ok");
            case TaskState::WaitingForDecision:
                return QIcon::fromTheme("question");
            case TaskState::WaitingForPassword:
                return QIcon::fromTheme("document-encrypted");
            default:
                Q_ASSERT(false); return QVariant();
            }
        }
        default:
            return QVariant();
        }
    }
    else if (role == UserRole::TaskInfo)
    {
        return priv->createTaskInfo(task);
    }
    else if (role == UserRole::TaskStateInfo)
    {
        return toString(task->getState());
    }
    else if (role == UserRole::ArchiveName)
    {
        return task->getArchiveName();
    }
    else if (role == UserRole::TaskProgress)
    {
        return task->getProgress();
    }
    else if (role == UserRole::TaskError)
    {
        return getTaskErrors();
    }
    else if (role == UserRole::TaskLogAvailable)
    {
        return task->hasLog();
    }

    return QVariant();
}


QVariant TaskQueueModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    if (orientation == Qt::Horizontal)
    {
        if (section >= 0 and section < priv->columnCount)
        {
            auto column = static_cast<TaskQueueHeaderData>(section);
            switch (column)
            {
            case TaskQueueHeaderData::TaskInfo:
                return Packuru::Core::Queue::TaskQueueModel::tr("Task");
            case TaskQueueHeaderData::ArchiveName:
                return Packuru::Core::Queue::TaskQueueModel::tr("Archive");
            case TaskQueueHeaderData::TaskState:
                return Packuru::Core::Queue::TaskQueueModel::tr("State");
            case TaskQueueHeaderData::TaskProgress:
                return Packuru::Core::Queue::TaskQueueModel::tr("Progress");
            case TaskQueueHeaderData::TaskError:
                return Packuru::Core::Queue::TaskQueueModel::tr("Error info");
            default:
                Q_ASSERT(false); return QString();
            }
        }
    }
    else if (orientation == Qt::Vertical)
    {
        if (section >= 0 && static_cast<uint>(section) < priv->queue.size())
            return section + 1;
    }

    return QVariant();
}


QHash<int, QByteArray> TaskQueueModel::roleNames() const
{
    auto map = QAbstractItemModel::roleNames();

    map[UserRole::TaskInfo] = "taskInfo";
    map[UserRole::TaskError] = "taskError";
    map[UserRole::TaskStateInfo] = "taskStateInfo";
    map[UserRole::TaskStateId] = "taskStateId";
    map[UserRole::ArchiveName] = "archiveName";
    map[UserRole::TaskProgress] = "taskProgress";
    map[UserRole::TaskLogAvailable] = "taskLogAvailable";

    return map;
}


bool TaskQueueModel::removeRows(int row, int count, const QModelIndex& parent)
{
    Q_UNUSED(parent)
    Q_ASSERT(row >= 0 &&
             static_cast<uint>(row) < priv->queue.size() &&
             count > 0 &&
             static_cast<uint>(row) + static_cast<uint>(count) <= priv->queue.size());

    const auto rangeBegin = priv->queue.cbegin() + row;
    const auto rangeEnd = priv->queue.cbegin() + row + count;

    beginRemoveRows(QModelIndex(), row, row + count - 1);

    int removedCompleted = 0;
    bool removedRunning = false;
    for (auto it = rangeBegin; it != rangeEnd; ++it)
    {
        auto* const task = *it;

        if (task == priv->stopAfterTask)
            priv->stopAfterTask = nullptr;

        if (task->isReadyToRun())
        {
            --priv->readyTasks;
        }
        else if (task->isBusy())
        {
            Q_ASSERT(task == priv->currentRunningTask);
            priv->stopCurrentRunningTask();
            removedRunning = true;
        }
        else if (task->isCompleted())
        {
            --priv->completedTasks;
            ++removedCompleted;
        }

        task->disconnect(this);
        task->deleteLater();
    }

    priv->queue.erase(rangeBegin, rangeEnd);

    endRemoveRows();

    if (removedCompleted > 0)
        emit completedTaskCountChanged(priv->completedTasks);

    if (removedRunning)
        priv->processQueue(row);

    // Needed for column auto-resizing in QTableView ?
    const auto indexTop = createIndex(row, 0);
    const auto indexBottom = createIndex(row + count - 1, priv->columnCount - 1);
    emit dataChanged(indexTop, indexBottom);

    return true;
}


void TaskQueueModel::resetTasks(const QModelIndexList &indexes)
{
    if (indexes.empty())
        return;

    const int previouslyCompleted = priv->completedTasks;

    for (const auto& index : indexes)
    {
        const auto stdRow = static_cast<std::size_t>(index.row());

        Q_ASSERT(index.isValid() && stdRow < priv->queue.size());

        const auto* const task = priv->queue[stdRow];
        const bool wasCompleted = task->isCompleted();
        const auto resetSuccess = priv->queue[stdRow]->reset();

        if (resetSuccess && wasCompleted)
            --priv->completedTasks;

    }

    if (previouslyCompleted != priv->completedTasks)
        emit completedTaskCountChanged(priv->completedTasks);

    if (priv->queueState == QueueState::Running && !priv->currentRunningTask)
        priv->processQueue(0);
}


void TaskQueueModel::stopAfterTask(const QModelIndex& index)
{
    const auto stdRow = static_cast<std::size_t>(index.row());
    auto task = priv->queue[stdRow];

    if (task == priv->stopAfterTask)
    {
        priv->stopAfterTask = nullptr;

        emit dataChanged(createIndex(index.row(), 0),
                         createIndex(index.row(), priv->columnCount));
    }
    else
    {
        const auto prevRow = priv->getTaskRow(priv->stopAfterTask);

        priv->stopAfterTask = task;

        if (prevRow >= 0)
        {
            emit dataChanged(createIndex(prevRow, 0),
                             createIndex(prevRow, priv->columnCount));
        }

        emit dataChanged(createIndex(index.row(), 0),
                         createIndex(index.row(), priv->columnCount));
    }
}


bool TaskQueueModel::hasErrorLog(const QModelIndex& index) const
{
    const auto* const task = priv->indexToTask(index);
    return task->hasLog();
}


QString TaskQueueModel::getErrorLog(const QModelIndex& index) const
{
    const auto* const task = priv->indexToTask(index);

    return task->getLog();
}


void TaskQueueModel::startQueue()
{
    if (priv->getQueueState() == QueueState::Stopped)
    {
        priv->setQueueState(QueueState::Running);
        priv->processQueue(0);
    }
}


void TaskQueueModel::stopQueue()
{
    if (priv->getQueueState() != QueueState::Running)
        return;

    priv->setQueueState(QueueState::Stopped);
}


void TaskQueueModel::removeCompletedTasks()
{
    beginResetModel();

    int removedCount = 0;

    for (auto it = priv->queue.begin(); it != priv->queue.end(); )
    {
        if ((*it)->isCompleted())
        {
            it = priv->queue.erase(it);
            --priv->completedTasks;
            ++removedCount;
        }
        else
            ++it;
    }

    endResetModel();

    if (removedCount > 0)
        emit completedTaskCountChanged(priv->completedTasks);
}


int TaskQueueModel::unfinishedTaskCount() const
{
    int result = 0;

    for (const auto& task : priv->queue)
        if (!task->isFinished())
            ++result;

    return result;
}


bool TaskQueueModel::areAllTasksCompleted() const
{
    return priv->completedTasks == static_cast<int>(priv->queue.size());
}


int TaskQueueModel::getCompletedTaskCount() const
{
    return priv->completedTasks;
}


bool TaskQueueModel::isRunning() const
{
    return priv->getQueueState() == QueueState::Running;
}


int TaskQueueModel::progressColumn() const
{
    return static_cast<int>(TaskQueueHeaderData::TaskProgress);
}


QString TaskQueueModel::getQueueDescription() const
{
    return priv->queueDescription;
}


QString TaskQueueModel::getString(TaskQueueModel::UserString value)
{
    switch (value)
    {

    case UserString::ActionErrorLog:
        return CommonStrings::getString(CommonStrings::UserString::ErrorLogDialogTitle);

    case UserString::ActionRemoveCompleted:
        return Packuru::Core::Queue::TaskQueueModel::tr("Remove completed");

    case UserString::ActionRemoveTask:
        return Packuru::Core::Queue::TaskQueueModel::tr("Remove task");

    case UserString::ActionResetTask:
        return Packuru::Core::Queue::TaskQueueModel::tr("Reset task");

    case UserString::ActionStartQueue:
        return Packuru::Core::Queue::TaskQueueModel::tr("Start queue");

    case UserString::ActionStopHere:
        return Packuru::Core::Queue::TaskQueueModel::tr("Stop here");

    case UserString::ActionStopQueue:
        return Packuru::Core::Queue::TaskQueueModel::tr("Stop queue");

    default:
        Q_ASSERT(false); return "";
    }
}


TaskQueueModel::Private::Private(TaskQueueModel* publ)
    : publ(publ),
      columnCount(static_cast<int>(TaskQueueHeaderData::___COUNT)),
      queueState(QueueState::Running),
      completedTasks(0),
      readyTasks(0),
      currentRunningTask(nullptr),
      stopAfterTask(nullptr)
{
    using Enum = GlobalSettings::Enum;

    abortEncrypted = GlobalSettingsManager::instance()
            ->getValueAs<bool>(Enum::QC_AbortTasksRequiringPassword);

    autoRemoveCompleted = GlobalSettingsManager::instance()
            ->getValueAs<bool>(Enum::QC_AutoRemoveCompletedTasks);

    maxErrorCount = GlobalSettingsManager::instance()
            ->getValueAs<int>(Enum::QC_MaxErrorCountToStop);

    updateQueueDescription();
    descriptionUpdateTimer = new QTimer(publ);
    descriptionUpdateTimer->setInterval(100);
    descriptionUpdateTimer->setSingleShot(true);

    const auto startTimer = [priv = this] ()
    { priv->startDescriptionUpdateTimer(); };

    QObject::connect(descriptionUpdateTimer, &QTimer::timeout,
                     publ, [publ = publ] () { publ->priv->updateQueueDescription(); });

    QObject::connect(publ, &TaskQueueModel::runningChanged,
                     publ, startTimer);

    QObject::connect(publ, &TaskQueueModel::rowsInserted,
                     publ, startTimer);

    QObject::connect(publ, &TaskQueueModel::rowsRemoved,
                     publ, startTimer);

    QObject::connect(GlobalSettingsManager::instance(), &GlobalSettingsManager::valueChanged,
                     publ, [publ] (auto key, const auto& value)
    {
        auto priv= publ->priv.get();
        const auto keyId = static_cast<GlobalSettings::Enum>(key);
        switch (keyId)
        {
        case Enum::QC_AbortTasksRequiringPassword:
            priv->abortEncrypted = Utils::getValueAs<bool>(value); break;

        case Enum::QC_AutoRemoveCompletedTasks:
            priv->autoRemoveCompleted = Utils::getValueAs<bool>(value); break;

        case Enum::QC_MaxErrorCountToStop:
            priv->maxErrorCount = Utils::getValueAs<int>(value); break;

        default:
            break;
        }
    });
}


TaskQueueModel::Private::~Private()
{

}


void TaskQueueModel::Private::addTasks(QList<Task*>& tasks)
{
    if (tasks.empty())
        return;

    const auto insertFirst = static_cast<int>(queue.size());
    const auto insertLast = static_cast<int>(queue.size()) + tasks.size() - 1;

    publ->beginInsertRows(QModelIndex{}, insertFirst, insertLast);

    TaskBatch* batch = nullptr;

    auto createBatch = [publ = publ] ()
    {
        auto batch = new TaskBatch(publ);
        QObject::connect(batch, &TaskBatch::openDestinationPath,
                         publ, [publ = publ] (const QString& path)
        { publ->openDestinationFolder(QUrl::fromLocalFile(path)); });

        return batch;
    };

    for (auto task : tasks)
    {
        queue.push_back(task);
        task->setParent(publ);

        if (task->isReadyToRun())
            ++readyTasks;

        QObject::connect(task, &Task::stateChanged,
                         publ, [publ = publ, task = task] ()
        {
            publ->priv->onTaskStateChanged(task);
        });

        QObject::connect(task, &Task::progressChanged,
                         publ, [publ = publ, task = task] ()
        {
            publ->priv->onTaskProgressChanged(task);
        });

        QObject::connect(task, &Task::passwordNeeded,
                         publ, [publ = publ, task = task] (auto promptCore)
        {
            if (publ->priv->abortEncrypted)
                task->stop();
            else
                emit publ->passwordNeeded(promptCore);
        });

        if (auto archivingTask = qobject_cast<ArchivingTask*>(task))
        {
            QObject::connect(archivingTask, &ArchivingTask::archiveCreationNameError,
                             publ, &TaskQueueModel::archiveCreationNameError);

            QObject::connect(archivingTask, &ArchivingTask::openNewArchive,
                             publ, [publ = publ] (const QString& path)
            { publ->openNewArchive(QUrl::fromLocalFile(path)); });

            if (!batch)
                batch = createBatch();

            batch->addTask(archivingTask);
        }
        else if (auto extractionTask = qobject_cast<ExtractionTask*>(task))
        {
            QObject::connect(extractionTask, &ExtractionTask::fileExists,
                             publ, &TaskQueueModel::fileExists);

            QObject::connect(extractionTask, &ExtractionTask::folderExists,
                             publ, &TaskQueueModel::folderExists);

            QObject::connect(extractionTask, &ExtractionTask::itemTypeMismatch,
                             publ, &TaskQueueModel::itemTypeMismatch);

            if (!batch)
                batch = createBatch();

            batch->addTask(extractionTask);
        }

    }

    publ->endInsertRows();

    const auto queueSize = static_cast<int>(queue.size());
    emit publ->dataChanged(publ->index(queueSize - tasks.size(), 0),
                           publ->index(queueSize - 1, columnCount - 1));

    if (queueState == QueueState::Running && !currentRunningTask)
        processQueue(queueSize - tasks.size());
}


void TaskQueueModel::Private::setQueueState(QueueState state)
{
    if (state == queueState)
        return;

    queueState = state;

    if (queueState == QueueState::Stopped)
    {
        stopCurrentRunningTask();
        errorCount = 0;
    }

    emit publ->runningChanged(queueState == QueueState::Running);
}


void TaskQueueModel::Private::onTaskStateChanged(Task* task)
{
    const int taskRow = getTaskRow(task);
    Q_ASSERT(taskRow >= 0);

    emit publ->dataChanged(publ->index(taskRow, 0),
                           publ->index(taskRow, columnCount - 1));

    emit publ->taskStateChanged(publ->index(taskRow, 0));

    if (task->isReadyToRun())
    {
        ++readyTasks;
    }
    else if (task->getState() == TaskState::StartingUp)
    {
        --readyTasks;
    }
    else if (task->isFinished())
    {
        currentRunningTask = nullptr;

        if (task == stopAfterTask)
            setQueueState(QueueState::Stopped);

        if (task->isCompleted())
        {
            ++completedTasks;
            emit publ->completedTaskCountChanged(completedTasks);

            if (autoRemoveCompleted)
                publ->removeRows(taskRow, 1);
        }
        else if (task->getState() == TaskState::Errors)
        {
            ++errorCount;
            if (maxErrorCount > 0 && errorCount >= maxErrorCount)
                setQueueState(QueueState::Stopped);
        }

        if (queueState == QueueState::Running)
        {
            if (!processQueue(0))
            {
                if (publ->areAllTasksCompleted())
                    emit publ->allTasksCompleted();
                emit publ->allTasksFinished();
            }
        }
    }

    startDescriptionUpdateTimer();
}


void TaskQueueModel::Private::onTaskProgressChanged(Task* task)
{
    const int taskRow = getTaskRow(task);
    Q_ASSERT(taskRow >= 0);

    emit publ->dataChanged(publ->index(taskRow, 0),
                           publ->index(taskRow, columnCount - 1));

    startDescriptionUpdateTimer();
}


int TaskQueueModel::Private::getTaskRow(const Task* task) const
{
    int pos= -1;

    for (std::size_t i = 0; i < queue.size(); ++i)
        if (queue[i] == task)
        {
            pos = static_cast<int>(i);
            break;
        }

    return pos;
}


bool TaskQueueModel::Private::processQueue(int fromPosition)
{
    Q_ASSERT(queueState == QueueState::Running);
    Q_ASSERT(!currentRunningTask);

    for (auto i = fromPosition; i < static_cast<int>(queue.size()); ++i)
    {
        auto task = queue[static_cast<std::size_t>(i)];
        Q_ASSERT(!task->isBusy());

        if (task->isReadyToRun())
        {
            currentRunningTask = task;
            QTimer::singleShot(1, task, &Task::start);
            return true;
        }
    }

    return false;
}


void TaskQueueModel::Private::stopCurrentRunningTask()
{
    if (currentRunningTask)
        currentRunningTask->stop();
    currentRunningTask = nullptr;
}


Task* TaskQueueModel::Private::indexToTask(const QModelIndex& index) const
{
    Q_ASSERT(index.isValid());

    const auto row = static_cast<std::size_t>(index.row());
    Q_ASSERT(row < queue.size());

    return queue[row];
}


void TaskQueueModel::Private::updateQueueDescription()
{
    QString text;
    if (queueState == QueueState::Running)
    {
        if (currentRunningTask)
        {
            if (currentRunningTask->getState() == TaskState::InProgress)
            {
                const auto prg = currentRunningTask->getProgress();
                if (prg >= 0)
                    text = QString::number(prg) + QLatin1String(" %");
            }

            if (text.isEmpty())
                text = toString(currentRunningTask->getState());
        }
    }
    else
    {
        text = Packuru::Core::Queue::TaskQueueModel::tr("Stopped");
    }

    if (readyTasks > 0)
    {
        if (!text.isEmpty())
            text += QLatin1String(" / ");
        text += Packuru::Core::Queue::TaskQueueModel::tr("%n left", "", readyTasks);
    }

    queueDescription = text;
    emit publ->queueDescriptionChanged(queueDescription);
}


QString TaskQueueModel::Private::createTaskInfo(const Task* task)
{
    Q_ASSERT(task);

    QString stop;
    if (task == stopAfterTask)
        stop = QLatin1Char('(') + Packuru::Core::Queue::TaskQueueModel::tr("Stop") + QLatin1String(") ");
    QString description;
    switch (task->getType())
    {
    case TaskType::Add:
        description = Packuru::Core::Queue::TaskQueueModel::tr("Add files"); break;
    case TaskType::Create:
        description = Packuru::Core::Queue::TaskQueueModel::tr("Create archive"); break;
    case TaskType::Extract:
        description = Packuru::Core::Queue::TaskQueueModel::tr("Extract archive"); break;
    case TaskType::Delete:
        description = Packuru::Core::Queue::TaskQueueModel::tr("Delete files"); break;
    case TaskType::Test:
        description = Packuru::Core::Queue::TaskQueueModel::tr("Test archive"); break;
    default:
        Q_ASSERT(false); break;
    }
    return stop + description;
}


void TaskQueueModel::Private::startDescriptionUpdateTimer()
{
    if (!descriptionUpdateTimer->isActive())
        descriptionUpdateTimer->start();
}

}
