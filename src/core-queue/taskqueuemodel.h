// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>

#include <QAbstractTableModel>

#include "symbol_export.h"

/// @file

namespace Packuru::Core
{
class PasswordPromptCore;
class CreationNameErrorPromptCore;
class ExtractionFileOverwritePromptCore;
class ExtractionFolderOverwritePromptCore;
class ExtractionTypeMismatchPromptCore;
}


namespace Packuru::Core::Queue
{

namespace TaskQueueModelAux
{
struct Init;
struct Backdoor;
}

/**
 * @brief Provides queued task execution.
 *
 * After creation the %queue is in 'Running' mode which means that all added tasks will be
 * automatically run in sequence. The state can be changed by calling startQueue()/stopQueue() functions.
 *
 * The queue always tries to find the first task from to the top that is in 'Ready to run' state and then
 * executes it.
 *
 * TaskQueueModel object is provided by MainWindowCore object.
 *
 * @headerfile "core-queue/taskqueuemodel.h"
 */
class PACKURU_CORE_QUEUE_EXPORT TaskQueueModel : public QAbstractTableModel
{
    Q_OBJECT

    /** @brief Holds whether the %queue is running.
     * @sa isRunning, runningChanged(), startQueue(), stopQueue()
     */
    Q_PROPERTY(bool running READ isRunning NOTIFY runningChanged)

    /** @brief Holds completed task count.
     *
     * A task is considered completed if it finishes with 'Success' or 'Warnings' status. All IO errors
     * should result in 'Errors' status.
     * @sa completedTaskCount(), completedTaskCountChanged(), areAllTasksCompleted(), removeCompletedTasks()
     */
    Q_PROPERTY(int completedTaskCount READ getCompletedTaskCount NOTIFY completedTaskCountChanged)

    /** @brief Holds short description to be shown as part of main window's title.
     * @sa getQueueDescription(), queueDescriptionChanged()
     */
    Q_PROPERTY(QString description READ getQueueDescription NOTIFY queueDescriptionChanged)

public:
    /// @copydoc ArchivingDialogCore::UserString
    enum class UserString
    {
        ___INVALID, // Keep at index 0 to catch implicit conversions of invalid values to 0 in QML

        ActionErrorLog,
        ActionRemoveCompleted,
        ActionRemoveTask,
        ActionResetTask,
        ActionStartQueue,
        ActionStopHere,
        ActionStopQueue
    };
    Q_ENUM(UserString)

    explicit TaskQueueModel(const TaskQueueModelAux::Init& initData,
                            TaskQueueModelAux::Backdoor& backdoorData,
                            QObject *parent = nullptr);
    ~TaskQueueModel() override;

    ///@{
    /// @name Qt Model-View API
    int columnCount(const QModelIndex &) const override;
    int rowCount(const QModelIndex &) const override;
    QVariant data(const QModelIndex &index, int role) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
    QHash<int, QByteArray> roleNames() const override;
    Q_INVOKABLE bool removeRows(int row, int count, const QModelIndex& parent = QModelIndex()) override;
    ///@}

    /** @brief Resets tasks specified by the indexes.
     *
     * A task has to be reset to 'Ready to run' state before it can be started again.
     * Resetting a running task does nothing.
     * @warning All indexes must be valid and up to date.
     */
    Q_INVOKABLE void resetTasks(const QModelIndexList& indexes);

    /**
     * @brief Marks the task specified by index to stop the queue.
     *
     * Marking the same task again toggles the state.
     * @warning The index must be valid and up to date.
     */
    Q_INVOKABLE void stopAfterTask(const QModelIndex& index);

    /**
     * @brief Returns whether a task specified by the index contains execution log.
     *
     * This can be used to enable/disable 'View log' action.
     */
    Q_INVOKABLE bool hasErrorLog(const QModelIndex& index) const;

    /**
     * @brief Returns execution log for a task specified by the index.
     */
    Q_INVOKABLE QString getErrorLog(const QModelIndex& index) const;

    /**
     * @brief Starts queue.
     *
     * All tasks in the 'Ready to run' state will be run in sequence, starting from the top.
     */
    Q_INVOKABLE void startQueue();

    /**
     * @brief Stops queue.
     *
     * Tasks are not processed when the queue is not running.
     *
     * If there is a task currently running it will be aborted.
     */
    Q_INVOKABLE void stopQueue();

    /**
     * @brief Removes completed tasks from the queue.
     *
     * @sa completedTaskCount, getCompletedTaskCount(), completedTaskCountChanged(), areAllTasksCompleted()
     */
    Q_INVOKABLE void removeCompletedTasks();

    /**
     * @brief Returns unfinished task count.
     *
     * A task is considered unfinished if it has not been started or is currently running.
     */
    Q_INVOKABLE int unfinishedTaskCount() const;

    /**
     * @brief Returns whether all tasks currently held in the queue are completed.
     * @sa completedTaskCount, getCompletedTaskCount(), completedTaskCountChanged(), removeCompletedTasks()
     */
    bool areAllTasksCompleted() const;

    /**
     * @brief Returns completed task count.
     * @sa completedTaskCount, completedTaskCountChanged(), removeCompletedTasks(), areAllTasksCompleted()
     */
    int getCompletedTaskCount() const;

    /**
     * @brief Returns whether the queue is currently running.
     * @sa running, runningChanged(), startQueue(), stopQueue()
     */
    bool isRunning() const;

    /**
     * @brief Returns the index of model's column where tasks' progress is shown.
     *
     * This information can be used in item delegates to draw progress bar.
     */
    int progressColumn() const;

    /**
     * @brief Returns short description to be shown as part of main window's title.
     * @sa description, queueDescriptionChanged()
     */
    Q_INVOKABLE QString getQueueDescription() const;

    /// @copydoc ArchivingDialogCore::getString
    Q_INVOKABLE static QString getString(UserString value);

signals:
    /**
     * @brief Notifies when 'Running' state of queue has changed.
     * @sa running, isRunning(), startQueue(), stopQueue()
     */
    void runningChanged(bool value);

    /**
     * @brief Notifies when completed task count has changed.
     * @sa completedTaskCount, getCompletedTaskCount(), removeCompletedTasks(), areAllTasksCompleted()
     */
    void completedTaskCountChanged(int value);

    /// @brief Notifies when all tasks in the queue has finished.
    void allTasksFinished();

    /** @brief Notifies when all tasks in the queue has been completed.
     * @sa completedTaskCount, getCompletedTaskCount(), removeCompletedTasks(), areAllTasksCompleted()
     */
    void allTasksCompleted();

    /**
     * @brief Notifies that current task requires password to proceed.
     * @param promptCore Contains all data and logic for the prompt.
     * @warning Ownership is not transferred. The object must not be destroyed manually.
     */
    void passwordNeeded(Packuru::Core::PasswordPromptCore* promptCore);

    /**
     * @brief Notifies that the archive cannot be created because of file name conflict.
     * @param promptCore Contains all data and logic for the prompt.
     * @warning Ownership is not transferred. The object must not be destroyed manually.
     */
    void archiveCreationNameError(Packuru::Core::CreationNameErrorPromptCore* promptCore);

    /**
     * @brief Notifies during extraction that a file already exists.
     * @param promptCore Contains all data and logic for the prompt.
     * @warning Ownership is not transferred. The object must not be destroyed manually.
     */
    void fileExists(Packuru::Core::ExtractionFileOverwritePromptCore* promptCore);

    /**
     * @brief Notifies during extraction that a folder already exists.
     * @param promptCore Contains all data and logic for the prompt.
     * @warning Ownership is not transferred. The object must not be destroyed manually.
     */
    void folderExists(Packuru::Core::ExtractionFolderOverwritePromptCore* promptCore);

    /**
     * @brief Notifies during extraction that there is type mismatch between source and destination item.
     * @param promptCore Contains all data and logic for the prompt.
     * @warning Ownership is not transferred. The object must not be destroyed manually.
     */
    void itemTypeMismatch(Packuru::Core::ExtractionTypeMismatchPromptCore* promptCore);

    /** @brief Notifies that the state of a task specified by the index has changed.
     *
     * The signal allows to enable/disable available actions in the window.
     */
    void taskStateChanged(const QModelIndex& index);

    /**
     * @brief Requests to open task's destination folder.
     *
     * Relates to archiving and extraction tasks.
     */
    void openDestinationFolder(const QUrl& url);

    /// @brief Requests to open a newly created archive.
    void openNewArchive(const QUrl& url);

    /**
     * @brief Notifies that queue description has changed.
     * @sa description, getQueueDescription()
     */
    void queueDescriptionChanged(const QString& text);

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}
