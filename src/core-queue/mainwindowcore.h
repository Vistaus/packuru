// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <memory>
#include <vector>

#include <QObject>

#include "symbol_export.h"

/// @file

namespace Packuru::Core::Queue
{

namespace MainWindowCoreAux
{
struct Init;
struct Backdoor;
}

class TaskQueueModel;

/**
 * @brief Contains core data and logic for %Queue main window.
 *
 * %MainWindowCore object is provided by QueueCore object.
 *
 * @headerfile "core-queue/mainwindowcore.h"
 */
class PACKURU_CORE_QUEUE_EXPORT MainWindowCore : public QObject
{
    Q_OBJECT
public:
    /// @copydoc ArchivingDialogCore::UserString
    enum class UserString
    {
        ___INVALID, // Keep at index 0 to catch implicit conversions of invalid values to 0 in QML

        ActionAbout, ///< 'About' action.
        ActionExtract, ///< 'Extract' action.
        ActionNew, ///< 'New' action.
        ActionQuit, ///< 'Quit' action.
        ActionSettings, ///< 'Settings' action.
        ActionTest, ///< 'Test' action.
        WindowTitle ///< Window title.
    };
    Q_ENUM(UserString)

    explicit MainWindowCore(const MainWindowCoreAux::Init& initData,
                            MainWindowCoreAux::Backdoor& backdoorData,
                            QObject *parent = nullptr);
    ~MainWindowCore() override;

    /// @copydoc ArchivingDialogCore::getString
    Q_INVOKABLE static QString getString(UserString value);

    /**
     * @brief Returns a pointer to TaskQueueModel object.
     * @note Object ownership is not transferred to the caller.
     */
    Q_INVOKABLE Packuru::Core::Queue::TaskQueueModel* getTaskQueueModel();

signals:
    /** @brief Signals when all tasks completed and main window can be closed.
     *
     * If there are any other opened windows or dialogs the signal can be ignored.
     *
     * The signal is emitted only if 'Quit if all tasks completed' is set to true (handled by ControllerEngine
     * in SettingsDialogCore).
     */
    void allTasksCompletedReadyToClose();

private:
    struct Private;
    std::unique_ptr<Private> priv;
};

}
