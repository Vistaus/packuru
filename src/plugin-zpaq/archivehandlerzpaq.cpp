// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "../core/private/plugin-api/archivetype.h"

#include "../plugin-zpaq-core/backendhandler.h"
#include "../plugin-zpaq-core/archivingparametershandler.h"
#include "../plugin-zpaq-core/registerstreamoperators.h"

#include "archivehandlerzpaq.h"


using namespace Packuru::Core;


namespace Packuru::Plugins::ZPAQ
{

ArchiveHandlerZPAQ::ArchiveHandlerZPAQ()
{
    Core::registerStreamOperators();
}


std::unordered_set<ArchiveType> ArchiveHandlerZPAQ::supportedTypesForReading() const
{
    return {ArchiveType::zpaq};
}


std::unordered_set<ArchiveType> ArchiveHandlerZPAQ::supportedTypesForWriting() const
{
    return {ArchiveType::zpaq};
}


bool ArchiveHandlerZPAQ::canAddFiles(ArchiveType archiveType, const BackendArchiveProperties& properties) const
{
    Q_UNUSED(properties)
    return archiveType == ArchiveType::zpaq;
}


bool ArchiveHandlerZPAQ::canDeleteFiles(ArchiveType archiveType, const BackendArchiveProperties& properties) const
{
    Q_UNUSED(archiveType)
    Q_UNUSED(properties)
    return false;
}


std::unique_ptr<AbstractBackendHandler> ArchiveHandlerZPAQ::createBackendHandler() const
{
    return std::make_unique<Core::BackendHandler>();
}


std::unique_ptr<ArchivingParameterHandlerBase>
ArchiveHandlerZPAQ::createArchivingParameterHandler(const ArchivingParameterHandlerInitData& initData) const
{
    return std::make_unique<Core::ArchivingParametersHandler>(initData);
}

}
