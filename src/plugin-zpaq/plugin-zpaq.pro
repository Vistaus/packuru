# SPDX-FileCopyrightText: The Packuru Developers
#
# SPDX-License-Identifier: 0BSD

include(../common-src.pri)
include(../common-plugin.pri)

MODULE_BUILD_NAME_SUFFIX = plugin-zpaq
MODULE_BUILD_NAME = $${PROJECT_BUILD_NAME}-$${MODULE_BUILD_NAME_SUFFIX}

TARGET = $$qtLibraryTarget($$MODULE_BUILD_NAME)

LIBS += -L$${PROJECT_TARGET_LIB_DIR}
LIBS += -l$${QCS_CORE_BUILD_NAME}
LIBS += -l$${CORE_BUILD_NAME}
LIBS += -l$${MODULE_BUILD_NAME}-core

DISTFILES += \
    pluginmetadata.json

HEADERS += \
    archivehandlerzpaq.h \

SOURCES += \
    archivehandlerzpaq.cpp \
