// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <unordered_map>

#include <QVariant>


namespace Packuru::Plugins::ZPAQ::Core
{

enum class PrivateArchivingParameter;

using PrivateArchivingParameterMap = std::unordered_map<PrivateArchivingParameter, QVariant>;

}

Q_DECLARE_METATYPE(Packuru::Plugins::ZPAQ::Core::PrivateArchivingParameterMap);

