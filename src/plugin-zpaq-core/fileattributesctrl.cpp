// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#include "fileattributesctrl.h"


using namespace qcs::core;


namespace Packuru::Plugins::ZPAQ::Core
{

FileAttributesCtrl::FileAttributesCtrl()
{
    setControllerType(ControllerType::Type::CheckBox);
    setCurrentValue(true);
}

}
