// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include <QMetaType>


namespace Packuru::Plugins::ZPAQ::Core
{

enum class PrivateArchivingParameter
{
    CompressionLevel, // int
    FileAttributes, // bool
    ThreadCount, // int
};

}

Q_DECLARE_METATYPE(Packuru::Plugins::ZPAQ::Core::PrivateArchivingParameter);
