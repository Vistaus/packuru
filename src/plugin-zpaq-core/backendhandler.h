// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "../core/private/plugin-api/clibackendhandler.h"

#include "invocationdata.h"


namespace Packuru::Plugins::ZPAQ::Core
{

class BackendHandler : public Packuru::Core::CLIBackendHandler
{
public:
    BackendHandler(QObject* parent = nullptr);

    static bool executableAvailable();

    void enterPassword(const QString& password) override;

    QString createSinglePartArchiveName(const QString& archiveBaseName, Packuru::Core::ArchiveType archiveType) const override;

private:
    void addToArchiveImpl(const Packuru::Core::BackendArchivingData& data) override;
    void extractArchiveImpl(const Packuru::Core::BackendExtractionData& data) override;
    void readArchiveImpl(const Packuru::Core::BackendReadData& data) override;
    void testArchiveImpl(const Packuru::Core::BackendTestData& data) override;

    void startBackend(const InvocationData& data);
    QStringList composeCommand(const InvocationData& data, bool includePassword) const;
    Packuru::Core::BackendHandlerState handleFinishedProcess();
    void onExtractionProcessFinished(QString& password);

    void errorLineHandler(const std::vector<QStringRef>& lines);
    void listSnaphotsHandler(const std::vector<QStringRef>& lines);
    void listItemsHandler(const std::vector<QStringRef>& lines);
    void progressLineHandler(QString& line, int newDataPosition);

    // Processes tasks in steps
    void startQueue();
    void onStepStateChanged(Packuru::Core::BackendHandlerState state);

    void removeFile(const QString& filePath); // Throws
    void swapArchives(const QString& temporaryFilePath, const QString& originalFilePath); // Throws
    QString createTempArchiveName(const QString& path, const QString& originalName); // Throws

    Packuru::Core::ArchiveItem tempItem;
    InvocationData lastInvocationData;
    bool passwordRequired = false;
    QString snapshotInfo;
    const int snapshotDigits;
    std::vector<std::function<void()>> stepQueue;
    std::size_t currentStep = 0;
    bool logErrors = true;
};

}
