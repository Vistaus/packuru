// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

#pragma once

#include "../qcs-core/controller.h"


namespace Packuru::Plugins::ZPAQ::Core
{

class FileAttributesCtrl : public qcs::core::Controller<bool>
{
public:
    FileAttributesCtrl();
};

}
