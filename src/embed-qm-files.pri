# SPDX-FileCopyrightText: The Packuru Developers
#
# SPDX-License-Identifier: 0BSD

CONFIG(release, debug|release) {
    CONFIG += embed_qm_files
}

embed_qm_files {
    isEmpty(MODULE_BUILD_NAME_SUFFIX) {
        error($$_FILE_: Variable MODULE_BUILD_NAME_SUFFIX has not been set. Included from $$_PRO_FILE_)
    }

    RES_QM_FILES_ROOT_DIR = $$absolute_path(resources/qm, $$dirname(_PRO_FILE_))
    QM_FILES_DEST_DIR = $${RES_QM_FILES_ROOT_DIR}/$${MODULE_BUILD_NAME_SUFFIX}
    
    # Resources file points to this folder so it must be available before assigning to RESOURCES variable
    mkpath($${QM_FILES_DEST_DIR})

    RESOURCES += $${RES_QM_FILES_ROOT_DIR}/$${MODULE_BUILD_NAME_SUFFIX}_qm.qrc

    QMAKE_EXTRA_COMPILERS += lrelease
    lrelease.input         = TRANSLATIONS
    lrelease.output        = ${QMAKE_FILE_BASE}.qm
    lrelease.commands      = $(MKDIR) $${QM_FILES_DEST_DIR} \
                             && $$[QT_INSTALL_BINS]/lrelease ${QMAKE_FILE_IN} -qm $${QM_FILES_DEST_DIR}/${QMAKE_FILE_BASE}.qm
    lrelease.CONFIG       += no_link target_predeps
}
