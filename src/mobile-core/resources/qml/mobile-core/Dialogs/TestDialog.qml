// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

import org.kde.kirigami 2.5 as Kirigami

import ControllerSystem.QQC 1.0 as CS_QQC

import PackuruCore 1.0


ArchiveTaskDialog {
    id: root
    property string absArchiveFilePath

    contentItem: TabWidget {
        id: tabWidget
        focus: true
    }

    CS_QQC.WidgetMapper {
        id: mapper
    }

    Component {
        id: generalOptionsComponent

        OptionsPage {
            title: dialogCore.getString(TestDialogCore.TabOptions)

            Label {
                id: passwordLabel
                Layout.alignment: Qt.AlignRight
            }
            PasswordField {
                id: passwordEdit
                Layout.fillWidth: true
                echoMode: TextInput.Password
                focus: visible
            }
            Component.onCompleted: {
                mapper.addLabelAndWidget(TestDialogControllerType.Password,
                                         passwordLabel,
                                         passwordEdit)
            }
        }
    }

    onAccepted: dialogCore.accept()

    Component.onCompleted: {
        if (!dialogCore)
            return

        var multiArchive = dialogCore.getMode() === TestDialogCore.DialogMode.MultiArchive

        if (multiArchive) {
            var comp = Qt.createComponent("ArchivesListWidget.qml")
            var model = dialogCore ? dialogCore.getArchivesModel() : null
            var archiveListPage = comp.createObject(tabWidget.swipeView,
                                                    {"title" : dialogCore.getString(TestDialogCore.TabArchives),
                                                        "model" : model})
            tabWidget.swipeView.addPage(archiveListPage)
        }

        var generalOptionsPage = generalOptionsComponent.createObject(tabWidget.swipeView)
        tabWidget.swipeView.addPage(generalOptionsPage)

        if (multiArchive && dialogCore.getArchivesModel().rowCount() === 0)
            tabWidget.swipeView.setCurrentIndex(archiveListPage.SwipeView.index)
        else
            tabWidget.swipeView.setCurrentIndex(generalOptionsPage.SwipeView.index)

        mapper.addWidgets(TestDialogControllerType.AcceptButton, root.acceptButton)
        mapper.addWidgets(TestDialogControllerType.DialogErrors, root.errorsItem)
        mapper.addWidgets(TestDialogControllerType.RunInQueue, root.runInQueueCheck)

        mapper.setEngine(dialogCore.getControllerEngine())

        root.cancelButton.text = dialogCore.getString(TestDialogCore.Cancel)

    }
}
