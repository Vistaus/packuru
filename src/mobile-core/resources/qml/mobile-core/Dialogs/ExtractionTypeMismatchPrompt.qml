// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3

import org.kde.kirigami 2.6 as Kirigami

import PackuruCore 1.0


FocusScope{
    id: root
    implicitHeight: col.implicitHeight
    implicitWidth: col.implicitWidth
    property QtObject promptCore
    property alias headerVisible: headerLabel.visible
    property int maxContentWidth: 0

    Connections {
        target: promptCore
        function onDeleted() {
            root.destroy()
        }
    }

    QtObject {
        id: priv
        readonly property string file: promptCore ? promptCore.getString(ExtractionTypeMismatchPromptCore.File) : ""
        readonly property string folder: promptCore ? promptCore.getString(ExtractionTypeMismatchPromptCore.Folder) : ""
        readonly property string link: promptCore ? promptCore.getString(ExtractionTypeMismatchPromptCore.Link) : ""
        readonly property string location: promptCore ? promptCore.getString(ExtractionTypeMismatchPromptCore.Location) : ""
    }

    ColumnLayout{
        id: col
        spacing: 10
        anchors.horizontalCenter: parent.horizontalCenter

        Label {
            id: headerLabel
            text: promptCore ? promptCore.getString(ExtractionTypeMismatchPromptCore.ItemsTypeMismatch) + "." : ""
            font.bold: true
            Layout.fillWidth: true
            horizontalAlignment: Text.AlignHCenter
        }

        Item {
            height: Kirigami.Units.smallSpacing
        }

        Label {
            text: promptCore ? promptCore.getString(ExtractionTypeMismatchPromptCore.Source) : ""
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
        }

        ColumnLayout {
            spacing: 0
            Label {
                text: promptCore ? (promptCore.sourceFileType() === ExtractionTypeMismatchPromptCore.FileType.Dir ?
                                        priv.folder : promptCore.sourceFileType() === ExtractionTypeMismatchPromptCore.FileType.File ?
                                            priv.file : promptCore.sourceFileType() === ExtractionTypeMismatchPromptCore.FileType.Link ?
                                                priv.link : "") : ""
                opacity: 0.5
            }
            Kirigami.UrlButton {
                text: promptCore ? promptCore.itemFileName() : null
                url: promptCore ? promptCore.sourceFilePath() : null
                wrapMode: Text.WrapAnywhere
                Layout.maximumWidth: root.maxContentWidth > 0 ? root.maxContentWidth : root.width
            }
        }

        ColumnLayout {
            spacing: 0
            Label {
                text: priv.location
                opacity: 0.5
            }
            Kirigami.UrlButton {
                url: promptCore ? promptCore.sourcePath() : null
                wrapMode: Text.WrapAnywhere
                Layout.maximumWidth: root.maxContentWidth > 0 ? root.maxContentWidth : root.width
            }
        }

        Item {
            height: Kirigami.Units.smallSpacing
        }

        Label {
            text: promptCore ? promptCore.getString(ExtractionTypeMismatchPromptCore.Destination) : ""
            font.bold: true
            horizontalAlignment: Text.AlignHCenter
        }

        ColumnLayout {
            spacing: 0
            Label {
                text: promptCore ? (promptCore.destinationFileType() === ExtractionTypeMismatchPromptCore.FileType.Dir ?
                                        priv.folder : promptCore.destinationFileType() === ExtractionTypeMismatchPromptCore.FileType.File ?
                                            priv.file : promptCore.destinationFileType() === ExtractionTypeMismatchPromptCore.FileType.Link ?
                                                priv.link : "") : ""
                opacity: 0.5
            }
            Kirigami.UrlButton {
                text: promptCore ? promptCore.itemFileName() : null
                url: promptCore ? promptCore.destinationFilePath() : null
                wrapMode: Text.WrapAnywhere
                Layout.maximumWidth: root.maxContentWidth > 0 ? root.maxContentWidth : root.width
            }
        }

        ColumnLayout {
            spacing: 0
            Label {
                text: priv.location
                opacity: 0.5
            }
            Kirigami.UrlButton {
                url: promptCore ? promptCore.destinationPath() : null
                wrapMode: Text.WrapAnywhere
                Layout.maximumWidth: root.maxContentWidth > 0 ? root.maxContentWidth : root.width
            }
        }

        RowLayout {
            id: l
            Layout.alignment: Qt.AlignHCenter

            Button {
                text: promptCore ? promptCore.getString(ExtractionTypeMismatchPromptCore.Skip) : ""
                icon.name: "go-next-skip"
                onClicked: promptCore.skip(applyToAllCheck.checked)
            }
            CheckBox {
                id: applyToAllCheck
                text: promptCore ? promptCore.getString(ExtractionTypeMismatchPromptCore.ApplyToAll) : ""
            }
        }

        RowLayout {
            Layout.alignment: Qt.AlignHCenter

            Button {
                text: promptCore ? promptCore.getString(ExtractionTypeMismatchPromptCore.Retry) : ""
                icon.name: "view-refresh"
                onClicked: promptCore.retry()
            }
            Button {
                text: promptCore ? promptCore.getString(ExtractionTypeMismatchPromptCore.Abort) : ""
                icon.name: "dialog-cancel"
                onClicked: promptCore.abort()
            }
        }
    }
}
