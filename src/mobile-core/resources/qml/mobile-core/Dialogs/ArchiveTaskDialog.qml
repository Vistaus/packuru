// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

import org.kde.kirigami 2.5 as Kirigami

import PackuruCore 1.0


AutoResizableDialog {
    id: root

    property var dialogCore
    readonly property alias runInQueueCheck: runInQueueCheck_
    readonly property alias acceptButton: acceptButton_
    readonly property alias cancelButton: cancelButton_
    readonly property alias errorsItem: errorWidget

    footer: RowLayout {
        CheckBox {
            id: runInQueueCheck_
        }

        Item {
            Layout.fillWidth: true
        }

        Control {
            id: errorWidget
            Layout.preferredHeight: buttonBox.implicitHeight - buttonBox.topPadding - buttonBox.bottomPadding
            Layout.preferredWidth: Layout.preferredHeight
            property bool qcsToolTipVisible: area.pressed // property used by Controller System
            Kirigami.Icon {
                id: icon
                source: "dialog-error"
                anchors.fill: parent

                MouseArea {
                    id: area
                    anchors.fill: parent
                }
            }
        }

        DialogButtonBox {
            id: buttonBox

            Button {
                id: acceptButton_
                text: "OK"
                icon.name: "dialog-ok"
                DialogButtonBox.buttonRole: DialogButtonBox.AcceptRole
            }
            Button {
                id: cancelButton_
                icon.name: "dialog-close"
                DialogButtonBox.buttonRole: DialogButtonBox.RejectRole
            }
            onAccepted: root.accept()
            onRejected: root.reject()
        }
    }

    onClosed: {
        dialogCore.destroyLater()
    }
}
