// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

import PackuruCore 1.0


AutoResizableDialog {
    id: root

    headerText: dialogCore.getString(AppClosingDialogCore.DialogTitle)

    property alias infoItem: infoContentItem.content
    property alias dialogCore: dialogCore

    contentItem: ColumnLayout {
        Item {
            id: infoContentItem
            property Item content
            onContentChanged: content ? content.parent = this : null
            implicitHeight: content.implicitHeight
            implicitWidth: content.implicitWidth
        }

        Label {
            text: dialogCore.getString(AppClosingDialogCore.QuitQuestion)
        }
    }

    footer: DialogButtonBox {
        id: buttonBox
        implicitWidth: contentWidth

        Button {
            text: dialogCore.getString(AppClosingDialogCore.Quit)
            DialogButtonBox.buttonRole: DialogButtonBox.AcceptRole
        }
        Button {
            text: dialogCore.getString(AppClosingDialogCore.Cancel)
            DialogButtonBox.buttonRole: DialogButtonBox.RejectRole
        }

        onAccepted: Qt.quit()
    }

   AppClosingDialogCore {
       id: dialogCore
   }

}
