// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3


AutoResizableDialog {
    id: root
    property alias text: label.text

    contentItem: Flickable {
        clip: true
        contentWidth: label.contentWidth
        contentHeight:  label.contentHeight
        implicitHeight: contentHeight
        implicitWidth:  contentWidth
        boundsBehavior: Flickable.StopAtBounds

        Label {
            id: label
            font.family: "monospace"
        }
    }
}
