// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3
import QtQml.Models 2.11
import QtQuick.Dialogs 1.3

import org.kde.kirigami 2.0 as Kirigami

import ControllerSystem.QQC 1.0 as CS_QQC

import PackuruCore 1.0


ArchiveTaskDialog {
    id: root

    contentItem: TabWidget {
        id: tabWidget
        focus: true
    }

    onAccepted: dialogCore.accept()

    CS_QQC.WidgetMapper {
        id: mapper
    }

    Component {
        id: generalOptionsComponent

        OptionsPage {
            title: dialogCore.getString(ExtractionDialogCore.TabOptions)
            Label {
                id: destinationModeLabel
                Layout.alignment: Qt.AlignRight
            }
            DialogComboBox {
                id: destinationModeCombo
            }

            Label {
                id: destinationPathLabel
                Layout.alignment: Qt.AlignRight
            }
            RowLayout {
                Layout.fillWidth: true

                TextField {
                    id: destinationPathEdit
                    Layout.fillWidth: true
                }
                Button {
                    id: destinationPathButton
                    icon.name: "document-open-folder"
                    Layout.preferredWidth: height
                    onClicked: fileDialogLoader.active = true
                    Loader {
                        id: fileDialogLoader
                        active: false
                        sourceComponent: FileDialog {
                            selectMultiple: false
                            selectFolder: true
                            onAccepted: {
                                destinationPathEdit.text = fileUrl
                                fileDialogLoader.active = false
                            }
                            onRejected: fileDialogLoader.active = false
                            Component.onCompleted: open()
                        }
                    }
                }
            }

            Label {
                id: topFolderModeLabel
                Layout.alignment: Qt.AlignRight
            }
            DialogComboBox {
                id: topFolderModeCombo
            }

            Label {
                id: fileExtractionModeLabel
                Layout.alignment: Qt.AlignRight
            }
            DialogComboBox {
                id: fileExtractionModeCombo
            }

            Label {
                id: fileOverwriteModeLabel
                Layout.alignment: Qt.AlignRight
            }
            DialogComboBox {
                id: fileOverwriteModeCombo
            }

            Label {
                id: folderOverwriteModeLabel
                Layout.alignment: Qt.AlignRight
            }
            DialogComboBox {
                id: folderOverwriteModeCombo
            }

            Label {
                id: passwordLabel
                Layout.alignment: Qt.AlignRight
            }
            PasswordField {
                id: passwordEdit
                Layout.fillWidth: true
                echoMode: TextInput.Password
                focus: visible
            }

            Item {
                Layout.preferredHeight: 1
            }

            CheckBox {
                id: openDestinationCheck
            }

            Component.onCompleted: {
                mapper.addLabelAndWidget(ExtractionDialogControllerType.DestinationMode,
                                         destinationModeLabel,
                                         destinationModeCombo)
                mapper.addLabelAndWidget(ExtractionDialogControllerType.DestinationPath,
                                         destinationPathLabel,
                                         destinationPathEdit)
                mapper.addLabelAndWidget(ExtractionDialogControllerType.TopFolderMode,
                                         topFolderModeLabel,
                                         topFolderModeCombo)
                mapper.addLabelAndWidget(ExtractionDialogControllerType.FileExtractionMode,
                                         fileExtractionModeLabel,
                                         fileExtractionModeCombo)
                mapper.addLabelAndWidget(ExtractionDialogControllerType.FileOverwriteMode,
                                         fileOverwriteModeLabel,
                                         fileOverwriteModeCombo)
                mapper.addLabelAndWidget(ExtractionDialogControllerType.FolderOverwriteMode,
                                         folderOverwriteModeLabel,
                                         folderOverwriteModeCombo)
                mapper.addLabelAndWidget(ExtractionDialogControllerType.Password,
                                         passwordLabel,
                                         passwordEdit)
                mapper.addWidget(ExtractionDialogControllerType.OpenDestinationOnCompletion, openDestinationCheck)

                mapper.addBuddy(ExtractionDialogControllerType.DestinationPath, destinationPathButton)
            }
        }
    }

    Component.onCompleted: {
        if (!dialogCore)
            return

        var multiArchive = dialogCore.getMode() === ExtractionDialogCore.DialogMode.MultiArchive

        if (multiArchive) {
            var comp = Qt.createComponent("ArchivesListWidget.qml")
            var model = dialogCore ? dialogCore.getArchivesModel() : null
            var archiveListPage = comp.createObject(tabWidget.swipeView,
                                                    {"title" : dialogCore.getString(ExtractionDialogCore.TabArchives),
                                                        "model" : model})
            tabWidget.swipeView.addPage(archiveListPage)
        }

        var generalOptionsPage = generalOptionsComponent.createObject(tabWidget.swipeView)
        tabWidget.swipeView.addPage(generalOptionsPage)

        if (multiArchive && dialogCore.getArchivesModel().rowCount() === 0)
            tabWidget.swipeView.setCurrentIndex(archiveListPage.SwipeView.index)
        else
            tabWidget.swipeView.setCurrentIndex(generalOptionsPage.SwipeView.index)

        mapper.addWidgets(ExtractionDialogControllerType.AcceptButton, root.acceptButton)
        mapper.addWidgets(ExtractionDialogControllerType.DialogErrors, root.errorsItem)
        mapper.addWidgets(ExtractionDialogControllerType.RunInQueue, root.runInQueueCheck)

        mapper.setEngine(dialogCore.getControllerEngine())

        root.cancelButton.text = dialogCore.getString(ExtractionDialogCore.Cancel)

    }
}
