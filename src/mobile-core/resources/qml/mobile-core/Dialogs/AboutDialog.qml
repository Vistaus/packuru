// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.0
import QtQuick.Controls 2.4
import QtQuick.Layouts 1.3

import PackuruCore 1.0


AutoResizableDialog {
    id: root
    headerText: core.getString(AboutDialogCore.DialogTitle)

    contentItem: Flickable {
        contentWidth: edit.paintedWidth
        contentHeight: edit.paintedHeight
        implicitWidth: contentWidth
        implicitHeight: contentHeight
        boundsBehavior: Flickable.StopAtBounds
        TextEdit {
            id: edit
            readOnly: true
            text: core.getString(AboutDialogCore.AppInfo)
            textFormat: TextEdit.MarkdownText
            onLinkActivated: Qt.openUrlExternally(link)
        }
    }

    AboutDialogCore {
        id: core
    }
}
