// SPDX-FileCopyrightText: The Packuru Developers
//
// SPDX-License-Identifier: BSD-2-Clause

import QtQuick 2.0
import QtQuick.Controls 2.4

import org.kde.kirigami 2.0 as Kirigami

import PackuruCore 1.0


Dialog {
    id: root
    modal: true
    property alias headerText: heading.text
    property bool destroyOnClose: false

    focus: true

    readonly property int maxContentWidth: parent.width - leftPadding - rightPadding
    readonly property int maxContentHeight: height - topPadding - bottomPadding

    signal finished()

    onAccepted: finished()
    onRejected: finished()

    anchors.centerIn: parent

    header: Kirigami.Heading {
        id: heading

        background: Rectangle {
            color: Kirigami.Theme.highlightColor
        }

        color: Kirigami.Theme.highlightedTextColor
        level: 1
        horizontalAlignment: Text.AlignHCenter
    }

    footer: DialogButtonBox {
        visible: !Kirigami.Settings.isMobile

        Button {
            text: commonStrings.getString(CommonStrings.Close)
            icon.name: "dialog-close"
            DialogButtonBox.buttonRole: DialogButtonBox.RejectRole
        }
        topPadding: 0
    }

    onClosed: {
        if (destroyOnClose)
            destroy()
    }

}
