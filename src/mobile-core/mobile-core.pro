# SPDX-FileCopyrightText: The Packuru Developers
#
# SPDX-License-Identifier: 0BSD

include(../common-src.pri)
include(../common-lib.pri)
include(../common-mobile.pri)

MODULE_BUILD_NAME_SUFFIX = $$MOBILE_CORE_BUILD_NAME_SUFFIX
MODULE_BUILD_NAME = $$MOBILE_CORE_BUILD_NAME

include(../embed-qm-files.pri)

TARGET = $$qtLibraryTarget($$MODULE_BUILD_NAME)

LIBS += -L$${PROJECT_TARGET_LIB_DIR}
LIBS += -l$$QCS_QQCONTROLS_BUILD_NAME
LIBS += -l$${CORE_BUILD_NAME}

DEFINES += $${PROJECT_MACRO_NAME}_MOBILE_CORE_LIBRARY

RESOURCES += resources/qml/$${MODULE_BUILD_NAME_SUFFIX}_qml.qrc

TRANSLATIONS += \
    translations/$${MODULE_BUILD_NAME_SUFFIX}_en.ts \

HEADERS += \
    private/aux.h \
    registertypes.h \
    symbol_export.h

SOURCES += \
    private/aux.cpp \
    registertypes.cpp
