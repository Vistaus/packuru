# SPDX-FileCopyrightText: The Packuru Developers
#
# SPDX-License-Identifier: 0BSD

# === MAIN VARIABLES =============================
PROJECT_BUILD_NAME = packuru
DEFINES += PROJECT_BUILD_NAME=\\\"$${PROJECT_BUILD_NAME}\\\"

CONFIG(debug, debug|release) {
    isEmpty(TARGET_NAME) {
        TARGET_NAME = debug
    }
} else {
    isEmpty(TARGET_NAME) {
        TARGET_NAME = release
    }
} 

PROJECT_ROOT_DIR = $${PWD}

isEmpty(PROJECT_TARGET_DIR) {
    PROJECT_TARGET_DIR = $${PROJECT_ROOT_DIR}/$${PROJECT_BUILD_NAME}-target-$${TARGET_NAME}
}

PROJECT_LIB_DIR_NAME = libs
PROJECT_TARGET_LIB_DIR = $${PROJECT_TARGET_DIR}/$${PROJECT_LIB_DIR_NAME}

PROJECT_PLUGIN_DIR_NAME = plugins
DEFINES += PROJECT_PLUGIN_DIR_NAME=\\\"$${PROJECT_PLUGIN_DIR_NAME}\\\"
PROJECT_TARGET_PLUGIN_DIR = $${PROJECT_TARGET_DIR}/$${PROJECT_PLUGIN_DIR_NAME}

# Where to copy public api headers when building
PROJECT_API_TARGET_DIR = $${PROJECT_ROOT_DIR}/include/$${PROJECT_BUILD_NAME}

# Used as part of macro names
PROJECT_MACRO_NAME = PACKURU

# === CORE LIBS ======================
CORE_BUILD_NAME_SUFFIX = core
CORE_BUILD_NAME = $${PROJECT_BUILD_NAME}-$${CORE_BUILD_NAME_SUFFIX} 

CORE_BROWSER_BUILD_NAME_SUFFIX = core-browser
CORE_BROWSER_BUILD_NAME = $${PROJECT_BUILD_NAME}-$${CORE_BROWSER_BUILD_NAME_SUFFIX} 

CORE_QUEUE_BUILD_NAME_SUFFIX = core-queue
CORE_QUEUE_BUILD_NAME = $${PROJECT_BUILD_NAME}-$${CORE_QUEUE_BUILD_NAME_SUFFIX} 

# === QCS ============================
QCS_BASE_BUILD_NAME=qcs

QCS_MACRO_NAME = QCS

QCS_CORE_BUILD_NAME_SUFFIX = core
QCS_CORE_BUILD_NAME = $${QCS_BASE_BUILD_NAME}-$${QCS_CORE_BUILD_NAME_SUFFIX}

QCS_QQCONTROLS_BUILD_NAME_SUFFIX = qqcontrols
QCS_QQCONTROLS_BUILD_NAME = $${QCS_BASE_BUILD_NAME}-$${QCS_QQCONTROLS_BUILD_NAME_SUFFIX}

QCS_QTWIDGETS_BUILD_NAME_SUFFIX = qtwidgets
QCS_QTWIDGETS_BUILD_NAME = $${QCS_BASE_BUILD_NAME}-$${QCS_QTWIDGETS_BUILD_NAME_SUFFIX}

# === MISC ===========================

# Allows using full project paths in internal #include directives rather than relative paths, which seems less error prone when relocating files
INCLUDEPATH += $${PROJECT_ROOT_DIR}/src

QMAKE_CXXFLAGS += -std=c++17

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x050F00
